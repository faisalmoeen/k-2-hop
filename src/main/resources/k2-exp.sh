#!/bin/bash

WORKDIR=/scratch/nas/1/forakzai
JAR=${HOME}/git/k2/target/k2-1.0-SNAPSHOT.jar
DATA_DIR=${HOME}/git/k2/src/main/resources


# DATA=${HOME}/disk/k2/datasets/tdrive.txt
# TABLE=tdrive-d
# for e in 6 
# do
# 	for m in 3
# 	do
# 		for k in 200 400 600 800 1000 1200
# 		do
# 			echo "$DATA:$ALGO e=$e, m=$m, k=$k"
# 			 java -Xmx6g -jar $JAR k2hbaseindl $m $k $e $TABLE ${HOME}/disk/k2/output/out-all.txt
# 			 java -Xmx6g -jar $JAR k2hbasebatch $m $k $e $TABLE ${HOME}/disk/k2/output/out-all.txt
# 			 java -Xmx6g -jar $JAR k2indl $m $k $e $DATA ${HOME}/disk/k2/output/out-all.txt
# 			 java -Xmx6g -jar $JAR k2batch $m $k $e $DATA ${HOME}/disk/k2/output/out-all.txt
# 			 java -Xmx6g -jar $JAR vcoda $m $k $e $DATA ${HOME}/disk/k2/output/out-all.txt
# 			 java -Xmx6g -jar $JAR vcodanlogn $m $k $e $DATA ${HOME}/disk/k2/output/out-all.txt
# 		done
# 	done
# done

#DATA=${HOME}/disk/k2/datasets/trucks.txt
#TABLE=trucks-d
#for e in 6
#do
#	for m in 3
#	do
#		for k in 200 400 600 800 1000 1200
#		do
#			echo "$DATA:$ALGO e=$e, m=$m, k=$k"
#			 java -Xmx6g -jar $JAR k2hbaseindl $m $k $e $TABLE ${HOME}/disk/k2/output/out-all.txt
#			 java -Xmx6g -jar $JAR k2hbasebatch $m $k $e $TABLE ${HOME}/disk/k2/output/out-all.txt
#			 java -Xmx6g -jar $JAR k2indl $m $k $e $DATA ${HOME}/disk/k2/output/out-all.txt
#			 java -Xmx6g -jar $JAR k2batch $m $k $e $DATA ${HOME}/disk/k2/output/out-all.txt
#			 java -Xmx6g -jar $JAR vcoda $m $k $e $DATA ${HOME}/disk/k2/output/out-all.txt
#			 java -Xmx6g -jar $JAR vcodanlogn $m $k $e $DATA ${HOME}/disk/k2/output/out-all.txt
#		done
#	done
#done

# DATA=${HOME}/disk/k2/datasets/5.txt
# TABLE=brink6.0-d
# for e in 0.000006 0.00006 0.0006
# do
# 	for m in 9 6 3
# 	do
# 		for k in 1200 1000 800 600 400 200
# 		do
# 			echo "$DATA:$ALGO e=$e, m=$m, k=$k"
# 			 java -Xmx6g -jar $JAR k2hbaseindl $m $k $e $TABLE ${HOME}/disk/k2/output/out-brink6.0-all.txt
# 			 java -Xmx6g -jar $JAR k2hbasebatch $m $k $e $TABLE ${HOME}/disk/k2/output/out-brink6.0-all.txt
# 			  # java -Xmx6g -jar $JAR k2indl $m $k $e $DATA ${HOME}/disk/k2/output/out-brink6.0-all.txt
# 			  # java -Xmx6g -jar $JAR k2batch $m $k $e $DATA ${HOME}/disk/k2/output/out-brink6.0-all.txt
# 			  # java -Xmx6g -jar $JAR vcoda $m $k $e $DATA ${HOME}/disk/k2/output/out-brink6.0-all.txt
# 			  # java -Xmx6g -jar $JAR vcodanlogn $m $k $e $DATA ${HOME}/disk/k2/output/out-brink6.0-all.txt
# 		done
# 	done
# done

# DATA=${HOME}/disk/k2/datasets/5.txt
#DATA=${HOME}/disk/k2/datasets/trucks.txt
# TABLE=brink6.0-d
# for e in 0.000006
# do
# 	for m in 3 3 3
# 	do
# 		for k in 1200
# 		do
## 			echo "$DATA:$ALGO e=$e, m=$m, k=$k"
## 			 java -Xmx6g -jar $JAR k2hbasebatch $m $k $e $TABLE ${HOME}/disk/k2/output/tmp.txt
## 			 java -Xmx6g -jar $JAR k2hbaseindl $m $k $e $TABLE ${HOME}/disk/k2/output/tmp.txt
#
# 			  # java -Xmx6g -jar $JAR k2indl $m $k $e $DATA ${HOME}/disk/k2/output/out-brink6.0-all.txt
# 			  # java -Xmx6g -jar $JAR k2batch $m $k $e $DATA ${HOME}/disk/k2/output/out-brink6.0-all.txt
## 			   java -Xmx6g -jar $JAR vcoda $m $k $e $DATA ${HOME}/disk/k2/output/tmp.txt
# 			  # java -Xmx6g -jar $JAR vcodanlogn $m $k $e $DATA ${HOME}/disk/k2/output/out-brink6.0-all.txt
# 		done
# 	done
# done


# DATA=${HOME}/disk/datasets/brink-d
# TABLE=brink6.0-d
# for e in 0.000006 0.00006 0.0006
# do
# 	for m in 9 6 3
# 	do
# 		for k in 1200 1000 800 600 400 200
# 		do
# 			echo "$DATA:$ALGO e=$e, m=$m, k=$k"
# 			java -Xmx6g -jar $JAR k2rdbmsbatch $m $k $e trucks ${HOME}/disk/k2/output/out-trucks-all.txt
# 			java -Xmx6g -jar $JAR k2rdbmsbatch $m $k $e tdrive ${HOME}/disk/k2/output/out-tdrive-all.txt
# 			java -Xmx6g -jar $JAR k2rdbmsbatch $m $k $e brink ${HOME}/disk/k2/output/out-brink6-all.txt
#
# 			java -Xmx6g -jar $JAR k2rdbmsindl $m $k $e trucks ${HOME}/disk/k2/output/out-trucks-all.txt
# 			java -Xmx6g -jar $JAR k2rdbmsindl $m $k $e tdrive ${HOME}/disk/k2/output/out-tdrive-all.txt
# 			java -Xmx6g -jar $JAR k2rdbmsindl $m $k $e brink ${HOME}/disk/k2/output/out-brink6-all.txt
#
#            java -Xmx6g -jar $JAR k2hbaseindl $m $k $e trucks-d ${HOME}/disk/k2/output/out-trucks-all.txt
#            java -Xmx6g -jar $JAR k2hbaseindl $m $k $e tdrive-d ${HOME}/disk/k2/output/out-tdrive-all.txt
# 			java -Xmx6g -jar $JAR k2hbaseindl $m $k $e brink6.0-d ${HOME}/disk/k2/output/out-brink6-all.txt
#
# 			java -Xmx6g -jar $JAR k2hbasebatch $m $k $e trucks-d ${HOME}/disk/k2/output/out-trucks-all.txt
#            java -Xmx6g -jar $JAR k2hbasebatch $m $k $e tdrive-d ${HOME}/disk/k2/output/out-tdrive-all.txt
# 			java -Xmx6g -jar $JAR k2hbasebatch $m $k $e brink6.0-d ${HOME}/disk/k2/output/out-brink6-all.txt
#
# 			java -Xmx6g -jar $JAR k2indl $m $k $e ${HOME}/disk/datasets/trucks-d ${HOME}/disk/k2/output/out-trucks-all.txt
#            java -Xmx6g -jar $JAR k2indl $m $k $e ${HOME}/disk/datasets/tdrive-d ${HOME}/disk/k2/output/out-tdrive-all.txt
# 			java -Xmx6g -jar $JAR k2indl $m $k $e ${HOME}/disk/datasets/brink-d ${HOME}/disk/k2/output/out-brink6-all.txt
#
#            java -Xmx6g -jar $JAR k2batch $m $k $e ${HOME}/disk/datasets/trucks-d ${HOME}/disk/k2/output/out-trucks-all.txt
#            java -Xmx6g -jar $JAR k2batch $m $k $e ${HOME}/disk/datasets/tdrive-d ${HOME}/disk/k2/output/out-tdrive-all.txt
# 			java -Xmx6g -jar $JAR k2batch $m $k $e ${HOME}/disk/datasets/brink-d ${HOME}/disk/k2/output/out-brink6-all.txt
#
#
## 			  # java -Xmx6g -jar $JAR vcoda $m $k $e $DATA ${HOME}/disk/k2/output/out-brink6.0-all.txt
## 			  # java -Xmx6g -jar $JAR vcodanlogn $m $k $e $DATA ${HOME}/disk/k2/output/out-brink6.0-all.txt
# 		done
# 	done
# done

# DATA=${HOME}/disk/datasets/brink-d
# TABLE=brink6.0-d
# for e in 0.00003 0.00006 0.00012 0.00024
# do
# 	for m in 3
# 	do
# 		for k in 100
# 		do
# 			echo "$DATA:$ALGO e=$e, m=$m, k=$k"
## 			java -Xmx6g -jar $JAR k2rdbmsbatch $m $k $e trucks ${HOME}/disk/k2/output/out-trucks-all.txt
## 			java -Xmx6g -jar $JAR k2rdbmsbatch $m $k $e tdrive ${HOME}/disk/k2/output/out-tdrive-all.txt
# 			java -Xmx6g -jar $JAR k2rdbmsbatch $m $k $e brink ${HOME}/disk/k2/output/out-brink6-all.txt
#
## 			java -Xmx6g -jar $JAR k2rdbmsindl $m $k $e trucks ${HOME}/disk/k2/output/out-trucks-all.txt
## 			java -Xmx6g -jar $JAR k2rdbmsindl $m $k $e tdrive ${HOME}/disk/k2/output/out-tdrive-all.txt
# 			java -Xmx6g -jar $JAR k2rdbmsindl $m $k $e brink ${HOME}/disk/k2/output/out-brink6-all.txt
#
##            java -Xmx6g -jar $JAR k2hbaseindl $m $k $e trucks-d ${HOME}/disk/k2/output/out-trucks-all.txt
##            java -Xmx6g -jar $JAR k2hbaseindl $m $k $e tdrive-d ${HOME}/disk/k2/output/out-tdrive-all.txt
# 			java -Xmx6g -jar $JAR k2hbaseindl $m $k $e brink6.0-d ${HOME}/disk/k2/output/out-brink6-all.txt
#
## 			java -Xmx6g -jar $JAR k2hbasebatch $m $k $e trucks-d ${HOME}/disk/k2/output/out-trucks-all.txt
##            java -Xmx6g -jar $JAR k2hbasebatch $m $k $e tdrive-d ${HOME}/disk/k2/output/out-tdrive-all.txt
# 			java -Xmx6g -jar $JAR k2hbasebatch $m $k $e brink6.0-d ${HOME}/disk/k2/output/out-brink6-all.txt
#
## 			java -Xmx6g -jar $JAR k2indl $m $k $e ${HOME}/disk/datasets/trucks-d ${HOME}/disk/k2/output/out-trucks-all.txt
##            java -Xmx6g -jar $JAR k2indl $m $k $e ${HOME}/disk/datasets/tdrive-d ${HOME}/disk/k2/output/out-tdrive-all.txt
# 			java -Xmx6g -jar $JAR k2indl $m $k $e ${HOME}/disk/datasets/brink-d ${HOME}/disk/k2/output/out-brink6-all.txt
#
##            java -Xmx6g -jar $JAR k2batch $m $k $e ${HOME}/disk/datasets/trucks-d ${HOME}/disk/k2/output/out-trucks-all.txt
##            java -Xmx6g -jar $JAR k2batch $m $k $e ${HOME}/disk/datasets/tdrive-d ${HOME}/disk/k2/output/out-tdrive-all.txt
# 			java -Xmx6g -jar $JAR k2batch $m $k $e ${HOME}/disk/datasets/brink-d ${HOME}/disk/k2/output/out-brink6-all.txt
#
#
## 			  # java -Xmx6g -jar $JAR vcoda $m $k $e $DATA ${HOME}/disk/k2/output/out-brink6.0-all.txt
## 			  # java -Xmx6g -jar $JAR vcodanlogn $m $k $e $DATA ${HOME}/disk/k2/output/out-brink6.0-all.txt
# 		done
# 	done
# done


DATA=${DATA_DIR}/trucks-d.txt
TABLE=trucks-d
for e in 0.06
do
	for m in 3
	do
		for k in 200 400 600 800 1000 1200
		do
			echo "$DATA:$ALGO e=$e, m=$m, k=$k"
			 java -Xmx6g -jar $JAR k2-streaming $m $k $e $DATA ${DATA_DIR}/out-streaming.txt
#			 java -Xmx6g -jar $JAR k2hbasebatch $m $k $e $TABLE ${HOME}/disk/k2/output/out-all.txt
#			 java -Xmx6g -jar $JAR k2indl $m $k $e $DATA ${HOME}/disk/k2/output/out-all.txt
#			 java -Xmx6g -jar $JAR k2batch $m $k $e $DATA ${HOME}/disk/k2/output/out-all.txt
#			 java -Xmx6g -jar $JAR vcoda $m $k $e $DATA ${HOME}/disk/k2/output/out-all.txt
#			 java -Xmx6g -jar $JAR vcodanlogn $m $k $e $DATA ${HOME}/disk/k2/output/out-all.txt
		done
	done
done