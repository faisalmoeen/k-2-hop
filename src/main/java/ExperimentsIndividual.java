import com.google.common.base.Stopwatch;
import dk.aau.convoy.K2Seq;
import dk.aau.convoy.model.Convoy;
import dk.aau.convoy.model.k2ExecutionTimeStruct;
import dk.aau.convoy.utils.K2SeqDataManager;
import org.apache.flink.api.java.tuple.Tuple2;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by faisal on 8/29/16.
 */
public class ExperimentsIndividual {

//	private static String inputFilePath = "/media/faisal/Cold Storage/Cloud Storage/owncloud/PhD Work/working-folder/experiments/trucks_dataset/trucks273s.txt";
	private static String inputFilePath = "/media/faisal/Cold Storage/data/Birkinhoff/output/1.txt";
	private static int[] kArray = {200,40,60,80,100,120,140,160,180};
	private static int[] mArray = {20,3,4,5,6,7,8,9,10};
	private static double[] eArray = {0.000006f, 0.00006f, 0.0006f, 0.006f, 0.06f, 0.6f};
	private static double eps = 0.00006;
	private static int batch;
	private static SortedMap<Integer,SortedMap<Integer,k2ExecutionTimeStruct>> k2ExpResults= new TreeMap<>();
	private static SortedMap<Integer,SortedMap<Integer,k2ExecutionTimeStruct>> vcodaExpResults= new TreeMap<>();
	private static char sep = ',';

	public static void main(String[] args){
		int m = Integer.parseInt(args[1]);
		int k = Integer.parseInt(args[2]);
		double e = Double.parseDouble(args[3]);
		String inputFilePath = args[4];
		String outputFilePath = args[5];
		PrintWriter pw = null;
		FileWriter fw = null;
		try {
			fw = new FileWriter(outputFilePath, true);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		BufferedWriter bw = new BufferedWriter(fw);
		pw = new PrintWriter(bw);
		Stopwatch timer = Stopwatch.createUnstarted();

		switch (args[0]) {
			case "k2":
				System.out.println("Running k2 m=" + m + ",k=" + k + ",e=" + e);
				timer.reset();
				timer.start();

				Tuple2<List<Convoy>,k2ExecutionTimeStruct> k2strictConvoys = K2Seq.k2Seq(inputFilePath, e, m, k);

				timer.stop();
				k2ExecutionTimeStruct k2TimeStruct = k2strictConvoys.f1;
				k2TimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
				pw.println(inputFilePath + sep + m + sep + k + sep + e + sep + "k2" + sep + k2TimeStruct.getText(sep));
				System.out.println(inputFilePath + sep + m + sep + k + sep + e + sep + "k2" + sep + k2TimeStruct.getText(sep));
				pw.flush();

				break;
			case "vcoda":
				System.out.println("Running Vcoda m=" + m + ",k=" + k + ",e=" + e);
				timer.reset();
				timer.start();

				Tuple2<List<Convoy>,k2ExecutionTimeStruct> vCodaStrictConvoys = K2Seq.vcoda(inputFilePath, e, m, k);

				timer.stop();
				k2ExecutionTimeStruct vcodaTimeStruct = vCodaStrictConvoys.f1;
				vcodaTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
				pw.println(inputFilePath + sep + m + sep + k + sep + e + sep + "vcoda" + sep + vcodaTimeStruct.getText(sep));
				System.out.println(inputFilePath + sep + m + sep + k + sep + e + sep + "vcoda" + sep + vcodaTimeStruct.getText(sep));
				pw.flush();

				break;
			case "load-test":
				System.out.println("Running Load test");
				K2SeqDataManager dataManager =  new K2SeqDataManager(inputFilePath, k);
				k2ExecutionTimeStruct timeStruct = new k2ExecutionTimeStruct();
				timer.reset();
				timer.start();

				while(dataManager.getNextK2Batch()!=null){

				}

				timer.stop();
				timeStruct.timeDataLoading = timer.elapsed(TimeUnit.MILLISECONDS);
				pw.println(inputFilePath + sep + 0 + sep + k + sep + 0 + sep + "load-test" + sep + timeStruct.getText(sep));
				pw.flush();

			default:
		}


		pw.close();
	}


}
