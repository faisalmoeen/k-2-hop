package test;

import com.google.common.base.Stopwatch;
import dk.aau.convoy.K2Seq;
import dk.aau.convoy.model.Convoy;
import dk.aau.convoy.model.k2ExecutionTimeStruct;
import org.apache.flink.api.java.tuple.Tuple2;
import org.junit.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by faisal on 2/23/17.
 */
public class K2Test {
	private static String inputFilePath = "/home/faisal/disk/k2/datasets/tdrive.txt";
	private static int k = 200;
	private static int m = 3;
	private static double eps = 0.0006;
	@Test
	public void testK2(){
		System.out.println("hello");
		Stopwatch timer = Stopwatch.createStarted();
		System.out.println("**********************K2-Strict Convoys*******************");
		Tuple2<List<Convoy>,k2ExecutionTimeStruct> k2strictConvoys = K2Seq.k2Seq(inputFilePath, eps, m, k);
		timer.stop();
		Long k2ExecTime = timer.elapsed(TimeUnit.MILLISECONDS);
		System.out.println("***K2 strict convoys***");
		System.out.println("k2 Strict Convoys = " + k2strictConvoys.f0.size());
		System.out.println("Execution Time = " + k2ExecTime);
		K2Seq.sortTe(k2strictConvoys.f0);
		K2Seq.print(k2strictConvoys.f0);
	}

	@Test
	public void testK2Batch() throws IOException, InterruptedException {
		System.out.println("hello");
		Stopwatch timer = Stopwatch.createStarted();
		System.out.println("**********************K2Batch-Strict Convoys*******************");
		Tuple2<List<Convoy>,k2ExecutionTimeStruct> k2strictConvoys = K2Seq.k2SeqBatch(inputFilePath, eps, m, k);
		timer.stop();
		Long k2ExecTime = timer.elapsed(TimeUnit.MILLISECONDS);
		System.out.println("***K2 strict convoys***");
		System.out.println("k2 Strict Convoys = " + k2strictConvoys.f0.size());
		System.out.println("Execution Time = " + k2ExecTime);
		K2Seq.sortTe(k2strictConvoys.f0);
		K2Seq.print(k2strictConvoys.f0);
	}

	@Test
	public void testK2HBaseIndl() throws IOException, InterruptedException {
		System.out.println("hello");
		Stopwatch timer = Stopwatch.createStarted();
		System.out.println("**********************K2hbase-Strict Convoys*******************");
		timer.reset();
		timer.start();
		Tuple2<List<Convoy>,k2ExecutionTimeStruct> k2strictConvoys = K2Seq.k2SeqHBaseIndl("tdrive-d", eps, m, k);
		Long k2ExecTime = k2ExecTime = timer.elapsed(TimeUnit.MILLISECONDS);
		System.out.println("k2hbase Strict Convoys = " + k2strictConvoys.f0.size());
		System.out.println("Execution Time = " + k2ExecTime);
		Collections.sort(k2strictConvoys.f0, new Comparator<Convoy>() {
			@Override
			public int compare(Convoy o1, Convoy o2) {
				if (o1.getTs() > o2.getTs() || (o1.getTs() == o2.getTs() && o1.getTe() > o2.getTe())) {
					return 1;
				} else if (o1.getTs() == o2.getTs() && o1.getTe() == o2.getTe()) {
					return 0;
				}
				return -1;
			}
		});

		for (Convoy v :
				k2strictConvoys.f0) {
			System.out.println(v);
		}
	}

	@Test
	public void testK2HBaseBatch() throws IOException, InterruptedException {
		System.out.println("hello");
		Stopwatch timer = Stopwatch.createStarted();
		System.out.println("**********************K2hbase-batch-Strict Convoys*******************");
		timer.reset();
		timer.start();
		Tuple2<List<Convoy>,k2ExecutionTimeStruct> k2strictConvoys = K2Seq.k2SeqHBaseBatch("tdrive-d", eps, m, k);
		Long k2ExecTime = k2ExecTime = timer.elapsed(TimeUnit.MILLISECONDS);
		System.out.println("***hbase strict convoys***");
		System.out.println("Strict Convoys = " + k2strictConvoys.f0.size());
		System.out.println("Execution Time = " + k2ExecTime);
		K2Seq.sortTe(k2strictConvoys.f0);
		K2Seq.print(k2strictConvoys.f0);
	}
}
