import com.google.common.base.Stopwatch;
import dk.aau.convoy.K2Seq;
import dk.aau.convoy.model.Convoy;
import dk.aau.convoy.model.k2ExecutionTimeStruct;
import dk.aau.convoy.utils.K2SeqDataManager;
import org.apache.avro.file.FileReader;
import org.apache.flink.api.java.tuple.Tuple2;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by faisal on 8/29/16.
 */
public class Experiments{

//	private static String inputFilePath = "/media/faisal/Cold Storage/Cloud Storage/owncloud/PhD Work/working-folder/experiments/trucks_dataset/trucks273s.txt";
	private static String inputFilePath = "/media/faisal/Cold Storage/data/Birkinhoff/output/1.txt";
	private static int[] kArray = {200,40,60,80,100,120,140,160,180};
	private static int[] mArray = {20,3,4,5,6,7,8,9,10};
	private static double[] eArray = {0.000006f, 0.00006f, 0.0006f, 0.006f, 0.06f, 0.6f};
	private static double eps = 0.00006;
	private static int batch;
	private static SortedMap<Integer,SortedMap<Integer,k2ExecutionTimeStruct>> k2ExpResults= new TreeMap<>();
	private static SortedMap<Integer,SortedMap<Integer,k2ExecutionTimeStruct>> vcodaExpResults= new TreeMap<>();
	private static char sep = ',';

	public static void main(String[] args){
		File inputDir = new File(args[0]);
		File outputFile = new File(args[1]+"/output.txt");
		try (BufferedReader br = new BufferedReader(new java.io.FileReader(args[2]))) {
			String mline = br.readLine();
			String kline = br.readLine();
			String eline = br.readLine();
			mArray = Arrays.stream(mline.split(","))
					.map(String::trim).mapToInt(Integer::parseInt).toArray();
			kArray = Arrays.stream(kline.split(","))
					.map(String::trim).mapToInt(Integer::parseInt).toArray();
			eArray = Arrays.stream(eline.split(","))
					.map(String::trim).mapToDouble(Double::parseDouble).toArray();
		}catch (IOException e){
			e.printStackTrace();
		}
		PrintWriter pw=null;
		try {
			outputFile.createNewFile();
			pw = new PrintWriter(outputFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String[] inputFiles = inputDir.list();
		Stopwatch timer = Stopwatch.createUnstarted();

		for (String inputFile :
				inputFiles) {
			String inputFilePath = inputDir+"/"+inputFile;
			for (double e :
					eArray) {
//				k2ExpResults.put(m, new TreeMap<Integer, k2ExecutionTimeStruct>());
//				vcodaExpResults.put(m, new TreeMap<Integer, k2ExecutionTimeStruct>());
				for (int m:
						mArray) {
					for (int k :
							kArray) {

						timer.reset();
						timer.start();
						Tuple2<List<Convoy>,k2ExecutionTimeStruct> k2strictConvoys = K2Seq.k2Seq(inputFilePath, e, m, k);
						timer.stop();
						k2ExecutionTimeStruct k2TimeStruct = k2strictConvoys.f1;
						k2TimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
						pw.println(inputFilePath+sep+m+sep+k+sep+e+sep+"k2"+sep+k2TimeStruct.getText(sep)+",convoys="+k2strictConvoys.f0.size());
						System.out.println("m="+m+":k="+k+sep+"e="+e+":k2"+k2TimeStruct.getText(',')+",convoys="+k2strictConvoys.f0.size());



						timer.reset();
						timer.start();
						Tuple2<List<Convoy>,k2ExecutionTimeStruct> vCodaStrictConvoys = K2Seq.vcoda(inputFilePath, e, m, k);
						timer.stop();
						k2ExecutionTimeStruct vcodaTimeStruct = vCodaStrictConvoys.f1;
						vcodaTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
						pw.println(inputFilePath+sep+m+sep+k+sep+"vcoda"+sep+vcodaTimeStruct.getText(sep));
						System.out.println("m="+m+":k="+k+sep+"e="+e+":vcoda"+sep+vcodaTimeStruct.getText(',')+",convoys="+vCodaStrictConvoys.f0.size());

					}

				}
			}
		}
		pw.flush();
		for (String inputFile :
				inputFiles) {
			String inputFilePath = inputDir+"/"+inputFile;
			for (int k:
					kArray) {
				K2SeqDataManager dataManager =  new K2SeqDataManager(inputFilePath, k);
				k2ExecutionTimeStruct timeStruct = new k2ExecutionTimeStruct();
				timer.reset();
				timer.start();
				while(dataManager.getNextK2Batch()!=null){

				}
				timer.stop();
				timeStruct.timeDataLoading = timer.elapsed(TimeUnit.MILLISECONDS);
				pw.println(inputFilePath+sep+0+sep+k+sep+timeStruct.getText(sep));
			}
		}
		pw.flush();
		pw.close();
	}


}
