package dk.aau.convoy;

/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.serializers.MapSerializer;
import dk.aau.convoy.functions.FlatMapEdgeGenerator;
import dk.aau.convoy.functions.GroupByPartition;
import dk.aau.convoy.functions.GroupByTime;
import dk.aau.convoy.functions.K2Compute;
import dk.aau.convoy.model.K2VertexValue;
import dk.aau.convoy.model.ClusterablePoint;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeutils.base.LongComparator;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.aggregation.Aggregations;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.graph.Edge;
import org.apache.flink.graph.Graph;
import org.apache.flink.graph.Vertex;
import org.apache.flink.graph.pregel.VertexCentricConfiguration;
import org.apache.flink.types.NullValue;

import java.util.Comparator;
import java.util.HashMap;

public class K2BatchJob {

	public static void main(String[] args) throws Exception {
		// set up the batch execution environment
		final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
		env.setParallelism(1);
		ParameterTool parameters = ParameterTool.fromPropertiesFile(args[0]);
		env.getConfig().setGlobalJobParameters(parameters);

		Kryo kryo = new Kryo();
		MapSerializer serializer = new MapSerializer();
		kryo.register(HashMap.class, serializer);
		serializer.setKeyClass(Long.class, kryo.getSerializer(Long.class));

//TODO: Read the data in the form <Long,Long,Double[]> so that DBSCANClusterer can cluster it without creating new objects
		DataSet<Tuple4<Long, Long, Double, Double>> data = env.readCsvFile("/media/faisal/Cold Storage/data/trucks/trucks273s.txt")
				.fieldDelimiter(',')
				.types(Long.class, Long.class, Double.class, Double.class);

//		outputs (partitionID, time, HashMap<oid,Point>)
		DataSet<Tuple3<Long, Long, HashMap<Long, ClusterablePoint>>> dataGpByTime =  data.groupBy(1) //groups by time
				.reduceGroup(new GroupByTime(parameters.getInt("k",0)));//group reduces all the data related to one time-instant and puts in one structure
//		dataGpByTime.print();

		System.out.println("******************************************************************************************");

//		group by partitionID such that each partition belongs to one vertex
		DataSet<Vertex<Long,K2VertexValue>> vertexes = dataGpByTime.groupBy(0) //group by partition id
				.reduceGroup(new GroupByPartition())
				.returns(new TypeHint<Vertex<Long,K2VertexValue>>() {});
//		vertexes.print();

		DataSet<Vertex<Long,K2VertexValue>> maxPartitionId = vertexes.aggregate(Aggregations.MAX,0);
		DataSet<Vertex<Long,K2VertexValue>> minPartitionId = vertexes.aggregate(Aggregations.MIN,0);
		DataSet<Tuple2<Vertex<Long,K2VertexValue>, Vertex<Long,K2VertexValue>>> minMaxPartitionTuple = minPartitionId.cross(maxPartitionId);

//		minMaxPartitionTuple contains two fields, minPartitionId and maxPartitionId. Using flatmap, I generate edges of
//		a Path graph
		DataSet<Edge<Long, NullValue>> edges = minMaxPartitionTuple.flatMap(new FlatMapEdgeGenerator())
				.returns(new TypeHint<Edge<Long, NullValue>>() {});
//		edges.print();

		System.out.println("********************************Graph Creation*******************************************");
//		NullValue for edgeValue
		Graph<Long,K2VertexValue, NullValue> graph = Graph.fromDataSet(vertexes, edges, env);

		System.out.println("********************************Graph Iterations*****************************************");
		VertexCentricConfiguration vcConf = new VertexCentricConfiguration();
		vcConf.setName("k2-iteration");
		vcConf.addBroadcastSet("minMaxPartitionTuple", minMaxPartitionTuple);
		vcConf.setSolutionSetUnmanagedMemory(true);
//		run 10 pregel iterations
		Graph<Long, K2VertexValue, NullValue> result =
				graph.runVertexCentricIteration(new K2Compute(), null, 10, vcConf);
		result.getVertexIds().collect().sort(new Comparator<Long>() {
			@Override
			public int compare(Long o1, Long o2) {
				if (o1>o2)
					return 1;
				if(o1<o2)
					return -1;
				else
					return 0;
			}
		});
		result.getVertices().print();

		env.setParallelism(1);
		// execute program
		env.execute("Flink k2");
	}
}
