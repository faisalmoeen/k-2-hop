package dk.aau.convoy;

/**
 * Created by faisal on 9/6/16.
 */
public class K2Utils {

	public static int gap(int superstep){
		return (int)Math.pow(2,superstep-2);
	}

	public static boolean isSourceVertex(long vertexId, long startVertexId, int gap){
		return (vertexId-startVertexId)%(2*gap) == 0;
	}
}
