package dk.aau.convoy.utils;

import hbase.ReadHBase;

import java.io.IOException;

/**
 * Created by faisal on 9/17/17.
 */
public class ExportHBase {

	public static void main(String[] args){
		try {
			ReadHBase.init("trucks-d");
			ReadHBase.writeTuplesForTimeRange(0L,999999L,"/home/faisal/disk/datasets/trucks-d");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
