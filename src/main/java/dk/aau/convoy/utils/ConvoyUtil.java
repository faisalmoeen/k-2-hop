package dk.aau.convoy.utils;

import com.google.common.collect.Sets;
import dk.aau.convoy.clustering.ClusteringUtils;
import dk.aau.convoy.model.Convoy;
import dk.aau.convoy.model.ConvoyBuilder;
import dk.aau.convoy.model.ClusterablePoint;
import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.commons.math3.ml.clustering.DBSCANClusterer;
import org.apache.flink.api.java.tuple.Tuple2;

import java.util.*;

/**
 * Created by faisal on 8/30/16.
 */
public class ConvoyUtil {

	private int m;
	private int k;
	private float eps;
	private DBSCANClusterer dbscanClusterer;

	public ConvoyUtil() {
	}

	public ConvoyUtil(int m, int k, float eps) {
		this.m = m;
		this.k = k;
		dbscanClusterer = new DBSCANClusterer(eps,m);
	}

	public static List<Convoy> objsListToConvoyList(List<Set<Long>> objsList, Long t){
		List<Convoy> V = new ArrayList<Convoy>();
		for(Set<Long> objs : objsList){
			Convoy v = new ConvoyBuilder().setTs(t).setTe(t).setObjs(objs).createConvoy();
			V.add(v);
		}
		return V;
	}

	public static List<Convoy> objsListToConvoyList(List<Set<Long>> objsList, Long ts, Long te){
		List<Convoy> V = new ArrayList<Convoy>();
		for(Set<Long> objs : objsList){
			Convoy v = new ConvoyBuilder().setTs(ts).setTe(te).setObjs(objs).createConvoy();
			V.add(v);
		}
		return V;
	}

	public static List<Set<Long>> convoyToClusterList(List<Convoy> V){
		List<Set<Long>> C = new ArrayList<Set<Long>>();
		for(Convoy v : V){
			Set<Long> objs = v.getObjs();
			C.add(objs);
		}
		return C;
	}

	public Convoy matchClusters(HashSet<Long> cluster1, Long t1, HashSet<Long> cluster2, Long t2){
		Sets.SetView<Long> intersection = Sets.intersection(cluster1,cluster2);
		if(intersection.size()>m){
			return new ConvoyBuilder().setTs(t1).setTe(t2).setObjs(intersection.copyInto(new HashSet<Long>())).createConvoy();
		}else
			return null;
	}

	public static Convoy matchClusters(Set<Long> cluster1, Long t1, Set<Long> cluster2, Long t2, int m){
		Sets.SetView<Long> intersection = Sets.intersection(cluster1,cluster2);
		if(intersection.size()>=m){
			return new ConvoyBuilder().setTs(t1).setTe(t2).setObjs(intersection.copyInto(new HashSet<Long>())).createConvoy();
		}else
			return null;
	}

	public static Convoy matchConvoyCluster(Convoy v1, HashSet<Long> cluster2, Long m){
		Sets.SetView<Long> intersection = Sets.intersection(v1.getObjs(),cluster2);
		if(intersection.size()>m){
			Set<Long> objs = v1.getObjs();
			intersection.copyInto(objs);
			v1.setObjs(objs);
			return v1;
		}else
			return null;
	}

	public static List<Set<Long>> matchClusterLists(List<Set<Long>> clusterList1, List<Set<Long>> clusterList2, int m){
		ArrayList<Set<Long>> matchedClusterList = new ArrayList<>();
		for (Set<Long> cc1: clusterList1) {
			for (Set<Long> cc2: clusterList2) {
				Sets.SetView<Long> intersection = Sets.intersection(cc1,cc2);
				if(intersection.size()>m){
					matchedClusterList.add(intersection.copyInto(new HashSet<Long>()));
				}
			}
		}
		return matchedClusterList;
	}

	public static List<Convoy> matchSnapshotConvoyLists(List<Convoy> clusterList1, List<Convoy> clusterList2, int m){
		ArrayList<Convoy> matchedConvoyList = new ArrayList<>();
		Convoy v=null;
		for (Convoy cc1: clusterList1) {
			for (Convoy cc2: clusterList2) {
				v = cc1.simpleIntersection(cc2,m);
				if(v!=null){
					matchedConvoyList.add(v);
				}
			}
		}
		return matchedConvoyList;
	}

	public List<Convoy> reClusterConvoyList(Long time, List<Convoy> inputConvoys, HashMap<Long, HashMap<Long, ClusterablePoint>> partitionData){
		HashMap timeData = partitionData.get(time);
		ArrayList<ClusterablePoint> inputData = new ArrayList<>();
		List<Cluster<ClusterablePoint>> clustersList = new ArrayList<>();
		for (Convoy convoy:
				inputConvoys) {
			for (Object obj:
					convoy.getObjs()) {
				inputData.add((ClusterablePoint) timeData.get((long)obj));
			}
			clustersList.addAll(dbscanClusterer.cluster(inputData)); //benchmark data is a Hashmap<oid,TupleClusterable>
		}
		return ClusteringUtils.convertToListOfConvoys(clustersList,time);
	}
	// returns all ending convoys added to the input
	// there will be no closed convoy as leftVsp is of order 1
	public List<Convoy> findStartingConvoys(List<Convoy> Vsp, List<Convoy> rightVsp, long ts, long te,
											Map<Long, Map<Long, ClusterablePoint>> partitionData){
		List<ClusterablePoint> inputConvoyObjs = new ArrayList<>();
		List<Cluster<ClusterablePoint>> clustersList = new ArrayList<>();
		List<Convoy> Vnext = null;
		List<Convoy> result = new ArrayList<>();
		if(rightVsp==null || rightVsp.size()==0){
			return null;
		}
		List<Convoy> V = rightVsp;
		for (long t=te ; t>ts ; t--){ //don't include benchmark point ts which has already been mined
			Map<Long, ClusterablePoint> timeData = partitionData.get(t);
			Vnext = new ArrayList<Convoy>();
			for (Convoy convoy:
					V) {
				for (Object obj:
						convoy.getObjs()) {
					ClusterablePoint point = (ClusterablePoint) timeData.get((long)obj);
					if(point!=null) {
						inputConvoyObjs.add(point);
					}
				}
				System.out.println("ts start:"+t);
				clustersList = dbscanClusterer.cluster(inputConvoyObjs);
				System.out.println("ts end reached:"+t);
				switch (clustersList.size()){
					case 0: //the convoy did not get extended
						convoy.setLeftOpen(false);
						if (!shouldBePrunned(convoy)) {//if its closed and lifetime<k
							result.add(convoy);
						}
						break;
					case 1: //either the original convoy has reduced in size or has remained the same
						if (clustersList.get(0).getPoints().size()<convoy.getObjs().size()){//if the original convoy has reduced in size after reclustering in this timestamp
							convoy.setLeftOpen(false);
							if (!shouldBePrunned(convoy)) {
								result.add(convoy);
							}
							Vnext.add(new ConvoyBuilder() //take the reduced convoy to the next iteration for convoy. we don't need to check its size as the clustering method returns only those clusters with size>=m
									.setTs(t)
									.setTe(convoy.getTe())
									.setCluster(clustersList.get(0).getPoints())
									.setLeftOpen(true)
									.setRightOpen(convoy.isRightOpen())
									.createConvoy());
						}else { //if the original convoy maintained it's size
							convoy.setTs(t);
							Vnext.add(convoy);
						}
						break;
					default: // the convoy got split into 2 or more convoys
						convoy.setLeftOpen(false);
						if (!shouldBePrunned(convoy)) {
							result.add(convoy);
						}
						for (Cluster cluster:
							 clustersList) {
							Vnext.add(new ConvoyBuilder()
									.setTs(t)
									.setTe(convoy.getTe())
									.setCluster(cluster.getPoints())
									.setLeftOpen(true)
									.setRightOpen(convoy.isRightOpen())
									.createConvoy());
						}
				}
			}
			//all input convoys from the next timestamp have been matched in this timestamp.
			//Vnext contains convoys that we want to take to the previous timestamp.
			//Vnext should not contain any convoy that is a subset of Vsp of this partition because such a convoy will
			// be a 2nd order spanning convoy spanning the right and the current hw which already have been found in
			//previous methods
			List<Convoy> subsetConvoys = new ArrayList<>();

			if (Vnext.size()>0 && Vsp!=null && Vsp.size()>0) {
				for (Convoy vsp :
						Vsp) {
					for (Convoy vnext :
							Vnext) {
						if(vsp.getObjs().containsAll(vnext.getObjs())){ //vnext is a subset of vsp
							subsetConvoys.add(vnext);
						}
					}
				}
				Vnext.removeAll(subsetConvoys); //remove all subset convoys
			}
			if (Vnext.size()==0){
				for (Convoy v:
					 V) {
					v.setLeftOpen(false);
					if (!shouldBePrunned(v)) {
						result.add(v);
					}
				}
			}
			return result;
		}
		return null;
	}

	private boolean shouldBePrunned(Convoy v){
		if (v==null || (v.isClosed() && (v.lifetime()<k || v.size()<m))){
			return true;
		}
		return false;
	}

	// returns all ending convoys added to the input
	// there will be no closed convoy as leftVsp is of order 1
	public List<Convoy> findEndingConvoys(List<Convoy> leftVsp, List<Convoy> Vsp, long ts, long te,
										  Map<Long, Map<Long, ClusterablePoint>> partitionData){
		List<ClusterablePoint> inputConvoyObjs = new ArrayList<>();
		List<Cluster<ClusterablePoint>> clustersList = new ArrayList<>();
		List<Convoy> Vnext = null;
		List<Convoy> result = new ArrayList<>();
		if(leftVsp==null || leftVsp.size()==0){
			return null;
		}
		List<Convoy> V = leftVsp;
		for (long t=ts+1 ; t<=te ; t++){ //benchmark point=ts which has already been mined
			Map<Long, ClusterablePoint> timeData = partitionData.get(t);
			Vnext = new ArrayList<Convoy>();
			for (Convoy convoy:
					V) {
				for (Object obj:	//make a list of clusterable objets from oids of convoy objs
						convoy.getObjs()) {
					inputConvoyObjs.add((ClusterablePoint) timeData.get((long)obj));
				}
				clustersList = dbscanClusterer.cluster(inputConvoyObjs);
				switch (clustersList.size()){
					case 0: //the convoy did not get extended
						convoy.setRightOpen(false);
						if (!shouldBePrunned(convoy)){ //if its closed and lifetime<k
							result.add(convoy);
						}
						break;
					case 1: //either the original convoy has reduced in size or has remained the same
						if (clustersList.get(0).getPoints().size()<convoy.getObjs().size()){ //if the original convoy has reduced in size after reclustering in this timestamp
							convoy.setRightOpen(false);
							if (!shouldBePrunned(convoy)) {
								result.add(convoy);	//add the original convoy to the result
							}
							Vnext.add(new ConvoyBuilder()	//take the reduced convoy to the next iteration for convoy. we don't need to check its size as the clustering method returns only those clusters with size>=m
									.setTs(convoy.getTs())
									.setTe(t)
									.setCluster(clustersList.get(0).getPoints())
									.setLeftOpen(convoy.isLeftOpen())
									.setRightOpen(true)
									.createConvoy());
						}else { //if the original convoy maintained it's size
							convoy.setTe(t);
							Vnext.add(convoy);
						}
						break;
					default: // the convoy got split into 2 or more convoys
						convoy.setRightOpen(false);
						if (!shouldBePrunned(convoy)) {
							result.add(convoy);
						}
						for (Cluster cluster:
								clustersList) {
							Vnext.add(new ConvoyBuilder()
									.setTs(convoy.getTs())
									.setTe(t)
									.setCluster(cluster.getPoints())
									.setLeftOpen(convoy.isLeftOpen())
									.setRightOpen(true)
									.createConvoy());
						}
				}
			}
			//all input convoys from the previous timestamp have been matched in this timestamp.
			//Vnext contains convoys that we want to take to the next timestamp.
			//Vnext should not contain any convoy that is a subset of Vsp of this partition because such a convoy will
			// be a 2nd order spanning convoy spanning the left and the current hw which already have been found in
			//previous methods
			List<Convoy> subsetConvoys = new ArrayList<>();
			for (Convoy vsp :
					Vsp) {
				for (Convoy vnext :
						Vnext) {
					if(vsp.getObjs().containsAll(vnext.getObjs())){ //vnext is a subset of vsp
						subsetConvoys.add(vnext);
					}
				}
			}
			Vnext.removeAll(subsetConvoys); //remove all subset convoys
			if (Vnext.size()==0){
				for (Convoy v:
						V) {
					v.setRightOpen(false);
					if (!shouldBePrunned(v)) {
						result.add(v);
					}
				}
			}
			return result;
		}
		return null;
	}

	/**
	 *
	 * @param leftConvoys List of right-open convoys (if the convoys ought to be reused, make a copy)
	 * @param rightConvoys List of left-open convoys (if the convoys ought to be reused, make a copy)
	 *
	 * @return A tuple of open and close list of convoys
	 * The closed convoys can not be further extended. They should be added to the result.
	 * The open convoys can be left or right open. They can further be used for extension. If a left convoy does not
	 * extend to the right, it is closed from the right.
	 */
	public Tuple2<List<Convoy>,List<Convoy>> matchConvoys(List<Convoy> leftConvoys, List<Convoy> rightConvoys){
		if (leftConvoys==null || rightConvoys==null){
			List<Convoy> result = new ArrayList<>();
			if (leftConvoys!=null){
				for (Convoy v :
						leftConvoys) {
					v.setRightOpen(false);
					if (!shouldBePrunned(v)){
						result.add(v);
					}
				}
				return new Tuple2<>(null,result);
			}
			if (rightConvoys!=null){
				for (Convoy v :
						rightConvoys) {
					v.setLeftOpen(false);
					if (!shouldBePrunned(v)){
						result.add(v);
					}
				}
				return new Tuple2<>(null,result);
			}
			return null;
		}
		Convoy convoy = null;
		List<Convoy> resultOpen = new ArrayList<>();
		List<Convoy> resultClosed = new ArrayList<>();
		for (Convoy leftConvoy:
			 leftConvoys) {
			for (Convoy rightConvoy :
					rightConvoys) {
				if (leftConvoy.isRightOpen() && rightConvoy.isLeftOpen()) {
					convoy = leftConvoy.disjointMatchToTheRight(rightConvoy, m); //returns null if intersetction is <m.
					if (convoy==null || shouldBePrunned(convoy)){ //no match
						continue;
					}else { //match
						if (convoy.isOpen()){
								resultOpen.add(convoy);
						}else if (convoy.lifetime() >= k){
							resultClosed.add(convoy);
						}
						if (leftConvoy.size()==convoy.size()){
							leftConvoy.setAbsorbed(true);
						}
						if (rightConvoy.size()==convoy.size()){
							rightConvoy.setAbsorbed(true);
						}
					}
				}
			}
			leftConvoy.setRightOpen(false); //all matches have been added to the result. No more to match
			if (!leftConvoy.isAbsorbed()) {
				if (leftConvoy.isOpen()) {
					resultOpen.add(leftConvoy);
				} else if (leftConvoy.lifetime() >= k) {
					resultClosed.add(leftConvoy);
				}
			}
		}
		for (Convoy rightConvoy :
				rightConvoys) {
			rightConvoy.setLeftOpen(false);
			if (!rightConvoy.isAbsorbed()) {
				if (rightConvoy.isOpen()) {
					resultOpen.add(rightConvoy);
				} else if (rightConvoy.lifetime() >= k) {
					resultClosed.add(rightConvoy);
				}
			}
		}
		return Tuple2.of(resultOpen,resultClosed);
	}

	public int getM() {
		return m;
	}

	public void setM(int m) {
		this.m = m;
	}

	public int getK() {
		return k;
	}

	public void setK(int k) {
		this.k = k;
	}
}
