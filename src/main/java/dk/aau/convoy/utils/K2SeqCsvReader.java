package dk.aau.convoy.utils;

import dk.aau.convoy.model.ClusterablePoint;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by faisal on 10/22/16.
 */
public class K2SeqCsvReader {
	private BufferedReader br=null;
	private String lineFromPrevBatch=null;
	private long batchTime=0;
	private boolean end = false;

	public K2SeqCsvReader() {
	}

	public K2SeqCsvReader(String inputFilePath){
		try {
			br = new BufferedReader(new FileReader(new File(inputFilePath)));
			lineFromPrevBatch = br.readLine();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// returns tuple of <time,Map<oid,Point>>
	public Tuple2<Long,Map<Long, ClusterablePoint>> readNextTimeBatch(){
		String line = null;
		Map<Long, ClusterablePoint> batch = new HashMap<Long, ClusterablePoint>();
		//lets start the batch from the line which belongs to this batch but was read in the previous batch
		String[] values = lineFromPrevBatch.split(",");
		batchTime = Long.valueOf(values[0]);
//		System.out.println("time=" + batchTime);
		batch.put(
				Long.valueOf(values[1]),
				new ClusterablePoint(Long.valueOf(values[1]), Double.valueOf(values[2]), Double.valueOf(values[3]))
		);
		try {
			while((line = br.readLine()) != null) {
				values = line.split(",");
				int t = Integer.valueOf(values[0]);
				if (t!=batchTime){
					//The new batch has started
					//prepare the result to return
					Tuple2 result = Tuple2.of(batchTime, batch);
					//Store this new line for the next call
					lineFromPrevBatch = line;
					//update the batchtime
					batchTime = t;
					return result;
				}
				batch.put(
						Long.valueOf(values[1]),
						new ClusterablePoint(Long.valueOf(values[1]),
								Double.valueOf(values[2]),
								Double.valueOf(values[3]))
				);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		// we reach here because this is the end of file
		// we haven't returned the result yet, but we need to check if we have something to retuen
		if (end){
			return null;
		}
		else{
			end = true;
			return Tuple2.of(new Long(batchTime), batch);
		}
	}

	public void endReading(){
		try {
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
