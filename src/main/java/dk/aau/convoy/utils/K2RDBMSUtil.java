package dk.aau.convoy.utils;

import dk.aau.convoy.base.utils.StrictConvoysUtil;
import dk.aau.convoy.clustering.ClusteringUtils;
import dk.aau.convoy.model.ClusterablePoint;
import dk.aau.convoy.model.Convoy;
import org.apache.flink.api.java.tuple.Tuple2;

import java.io.IOException;
import java.util.*;

/**
 * Created by faisal on 2/21/17.
 */
public class K2RDBMSUtil {

	private static int batch=0;
	private static int t=0;

	public static List<Convoy> findK2Convoys(String inputFilePath, int eps, int m, int k){
		K2SeqDataManager dataManager = new K2SeqDataManager(inputFilePath, k);
		ClusteringUtils clusteringUtils = new ClusteringUtils(eps, m);

//		List<Convoy> Vlo = K2SeqUtils.findVsp(dataManager, clusteringUtils);

		LinkedList<List<Convoy>> VspBuckets = K2SeqUtils.findVspBucketed(dataManager, clusteringUtils);
//		//Iterate over buckets and merge with the next bucket
		List<Convoy> Vspext = K2SeqUtils.mergeVspBuckets(VspBuckets, m, k);
		List<Convoy> Vlo = K2SeqUtils.extendRight(Vspext, dataManager, clusteringUtils);
		List<Convoy> Vrelaxed = K2SeqUtils.extendLeft(Vlo, dataManager, clusteringUtils, k);
		List<Convoy> k2strictConvoys = StrictConvoysUtil.mineStrictModelConvoys(Vrelaxed, eps, m, k, dataManager, clusteringUtils);
		return k2strictConvoys;
	}


	public static LinkedList<List<Convoy>> findVspBucketed(K2SeqDataManager dataManager, int k, ClusteringUtils clusteringUtils) throws IOException, InterruptedException {
		long b1=0;
		long b2 = b1 + (k / 2) -1;
		LinkedList<List<Convoy>> VspBuckets = new LinkedList<>();
		List<ClusterablePoint> b1Data = dataManager.getRDBMSTuplesForTime(b1);
		List<Set<Long>> clustersTs = clusteringUtils.clusterHBase(b1Data);
		List<ClusterablePoint> b2Data = null;
		List<Set<Long>> clustersTe = null;

		while ((b2Data = dataManager.getHBaseTuplesForTime(b2)) != null) {
			b2Data = dataManager.getRDBMSTuplesForTime(b2);
			clustersTe = clusteringUtils.clusterHBase(b2Data);
			List<Convoy> result = clusteringUtils.runK2HBaseSpHWMT(b1, b2, clustersTs, clustersTe,dataManager);
			if (result != null && result.size() > 0) {
				VspBuckets.add(result);
			}
			b1 = b2;
			b2 = b1 + (k / 2) ;
			clustersTs = clustersTe;
		}
		return VspBuckets;
	}

	public static Long getTs(Map<Long, Map<Long, ClusterablePoint>> batch ) {
		Set<Long> keys = batch.keySet();
		List<Long> timeList = new ArrayList<>(keys);
		timeList.sort(new Comparator<Long>() {
			@Override
			public int compare(Long o1, Long o2) {
				return Long.compare(o1, o2);
			}
		});
		return timeList.get(0);
	}

	public static List<Convoy> mergeVspBuckets(LinkedList<List<Convoy>> VspBuckets, int m, int k) {
		Iterator<List<Convoy>> buckets = VspBuckets.iterator();
		List<Convoy> Vnext = new ArrayList<>();
		List<Convoy> V = new ArrayList<>();
		if (buckets.hasNext()) {
			V = buckets.next();
		} else {
			return V;
		}
		List<Convoy> Vpcc = new ArrayList<>();
		long currentTime=0;
		long previousTime=V.get(0).getTe();
		while (buckets.hasNext()) {
			List<Convoy> C = buckets.next();
			Vnext=new ArrayList<>();
			currentTime=C.get(0).getTs();
			if(currentTime!=previousTime){
				//not mergeable
				for (Convoy v :
						V) {
					if (v.lifetime() >= k/2) {
						Vpcc = StrictConvoysUtil.updateVnext(Vpcc, v);
					}
				}
				previousTime = C.get(0).getTe();
				V = C;
				continue;

			}
			for(Convoy c : C){
				c.setMatched(false);c.setAbsorbed(false);
			}
			for(Convoy v : V){
				v.setExtended(false);v.setAbsorbed(false);
				for(Convoy c : C){
					if(c.size()>=m && v.simpleIntersection(c,m) != null){ //extend (v,c)
						v.setExtended(true);c.setMatched(true);
						Convoy vext = v.simpleIntersection(c,m);
						Vnext = StrictConvoysUtil.updateVnext(Vnext, vext);
						if(v.isSubsetInObjs(c)){v.setAbsorbed(true);}
						if(c.isSubsetInObjs(v)){c.setAbsorbed(true);}
					}
				}
				if(!v.isAbsorbed()){
					if(v.lifetime() >= k/2){
						Vpcc.add(v);
					}
				}
			}
			for(Convoy c : C){
				if(!c.isAbsorbed()){
					Vnext=StrictConvoysUtil.updateVnext(Vnext, c);
				}
			}
			V=Vnext;
			previousTime=C.get(0).getTe();
		}
		for(Convoy v:V){
			if(v.lifetime()>=k/2){
				Vpcc.add(v);
			}
		}

		//Print the Vsp convoys
//		for (Convoy v :
//				Vpcc) {
//			System.out.println(v);
//		}
		return Vpcc;
	}

	public static List<Convoy> getClusters(Long t, List<Convoy> inputConvoys, ClusteringUtils clusteringUtils, K2SeqDataManager dataManager) throws IOException, InterruptedException {
		Map<Long, ClusterablePoint> data = dataManager.getFromHBase2(t, inputConvoys);
		return clusteringUtils.clusterTimeData(t, data);
	}

	public static Tuple2<List<Convoy>,List<Convoy>> reCluster(Long t, List<Convoy> inputConvoys, ClusteringUtils clusteringUtils, K2SeqDataManager dataManager) throws IOException, InterruptedException {
		Map<Long, ClusterablePoint> data = dataManager.getFromHBase2(t, inputConvoys);
		Tuple2<List<Convoy>, List<Convoy>> result = clusteringUtils.reCluster(t, inputConvoys, dataManager);
		return result;
	}

	public static Tuple2<List<Convoy>,List<Convoy>> reClusterLeft(Long t, List<Convoy> inputConvoys, ClusteringUtils clusteringUtils, K2SeqDataManager dataManager) throws IOException, InterruptedException {
		Map<Long, ClusterablePoint> data = dataManager.getFromHBase2(t, inputConvoys);
		Tuple2<List<Convoy>, List<Convoy>> result = clusteringUtils.reClusterLeft(t, inputConvoys, dataManager);
		return result;
	}

	public static List<Convoy> extendRightHBaseBatch(List<Convoy> Vspext, int m, int k, ClusteringUtils clusteringUtils, K2SeqDataManager dataManager) throws IOException, InterruptedException {
		TreeMap<Long, List<Convoy>> timeConvoyMap = new TreeMap<>();
		for (Convoy v :
				Vspext) {
			if (timeConvoyMap.containsKey(v.getTe())) {
				timeConvoyMap.get(v.getTe()).add(v);
			} else {
				timeConvoyMap.put(v.getTe(), new ArrayList<Convoy>());
				timeConvoyMap.get(v.getTe()).add(v);
			}
		}
		Long currentTime = timeConvoyMap.firstKey();
		Long nextBenchmarkPoint = currentTime+k/2;
		Long nextBenchmarkTimeInVspExt = timeConvoyMap.higherKey(currentTime);


		List<Convoy> Vnext = null;
		List<Convoy> V = timeConvoyMap.get(currentTime);
		List<Convoy> C = null;
		List<Convoy> Vresult = new ArrayList<>();
		while (true) {
			C=getClusters(++currentTime,V,clusteringUtils,dataManager);
			Vnext = new ArrayList<>();
			if (C==null || C.size()==0){
				//the current convoys in V cant get more extended so add them to the result
				for(Convoy v:V){
					if (v.lifetime()>=k/2) {
						StrictConvoysUtil.updateStrictVnext(Vresult, v);
					}
				}
				//jump to the next group of spanning convoy to start extending them to the right
				if (nextBenchmarkTimeInVspExt != null) {
					currentTime = nextBenchmarkTimeInVspExt;
					nextBenchmarkPoint = currentTime + (k / 2);
					Vnext.addAll(timeConvoyMap.get(currentTime));
					nextBenchmarkTimeInVspExt = timeConvoyMap.higherKey(currentTime);
					V=Vnext;
					continue;
				} else {
					//there are no more Vsps to be extended to the right so exit
					break;
				}
			}
			//now we start the normal extension process based on PCCD

			for(Convoy c : C){
				c.setMatched(false);c.setAbsorbed(false);
//				System.out.println(c);
			}

			for(Convoy v : V){
				v.setExtended(false);v.setAbsorbed(false);
				for(Convoy c : C){
					Convoy vext = v.simpleIntersection(c, m);
					if(vext!= null){ //extend (v,c)
						v.setExtended(true);c.setMatched(true);
						vext.setTs(v.getTs());vext.setTe(currentTime);
						Vnext = StrictConvoysUtil.updateVnext(Vnext, vext);
						if(v.isSubsetInObjs(c)){v.setAbsorbed(true);}
						if(c.isSubsetInObjs(v)){c.setAbsorbed(true);}
					}
				}
				if(!v.isAbsorbed() && v.lifetime()>=k/2){
					StrictConvoysUtil.updateStrictVnext(Vresult, v);
				}
			}
//			for(Convoy c : C){
//				if(!c.isAbsorbed()){
//					Vnext=StrictConvoysUtil.updateVnext(Vnext, c);
//				}
//			}

			//if moving from left to right, the current convoys get extended to the right such that
			//we reach the nextBenchmarkTimeInVspExt, get the convoys corresponding to the next time (benchmark point)
			if (currentTime.longValue() == nextBenchmarkPoint.longValue()) {
				if (nextBenchmarkTimeInVspExt!=null){
					Vnext = new ArrayList<>();
					Vnext.addAll(timeConvoyMap.get(nextBenchmarkTimeInVspExt));
					currentTime = nextBenchmarkTimeInVspExt;
					nextBenchmarkPoint = currentTime + k / 2;
					nextBenchmarkTimeInVspExt = timeConvoyMap.higherKey(nextBenchmarkTimeInVspExt);
					V = Vnext;
					for (Convoy v :
							V) {
						if (!v.isExtended() && v.lifetime()>=k/2) {
							Vresult = StrictConvoysUtil.updateVnext(Vresult, v);
						}
					}
					continue;
				}else
					break;
			}
					//The above code ensures that as we scan from left to right, when ever we reach a benchmark point at which
			//there are some Vsps which end, we add those Vsps to the extension process. If at any point, no clusters exist
			//which means that the current convoys in V cant be extended any more, we jump to the next benchmark point having
			//ending convoys
			V=Vnext;
		}
		for(Convoy v:V){
			if (v.lifetime()>=k/2) {
				StrictConvoysUtil.updateStrictVnext(Vresult, v);
			}
		}


//		List<Convoy> Vr = new ArrayList<>();
//		for (Convoy v :
//				Vspext) {
//			//extend v to the right
//			Vr = ExtendRightVspext(v, clusteringUtils, Vr, dataManager);
//		}
		return Vresult;
	}

	public static List<Convoy> extendLeftHBaseBatch(List<Convoy> Vspext, int m, int k, ClusteringUtils clusteringUtils, K2SeqDataManager dataManager) throws IOException, InterruptedException {
		TreeMap<Long, List<Convoy>> timeConvoyMap = new TreeMap<>();
		for (Convoy v :
				Vspext) {
			if (timeConvoyMap.containsKey(v.getTs())) {
				timeConvoyMap.get(v.getTs()).add(v);
			} else {
				timeConvoyMap.put(v.getTs(), new ArrayList<Convoy>());
				timeConvoyMap.get(v.getTs()).add(v);
			}
		}
		Long currentTime = timeConvoyMap.lastKey();
		Long nextBenchmarkPoint = currentTime-k/2;
		Long nextBenchmarkTimeInVspExt = timeConvoyMap.lowerKey(currentTime);


		List<Convoy> Vnext = null;
		List<Convoy> V = timeConvoyMap.get(currentTime);
		List<Convoy> C = null;
		List<Convoy> Vresult = new ArrayList<>();
		while (true) {
			C=getClusters(--currentTime,V,clusteringUtils,dataManager);
			Vnext = new ArrayList<>();
			if (C==null || C.size()==0){
				//the current convoys in V cant get more extended so add them to the result
				for(Convoy v:V){
					if (v.lifetime()>=k) {
						StrictConvoysUtil.updateStrictVnext(Vresult, v);
					}
				}
				//jump to the next group of spanning convoy to start extending them to the right
				if (nextBenchmarkTimeInVspExt != null) {
					currentTime = nextBenchmarkTimeInVspExt;
					nextBenchmarkPoint = currentTime - (k / 2);
					Vnext.addAll(timeConvoyMap.get(currentTime));
					nextBenchmarkTimeInVspExt = timeConvoyMap.lowerKey(currentTime);
					V=Vnext;
					continue;
				} else {
					//there are no more Vsps to be extended to the right so exit
					break;
				}
			}
			//now we start the normal extension process based on PCCD

//			for(Convoy c : C){
//				c.setMatched(false);c.setAbsorbed(false);
////				System.out.println(c);
//			}

			for(Convoy v : V){
				v.setExtended(false);v.setAbsorbed(false);
				for(Convoy c : C){
					Convoy vext = v.simpleIntersection(c, m);
					if(vext!= null){ //extend (v,c)
						v.setExtended(true);c.setMatched(true);
						vext.setTs(currentTime);vext.setTe(v.getTe());
						Vnext = StrictConvoysUtil.updateVnext(Vnext, vext);
						if(v.isSubsetInObjs(c)){v.setAbsorbed(true);}
						if(c.isSubsetInObjs(v)){c.setAbsorbed(true);}
					}
				}
				if(!v.isAbsorbed() && v.lifetime()>=k/2){
					StrictConvoysUtil.updateStrictVnext(Vresult, v);
				}
			}

			//if moving from left to right, the current convoys get extended to the right such that
			//we reach the nextBenchmarkTimeInVspExt, get the convoys corresponding to the next time (benchmark point)
			if (currentTime.longValue() == nextBenchmarkPoint.longValue()) {
				if (nextBenchmarkTimeInVspExt!=null){
					Vnext = new ArrayList<>();
					Vnext.addAll(timeConvoyMap.get(nextBenchmarkTimeInVspExt));
					currentTime = nextBenchmarkTimeInVspExt;
					nextBenchmarkPoint = currentTime - k / 2;
					nextBenchmarkTimeInVspExt = timeConvoyMap.lowerKey(nextBenchmarkTimeInVspExt);
					V = Vnext;
					for (Convoy v :
							V) {
						if (!v.isExtended() && v.lifetime()>=k) {
							Vresult = StrictConvoysUtil.updateVnext(Vresult, v);
						}
					}
					continue;
				}else
					break;
			}
			//The above code ensures that as we scan from left to right, when ever we reach a benchmark point at which
			//there are some Vsps which end, we add those Vsps to the extension process. If at any point, no clusters exist
			//which means that the current convoys in V cant be extended any more, we jump to the next benchmark point having
			//ending convoys
			V=Vnext;
		}
		for(Convoy v:V){
			if (v.lifetime()>=k) {
				StrictConvoysUtil.updateStrictVnext(Vresult, v);
			}
		}


//		List<Convoy> Vr = new ArrayList<>();
//		for (Convoy v :
//				Vspext) {
//			//extend v to the right
//			Vr = ExtendRightVspext(v, clusteringUtils, Vr, dataManager);
//		}
		return Vresult;
	}

//	public static List<Convoy> extendRightOptim(List<Convoy> Vspext, K2SeqDataManager dataManager, ClusteringUtils clusteringUtils) {
//		List<Convoy> Vr = new ArrayList<>();
//		for (Convoy v :
//				Vspext) {
//			//extend v to the right
//			Vr = ExtendRightVspext(v, dataManager, clusteringUtils, Vr);
//		}
//		return Vr;
//	}

	public static List<Convoy> extendRightIndl(List<Convoy> Vspext, int m, int k, ClusteringUtils clusteringUtils, K2SeqDataManager dataManager) throws IOException, InterruptedException {
		List<Convoy> Vresult = new ArrayList<>();
		if (Vspext == null || Vspext.size() == 0) {
			return Vresult;
		}
		TreeMap<Long, List<Convoy>> timeConvoyMap = new TreeMap<>();
		for (Convoy v :
				Vspext) {
			if (timeConvoyMap.containsKey(v.getTe())) {
				timeConvoyMap.get(v.getTe()).add(v);
			} else {
				timeConvoyMap.put(v.getTe(), new ArrayList<Convoy>());
				timeConvoyMap.get(v.getTe()).add(v);
			}
		}
		Long currentTime = timeConvoyMap.firstKey();
		Long nextBenchmarkPoint = currentTime+k/2;
		Long nextBenchmarkTimeInVspExt = timeConvoyMap.higherKey(currentTime);


		List<Convoy> Vnext = null;
		List<Convoy> V = timeConvoyMap.get(currentTime);
		List<Convoy> C = null;
		while (true) {
			Tuple2<List<Convoy>, List<Convoy>> clusteringResult = reCluster(++currentTime, V, clusteringUtils, dataManager);
			List<Convoy> extendedConvoys = clusteringResult.f1;
			List<Convoy> nonExtendedConvoys = clusteringResult.f0;
			if (nonExtendedConvoys != null && nonExtendedConvoys.size() > 0){
				for (Convoy v :
						nonExtendedConvoys) {
					if (v.lifetime() >= (k / 2)) {
						Vresult = StrictConvoysUtil.updateVnext(Vresult, v);
					}
				}
			}
			Vnext = new ArrayList<>();
			//No maximal spanning convoy can extend right to the next benchmark point without becomming a subset of an existing maximal spanning convoy
			//If a convoy extends to the next benchmark point, we can discard it and stop the extention process unless there
			//are new convoys in Vspext (maximal spanning convoy set) which end at the benchmark point.
			//we reach the nextBenchmarkTime, get the convoys corresponding to the next time (benchmark point)
			if (currentTime.longValue() == nextBenchmarkPoint.longValue()) {
				if (nextBenchmarkTimeInVspExt!=null){
					Vnext.addAll(timeConvoyMap.get(nextBenchmarkTimeInVspExt));
					currentTime = nextBenchmarkTimeInVspExt;
					nextBenchmarkPoint = currentTime + k / 2;
					nextBenchmarkTimeInVspExt = timeConvoyMap.higherKey(nextBenchmarkTimeInVspExt);
					V = Vnext;
					continue;
				}else
					break;
			}
			if (extendedConvoys != null && extendedConvoys.size() > 0) {
				Vnext.addAll(extendedConvoys);
				V=Vnext;
				continue;
			}else {
				if (nextBenchmarkTimeInVspExt!=null){
					currentTime = nextBenchmarkTimeInVspExt;
					Vnext.addAll(timeConvoyMap.get(currentTime));
					V = Vnext;
					nextBenchmarkTimeInVspExt = timeConvoyMap.higherKey(currentTime);
					nextBenchmarkPoint = currentTime + k / 2;
				}else
					break;
			}


			//The above code ensures that as we scan from left to right, when ever we reach a benchmark point at which
			//there are some Vsps which end, we add those Vsps to the extension process. If at any point, no clusters exist
			//which means that the current convoys in V cant be extended any more, we jump to the next benchmark point having
			//ending convoys

		}
		for (Convoy v : V) {
			if (v.lifetime() >= (k / 2)) {
				Vresult = StrictConvoysUtil.updateVnext(Vresult, v);
			}
		}
		return Vresult;
	}

	public static List<Convoy> extendLefttIndl(List<Convoy> Vspext, int m, int k, ClusteringUtils clusteringUtils, K2SeqDataManager dataManager) throws IOException, InterruptedException {
		List<Convoy> Vresult = new ArrayList<>();
		if (Vspext == null || Vspext.size() == 0) {
			return Vresult;
		}
		TreeMap<Long, List<Convoy>> timeConvoyMap = new TreeMap<>();
		for (Convoy v :
				Vspext) {
			if (timeConvoyMap.containsKey(v.getTs())) {
				timeConvoyMap.get(v.getTs()).add(v);
			} else {
				timeConvoyMap.put(v.getTs(), new ArrayList<Convoy>());
				timeConvoyMap.get(v.getTs()).add(v);
			}
		}
		Long currentTime = timeConvoyMap.lastKey();
		Long nextBenchmarkPoint = currentTime-k/2;
		Long nextBenchmarkTimeInVspExt = timeConvoyMap.lowerKey(currentTime);


		List<Convoy> Vnext = null;
		List<Convoy> V = timeConvoyMap.get(currentTime);
		List<Convoy> C = null;
		while (true) {
			Tuple2<List<Convoy>, List<Convoy>> clusteringResult = reClusterLeft(--currentTime, V, clusteringUtils, dataManager);
			List<Convoy> extendedConvoys = clusteringResult.f1;
			List<Convoy> nonExtendedConvoys = clusteringResult.f0;
			if (nonExtendedConvoys != null && nonExtendedConvoys.size() > 0){
				for (Convoy v :
						nonExtendedConvoys) {
					if (v.lifetime() >= k ) {
						Vresult = StrictConvoysUtil.updateVnext(Vresult, v);
					}
				}
			}
			Vnext = new ArrayList<>();
			//No maximal spanning convoy can extend right to the next benchmark point without becomming a subset of an existing maximal spanning convoy
			//If a convoy extends to the next benchmark point, we can discard it and stop the extention process unless there
			//are new convoys in Vspext (maximal spanning convoy set) which end at the benchmark point.
			//we reach the nextBenchmarkTime, get the convoys corresponding to the next time (benchmark point)
			if (currentTime.longValue() == nextBenchmarkPoint.longValue()) {
				if (nextBenchmarkTimeInVspExt!=null){
					Vnext.addAll(timeConvoyMap.get(nextBenchmarkTimeInVspExt));
					currentTime = nextBenchmarkTimeInVspExt;
					nextBenchmarkPoint = currentTime - (k / 2);
					nextBenchmarkTimeInVspExt = timeConvoyMap.lowerKey(nextBenchmarkTimeInVspExt);
					V = Vnext;
					continue;
				}else
					break;
			}
			if (extendedConvoys != null && extendedConvoys.size() > 0) {
				Vnext.addAll(extendedConvoys);
				V=Vnext;
				continue;
			}else {
				if (nextBenchmarkTimeInVspExt!=null){
					currentTime = nextBenchmarkTimeInVspExt;
					Vnext.addAll(timeConvoyMap.get(currentTime));
					V = Vnext;
					nextBenchmarkTimeInVspExt = timeConvoyMap.lowerKey(currentTime);
					nextBenchmarkPoint = currentTime - (k / 2);
				}else
					break;
			}


			//The above code ensures that as we scan from left to right, when ever we reach a benchmark point at which
			//there are some Vsps which end, we add those Vsps to the extension process. If at any point, no clusters exist
			//which means that the current convoys in V cant be extended any more, we jump to the next benchmark point having
			//ending convoys

		}
		return Vresult;
	}

	private static List<Convoy> ExtendRightVspext(Convoy inputConvoy, K2SeqDataManager dataManager, ClusteringUtils clusteringUtils, List<Convoy> Vlo) {
		//extend to the right
		if (!dataManager.hasTimeStamp(inputConvoy.getTe()+1)) {
			//end convoy, no extension possible
			Vlo.add(inputConvoy);
			return Vlo;
		}
		List<Convoy> Vnext = null;
		List<Convoy> V = new ArrayList<>();
		V.add(inputConvoy);
//		List<Convoy> Vlo = new ArrayList<>(); //leftopen
		List<Convoy> C = null;
		for(long t = inputConvoy.getTe() + 1; V.size()>0 && dataManager.hasTimeStamp(t); t++){ //maxTime;t++){
			Vnext = new ArrayList<>();
			for (Convoy v :
					V) {
				List<Convoy> Vnew = clusteringUtils.reCluster(t, v, dataManager.getData());
				if (Vnew == null || Vnew.size() == 0) {
					//convoy ends. Add it to the result
					Vlo = StrictConvoysUtil.updateVnext(Vlo, v);
				} else {
					for (Convoy vnew :
							Vnew) {
						vnew.setTs(v.getTs());
					}
					Vnext.addAll(Vnew);
					if ((Vnew.size() == 1 && Vnew.get(0).size() < v.size())
							|| Vnew.size()>1) {
						// either the size of v after reclustering remains the same or it decreases
						//if it remains the same, the same convoy got extended
						//but if the size reduces, v no longer gets extended so we should add it to the result
						//also if the convoy gets split i.e. Vnew.size>1, we add the input conovy to the result
						Vlo = StrictConvoysUtil.updateVnext(Vlo, v);
					}
				}
			}
			V=Vnext;
//			System.out.println("ts = "+t+", |V| = "+V.size()+", |Vpcc| = "+Vpcc.size());
		}
		return Vlo;
	}


	private static List<Convoy> ExtendRightVspextOptim(Convoy inputConvoy, K2SeqDataManager dataManager, ClusteringUtils clusteringUtils, List<Convoy> Vlo) {
		//extend to the right
		if (!dataManager.hasTimeStamp(inputConvoy.getTe()+1)) {
			//end convoy, no extension possible
			Vlo.add(inputConvoy);
			return Vlo;
		}
		List<Convoy> Vnext = null;
		List<Convoy> V = new ArrayList<>();
		V.add(inputConvoy);
//		List<Convoy> Vlo = new ArrayList<>(); //leftopen
		List<Convoy> C = null;
		for(long t = inputConvoy.getTe() + 1; V.size()>0 && dataManager.hasTimeStamp(t); t++){ //maxTime;t++){
			Vnext = new ArrayList<>();
			for (Convoy v :
					V) {
				List<Convoy> Vnew = clusteringUtils.reCluster(t, v, dataManager.getData());
				if (Vnew == null || Vnew.size() == 0) {
					//convoy ends. Add it to the result
					Vlo = StrictConvoysUtil.updateVnext(Vlo, v);
				} else {
					for (Convoy vnew :
							Vnew) {
						vnew.setTs(v.getTs());
					}
					Vnext.addAll(Vnew);
					if ((Vnew.size() == 1 && Vnew.get(0).size() < v.size())
							|| Vnew.size()>1) {
						// either the size of v after reclustering remains the same or it decreases
						//if it remains the same, the same convoy got extended
						//but if the size reduces, v no longer gets extended so we should add it to the result
						//also if the convoy gets split i.e. Vnew.size>1, we add the input conovy to the result
						Vlo = StrictConvoysUtil.updateVnext(Vlo, v);
					}
				}
			}
			V=Vnext;
//			System.out.println("ts = "+t+", |V| = "+V.size()+", |Vpcc| = "+Vpcc.size());
		}
		return Vlo;
	}

	public static List<Convoy> extendLeft(List<Convoy> Vspext, K2SeqDataManager dataManager, ClusteringUtils clusteringUtils,int k) {
		List<Convoy> Vl = new ArrayList<>();
		for (Convoy v :
				Vspext) {
			//extend v to the right
			Vl = ExtendLeftVspext(v, dataManager, clusteringUtils, k, Vl);
		}
		return Vl;
	}

	private static List<Convoy> ExtendLeftVspext(Convoy inputConvoy, K2SeqDataManager dataManager, ClusteringUtils clusteringUtils, int k, List<Convoy> Vr) {
		//extend to the right
		List<Convoy> Vnext = null;
		List<Convoy> V = new ArrayList<>();
		V.add(inputConvoy);
//		List<Convoy> Vr = new ArrayList<>(); //leftopen
		List<Convoy> C = null;
		for(long t = inputConvoy.getTs() - 1; V.size()>0 && dataManager.hasTimeStamp(t); t--){ //maxTime;t++){
			Vnext = new ArrayList<>();
			for (Convoy v :
					V) {
				List<Convoy> Vnew = clusteringUtils.reCluster(t, v, dataManager.getData());
				if (Vnew == null || Vnew.size() == 0) {
					//convoy ends. Add it to the result
					if (v.lifetime() >= k) {
						Vr = StrictConvoysUtil.updateVnextToLeft(Vr, v);
					}

				} else {
					for (Convoy vnew :
							Vnew) {
						vnew.setTe(v.getTe());
					}
					Vnext.addAll(Vnew);
					if ((Vnew.size() == 1 && Vnew.get(0).size() < v.size())
							|| Vnew.size()>1) {
						// either the size of v after reclustering remains the same or it decreases
						//if it remains the same, the same convoy got extended
						//but if the size reduces, v no longer gets extended so we should add it to the result
						//also if the convoy gets split i.e. Vnew.size>1, we add the input conovy to the result
						if (v.lifetime()>=k) {
							Vr = StrictConvoysUtil.updateVnextToLeft(Vr, v);
						}
					}
				}
			}
			V=Vnext;
//			System.out.println("ts = "+t+", |V| = "+V.size()+", |Vpcc| = "+Vpcc.size());
		}
		return Vr;
	}
}

