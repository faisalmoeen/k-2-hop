package dk.aau.convoy.utils;

import com.google.common.collect.Sets;
import dk.aau.convoy.model.ClusterablePoint;
import dk.aau.convoy.model.Convoy;
import hbase.ReadHBase;
import org.apache.flink.api.java.tuple.Tuple2;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by faisal on 10/22/16.
 */
public class K2SeqRDBMSDataManager extends K2SeqDataManager{
	private int batchNum = 0;

	public int getK() {
		return k;
	}

	private int k;
	//data var stores all the data retrieved from the file
	private Map<Long, Map<Long, ClusterablePoint>> data = new HashMap<>();

	public K2SeqRDBMSDataManager(String tableName, int k){
		this.k = k;
		try {
			ReadRDBMS.init(tableName);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public long getNumPoints(){
		long numPoints = 0;
		for (Long i:
			 data.keySet()) {
			numPoints += data.get(i).size();
		}
		System.out.println("Total number of points processed = "+ numPoints);
		return numPoints;
	}

	//returns Map<time,HashMap<oid,point>>
//	public  Map<Long, Map<Long, ClusterablePoint>> getNextK2Batch(){
//		//following returns Tuple2<ts,Map<time,Map<oid,Point>>>
//		Tuple2<Long,Map<Long, Map<Long, ClusterablePoint>>> k2Batch = getNextBatch();
//		//store the batch in the data hashmap field
//		if (k2Batch != null) {
//			data.putAll(k2Batch.f1);
//		}
//		//return the batch
//		return (k2Batch!=null && k2Batch.f1.size()>0?k2Batch.f1:null);
//	}

	//returns Tuple2<ts,Map<time,Map<oid,Point>>>
//	private Tuple2<Long,Map<Long, Map<Long, ClusterablePoint>>> getNextBatch(){
//		Map<Long, Map<Long, ClusterablePoint>> k2Batch = new HashMap<>();
//		Tuple2<Long,Map<Long, ClusterablePoint>> timeBatch = null;//Tuple2<time,HashMap<oid,point>>
//		Long ts = null;
//		for (int i=1; i<=k/2;i++){
//			if((timeBatch = csvReader.readNextTimeBatch())!=null){
//				k2Batch.put(timeBatch.f0, timeBatch.f1);
//				//if its the first timestamp in the batch, it is the ts of the batch
//				if (i==1){
//					ts = timeBatch.f0;
//				}
//			}else {
//				//end of file. return the result if not with size 0
//				return (k2Batch.size()>0?Tuple2.of(ts,k2Batch):null);
//			}
//		}
//		//we reached here either because k2 batch is complete or its the end of file
//		return (k2Batch.size()>0?Tuple2.of(ts,k2Batch):null);
//	}

	public List<ClusterablePoint> getHBaseTuplesForTime(long t) throws IOException {
		if (data.containsKey(t)) {
			return new ArrayList<>(data.get(t).values());
		}else{
			Map<Long,ClusterablePoint> tdata = null;
			tdata = ReadRDBMS.getTuplesForTime(t);
			if (tdata != null) {
				data.put(t, tdata);
				return new ArrayList<>(data.get(t).values());
			} else {
				return null;
			}
		}
	}


//	private Map<Long, Map<Long, ClusterablePoint>> getBatch(Long ts){
//		if (data != null) {
//			return data.get(ts);
//		}
//		return null;
//	}

//	private Map<Long, Map<Long, ClusterablePoint>> getBatchContaining(Long time) {
//		long batchnum = time/hopWindowWidth;
//		return data.get(batchnum + 1);
//	}

//	public Map<Long, ClusterablePoint> getTimeBatch(Long time) {
//		return getBatchContaining(time).get(time);
//	}

	public Map<Long, Map<Long, ClusterablePoint>> getData() {
		return data;
	}

	public Map<Long, ClusterablePoint> getData(long t, List<Convoy> inputConvoys) {
		Map<Long, ClusterablePoint> reqData = new HashMap<>();
		Map<Long, ClusterablePoint> timeData = data.get(t);
		if (timeData == null || timeData.size()==0) {
			return null;
		}
		for (Convoy v :
				inputConvoys) {
			for (long obj :
					v.getObjs()) {
				reqData.put(obj, timeData.get(obj));
			}
		}
		return reqData;
	}

	public boolean hasTimeStamp(long t) {
		return data.containsKey(t);
	}

	public Map<Long, ClusterablePoint> getHBaseTuples(Long time, Set<Long> desiredObjs) throws IOException, InterruptedException {
		if (data.containsKey(time)) {
			Set<Long> availableObjs = data.get(time).keySet();
			Set<Long> unavailableObjs = Sets.difference(desiredObjs, availableObjs);
			if (unavailableObjs==null || unavailableObjs.size()==0) {
				return data.get(time);
			}else {
				Map<Long, ClusterablePoint> fetchedTuples = ReadRDBMS.batchGet(time, unavailableObjs);
				data.get(time).putAll(fetchedTuples);
			}
		} else {
			Map<Long, ClusterablePoint> tdata = ReadRDBMS.batchGet(time, desiredObjs);
			data.put(time, tdata);
		}
		return data.get(time);
	}

	public Map<Long, ClusterablePoint> getFromHBase1(Long time, List<Set<Long>> inputClusters) throws IOException, InterruptedException {
		Set<Long> desiredObjs = new HashSet<>();
		inputClusters.forEach(desiredObjs::addAll);
		return getHBaseTuples(time,desiredObjs);
	}

	public Map<Long, ClusterablePoint> getFromHBase2(Long time, List<Convoy> inputConvoys) throws IOException, InterruptedException {
		Set<Long> desiredObjs = new HashSet<>();
		for (Convoy v :
				inputConvoys) {
			desiredObjs.addAll(v.getObjs());
		}
		return getHBaseTuples(time,desiredObjs);
	}
}
