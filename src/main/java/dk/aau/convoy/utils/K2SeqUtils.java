package dk.aau.convoy.utils;

import dk.aau.convoy.base.utils.StrictConvoysUtil;
import dk.aau.convoy.clustering.ClusteringUtils;
import dk.aau.convoy.model.ClusterablePoint;
import dk.aau.convoy.model.Convoy;
import org.apache.avro.generic.GenericData;
import org.apache.flink.api.java.tuple.Tuple3;

import java.io.IOException;
import java.util.*;

/**
 * Created by faisal on 10/25/16.
 */
public class K2SeqUtils {

	private static int batch=0;

	public static List<Convoy> findK2Convoys(String inputFilePath, int eps, int m, int k){
		K2SeqDataManager dataManager = new K2SeqDataManager(inputFilePath, k);
		ClusteringUtils clusteringUtils = new ClusteringUtils(eps, m);

//		List<Convoy> Vlo = K2SeqUtils.findVsp(dataManager, clusteringUtils);

		LinkedList<List<Convoy>> VspBuckets = K2SeqUtils.findVspBucketed(dataManager, clusteringUtils);
//		//Iterate over buckets and merge with the next bucket
		List<Convoy> Vspext = K2SeqUtils.mergeVspBuckets(VspBuckets, m, k);
		List<Convoy> Vlo = K2SeqUtils.extendRight(Vspext, dataManager, clusteringUtils);
		List<Convoy> Vrelaxed = K2SeqUtils.extendLeft(Vlo, dataManager, clusteringUtils, k);
		List<Convoy> k2strictConvoys = StrictConvoysUtil.mineStrictModelConvoys(Vrelaxed, eps, m, k, dataManager, clusteringUtils);
		return k2strictConvoys;
	}

	public static List<Convoy> findVsp(K2SeqDataManager dataManager, ClusteringUtils clusteringUtils) {
		List<Convoy> Vsp = new ArrayList<>();
		Map<Long, Map<Long, ClusterablePoint>> k2Batch = dataManager.getNextK2Batch();
		Long ts = getTs(k2Batch);
		List<Set<Long>> clustersTs = clusteringUtils.clusterNoTime(ts, k2Batch);
		// returns Vsp for a partition and clustersTe which will be passed to the next partition as clustersTs
		Tuple3<ArrayList<Convoy>, Long, List<Set<Long>>> result = clusteringUtils.runK2SeqSpHWMT(ts, clustersTs, k2Batch);
		Vsp.addAll(result.f0);
		ts = result.f1;
		clustersTs = result.f2;
		while ((k2Batch = dataManager.getNextK2Batch()) != null) {
			result = clusteringUtils.runK2SeqSpHWMT(ts, clustersTs, k2Batch);
			Vsp.addAll(result.f0);
			ts = result.f1;
			clustersTs = result.f2;
//			System.out.println("Batch:" + batch++);
		}
		return Vsp;
	}

	public static LinkedList<List<Convoy>> findVspBucketed(K2SeqDataManager dataManager, ClusteringUtils clusteringUtils) {
		LinkedList<List<Convoy>> VspBuckets = new LinkedList<>();
		Map<Long, Map<Long, ClusterablePoint>> k2Batch = dataManager.getNextK2Batch();
		Long ts = getTs(k2Batch);
		List<Set<Long>> clustersTs = clusteringUtils.clusterNoTime(ts, k2Batch);
		// returns Vsp for a partition and clustersTe which will be passed to the next partition as clustersTs
		Tuple3<ArrayList<Convoy>, Long, List<Set<Long>>> result = clusteringUtils.runK2SeqSpHWMT(ts, clustersTs, k2Batch);
		if (result.f0!=null && result.f0.size()>0) {
			VspBuckets.add(result.f0);
		}
		ts = result.f1;
		clustersTs = result.f2;
		while ((k2Batch = dataManager.getNextK2Batch()) != null) {
			result = clusteringUtils.runK2SeqSpHWMT(ts, clustersTs, k2Batch);
			if (result.f0!=null && result.f0.size()>0) {
				VspBuckets.add(result.f0);
			}
			ts = result.f1;
			clustersTs = result.f2;
//			System.out.println("Batch:" + batch++);
		}
		return VspBuckets;
	}



	public static Long getTs(Map<Long, Map<Long, ClusterablePoint>> batch ) {
		Set<Long> keys = batch.keySet();
		List<Long> timeList = new ArrayList<>(keys);
		timeList.sort(new Comparator<Long>() {
			@Override
			public int compare(Long o1, Long o2) {
				return Long.compare(o1, o2);
			}
		});
		return timeList.get(0);
	}

	public static List<Convoy> mergeVspBuckets(LinkedList<List<Convoy>> VspBuckets, int m, int k) {
		Iterator<List<Convoy>> buckets = VspBuckets.iterator();
		List<Convoy> Vnext = new ArrayList<>();
		List<Convoy> V = new ArrayList<>();
		if (buckets.hasNext()) {
			V = buckets.next();
		} else {
			return V;
		}
		List<Convoy> Vpcc = new ArrayList<>();
		long currentTime=0;
		long previousTime=V.get(0).getTe();
		while (buckets.hasNext()) {
			List<Convoy> C = buckets.next();
			Vnext=new ArrayList<>();
			currentTime=C.get(0).getTs();
			if(currentTime!=previousTime){
				//not mergeable
				for (Convoy v :
						V) {
					if (v.lifetime() >= k/2) {
						Vpcc = StrictConvoysUtil.updateVnext(Vpcc, v);
					}
				}
				previousTime = C.get(0).getTe();
				V = C;
				continue;

			}
			for(Convoy c : C){
				c.setMatched(false);c.setAbsorbed(false);
			}
			for(Convoy v : V){
				v.setExtended(false);v.setAbsorbed(false);
				for(Convoy c : C){
					if(c.size()>=m && v.simpleIntersection(c,m) != null){ //extend (v,c)
						v.setExtended(true);c.setMatched(true);
						Convoy vext = v.simpleIntersection(c,m);
						Vnext = StrictConvoysUtil.updateVnext(Vnext, vext);
						if(v.isSubsetInObjs(c)){v.setAbsorbed(true);}
						if(c.isSubsetInObjs(v)){c.setAbsorbed(true);}
					}
				}
				if(!v.isAbsorbed()){
					if(v.lifetime() >= k/2){
						Vpcc.add(v);
					}
				}
			}
			for(Convoy c : C){
				if(!c.isAbsorbed()){
					Vnext=StrictConvoysUtil.updateVnext(Vnext, c);
				}
			}
			V=Vnext;
			previousTime=C.get(0).getTe();
		}
		for(Convoy v:V){
			if(v.lifetime()>=k/2){
				Vpcc.add(v);
			}
		}

		//Print the Vsp convoys
//		for (Convoy v :
//				Vpcc) {
//			System.out.println(v);
//		}
		return Vpcc;
	}

//	returns Tuple3<>(V_m,Vl_unmerged,Vr_unmerged)
	public static Tuple3<List<Convoy>,List<Convoy>,List<Convoy>> merge(List<Convoy> V_l, List<Convoy> V_r, int m, int k) {
		if (V_l == null || V_l.size()==0 ){
			return new Tuple3<>(null, null, V_r);
		}else if(V_r == null || V_r.size()==0 ){
			return new Tuple3<>(null, V_l, null);
		}
		List<Convoy> V_m = new ArrayList<>(); //result of merging
		List<Convoy> Vl_unmerged = new ArrayList<>(); //result of merging
		List<Convoy> Vr_unmerged = new ArrayList<>(); //result of merging




		V_m=new ArrayList<>();

		for(Convoy c : V_r){
			c.setMatched(false);c.setAbsorbed(false);
		}
		for(Convoy v : V_l){
			v.setExtended(false);v.setAbsorbed(false);
			for(Convoy c : V_r){
				if(c.size()>=m && v.simpleIntersection(c,m) != null){ //extend (v,c)
					v.setExtended(true);c.setMatched(true);
					Convoy vext = v.simpleIntersection(c,m);
					V_m = StrictConvoysUtil.updateVnext(V_m, vext);
					if(v.isSubsetInObjs(c)){v.setAbsorbed(true);}
					if(c.isSubsetInObjs(v)){c.setAbsorbed(true);}
				}
			}
			if(!v.isAbsorbed()){
				if(v.lifetime() >= k/2){
					Vl_unmerged.add(v);
				}
			}
		}
		for(Convoy c : V_r){
			if(!c.isAbsorbed()){
				Vr_unmerged.add(c);
			}
		}

		return new Tuple3<>(V_m,Vl_unmerged,Vr_unmerged);
	}

	public static List<Convoy> extendRight(List<Convoy> Vspext, K2SeqDataManager dataManager, ClusteringUtils clusteringUtils) {
		List<Convoy> Vr = new ArrayList<>();
		for (Convoy v :
				Vspext) {
			//extend v to the right
			Vr = ExtendRightVspext(v, dataManager, clusteringUtils, Vr);
		}
		return Vr;
	}

	public static List<Convoy> streamingExtendRightSet(List<Convoy> V, Map<Long, Map<Long, ClusterablePoint>> k2Batch, ClusteringUtils clusteringUtils, int k) {
		if (V==null || V.size()==0)
			return new ArrayList<>();
		List<Convoy> rExtended = new ArrayList<>();
		List<Convoy> Vr = new ArrayList<>();
		for (Convoy v :
				V) {
			//extend v to the right
			rExtended = streamingExtendRight(v, k2Batch, clusteringUtils, k);
			if (rExtended!=null && rExtended.size()>0)
				Vr.addAll(rExtended);
		}
		return Vr;
	}

	public static List<Convoy> extendRightOptim(List<Convoy> Vspext, K2SeqDataManager dataManager, ClusteringUtils clusteringUtils) {
		List<Convoy> Vr = new ArrayList<>();
		for (Convoy v :
				Vspext) {
			//extend v to the right
			Vr = ExtendRightVspext(v, dataManager, clusteringUtils, Vr);
		}
		return Vr;
	}

	private static List<Convoy> ExtendRightVspext(Convoy inputConvoy, K2SeqDataManager dataManager, ClusteringUtils clusteringUtils, List<Convoy> Vlo) {
		//extend to the right
		if (!dataManager.hasTimeStamp(inputConvoy.getTe()+1)) {
			//end convoy, no extension possible
			Vlo = StrictConvoysUtil.updateVnext(Vlo, inputConvoy);
			return Vlo;
		}
		List<Convoy> Vnext = null;
		List<Convoy> V = new ArrayList<>();
		V.add(inputConvoy);
		long b2 = inputConvoy.getTe()+(dataManager.getK()/2);
//		List<Convoy> Vlo = new ArrayList<>(); //leftopen
		List<Convoy> C = null;
		for(long t = inputConvoy.getTe() + 1; V.size()>0 && t<=b2 && dataManager.hasTimeStamp(t); t++){ //maxTime;t++){
			Vnext = new ArrayList<>();
			for (Convoy v :
					V) {
				List<Convoy> Vnew = clusteringUtils.reCluster(t, v, dataManager.getData());
				if (Vnew == null || Vnew.size() == 0) {
					//convoy ends. Add it to the result
					Vlo = StrictConvoysUtil.updateVnext(Vlo, v);
				} else {
					for (Convoy vnew :
							Vnew) {
						vnew.setTs(v.getTs());
					}
					Vnext.addAll(Vnew);
					if ((Vnew.size() == 1 && Vnew.get(0).size() < v.size())
							|| Vnew.size()>1) {
						// either the size of v after reclustering remains the same or it decreases
						//if it remains the same, the same convoy got extended
						//but if the size reduces, v no longer gets extended so we should add it to the result
						//also if the convoy gets split i.e. Vnew.size>1, we add the input conovy to the result
						Vlo = StrictConvoysUtil.updateVnext(Vlo, v);
					}
				}
			}
			V=Vnext;
//			System.out.println("ts = "+t+", |V| = "+V.size()+", |Vpcc| = "+Vpcc.size());
		}
		return Vlo;
	}

	private static List<Convoy> streamingExtendRight(Convoy inputConvoy, Map<Long, Map<Long, ClusterablePoint>> k2Batch, ClusteringUtils clusteringUtils, int k) {
		//extend to the right
		List<Convoy> V_out = new ArrayList<>();
		if (k2Batch==null || !k2Batch.containsKey(inputConvoy.getTe()+1)) {
			//end convoy, no extension possible
			V_out = StrictConvoysUtil.updateVnext(V_out, inputConvoy);
			return V_out;
		}
		List<Convoy> Vnext = null;
		List<Convoy> V = new ArrayList<>();
		V.add(inputConvoy);
		long b2 = Collections.max(k2Batch.keySet());
		List<Convoy> C = null;
		for(long t = inputConvoy.getTe() + 1; V.size()>0 && t<=b2 && k2Batch.keySet().contains(t); t++){
			Vnext = new ArrayList<>();
			for (Convoy v :
					V) {
				List<Convoy> Vnew = clusteringUtils.reCluster(t, v, k2Batch);
				if (Vnew == null || Vnew.size() == 0) {
					//convoy ends. Add it to the result
					if (v.lifetime()>=k)
						V_out = StrictConvoysUtil.updateVnext(V_out, v);
				} else {
					for (Convoy vnew :
							Vnew) {
						vnew.setTs(v.getTs());
					}
					Vnext.addAll(Vnew);
					if ((Vnew.size() == 1 && Vnew.get(0).size() < v.size())
							|| Vnew.size()>1) {
						// either the size of v after reclustering remains the same or it decreases
						//if it remains the same, the same convoy got extended
						//but if the size reduces, v no longer gets extended so we should add it to the result
						//also if the convoy gets split i.e. Vnew.size>1, we add the input conovy to the result
						if (v.lifetime()>k)
							V_out = StrictConvoysUtil.updateVnext(V_out, v);
					}
				}
			}
			V=Vnext;
//			System.out.println("ts = "+t+", |V| = "+V.size()+", |Vpcc| = "+Vpcc.size());
		}
		for (Convoy v:V) {
			if (v.lifetime()>k)
				V_out = StrictConvoysUtil.updateVnext(V_out, v);
		}
		return V_out;
	}

	public static List<Convoy> getClusters(Long t, List<Convoy> inputConvoys, ClusteringUtils clusteringUtils, K2SeqDataManager dataManager) throws IOException, InterruptedException {
		Map<Long, ClusterablePoint> data = dataManager.getData(t, inputConvoys);
		if (data == null) {
			return null;
		}
		return clusteringUtils.clusterTimeData(t, data);
	}

	public static List<Convoy> extendRightBatch(List<Convoy> Vspext, int m, int k, ClusteringUtils clusteringUtils, K2SeqDataManager dataManager) throws IOException, InterruptedException {
		if (Vspext == null || Vspext.size() == 0) {
			return new ArrayList<>();
		}
		TreeMap<Long, List<Convoy>> timeConvoyMap = new TreeMap<>();
		for (Convoy v :
				Vspext) {
			if (timeConvoyMap.containsKey(v.getTe())) {
				timeConvoyMap.get(v.getTe()).add(v);
			} else {
				timeConvoyMap.put(v.getTe(), new ArrayList<Convoy>());
				timeConvoyMap.get(v.getTe()).add(v);
			}
		}
		Long currentTime = timeConvoyMap.firstKey();
		Long nextBenchmarkPoint = currentTime+k/2;
		Long nextBenchmarkTimeInVspExt = timeConvoyMap.higherKey(currentTime);


		List<Convoy> Vnext = null;
		List<Convoy> V = timeConvoyMap.get(currentTime);
		List<Convoy> C = null;
		List<Convoy> Vresult = new ArrayList<>();
		while (true) {
			C=getClusters(++currentTime,V,clusteringUtils,dataManager);
			Vnext = new ArrayList<>();
			if (C==null || C.size()==0){
				//the current convoys in V cant get more extended so add them to the result
				for(Convoy v:V){
					if (v.lifetime()>=k/2) {
						StrictConvoysUtil.updateStrictVnext(Vresult, v);
					}
				}
				//jump to the next group of spanning convoy to start extending them to the right
				if (nextBenchmarkTimeInVspExt != null) {
					currentTime = nextBenchmarkTimeInVspExt;
					nextBenchmarkPoint = currentTime + (k / 2);
					Vnext.addAll(timeConvoyMap.get(currentTime));
					nextBenchmarkTimeInVspExt = timeConvoyMap.higherKey(currentTime);
					V=Vnext;
					continue;
				} else {
					//there are no more Vsps to be extended to the right so exit
					break;
				}
			}
			//now we start the normal extension process based on PCCD

			for(Convoy c : C){
				c.setMatched(false);c.setAbsorbed(false);
//				System.out.println(c);
			}

			for(Convoy v : V){
				v.setExtended(false);v.setAbsorbed(false);
				for(Convoy c : C){
					Convoy vext = v.simpleIntersection(c, m);
					if(vext!= null){ //extend (v,c)
						v.setExtended(true);c.setMatched(true);
						vext.setTs(v.getTs());vext.setTe(currentTime);
						Vnext = StrictConvoysUtil.updateVnext(Vnext, vext);
						if(v.isSubsetInObjs(c)){v.setAbsorbed(true);}
						if(c.isSubsetInObjs(v)){c.setAbsorbed(true);}
					}
				}
				if(!v.isAbsorbed() && v.lifetime()>=k/2){
					StrictConvoysUtil.updateStrictVnext(Vresult, v);
				}
			}
//			for(Convoy c : C){
//				if(!c.isAbsorbed()){
//					Vnext=StrictConvoysUtil.updateVnext(Vnext, c);
//				}
//			}

			//if moving from left to right, the current convoys get extended to the right such that
			//we reach the nextBenchmarkTimeInVspExt, get the convoys corresponding to the next time (benchmark point)
			if (currentTime.longValue() == nextBenchmarkPoint.longValue()) {
				if (nextBenchmarkTimeInVspExt!=null){
					Vnext = new ArrayList<>();
					Vnext.addAll(timeConvoyMap.get(nextBenchmarkTimeInVspExt));
					currentTime = nextBenchmarkTimeInVspExt;
					nextBenchmarkPoint = currentTime + k / 2;
					nextBenchmarkTimeInVspExt = timeConvoyMap.higherKey(nextBenchmarkTimeInVspExt);
					V = Vnext;
					for (Convoy v :
							V) {
						if (!v.isExtended() && v.lifetime()>=k/2) {
							Vresult = StrictConvoysUtil.updateVnext(Vresult, v);
						}
					}
					continue;
				}else
					break;
			}
			//The above code ensures that as we scan from left to right, when ever we reach a benchmark point at which
			//there are some Vsps which end, we add those Vsps to the extension process. If at any point, no clusters exist
			//which means that the current convoys in V cant be extended any more, we jump to the next benchmark point having
			//ending convoys
			V=Vnext;
		}
		for(Convoy v:V){
			if (v.lifetime()>=k/2) {
				StrictConvoysUtil.updateStrictVnext(Vresult, v);
			}
		}

		return Vresult;
	}


	public static List<Convoy> extendLeftBatch(List<Convoy> Vspext, int m, int k, ClusteringUtils clusteringUtils, K2SeqDataManager dataManager) throws IOException, InterruptedException {
		if (Vspext == null || Vspext.size() == 0) {
			return new ArrayList<>();
		}
		TreeMap<Long, List<Convoy>> timeConvoyMap = new TreeMap<>();
		for (Convoy v :
				Vspext) {
			if (timeConvoyMap.containsKey(v.getTs())) {
				timeConvoyMap.get(v.getTs()).add(v);
			} else {
				timeConvoyMap.put(v.getTs(), new ArrayList<Convoy>());
				timeConvoyMap.get(v.getTs()).add(v);
			}
		}
		Long currentTime = timeConvoyMap.lastKey();
		Long nextBenchmarkPoint = currentTime-k/2;
		Long nextBenchmarkTimeInVspExt = timeConvoyMap.lowerKey(currentTime);


		List<Convoy> Vnext = null;
		List<Convoy> V = timeConvoyMap.get(currentTime);
		List<Convoy> C = null;
		List<Convoy> Vresult = new ArrayList<>();
		while (true) {
			C=getClusters(--currentTime,V,clusteringUtils,dataManager);
			Vnext = new ArrayList<>();
			if (C==null || C.size()==0){
				//the current convoys in V cant get more extended so add them to the result
				for(Convoy v:V){
					if (v.lifetime()>=k) {
						StrictConvoysUtil.updateStrictVnext(Vresult, v);
					}
				}
				//jump to the next group of spanning convoy to start extending them to the right
				if (nextBenchmarkTimeInVspExt != null) {
					currentTime = nextBenchmarkTimeInVspExt;
					nextBenchmarkPoint = currentTime - (k / 2);
					Vnext.addAll(timeConvoyMap.get(currentTime));
					nextBenchmarkTimeInVspExt = timeConvoyMap.lowerKey(currentTime);
					V=Vnext;
					continue;
				} else {
					//there are no more Vsps to be extended to the right so exit
					break;
				}
			}
			//now we start the normal extension process based on PCCD

			for(Convoy v : V){
				v.setExtended(false);v.setAbsorbed(false);
				for(Convoy c : C){
					Convoy vext = v.simpleIntersection(c, m);
					if(vext!= null){ //extend (v,c)
						v.setExtended(true);c.setMatched(true);
						vext.setTs(currentTime);vext.setTe(v.getTe());
						Vnext = StrictConvoysUtil.updateVnext(Vnext, vext);
						if(v.isSubsetInObjs(c)){v.setAbsorbed(true);}
						if(c.isSubsetInObjs(v)){c.setAbsorbed(true);}
					}
				}
				if(!v.isAbsorbed() && v.lifetime()>=k/2){
					StrictConvoysUtil.updateStrictVnext(Vresult, v);
				}
			}

			//if moving from left to right, the current convoys get extended to the right such that
			//we reach the nextBenchmarkTimeInVspExt, get the convoys corresponding to the next time (benchmark point)
			if (currentTime.longValue() == nextBenchmarkPoint.longValue()) {
				if (nextBenchmarkTimeInVspExt!=null){
					Vnext = new ArrayList<>();
					Vnext.addAll(timeConvoyMap.get(nextBenchmarkTimeInVspExt));
					currentTime = nextBenchmarkTimeInVspExt;
					nextBenchmarkPoint = currentTime - k / 2;
					nextBenchmarkTimeInVspExt = timeConvoyMap.lowerKey(nextBenchmarkTimeInVspExt);
					V = Vnext;
					for (Convoy v :
							V) {
						if (!v.isExtended() && v.lifetime()>=k) {
							Vresult = StrictConvoysUtil.updateVnext(Vresult, v);
						}
					}
					continue;
				}else
					break;
			}
			//The above code ensures that as we scan from left to right, when ever we reach a benchmark point at which
			//there are some Vsps which end, we add those Vsps to the extension process. If at any point, no clusters exist
			//which means that the current convoys in V cant be extended any more, we jump to the next benchmark point having
			//ending convoys
			V=Vnext;
		}
		for(Convoy v:V){
			if (v.lifetime()>=k) {
				StrictConvoysUtil.updateStrictVnext(Vresult, v);
			}
		}


//		List<Convoy> Vr = new ArrayList<>();
//		for (Convoy v :
//				Vspext) {
//			//extend v to the right
//			Vr = ExtendRightVspext(v, clusteringUtils, Vr, dataManager);
//		}
		return Vresult;
	}


	private static List<Convoy> ExtendRightVspextOptim(Convoy inputConvoy, K2SeqDataManager dataManager, ClusteringUtils clusteringUtils, List<Convoy> Vlo) {
		//extend to the right
		if (!dataManager.hasTimeStamp(inputConvoy.getTe()+1)) {
			//end convoy, no extension possible
			Vlo.add(inputConvoy);
			return Vlo;
		}
		List<Convoy> Vnext = null;
		List<Convoy> V = new ArrayList<>();
		V.add(inputConvoy);
//		List<Convoy> Vlo = new ArrayList<>(); //leftopen
		List<Convoy> C = null;
		for(long t = inputConvoy.getTe() + 1; V.size()>0 && dataManager.hasTimeStamp(t); t++){ //maxTime;t++){
			Vnext = new ArrayList<>();
			for (Convoy v :
					V) {
				List<Convoy> Vnew = clusteringUtils.reCluster(t, v, dataManager.getData());
				if (Vnew == null || Vnew.size() == 0) {
					//convoy ends. Add it to the result
					Vlo = StrictConvoysUtil.updateVnext(Vlo, v);
				} else {
					for (Convoy vnew :
							Vnew) {
						vnew.setTs(v.getTs());
					}
					Vnext.addAll(Vnew);
					if ((Vnew.size() == 1 && Vnew.get(0).size() < v.size())
							|| Vnew.size()>1) {
						// either the size of v after reclustering remains the same or it decreases
						//if it remains the same, the same convoy got extended
						//but if the size reduces, v no longer gets extended so we should add it to the result
						//also if the convoy gets split i.e. Vnew.size>1, we add the input conovy to the result
						Vlo = StrictConvoysUtil.updateVnext(Vlo, v);
					}
				}
			}
			V=Vnext;
//			System.out.println("ts = "+t+", |V| = "+V.size()+", |Vpcc| = "+Vpcc.size());
		}
		return Vlo;
	}

	public static List<Convoy> extendLeft(List<Convoy> Vspext, K2SeqDataManager dataManager, ClusteringUtils clusteringUtils,int k) {
		List<Convoy> Vl = new ArrayList<>();
		for (Convoy v :
				Vspext) {
			//extend v to the right
			Vl = ExtendLeftVspext(v, dataManager, clusteringUtils, k, Vl);
		}
		return Vl;
	}

	public static List<Convoy> streamingExtendLeftSet(List<Convoy> V, Map<Long, Map<Long, ClusterablePoint>> k2Batch, ClusteringUtils clusteringUtils,int k) {
		List<Convoy> Vs = new ArrayList<>();
		if (V==null || V.size()==0) {
			return Vs;
		}
		for (Convoy v :
				V) {
			//extend v to the right
			Vs = streamingExtendLeft(v, k2Batch, clusteringUtils, k);
		}
		return Vs;
	}

	private static List<Convoy> streamingExtendLeft(Convoy inputConvoy, Map<Long, Map<Long, ClusterablePoint>> k2Batch, ClusteringUtils clusteringUtils, int k) {
		//extend to the left
		List<Convoy> V_out = new ArrayList<>();
		if (k2Batch==null || !k2Batch.containsKey(inputConvoy.getTs()-1)) {
			//end convoy, no extension possible
			V_out = StrictConvoysUtil.updateVnext(V_out, inputConvoy);
			return V_out;
		}
		List<Convoy> Vnext = null;
		List<Convoy> V = new ArrayList<>();
		V.add(inputConvoy);
		long b1 = Collections.min(k2Batch.keySet());
		List<Convoy> C = null;
		for(long t = inputConvoy.getTs() - 1; V.size()>0 && t>=b1 && k2Batch.keySet().contains(t); t--){ //maxTime;t++){
			Vnext = new ArrayList<>();
			for (Convoy v :
					V) {
				List<Convoy> Vnew = clusteringUtils.reCluster(t, v, k2Batch);
				if (Vnew == null || Vnew.size() == 0) {
					//convoy starts. Add it to the result
					V_out = StrictConvoysUtil.updateVnextToLeft(V_out, v);
				} else {
					for (Convoy vnew :
							Vnew) {
						vnew.setTe(v.getTe());
					}
					Vnext.addAll(Vnew);
					if ((Vnew.size() == 1 && Vnew.get(0).size() < v.size())
							|| Vnew.size()>1) {
						// either the size of v after reclustering remains the same or it decreases
						//if it remains the same, the same convoy got extended
						//but if the size reduces, v no longer gets extended so we should add it to the result
						//also if the convoy gets split i.e. Vnew.size>1, we add the input conovy to the result
						V_out = StrictConvoysUtil.updateVnextToLeft(V_out, v);
					}
				}
			}
			V=Vnext;
//			System.out.println("ts = "+t+", |V| = "+V.size()+", |Vpcc| = "+Vpcc.size());
		}
		for (Convoy v:V)
				V_out = StrictConvoysUtil.updateVnext(V_out, v);
		return V_out;
	}


	private static List<Convoy> ExtendLeftVspext(Convoy inputConvoy, K2SeqDataManager dataManager, ClusteringUtils clusteringUtils, int k, List<Convoy> Vr) {
		//extend to the right
		List<Convoy> Vnext = null;
		List<Convoy> V = new ArrayList<>();
		V.add(inputConvoy);
//		List<Convoy> Vr = new ArrayList<>(); //leftopen
		List<Convoy> C = null;
		long b1 = inputConvoy.getTs()-(dataManager.getK()/2);
		if (!dataManager.hasTimeStamp(inputConvoy.getTs() - 1) && inputConvoy.lifetime() >= k) {
			StrictConvoysUtil.updateVnextToLeft(Vr, inputConvoy);
			return Vr;
		}
		for(long t = inputConvoy.getTs() - 1; V.size()>0 && t>=b1 && dataManager.hasTimeStamp(t); t--){ //maxTime;t++){
			Vnext = new ArrayList<>();
			for (Convoy v :
					V) {
				List<Convoy> Vnew = clusteringUtils.reCluster(t, v, dataManager.getData());
				if (Vnew == null || Vnew.size() == 0) {
					//convoy ends. Add it to the result
					if (v.lifetime() >= k) {
						Vr = StrictConvoysUtil.updateVnextToLeft(Vr, v);
					}

				} else {
					for (Convoy vnew :
							Vnew) {
						vnew.setTe(v.getTe());
					}
					Vnext.addAll(Vnew);
					if ((Vnew.size() == 1 && Vnew.get(0).size() < v.size())
							|| Vnew.size()>1) {
						// either the size of v after reclustering remains the same or it decreases
						//if it remains the same, the same convoy got extended
						//but if the size reduces, v no longer gets extended so we should add it to the result
						//also if the convoy gets split i.e. Vnew.size>1, we add the input conovy to the result
						if (v.lifetime()>=k) {
							Vr = StrictConvoysUtil.updateVnextToLeft(Vr, v);
						}
					}
				}
			}
			V=Vnext;
//			System.out.println("ts = "+t+", |V| = "+V.size()+", |Vpcc| = "+Vpcc.size());
		}
		return Vr;
	}
}
