package dk.aau.convoy.utils;

import dk.aau.convoy.model.ClusterablePoint;
import hbase.ReadHBase;

import java.io.IOException;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by faisal on 9/17/17.
 */
public class ReadRDBMS extends ReadHBase {
	static String host="localhost";
	static String port="5432";
	static String table;
	static PreparedStatement queryByTime;
	static PreparedStatement queryByTimeOid;
	static PreparedStatement queryByTimeOidMultiple;
	static long oid;
	static long t;
	static double x;
	static double y;
	static Connection connection = null;

	public ReadRDBMS(String table){
		super();

	}

	public static void init(String tableName) throws IOException {
		table = tableName;
//		System.out.println("-------- PostgreSQL "
//				+ "JDBC Connection Testing ------------");

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Where is your PostgreSQL JDBC Driver? "
					+ "Include in your library path!");
			e.printStackTrace();
			return;
		}
//		System.out.println("PostgreSQL JDBC Driver Registered!");



		try {
			connection = DriverManager.getConnection(
					"jdbc:postgresql://"+host+":"+port+"/postgres","faisal", "1234");
		} catch (SQLException e) {
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;
		}
		if (connection != null) {
//			System.out.println("Connection to the database successful now!");
		} else {
			System.out.println("Failed to make connection!");
		}
		try {
			queryByTime = connection.prepareStatement("SELECT oid, x, y FROM public."+tableName+" rel WHERE rel.t=?");
			queryByTimeOid = connection.prepareStatement("SELECT oid, x, y FROM public."+tableName+" rel WHERE rel.t=? and rel.oid=?");
			queryByTimeOidMultiple = connection.prepareStatement("SELECT oid, x, y FROM public."+tableName+" rel WHERE rel.t=? and rel.oid = ANY (?)");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void close() throws IOException {
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) throws SQLException {
		System.out.println("-------- PostgreSQL "
				+ "JDBC Connection Testing ------------");

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Where is your PostgreSQL JDBC Driver? "
					+ "Include in your library path!");
			e.printStackTrace();
			return;
		}
		System.out.println("PostgreSQL JDBC Driver Registered!");



		try {
			connection = DriverManager.getConnection(
					"jdbc:postgresql://"+host+":"+port+"/postgres","faisal", "1234");
		} catch (SQLException e) {
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;
		}
		if (connection != null) {
			System.out.println("Connection to the database successful now!");
		} else {
			System.out.println("Failed to make connection!");
		}
		try {
			Map<Long,ClusterablePoint> tdata=getTuplesForTime(500);
			System.out.println(tdata.get(500).getPoint());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static Map<Long, ClusterablePoint> getTuplesForTime(long time) throws IOException {
		try {
			queryByTime.setLong(1,time);
			ResultSet rs = queryByTime.executeQuery();
			Map<Long, ClusterablePoint> mapClusterablePoint = new HashMap<>();
			while (rs.next()) {
				oid = rs.getInt(1);
//				System.out.print(oid);
//				System.out.print(" ");
				x = rs.getDouble(2);
//				System.out.print(x);
//				System.out.print(" ");
				y = rs.getDouble(3);
//				System.out.println(y);
				mapClusterablePoint.put(oid,new ClusterablePoint(oid,x,y));
			}
			if (mapClusterablePoint.size() == 0) {
				return null;
			} else {
				return mapClusterablePoint;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static ClusterablePoint get(long time, long oid) throws IOException {
		try {
			queryByTimeOid.setLong(1, time);
			queryByTimeOid.setLong(2, oid);
			ResultSet rs = queryByTimeOid.executeQuery();
			while (rs.next()) {
				oid = rs.getInt(1);
//				System.out.print(oid);
//				System.out.print(" ");
				x = rs.getDouble(2);
//				System.out.print(x);
//				System.out.print(" ");
				y = rs.getDouble(3);
//				System.out.println(y);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
//		System.out.println("get request for "+time+":"+oid);
		return new ClusterablePoint(oid,x,y);
	}

	public static Map<Long, ClusterablePoint> batchGet(long time, Set<Long> desiredObjs) throws IOException, InterruptedException {
		Map<Long, ClusterablePoint> map = new HashMap<>();
		if (desiredObjs == null || desiredObjs.size() == 0) {
			return map;
		}
		StringBuilder desiredOids = new StringBuilder("(");
//		final String[] data = desiredObjs.toArray(new String[desiredObjs.size()]);
		try {
			final Array sqlArray = connection.createArrayOf("integer", desiredObjs.toArray());

			//batching the get requests
			for (Long oid :
					desiredObjs) {
				desiredOids=desiredOids.append(+oid+",");
			}
//			System.out.println(desiredObjs.size() + " " + desiredOids);
			desiredOids.setCharAt(desiredOids.lastIndexOf(","),')');

//			System.out.println("t="+time);
			queryByTimeOidMultiple.setLong(1, time);
//			System.out.println("desired oids="+desiredOids);
			queryByTimeOidMultiple.setArray(2,sqlArray);
//			System.out.println(queryByTimeOidMultiple.toString());
			ResultSet rs = queryByTimeOidMultiple.executeQuery();
			while (rs.next()) {
				oid = rs.getInt(1);
//				System.out.print(oid);
//				System.out.print(" ");
				x = rs.getDouble(2);
//				System.out.print(x);
//				System.out.print(" ");
				y = rs.getDouble(3);
//				System.out.println(y);
				map.put(oid, new ClusterablePoint(oid, x, y));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return map;
	}
}
