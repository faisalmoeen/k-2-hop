package dk.aau.convoy.base.utils;

import dk.aau.convoy.clustering.ClusteringUtils;
import dk.aau.convoy.model.ClusterablePoint;
import dk.aau.convoy.model.Convoy;
import dk.aau.convoy.utils.K2SeqDataManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by faisal on 10/25/16.
 */
public class StrictConvoysUtil {


	public static List<Convoy> DCVal(Convoy convoy, double eps, int m, int k, K2SeqDataManager dataManager, ClusteringUtils clusteringUtils){
		List<Convoy> V = new ArrayList<>();
		List<Convoy> Vval = new ArrayList<Convoy>();
		List<Convoy> Vnext = null;
		for (long t = convoy.getTs(); t <= convoy.getTe(); t++) {
			Vnext = new ArrayList<>();
			List<Convoy> C = clusteringUtils.reCluster(t,convoy,dataManager.getData());
			if (C == null) {
				C = new ArrayList<>();
			}
			//initialize the clusters
			for (Convoy c:
					C) {
				c.setMatched(false);c.setAbsorbed(false);c.setTs(t);c.setTe(t);
			}
			//initialize the convoys
			for (Convoy v :
					V) {
				v.setExtended(false);v.setAbsorbed(false);
			}
			for (Convoy v :
					V) {
				for (Convoy c :
						C) {
					Convoy vext = v.simpleIntersection(c, m);
					if (vext!=null) { //means that vext.size()>=m
						//here the main algorithm starts
						vext.setValidated(false);vext.setTs(v.getTs());vext.setTe(t);
						if (v.hasSameObjs(c)) {
							vext.setValidated(true);Vnext = updateVnext(Vnext, vext);
							v.setExtended(true);v.setAbsorbed(true);
							c.setMatched(true); c.setAbsorbed(true);
						} else if (v.isSubsetInObjs(c)) {
							List<Convoy> Ct = clusteringUtils.reCluster(t, vext, dataManager.getData());
							if (Ct == null) {
								vext.setValidated(true);vext.setTe(t-1);
							} else if (Ct.size() == 1 && vext.hasSameObjs(Ct.get(0))) {
								vext.setValidated(true);
								Vnext = updateVnext(Vnext, vext);
								v.setExtended(true);
								v.setAbsorbed(true);
								c.setMatched(true);
							} else {
								vext = vext.simpleIntersection(Ct.get(0), m);
							}
						}
						if (vext!=null && !vext.isValidated()) {
//							List<Convoy> VC = DCVal(vext, eps, m, k, dataManager, clusteringUtils);
							List<Convoy> VC = DCVal(vext, eps, m, (k<(int) vext.lifetime()?k:(int) vext.lifetime()), dataManager, clusteringUtils);
//							List<Convoy> VC = DCVal(vext, eps, m, ((k-(convoy.getTe()-t))>0?k:(int) vext.lifetime()), dataManager, clusteringUtils);
							for (Convoy vc:
									VC) {
								v.setExtended(true);
								c.setMatched(true);
								if (vc.getTe() == t) {
									Vnext = updateVnext(Vnext, vc);
								} else {
									if (vc.lifetime() >= k) {
										Vval.add(vc);
									}
								}
							}
						}
					}
				}
				if (!v.isExtended() || !v.isAbsorbed()) {
					if (v.lifetime() >= k) {
						Vval.add(v);
					}
				}

			}
			for (Convoy c :
					C) {
				if (!c.isMatched() || !c.isAbsorbed()) {
					Vnext = updateVnext(Vnext, c);
				}
			}
			V=Vnext;
		}
		for (Convoy v :
				V) {
			if (v.lifetime() >= k || v.getTe()==convoy.getTe()) {
				Vval = updateVnext(Vval, v);
			}
		}
		return Vval;

	}

	public static List<Convoy> streamingDCVal(Convoy convoy, double eps, int m, int k, Map<Long, Map<Long, ClusterablePoint>> k2Batch, ClusteringUtils clusteringUtils){
		List<Convoy> V = new ArrayList<>();
		List<Convoy> Vval = new ArrayList<Convoy>();
		List<Convoy> Vnext = null;
		for (long t = convoy.getTs(); t <= convoy.getTe(); t++) {
			Vnext = new ArrayList<>();
			List<Convoy> C = clusteringUtils.reCluster(t,convoy,k2Batch);
			if (C == null) {
				C = new ArrayList<>();
			}
			//initialize the clusters
			for (Convoy c:
					C) {
				c.setMatched(false);c.setAbsorbed(false);c.setTs(t);c.setTe(t);
			}
			//initialize the convoys
			for (Convoy v :
					V) {
				v.setExtended(false);v.setAbsorbed(false);
			}
			for (Convoy v :
					V) {
				for (Convoy c :
						C) {
					Convoy vext = v.simpleIntersection(c, m);
					if (vext!=null) { //means that vext.size()>=m
						//here the main algorithm starts
						vext.setValidated(false);vext.setTs(v.getTs());vext.setTe(t);
						if (v.hasSameObjs(c)) {
							vext.setValidated(true);Vnext = updateVnext(Vnext, vext);
							v.setExtended(true);v.setAbsorbed(true);
							c.setMatched(true); c.setAbsorbed(true);
						} else if (v.isSubsetInObjs(c)) {
							List<Convoy> Ct = clusteringUtils.reCluster(t, vext, k2Batch);
							if (Ct == null) {
								vext.setValidated(true);vext.setTe(t-1);
							} else if (Ct.size() == 1 && vext.hasSameObjs(Ct.get(0))) {
								vext.setValidated(true);
								Vnext = updateVnext(Vnext, vext);
								v.setExtended(true);
								v.setAbsorbed(true);
								c.setMatched(true);
							} else {
								vext = vext.simpleIntersection(Ct.get(0), m);
							}
						}
						if (vext!=null && !vext.isValidated()) {
//							List<Convoy> VC = DCVal(vext, eps, m, k, dataManager, clusteringUtils);
							List<Convoy> VC = streamingDCVal(vext, eps, m, (k<(int) vext.lifetime()?k:(int) vext.lifetime()), k2Batch, clusteringUtils);
//							List<Convoy> VC = DCVal(vext, eps, m, ((k-(convoy.getTe()-t))>0?k:(int) vext.lifetime()), dataManager, clusteringUtils);
							for (Convoy vc:
									VC) {
								v.setExtended(true);
								c.setMatched(true);
								if (vc.getTe() == t) {
									Vnext = updateVnext(Vnext, vc);
								} else {
									if (vc.lifetime() >= k) {
										Vval.add(vc);
									}
								}
							}
						}
					}
				}
				if (!v.isExtended() || !v.isAbsorbed()) {
					if (v.lifetime() >= k) {
						Vval.add(v);
					}
				}

			}
			for (Convoy c :
					C) {
				if (!c.isMatched() || !c.isAbsorbed()) {
					Vnext = updateVnext(Vnext, c);
				}
			}
			V=Vnext;
		}
		for (Convoy v :
				V) {
			if (v.lifetime() >= k || v.getTe()==convoy.getTe()) {
				Vval = updateVnext(Vval, v);
			}
		}
		return Vval;

	}

	public static List<Convoy> updateVnext(List<Convoy> Vnext, Convoy vnew){
		boolean added=false;
		for(Convoy v : Vnext){
			if(v.hasSameObjs(vnew)){
				if(v.isSubsetInTime(vnew)){//v is a subconvoy of vnew
//					Vnext.remove(v);
//					Vnext.add(vnew);
					v.setTs(vnew.getTs());
					v.setTe(vnew.getTe());
					added=true;
					break;
				}
				else if(vnew.isSubsetInTime(v)){//vnew is a subconvoy of v *****different from vcoda
					added=true;
					break;
				}
			}
		}
		if(added==false){
			Vnext.add(vnew);
		}
		return Vnext;
	}

	public static List<Convoy> updateStrictVnext(List<Convoy> Vnext, Convoy vnew){
		boolean added=false;
		List<Convoy> toRemove = new ArrayList<>();
		for(Convoy v : Vnext){
			if(v.isSubset(vnew)){
				if (added){
					toRemove.add(v);
					continue;
				}
				v.setTs(vnew.getTs());
				v.setTe(vnew.getTe());
				v.setObjs(vnew.getObjs());
				added=true;
			}else if (vnew.isSubset(v)) {//vnew is a subconvoy of v *****different from vcoda
				added = true;
			}
		}
		for (Convoy v :
				toRemove) {
			Vnext.remove(v);
		}
		if(added==false){
			Vnext.add(vnew);
		}
		return Vnext;
	}

	public static List<Convoy> updateVnextToLeft(List<Convoy> Vnext, Convoy vnew){
		boolean added=false;
		for(Convoy v : Vnext){
			if(v.hasSameObjs(vnew)){
				if(v.isSubsetInTime(vnew)){//v is a subconvoy of vnew
//					Vnext.remove(v);
//					Vnext.add(vnew);
					v.setTs(vnew.getTs());
					v.setTe(vnew.getTe());
					added=true;
				}
				else if(vnew.getTe()<=v.getTe()){//vnew is a subconvoy of v *****different from vcoda
					added=true;
				}
			}
		}
		if(added==false){
			Vnext.add(vnew);
		}
		return Vnext;
	}

	public static List<Convoy> mineStrictConvoys(List<dk.aau.convoy.base.Convoy> Vpcc, double eps, int m, int k,
												 K2SeqDataManager dataManager, ClusteringUtils clusteringUtils){
		List<Convoy> strictConvoys = new ArrayList<>();
		for (dk.aau.convoy.base.Convoy v :
				Vpcc) {
			List<Convoy> results = StrictConvoysUtil.DCVal(v.getModelConvoy(), eps, m, k, dataManager, clusteringUtils);
			for (Convoy r :
					results) {
				if (r.lifetime() >= k) {
					strictConvoys = StrictConvoysUtil.updateStrictVnext(strictConvoys, r);
				}
			}
		}
		return strictConvoys;
	}

	public static List<Convoy> mineStrictModelConvoys(List<Convoy> Vpcc, double eps, int m, int k,
												 K2SeqDataManager dataManager, ClusteringUtils clusteringUtils){
		List<Convoy> strictConvoys = new ArrayList<>();
		for (Convoy v :
				Vpcc) {
			List<Convoy> results = StrictConvoysUtil.DCVal(v, eps, m, k, dataManager, clusteringUtils);
			for (Convoy r :
					results) {
				if (r.lifetime() >= k) {
					strictConvoys = StrictConvoysUtil.updateStrictVnext(strictConvoys, r);
				}
			}
		}
		return strictConvoys;
	}

	public static List<Convoy> streamingMineStrictModelConvoys(List<Convoy> Vpcc, double eps, int m, int k,
															   Map<Long, Map<Long, ClusterablePoint>> k2Batch, ClusteringUtils clusteringUtils){
		List<Convoy> strictConvoys = new ArrayList<>();
		for (Convoy v :
				Vpcc) {
			List<Convoy> results = StrictConvoysUtil.streamingDCVal(v, eps, m, k, k2Batch, clusteringUtils);
			for (Convoy r :
					results) {
				if (r.lifetime() >= k) {
					strictConvoys = StrictConvoysUtil.updateStrictVnext(strictConvoys, r);
				}
			}
		}
		return strictConvoys;
	}
}
