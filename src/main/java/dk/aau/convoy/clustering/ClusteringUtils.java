package dk.aau.convoy.clustering;

import com.google.common.collect.Sets;
import dk.aau.convoy.utils.ConvoyUtil;
import dk.aau.convoy.model.Convoy;
import dk.aau.convoy.model.ConvoyBuilder;
import dk.aau.convoy.model.ClusterablePoint;
import dk.aau.convoy.utils.K2SeqDataManager;
import hbase.ReadHBase;
import org.apache.commons.collections.ArrayStack;
import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.commons.math3.ml.clustering.DBSCANClusterer;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;

import java.io.IOException;
import java.util.*;

/**
 * Created by faisal on 8/29/16.
 */
public class ClusteringUtils {
	public ClusteringUtils(double eps, int m) {
		this.eps = eps;
		this.m = m;
		dbscanClusterer = new DBSCANClusterer(eps,m);
	}

	private double eps;

	public int getM() {
		return m;
	}

	private int m;
	private DBSCANClusterer<ClusterablePoint> dbscanClusterer;

	public List<Long> getBenchmarkPoints(Long ts, Long te, Long hopWindowWidth, Set<Long> processed){
		List<Long> benchmarkPoints = new ArrayList<>();
		for (long i=ts; i<=te; i+=hopWindowWidth){
			if (!processed.contains(i)) {
				benchmarkPoints.add(i);
			}
		}
		return benchmarkPoints;
	}
	public List<Long> deriveBenchmarkPoints(List<Long> benchmarkPoints){
		List<Long> newBenchmarkPoints = new ArrayList<>();
		for (int i=0; i<benchmarkPoints.size()-1; i+=1){
			long benchmarkPoint = (int)((benchmarkPoints.get(i+1)-benchmarkPoints.get(i))/2) + benchmarkPoints.get(i);
			if(benchmarkPoint!=benchmarkPoints.get(i) && benchmarkPoint!=benchmarkPoints.get(i+1)){
				newBenchmarkPoints.add(benchmarkPoint);
			}
		}
		return newBenchmarkPoints;
	}


	public List<Convoy> cluster(Long time, Map<Long, Map<Long, ClusterablePoint>> partitionData){
		Map<Long, ClusterablePoint> timeData = partitionData.get(time);
		List<Cluster<ClusterablePoint>> clustersList = dbscanClusterer.cluster(timeData.values()); //benchmark data is a Hashmap<oid,TupleClusterable>
		return convertToListOfConvoys(clustersList,time);
	}

	public List<Convoy> clusterTimeData(Long time, Map<Long, ClusterablePoint> timeData){
		List<Cluster<ClusterablePoint>> clustersList = dbscanClusterer.cluster(timeData.values()); //benchmark data is a Hashmap<oid,TupleClusterable>
		return convertToListOfConvoys(clustersList,time);
	}

	public List<Set<Long>> clusterNoTime(Long time, Map<Long, Map<Long, ClusterablePoint>> partitionData){
		Map<Long, ClusterablePoint> timeData = partitionData.get(time);
		List<Cluster<ClusterablePoint>> clustersList = dbscanClusterer.cluster(timeData.values()); //benchmark data is a Hashmap<oid,TupleClusterable>
		return clusterListToSetList(clustersList);
	}

	public List<Set<Long>> clusterHBase(List<ClusterablePoint> tuples){
		if (tuples == null || tuples.size() == 0) {
			return new ArrayList<>();
		}
		List<Cluster<ClusterablePoint>> clustersList = dbscanClusterer.cluster(tuples); //benchmark data is a Hashmap<oid,TupleClusterable>
		return clusterListToSetList(clustersList);
	}


	public ArrayList<Set<Long>> reClusterList(Long time, List<Set<Long>> inputClusters, Map<Long, Map<Long, ClusterablePoint>> partitionData){
		Map<Long, ClusterablePoint> timeData = partitionData.get(time);
		ArrayList<ClusterablePoint> inputData = null;
		List<Cluster<ClusterablePoint>> clustersList = new ArrayList<>();
		for (Set cluster:
			 inputClusters) {
			inputData = new ArrayList<>();
			for (Object obj:
				 cluster) {
				ClusterablePoint cp = (ClusterablePoint) timeData.get((long)obj);
				if (cp != null) {
					inputData.add(cp);
				}
			}
			clustersList.addAll(dbscanClusterer.cluster(inputData)); //benchmark data is a Hashmap<oid,TupleClusterable>
		}
		return clusterListToSetList(clustersList);
	}

	public ArrayList<Set<Long>> reClusterListHBase(Long time, List<Set<Long>> inputClusters, K2SeqDataManager dataManager) throws IOException, InterruptedException {
		Map<Long, ClusterablePoint> data = dataManager.getFromHBase1(time, inputClusters);
		ArrayList<ClusterablePoint> inputData = null;
		List<Cluster<ClusterablePoint>> clustersList = new ArrayList<>();
		for (Set cluster:
				inputClusters) {
			inputData = new ArrayList<>();
			for (Object obj:
					cluster) {
				ClusterablePoint cp = (ClusterablePoint) data.get((long)obj);
				if (cp != null) {
					inputData.add(cp);
				}
			}
			clustersList.addAll(dbscanClusterer.cluster(inputData)); //benchmark data is a Hashmap<oid,TupleClusterable>
		}
		return clusterListToSetList(clustersList);
	}

	public List<Convoy> reCluster(Long time, Set<Long> inputCluster, Map<Long, Map<Long, ClusterablePoint>> data){
		Map timeData = data.get(time);
		ArrayList<ClusterablePoint> inputData = new ArrayList<>();

		List<Cluster<ClusterablePoint>> clustersList = new ArrayList<>();
		for (Object obj:
				inputCluster) {
			inputData.add((ClusterablePoint) timeData.get((long)obj));
		}
		clustersList.addAll(dbscanClusterer.cluster(inputData)); //benchmark data is a Hashmap<oid,TupleClusterable>
		return clusterListToConvoyList(clustersList, time);
	}


	public List<Convoy> reCluster(Long time, Convoy inputCluster, Map<Long, Map<Long, ClusterablePoint>> partitionData){
		Map timeData = partitionData.get(time);
		ArrayList<ClusterablePoint> inputData = new ArrayList<>();

		List<Cluster<ClusterablePoint>> clustersList = new ArrayList<>();
		for (Object obj:
				inputCluster.getObjs()) {
			if (timeData.containsKey(obj)) {
				inputData.add((ClusterablePoint) timeData.get((long) obj));
			}
		}
		if (inputData.size()<m){
			return null;
		}
		clustersList.addAll(dbscanClusterer.cluster(inputData)); //benchmark data is a Hashmap<oid,TupleClusterable>
		return clusterListToConvoyList(clustersList, time);
	}

	public List<Convoy> reCluster(Long time, Convoy inputConvoy, K2SeqDataManager dataManager) throws IOException{
		ArrayList<ClusterablePoint> inputData = new ArrayList<>();

		for (Long oid:
				inputConvoy.getObjs()) {
			ClusterablePoint cp = dataManager.getData().get(time).get(oid);
			if (cp!=null) {
				inputData.add(cp);
			}
		}
		if (inputData.size()<m){
			return null;
		}
		List<Cluster<ClusterablePoint>> clusters = dbscanClusterer.cluster(inputData);
		if(clusters!=null && clusters.size()>0){
			List<Convoy> convoyList = clusterListToConvoyList(clusters, time);
			for (Convoy v:
			convoyList) {
				v.setTs(inputConvoy.getTs());
			}
			return convoyList;
		}
		return null;
	}

	public List<Convoy> reClusterLeft(Long time, Convoy inputConvoy, K2SeqDataManager dataManager) throws IOException{
		ArrayList<ClusterablePoint> inputData = new ArrayList<>();

		for (Long oid:
				inputConvoy.getObjs()) {
			ClusterablePoint cp = dataManager.getData().get(time).get(oid);
			if (cp!=null) {
				inputData.add(cp);
			}
		}
		if (inputData.size()<m){
			return null;
		}
		List<Cluster<ClusterablePoint>> clusters = dbscanClusterer.cluster(inputData);
		if(clusters!=null && clusters.size()>0){
			List<Convoy> convoyList = clusterListToConvoyList(clusters, time);
			for (Convoy v:
					convoyList) {
				v.setTs(time);
				v.setTe(inputConvoy.getTe());
			}
			return convoyList;
		}
		return null;
	}

	public List<Set<Long>> reClusterAndMatch(List<Long> benchmarkPoints, List<Set<Long>> inputClusters, Map<Long, Map<Long, ClusterablePoint>> partitionData){
		//recluster in a scanning fashion so that we don't need a separate match
		for (Long time:
			 benchmarkPoints) {
			inputClusters = reClusterList(time,inputClusters, partitionData);
		}
		return inputClusters;
	}

	public List<Set<Long>> reClusterAndMatchHBase(List<Long> benchmarkPoints, List<Set<Long>> inputClusters, K2SeqDataManager dataManager) throws IOException, InterruptedException {
		//recluster in a scanning fashion so that we don't need a separate match
		for (Long time:
				benchmarkPoints) {
			inputClusters = reClusterListHBase(time,inputClusters, dataManager);
		}
		return inputClusters;
	}



	public List<Set<Long>> runCCspHWMT(Long ts, Long te, Set<Long> processedTimeStamps, Long level, List<Set<Long>> inputClusters, Map<Long, Map<Long, ClusterablePoint>> partitionData){
		int hopWindowWidth = 0;
		List<Long> inputBenchmarkPoints = new ArrayList<>();
		inputBenchmarkPoints.add(ts);
		inputBenchmarkPoints.add(te);
		while (true) {
			List<Long> newBenchmarkPoints = null;
			hopWindowWidth = (int) Math.ceil((te-ts)/Math.pow(2, level));
			level++;
//			benchmarkPoints = getBenchmarkPoints(ts, te, new Long((long) hopWindowWidth), processedTimeStamps);
			newBenchmarkPoints = deriveBenchmarkPoints(inputBenchmarkPoints);
//			what if no more benchmarkPoints
			if (newBenchmarkPoints != null && newBenchmarkPoints.size() != 0) {
				inputClusters = reClusterAndMatch(newBenchmarkPoints, inputClusters, partitionData);
				inputBenchmarkPoints.addAll(newBenchmarkPoints);
				Collections.sort(inputBenchmarkPoints);
			} else {
				return inputClusters;
			}
		}
	}

	// returns Vsp for a partition and clustersTe which will be passed to the next partition as clustersTs
	public Tuple3<ArrayList<Convoy>, Long, List<Set<Long>>> runK2SeqSpHWMT(Long ts, List<Set<Long>> clustersTs, Map<Long, Map<Long, ClusterablePoint>> partitionData){
		List<Long> timeList = new ArrayList<>(partitionData.keySet());
		timeList.sort(new Comparator<Long>() {
			@Override
			public int compare(Long o1, Long o2) {
				return Long.compare(o1, o2);
			}
		});
//		System.out.println("ts="+ts);
		Long te = timeList.get(timeList.size()-1);
		List<Set<Long>> clustersTe = clusterNoTime(te, partitionData);
		if (clustersTs==null || clustersTs.size()==0){
			return Tuple3.of(new ArrayList<Convoy>(), te, clustersTe);
		}
		List<Set<Long>> inputClusters = ConvoyUtil.matchClusterLists(clustersTs, clustersTe, m);
		List<Long> inputBenchmarkPoints = new ArrayList<>();
		inputBenchmarkPoints.add(ts);
		inputBenchmarkPoints.add(te);
		while (true) {
			List<Long> newBenchmarkPoints = null;
//			benchmarkPoints = getBenchmarkPoints(ts, te, new Long((long) hopWindowWidth), processedTimeStamps);
			newBenchmarkPoints = deriveBenchmarkPoints(inputBenchmarkPoints);
//			what if no more benchmarkPoints
			if (newBenchmarkPoints != null && newBenchmarkPoints.size() != 0) {
				inputClusters = reClusterAndMatch(newBenchmarkPoints, inputClusters, partitionData);
				inputBenchmarkPoints.addAll(newBenchmarkPoints);
				Collections.sort(inputBenchmarkPoints);
			} else {
				return Tuple3.of(convertToListOfConvoys(inputClusters, ts, te), te, clustersTe);
			}
		}
	}

	public List<Convoy> runK2HBaseSpHWMT(Long ts, Long te, List<Set<Long>> clustersTs, List<Set<Long>> clustersTe, K2SeqDataManager dataManager) throws IOException, InterruptedException {
//		List<Long> timeList = new ArrayList<>(partitionData.keySet());
//		timeList.sort(new Comparator<Long>() {
//			@Override
//			public int compare(Long o1, Long o2) {
//				return Long.compare(o1, o2);
//			}
//		});
//		System.out.println("ts="+ts);
		if (clustersTs==null || clustersTs.size()==0){
			return new ArrayList<Convoy>();
		}
		List<Set<Long>> inputClusters = ConvoyUtil.matchClusterLists(clustersTs, clustersTe, m);
		List<Long> inputBenchmarkPoints = new ArrayList<>();
		inputBenchmarkPoints.add(ts);
		inputBenchmarkPoints.add(te);
		while (true) {
			List<Long> newBenchmarkPoints = null;
//			benchmarkPoints = getBenchmarkPoints(ts, te, new Long((long) hopWindowWidth), processedTimeStamps);
			newBenchmarkPoints = deriveBenchmarkPoints(inputBenchmarkPoints);
//			what if no more benchmarkPoints
			if (newBenchmarkPoints != null && newBenchmarkPoints.size() != 0) {
				inputClusters = reClusterAndMatchHBase(newBenchmarkPoints, inputClusters,dataManager);
				inputBenchmarkPoints.addAll(newBenchmarkPoints);
				Collections.sort(inputBenchmarkPoints);
			} else {
				return convertToListOfConvoys(inputClusters, ts, te);
			}
		}
	}


	public ArrayList<Set<Long>> match(ArrayList<Set<Long>> clusterList1, ArrayList<Set<Long>> clusterList2){
		ArrayList<Set<Long>> matchedClusterList = new ArrayList<>();
		for (Set<Long> cc1: clusterList1) {
			for (Set<Long> cc2: clusterList2) {
				Sets.SetView<Long> intersection = Sets.intersection(cc1,cc2);
				if(intersection.size()>m){
					matchedClusterList.add(intersection.copyInto(new HashSet<Long>()));
				}
			}
		}
		return matchedClusterList;
	}

	public static HashSet<Long> match(HashSet<Long> cluster1, HashSet<Long> cluster2, int m){
		Sets.SetView<Long> intersection = Sets.intersection(cluster1,cluster2);
		if(intersection.size()>m){
			HashSet<Long> result = new HashSet<>();
			intersection.copyInto(result);
			return result;
		}
		return null;
	}

	public static ArrayList<Set<Long>> match(ArrayList<Set<Long>> clusterList1, ArrayList<Set<Long>> clusterList2, int m){
		ArrayList<Set<Long>> matchedClusterList = new ArrayList<>();
		for (Set<Long> cc1: clusterList1) {
			for (Set<Long> cc2: clusterList2) {
				Sets.SetView<Long> intersection = Sets.intersection(cc1,cc2);
				if(intersection.size()>m){
					matchedClusterList.add(intersection.copyInto(new HashSet<Long>()));
				}
			}
		}
		return matchedClusterList;
	}

	public static ArrayList<Set<Long>> clusterListToSetList(List<Cluster<ClusterablePoint>> clustersList){
		ArrayList<Set<Long>> listOfSets = new ArrayList<>();
		for (Cluster c: clustersList) {
			Set<Long> clusteredOids = new HashSet<>();
			for (ClusterablePoint tuple: (List<ClusterablePoint>)c.getPoints()) {
				clusteredOids.add(tuple.oid);
			}
			listOfSets.add(clusteredOids);
		}
		return listOfSets;
	}

	public static List<Convoy> clusterListToConvoyList(List<Cluster<ClusterablePoint>> clustersList, long t){
		if (clustersList==null || clustersList.size()==0){
			return null;
		}
		ArrayList<Convoy> listOfConvoys = new ArrayList<>();
		for (Cluster c: clustersList) {
			Set<Long> clusteredOids = new HashSet<>();
			listOfConvoys.add(new ConvoyBuilder()
					.setCluster(c.getPoints())
					.setTe(t)
					.setTs(t)
					.createConvoy());
		}
		return listOfConvoys;
	}

	public static Convoy clusterToConvoy(Cluster<ClusterablePoint> cluster, long ts,long te){
		if (cluster==null){
			return null;
		}
		return  new ConvoyBuilder()
					.setCluster(cluster.getPoints())
					.setTe(te)
					.setTs(ts)
					.createConvoy();
	}

	public static ArrayList<Convoy> convertToListOfConvoys(List<Cluster<ClusterablePoint>> clustersList, long t){
		ArrayList<Convoy> listOfConvoys = new ArrayList<>();
		for (Cluster c: clustersList) {
			Set<Long> clusteredOids = new HashSet<>();
			for (ClusterablePoint tuple: (List<ClusterablePoint>)c.getPoints()) {
				clusteredOids.add(tuple.oid);
			}
			listOfConvoys.add(new ConvoyBuilder()
					.setTs(t)
					.setTe(t)
					.setObjs(clusteredOids)
					.createConvoy());
		}
		return listOfConvoys;
	}
	public static ArrayList<Convoy> convertToListOfConvoys(List<Set<Long>> setList, long ts, long te){
		ArrayList<Convoy> listOfConvoys = new ArrayList<>();
		for (Set c: setList) {
			listOfConvoys.add(new ConvoyBuilder()
					.setTs(ts)
					.setTe(te)
					.setObjs(c)
					.createConvoy());
		}
		return listOfConvoys;
	}

	public Tuple2<List<Convoy>,List<Convoy>> reCluster(Long t, List<Convoy> inputConvoys, K2SeqDataManager dataManager) throws IOException {
		List<Convoy> extendedConvoys = new ArrayList<>(), nonExtendedConvoys = new ArrayList<>();
		for (Convoy v :
				inputConvoys) {
			List<Convoy> result = reCluster(t, v, dataManager);
			if (result == null) {
				nonExtendedConvoys.add(v);
			} else if (result.size()==1){
				if (result.get(0).size() == v.size()) {
					extendedConvoys.add(result.get(0));
				} else {
					extendedConvoys.add(result.get(0));
					nonExtendedConvoys.add(v);
				}
			}
		}
		return Tuple2.of(nonExtendedConvoys, extendedConvoys);
	}

	public Tuple2<List<Convoy>,List<Convoy>> reClusterLeft(Long t, List<Convoy> inputConvoys, K2SeqDataManager dataManager) throws IOException {
		List<Convoy> extendedConvoys = new ArrayList<>(), nonExtendedConvoys = new ArrayList<>();
		for (Convoy v :
				inputConvoys) {
			List<Convoy> result = reClusterLeft(t, v, dataManager);
			if (result == null) {
				nonExtendedConvoys.add(v);
			} else if (result.size()==1){
				if (result.get(0).size() == v.size()) {
					extendedConvoys.add(result.get(0));
				} else {
					extendedConvoys.add(result.get(0));
					nonExtendedConvoys.add(v);
				}
			}
		}
		return Tuple2.of(nonExtendedConvoys, extendedConvoys);
	}
}
