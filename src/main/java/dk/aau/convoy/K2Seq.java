package dk.aau.convoy;

import com.google.common.base.Stopwatch;
import dk.aau.convoy.base.Vcoda;
import dk.aau.convoy.base.VcodaNLogN;
import dk.aau.convoy.base.clustering.PointWrapper;
import dk.aau.convoy.base.utils.StrictConvoysUtil;
import dk.aau.convoy.base.utils.Utils;
import dk.aau.convoy.clustering.ClusteringUtils;
import dk.aau.convoy.model.k2ExecutionTimeStruct;
import dk.aau.convoy.model.ClusterablePoint;
import dk.aau.convoy.model.Convoy;
import dk.aau.convoy.model.ConvoyBuilder;
import dk.aau.convoy.utils.*;
import hbase.ReadHBase;
import org.apache.avro.generic.GenericData;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

/**
 * Created by faisal on 10/22/16.
 */
public class K2Seq {
//	private static String inputFilePath = "/media/faisal/Cold Storage/Cloud Storage/owncloud/PhD Work/working-folder/experiments/trucks_dataset/trucks273s.txt";
//	private static String inputFilePath = "/home/faisal/disk/ramdisk/trucks273s.txt";
//	private static String inputFilePath = "/home/faisal/disk/k2/datasets/brink-5v-1000t-1n.txt";
//	private static String inputFilePath = "/home/faisal/disk/k2/datasets/brink-1.5.txt";
// 	private static String inputFilePath = "/home/faisal/disk/k2/datasets/tdrive.txt";
	private static String inputFilePath = "/Users/faisal/git/k2/src/main/resources/trucks-d.txt";
//	private static String inputFilePath = "/media/faisal/Cold Storage/data/Birkinhoff/output/2.txt";
	private static int k = 90;
	private static int m = 3;
	private static double eps = 0.0006;
	private static int batch;
	public static K2SeqDataManager dataManager;
	public static ClusteringUtils clusteringUtils;


	public static void main(String[] args) throws IOException, InterruptedException {


		Stopwatch timer = Stopwatch.createStarted();
//		System.out.println("**********************K2-Strict Convoys*******************");
//		Tuple2<List<Convoy>,k2ExecutionTimeStruct> k2strictConvoys = k2Seq(inputFilePath, eps, m, k);
//		timer.stop();
//		Long k2ExecTime = timer.elapsed(TimeUnit.MILLISECONDS);
//		System.out.println("k2 Strict Convoys = " + k2strictConvoys.f0.size());
//		System.out.println("Execution Time = " + k2ExecTime);
//		Collections.sort(k2strictConvoys.f0, new Comparator<Convoy>() {
//			@Override
//			public int compare(Convoy o1, Convoy o2) {
//				if (o1.getTs() > o2.getTs() || (o1.getTs() == o2.getTs() && o1.getTe() > o2.getTe())) {
//					return 1;
//				} else if (o1.getTs() == o2.getTs() && o1.getTe() == o2.getTe()) {
//					return 0;
//				}
//				return -1;
//			}
//		});
//		for (Convoy v :
//				k2strictConvoys.f0) {
//			System.out.println(v);
//		}


		System.out.println("**********************K2-Streaming Convoys*******************");
		Tuple2<List<Convoy>,k2ExecutionTimeStruct> k2strictConvoys = k2SeqStreaming(inputFilePath, eps, m, k);
		timer.stop();
		Long k2ExecTime = timer.elapsed(TimeUnit.MILLISECONDS);
		System.out.println("k2 Strict Convoys = " + k2strictConvoys.f0.size());
		System.out.println("Execution Time = " + k2ExecTime);
		Collections.sort(k2strictConvoys.f0, new Comparator<Convoy>() {
			@Override
			public int compare(Convoy o1, Convoy o2) {
				if (o1.getTs() > o2.getTs() || (o1.getTs() == o2.getTs() && o1.getTe() > o2.getTe())) {
					return 1;
				} else if (o1.getTs() == o2.getTs() && o1.getTe() == o2.getTe()) {
					return 0;
				}
				return -1;
			}
		});
		for (Convoy v :
				k2strictConvoys.f0) {
			System.out.println(v);
		}

//		System.out.println("**********************K2hbase-Strict Convoys*******************");
//		timer.reset();
//		timer.start();
//		Tuple2<List<Convoy>,k2ExecutionTimeStruct> k2strictConvoys = k2SeqHBaseIndl("tdrive-d", eps, m, k);
//		Long k2ExecTime = timer.elapsed(TimeUnit.MILLISECONDS);
//		System.out.println("***hbase strict convoys***");
//		System.out.println("Strict Convoys = " + k2strictConvoys.f0.size());
//		System.out.println("Execution Time = " + k2ExecTime);
//		sortTe(k2strictConvoys.f0);
//		print(k2strictConvoys.f0);
/*


		System.out.println("**********************vcoda nlogn*******************");


		timer.reset();
		timer.start();
		Tuple2<List<Convoy>,k2ExecutionTimeStruct> vCodaNlogNStrictConvoys = K2Seq.vcodaNlogN(inputFilePath, eps, m, k);
		Long vcodaNlogNExecTime = timer.elapsed(TimeUnit.MILLISECONDS);
		System.out.println("vcodaNLogN Strict Convoys = " + vCodaNlogNStrictConvoys.f0.size());
		System.out.println("Execution Time = " + vcodaNlogNExecTime);


		System.out.println("**********************vcoda *******************");
		//***************Comparison****************
		timer.reset();
		timer.start();
		Tuple2<List<Convoy>,k2ExecutionTimeStruct> vCodaStrictConvoys = K2Seq.vcoda(inputFilePath, eps, m, k);
		Long vcodaExecTime = timer.elapsed(TimeUnit.MILLISECONDS);
//		for (Convoy v :
//				vCodaStrictConvoys.f0) {
//			System.out.println(v);
//		}


		System.out.println("vcoda Strict Convoys = " + vCodaStrictConvoys.f0.size());
		System.out.println("Execution Time = " + vcodaExecTime);


*/
	}


	public static Tuple2<List<Convoy>,k2ExecutionTimeStruct> k2Seq(String inputFilePath, double eps, int m, int k) {
		k2ExecutionTimeStruct timeStruct = new k2ExecutionTimeStruct();
		/**** Timer Start****/Stopwatch timer = Stopwatch.createStarted();
		dataManager = new K2SeqDataManager(inputFilePath, k);
		/**** Timer Stop****/timer.stop(); timeStruct.timeDataLoading = timer.elapsed(TimeUnit.MILLISECONDS);
		clusteringUtils = new ClusteringUtils(eps, m-1);
//		List<Convoy> Vlo = K2SeqUtils.findVsp(dataManager, clusteringUtils);
		/**** Timer Start****/timer.reset();timer.start();
		LinkedList<List<Convoy>> VspBuckets = K2SeqUtils.findVspBucketed(dataManager, clusteringUtils);
//		System.out.println("**k2 VspBuckets**");
//		System.out.println("VspBuckets size = " + VspBuckets.size());
//		for (List<Convoy> l :
//				VspBuckets) {
//			for (Convoy v :
//					l) {
//				System.out.println(v);
//			}
//		}
		/**** Timer Stop****/timer.stop(); timeStruct.timeHWMT = timer.elapsed(TimeUnit.MILLISECONDS);
//		//Iterate over buckets and merge with the next bucket
		/**** Timer Start****/timer.reset();timer.start();
		List<Convoy> Vspext = K2SeqUtils.mergeVspBuckets(VspBuckets, m, k);
//		System.out.println("**k2 Vsp merged**");
//		System.out.println("VspExtended size = " + Vspext.size());
		/**** Timer Stop****/timer.stop(); timeStruct.timeMergeVsps = timer.elapsed(TimeUnit.MILLISECONDS);
//		sortTs(Vspext);
//		print(Vspext);
		/**** Timer Start****/timer.reset();timer.start();
		List<Convoy> Vlo = K2SeqUtils.extendRight(Vspext, dataManager, clusteringUtils);

//		System.out.println("***hbase right extended ***");
//		System.out.println("Right extended size = " + Vlo.size());
//		sortTs(Vlo);
//		print(Vlo);
		/**** Timer Stop****/timer.stop(); timeStruct.timeExtendRight = timer.elapsed(TimeUnit.MILLISECONDS);
		/**** Timer Start****/timer.reset();timer.start();
		List<Convoy> Vrelaxed = K2SeqUtils.extendLeft(Vlo, dataManager, clusteringUtils, k);
		/**** Timer Stop****/timer.stop(); timeStruct.timeExtendLeft = timer.elapsed(TimeUnit.MILLISECONDS);
		/**** Timer Start****/timer.reset();timer.start();

//		System.out.println("***hbase relaxed ***");
//		sortTe(Vrelaxed);
//		print(Vrelaxed);


		System.out.println("Semi-Relaxed Convoys=" + Vrelaxed.size());
		timeStruct.numPreValConvoys = Integer.toUnsignedLong(Vrelaxed.size());
		List<Convoy> k2strictConvoys = StrictConvoysUtil.mineStrictModelConvoys(Vrelaxed, eps, m, k, dataManager, clusteringUtils);
		/**** Timer Stop****/timer.stop(); timeStruct.timeStrictValidation = timer.elapsed(TimeUnit.MILLISECONDS);
		timeStruct.numPostValConvoys = Integer.toUnsignedLong(k2strictConvoys.size());
		System.out.println("Strict Convoys=" + k2strictConvoys.size());
		List<Convoy> unique = new ArrayList<>();
		for (Convoy v :
				k2strictConvoys) {
			StrictConvoysUtil.updateStrictVnext(unique, v);
		}
		return Tuple2.of(unique,timeStruct);
	}

	public static Tuple2<List<Convoy>,k2ExecutionTimeStruct> k2SeqBatch(String inputFilePath, double eps, int m, int k) throws IOException, InterruptedException {
		k2ExecutionTimeStruct timeStruct = new k2ExecutionTimeStruct();
		/**** Timer Start****/Stopwatch timer = Stopwatch.createStarted();
		dataManager = new K2SeqDataManager(inputFilePath, k);
		/**** Timer Stop****/timer.stop(); timeStruct.timeDataLoading = timer.elapsed(TimeUnit.MILLISECONDS);
		clusteringUtils = new ClusteringUtils(eps, m-1);
//		List<Convoy> Vlo = K2SeqUtils.findVsp(dataManager, clusteringUtils);
		/**** Timer Start****/timer.reset();timer.start();
		LinkedList<List<Convoy>> VspBuckets = K2SeqUtils.findVspBucketed(dataManager, clusteringUtils);
//		System.out.println("**k2 VspBuckets**");
//		System.out.println("VspBuckets size = " + VspBuckets.size());
//		for (List<Convoy> l :
//				VspBuckets) {
//			for (Convoy v :
//					l) {
//				System.out.println(v);
//			}
//		}
		/**** Timer Stop****/timer.stop(); timeStruct.timeHWMT = timer.elapsed(TimeUnit.MILLISECONDS);
//		//Iterate over buckets and merge with the next bucket
		/**** Timer Start****/timer.reset();timer.start();
		List<Convoy> Vspext = K2SeqUtils.mergeVspBuckets(VspBuckets, m, k);
//		System.out.println("**k2 Vsp merged**");
//		System.out.println("VspExtended size = " + Vspext.size());
		/**** Timer Stop****/timer.stop(); timeStruct.timeMergeVsps = timer.elapsed(TimeUnit.MILLISECONDS);
//		sortTs(Vspext);
//		print(Vspext);
		/**** Timer Start****/timer.reset();timer.start();
		List<Convoy> Vlo = K2SeqUtils.extendRightBatch(Vspext, m, k, clusteringUtils, dataManager);

//		System.out.println("***hbase right extended ***");
//		System.out.println("Right extended size = " + Vlo.size());
//		sortTs(Vlo);
//		print(Vlo);
		/**** Timer Stop****/timer.stop(); timeStruct.timeExtendRight = timer.elapsed(TimeUnit.MILLISECONDS);
		/**** Timer Start****/timer.reset();timer.start();
		List<Convoy> Vrelaxed = K2SeqUtils.extendLeftBatch(Vlo, m,k, clusteringUtils, dataManager);
		/**** Timer Stop****/timer.stop(); timeStruct.timeExtendLeft = timer.elapsed(TimeUnit.MILLISECONDS);
		/**** Timer Start****/timer.reset();timer.start();

//		System.out.println("***hbase relaxed ***");
//		sortTe(Vrelaxed);
//		print(Vrelaxed);


//		System.out.println("Semi-Relaxed Convoys=" + Vrelaxed.size());
		timeStruct.numPreValConvoys = Integer.toUnsignedLong(Vrelaxed.size());
		List<Convoy> k2strictConvoys = StrictConvoysUtil.mineStrictModelConvoys(Vrelaxed, eps, m, k, dataManager, clusteringUtils);
		/**** Timer Stop****/timer.stop(); timeStruct.timeStrictValidation = timer.elapsed(TimeUnit.MILLISECONDS);
		timeStruct.numPostValConvoys = Integer.toUnsignedLong(k2strictConvoys.size());
//		System.out.println("Strict Convoys=" + k2strictConvoys.size());
//		for (Convoy v :
//				k2strictConvoys) {
//			System.out.println(v);
//		}
		return Tuple2.of(k2strictConvoys,timeStruct);
	}

	public static Tuple2<List<Convoy>,k2ExecutionTimeStruct> k2SeqStreaming(String inputFilePath, double eps, int m, int k) throws IOException, InterruptedException {
		k2ExecutionTimeStruct timeStruct = new k2ExecutionTimeStruct();
		/**** Timer Start****/Stopwatch timer = Stopwatch.createStarted();
		dataManager = new K2SeqDataManager(inputFilePath, k);
		/**** Timer Stop****/timer.stop(); timeStruct.timeDataLoading = timer.elapsed(TimeUnit.MILLISECONDS);
		clusteringUtils = new ClusteringUtils(eps, m-1);
//		List<Convoy> Vlo = K2SeqUtils.findVsp(dataManager, clusteringUtils);
		/**** Timer Start****/timer.reset();timer.start();

		/********Initialization Find C_0: line 1-2**********/

		List<Convoy> Vsp_i = new LinkedList<>();
		List<Convoy> Ve_i = new LinkedList<>();
		List<Convoy> Vf_i = new LinkedList<>();
		List<Convoy> Vf = new LinkedList<>();
		List<Convoy> Vout = new LinkedList<>();
		Map<Long, Map<Long, ClusterablePoint>> k2Batch = null;
		Map<Long, Map<Long, ClusterablePoint>> k2Batch_i = dataManager.getNextK2BatchAndForget();
		Long ts = K2SeqUtils.getTs(k2Batch_i);
		List<Set<Long>> C_i = clusteringUtils.clusterNoTime(ts, k2Batch_i);

		do {
			/********Find C_{i+1}, CC_i and HWMT: line 3-5**********/
			// returns Vsp for a partition and clustersTe which will be passed to the next partition as C_i
			Tuple3<ArrayList<Convoy>, Long, List<Set<Long>>> result = clusteringUtils.runK2SeqSpHWMT(ts, C_i, k2Batch_i);
//			Above returns <Vsp_i,te,C_i+1>
			if (result.f0!=null && result.f0.size()>0) {
				Vsp_i=result.f0;
			}

			/********merge: line 6**********/
			//returns<V_m,Vl_unmerged,Vr_unmerged>
			Tuple3<List<Convoy>, List<Convoy>, List<Convoy>> mergedConvoys = K2SeqUtils.merge(Vf, Vsp_i, m, k);
			List<Convoy> V_m = mergedConvoys.f0; List<Convoy> Vf_unmerged = mergedConvoys.f1; List<Convoy> Vsp_unmerged = mergedConvoys.f2;

			/********extendRight: line 7**********/
			Ve_i = K2SeqUtils.streamingExtendRightSet(Vf_unmerged, k2Batch_i, clusteringUtils, k);
//			skip validation. we don't have locations in the convoy. enable it after adding point locations to convoys too.
//			Ve_i = StrictConvoysUtil.streamingMineStrictModelConvoys(Ve_i, eps, m, k, k2Batch, clusteringUtils);

			/********extendLeft: line 8**********/
			List<Convoy> Vs = K2SeqUtils.streamingExtendLeftSet(Vsp_unmerged, k2Batch, clusteringUtils, k);

			/********output: line 9**********/
			for (Convoy vnew:
				 Ve_i) {
				StrictConvoysUtil.updateStrictVnext(Vout,vnew);
			}

			/********forward convoy set: line 10**********/
			if (V_m!=null) {
				V_m.addAll(Vs);
			}else {
				V_m = Vs;
			}
			Vf_i = V_m;

			Vf = Vf_i;
			ts = result.f1;
			C_i = result.f2;
			k2Batch = k2Batch_i;
//			System.out.println("ts="+ts);
		}while ((k2Batch_i = dataManager.getNextK2BatchAndForget()) != null);

		return Tuple2.of(Vout,timeStruct);
	}


	public static Tuple2<List<Convoy>,k2ExecutionTimeStruct> vcoda(String inputFilePath, double eps, int m, int k) {
		clusteringUtils = new ClusteringUtils(eps, m-1);
		dataManager = new K2SeqDataManager(inputFilePath, k);
		k2ExecutionTimeStruct timeStruct = new k2ExecutionTimeStruct();
		/**** Timer Start****/Stopwatch timer = Stopwatch.createStarted();
		List<dk.aau.convoy.base.Convoy> Vpcc = Vcoda.PCCD(inputFilePath, k, m, eps);
//		System.out.println("Relaxed Convoys=" + Vpcc.size());
		timeStruct.numPreValConvoys = Integer.toUnsignedLong(Vpcc.size());
		/**** Timer Stop****/timer.stop(); timeStruct.timeHWMT = timer.elapsed(TimeUnit.MILLISECONDS);
		/**** Timer Start****/timer.reset();timer.start();
		while(dataManager.getNextK2Batch()!=null){

		}
		List<Convoy> strictConvoys = StrictConvoysUtil.mineStrictConvoys(Vpcc, eps, m, k, dataManager, clusteringUtils);
		/**** Timer Stop****/timer.stop(); timeStruct.timeStrictValidation = timer.elapsed(TimeUnit.MILLISECONDS);
		timeStruct.numPostValConvoys = Integer.toUnsignedLong(strictConvoys.size());
//		Collections.sort(strictConvoys, new Comparator<Convoy>() {
//			@Override
//			public int compare(Convoy o1, Convoy o2) {
//				if (o1.getTs() > o2.getTs() || (o1.getTs() == o2.getTs() && o1.getTe() > o2.getTe())) {
//					return 1;
//				} else if (o1.getTs() == o2.getTs() && o1.getTe() == o2.getTe()) {
//					return 0;
//				}
//				return -1;
//			}
//		});
		return Tuple2.of(strictConvoys,timeStruct);
	}

	public static void sortTe(List<Convoy> V){
		Collections.sort(V, new Comparator<Convoy>() {
			@Override
			public int compare(Convoy o1, Convoy o2) {
				if (o1.getTe() > o2.getTe() || (o1.getTe() == o2.getTe() && o1.getTs() > o2.getTs())) {
					return 1;
				} else if (o1.getTe() == o2.getTe() && o1.getTs() == o2.getTs()) {
					return 0;
				}
				return -1;
			}
		});
	}



	public static void sortTs(List<Convoy> V){
		Collections.sort(V, new Comparator<Convoy>() {
			@Override
			public int compare(Convoy o1, Convoy o2) {
				if (o1.getTs() > o2.getTs() || (o1.getTs() == o2.getTs() && o1.getTe() > o2.getTe())) {
					return 1;
				} else if (o1.getTs() == o2.getTs() && o1.getTe() == o2.getTe()) {
					return 0;
				}
				return -1;
			}
		});
	}
	public static void print(List<Convoy> convoys){
		for (Convoy v :
				convoys) {
			System.out.println(v);
		}
	}


	public static Tuple2<List<Convoy>,k2ExecutionTimeStruct> vcodaNlogN(String inputFilePath, double eps, int m, int k) {
		clusteringUtils = new ClusteringUtils(eps, m-1);
		dataManager = new K2SeqDataManager(inputFilePath, k);
		k2ExecutionTimeStruct timeStruct = new k2ExecutionTimeStruct();
		/**** Timer Start****/Stopwatch timer = Stopwatch.createStarted();
		List<dk.aau.convoy.base.Convoy> Vpcc = null;
		try {
			Vpcc = VcodaNLogN.NlogNClustering(inputFilePath, k, m, eps);
		} catch (IOException e) {
			e.printStackTrace();
		}

//		System.out.println("Relaxed Convoys=" + Vpcc.size());
		timeStruct.numPreValConvoys = Integer.toUnsignedLong(Vpcc.size());
//		System.out.println("Finished PCCD");
		/**** Timer Stop****/timer.stop(); timeStruct.timeHWMT = timer.elapsed(TimeUnit.MILLISECONDS);
		/**** Timer Start****/timer.reset();timer.start();
		while(dataManager.getNextK2Batch()!=null){

		}
//		System.out.println("Finished data loading");
		List<Convoy> strictConvoys = StrictConvoysUtil.mineStrictConvoys(Vpcc, eps, m, k, dataManager, clusteringUtils);
		/**** Timer Stop****/timer.stop(); timeStruct.timeStrictValidation = timer.elapsed(TimeUnit.MILLISECONDS);
//		System.out.println("Finished strict convoys validation");
//		timeStruct.numPostValConvoys = Integer.toUnsignedLong(strictConvoys.size());
//		strictConvoys.forEach(System.out::println);
//		Collections.sort(strictConvoys, new Comparator<Convoy>() {
//			@Override
//			public int compare(Convoy o1, Convoy o2) {
//				if (o1.getTs() > o2.getTs() || (o1.getTs() == o2.getTs() && o1.getTe() > o2.getTe())) {
//					return 1;
//				} else if (o1.getTs() == o2.getTs() && o1.getTe() == o2.getTe()) {
//					return 0;
//				}
//				return -1;
//			}
//		});
		return Tuple2.of(null,timeStruct);
	}


	public static Tuple2<List<Convoy>,k2ExecutionTimeStruct> k2SeqHBaseBatch(String tableName, double eps, int m, int k) throws IOException, InterruptedException {
		ReadHBase.init(tableName);
		dataManager = new K2SeqDataManager(k);
		k2ExecutionTimeStruct timeStruct = new k2ExecutionTimeStruct();
		/**** Timer Start****/Stopwatch timer = Stopwatch.createStarted();
		/**** Timer Stop****/timer.stop(); timeStruct.timeDataLoading = timer.elapsed(TimeUnit.MILLISECONDS);
		clusteringUtils = new ClusteringUtils(eps, m-1);
//		List<Convoy> Vlo = K2SeqUtils.findVsp(dataManager, clusteringUtils);
		/**** Timer Start****/timer.reset();timer.start();
		LinkedList<List<Convoy>> VspBuckets = K2HBaseUtil.findVspBucketed(dataManager, k, clusteringUtils);
//		System.out.println("VspBuckets size = " + VspBuckets.size());
		/**** Timer Stop****/timer.stop(); timeStruct.timeHWMT = timer.elapsed(TimeUnit.MILLISECONDS);
//		System.out.println("***hbase VspBuckets***");
//		System.out.println("VspBuckets size = " + VspBuckets.size());
//		for (List<Convoy> l :
//				VspBuckets) {
//			for (Convoy v :
//					l) {
//				System.out.println(v);
//			}
//		}
//		//Iterate over buckets and merge with the next bucket
		/**** Timer Start****/timer.reset();timer.start();
		List<Convoy> Vspext = K2SeqUtils.mergeVspBuckets(VspBuckets, m, k);
//		System.out.println("**hbase Vsp merged**");
//		System.out.println("VspExtended size = " + Vspext.size());
		/**** Timer Stop****/timer.stop(); timeStruct.timeMergeVsps = timer.elapsed(TimeUnit.MILLISECONDS);
//		sortTs(Vspext);
//		print(Vspext);
		/**** Timer Start****/timer.reset();timer.start();
		List<Convoy> Vlo = K2HBaseUtil.extendRightHBaseBatch(Vspext, m, k, clusteringUtils, dataManager);

//		System.out.println("***hbase batch right extended ***");
//		System.out.println("Right extended size = " + Vlo.size());
//		sortTs(Vlo);
//		print(Vlo);
		/**** Timer Stop****/timer.stop(); timeStruct.timeExtendRight = timer.elapsed(TimeUnit.MILLISECONDS);
		/**** Timer Start****/timer.reset();timer.start();
		List<Convoy> Vrelaxed = K2HBaseUtil.extendLeftHBaseBatch(Vlo, m, k, clusteringUtils, dataManager);
		/**** Timer Stop****/timer.stop(); timeStruct.timeExtendLeft = timer.elapsed(TimeUnit.MILLISECONDS);
		/**** Timer Start****/timer.reset();timer.start();

//		System.out.println("***hbase batch relaxed ***");
//		sortTe(Vrelaxed);
//		print(Vrelaxed);

//		System.out.println("Semi-Relaxed Convoys=" + Vrelaxed.size());
		timeStruct.numPreValConvoys = Integer.toUnsignedLong(Vrelaxed.size());
		List<Convoy> k2strictConvoys = StrictConvoysUtil.mineStrictModelConvoys(Vrelaxed, eps, m, k, dataManager, clusteringUtils);
		/**** Timer Stop****/timer.stop(); timeStruct.timeStrictValidation = timer.elapsed(TimeUnit.MILLISECONDS);
		timeStruct.numPostValConvoys = Integer.toUnsignedLong(k2strictConvoys.size());
//		System.out.println("Strict Convoys=" + k2strictConvoys.size());
		return Tuple2.of(k2strictConvoys,timeStruct);
	}


	public static Tuple2<List<Convoy>,k2ExecutionTimeStruct> k2SeqRDBMSBatch(String tableName, double eps, int m, int k) throws IOException, InterruptedException {
		ReadRDBMS.init(tableName);
		dataManager = new K2SeqRDBMSDataManager(tableName, k);
		k2ExecutionTimeStruct timeStruct = new k2ExecutionTimeStruct();
		/**** Timer Start****/Stopwatch timer = Stopwatch.createStarted();
		/**** Timer Stop****/timer.stop(); timeStruct.timeDataLoading = timer.elapsed(TimeUnit.MILLISECONDS);
		clusteringUtils = new ClusteringUtils(eps, m-1);
//		List<Convoy> Vlo = K2SeqUtils.findVsp(dataManager, clusteringUtils);
		/**** Timer Start****/timer.reset();timer.start();
		LinkedList<List<Convoy>> VspBuckets = K2HBaseUtil.findVspBucketed(dataManager, k, clusteringUtils);
//		System.out.println("VspBuckets size = " + VspBuckets.size());
		/**** Timer Stop****/timer.stop(); timeStruct.timeHWMT = timer.elapsed(TimeUnit.MILLISECONDS);
//		System.out.println("***hbase VspBuckets***");
//		System.out.println("VspBuckets size = " + VspBuckets.size());
//		for (List<Convoy> l :
//				VspBuckets) {
//			for (Convoy v :
//					l) {
//				System.out.println(v);
//			}
//		}
//		//Iterate over buckets and merge with the next bucket
		/**** Timer Start****/timer.reset();timer.start();
		List<Convoy> Vspext = K2SeqUtils.mergeVspBuckets(VspBuckets, m, k);
//		System.out.println("**hbase Vsp merged**");
//		System.out.println("VspExtended size = " + Vspext.size());
		/**** Timer Stop****/timer.stop(); timeStruct.timeMergeVsps = timer.elapsed(TimeUnit.MILLISECONDS);
//		sortTs(Vspext);
//		print(Vspext);
		/**** Timer Start****/timer.reset();timer.start();
		List<Convoy> Vlo = K2HBaseUtil.extendRightHBaseBatch(Vspext, m, k, clusteringUtils, dataManager);

//		System.out.println("***hbase batch right extended ***");
//		System.out.println("Right extended size = " + Vlo.size());
//		sortTs(Vlo);
//		print(Vlo);
		/**** Timer Stop****/timer.stop(); timeStruct.timeExtendRight = timer.elapsed(TimeUnit.MILLISECONDS);
		/**** Timer Start****/timer.reset();timer.start();
		List<Convoy> Vrelaxed = K2HBaseUtil.extendLeftHBaseBatch(Vlo, m, k, clusteringUtils, dataManager);
		/**** Timer Stop****/timer.stop(); timeStruct.timeExtendLeft = timer.elapsed(TimeUnit.MILLISECONDS);
		/**** Timer Start****/timer.reset();timer.start();

//		System.out.println("***hbase batch relaxed ***");
//		sortTe(Vrelaxed);
//		print(Vrelaxed);

//		System.out.println("Semi-Relaxed Convoys=" + Vrelaxed.size());
		timeStruct.numPreValConvoys = Integer.toUnsignedLong(Vrelaxed.size());
		List<Convoy> k2strictConvoys = StrictConvoysUtil.mineStrictModelConvoys(Vrelaxed, eps, m, k, dataManager, clusteringUtils);
		/**** Timer Stop****/timer.stop(); timeStruct.timeStrictValidation = timer.elapsed(TimeUnit.MILLISECONDS);
		timeStruct.numPostValConvoys = Integer.toUnsignedLong(k2strictConvoys.size());
//		System.out.println("Strict Convoys=" + k2strictConvoys.size());
		((K2SeqRDBMSDataManager)dataManager).getNumPoints();
		return Tuple2.of(k2strictConvoys,timeStruct);
	}

	public static Tuple2<List<Convoy>,k2ExecutionTimeStruct> k2SeqRDBMSIndl(String tableName, double eps, int m, int k) throws IOException, InterruptedException {
		ReadHBase.init(tableName);
		dataManager = new K2SeqRDBMSDataManager(tableName, k);
		k2ExecutionTimeStruct timeStruct = new k2ExecutionTimeStruct();
		/**** Timer Start****/Stopwatch timer = Stopwatch.createStarted();
		/**** Timer Stop****/timer.stop(); timeStruct.timeDataLoading = timer.elapsed(TimeUnit.MILLISECONDS);
		clusteringUtils = new ClusteringUtils(eps, m-1);
//		List<Convoy> Vlo = K2SeqUtils.findVsp(dataManager, clusteringUtils);
		/**** Timer Start****/timer.reset();timer.start();
		LinkedList<List<Convoy>> VspBuckets = K2HBaseUtil.findVspBucketed(dataManager, k, clusteringUtils);
//		System.out.println("VspBuckets size = " + VspBuckets.size());
		/**** Timer Stop****/timer.stop(); timeStruct.timeHWMT = timer.elapsed(TimeUnit.MILLISECONDS);
//		System.out.println("***hbase VspBuckets***");
//		System.out.println("VspBuckets size = " + VspBuckets.size());
//		for (List<Convoy> l :
//				VspBuckets) {
//			for (Convoy v :
//					l) {
//				System.out.println(v);
//			}
//		}
//		//Iterate over buckets and merge with the next bucket
		/**** Timer Start****/timer.reset();timer.start();
		List<Convoy> Vspext = K2SeqUtils.mergeVspBuckets(VspBuckets, m, k);
//		System.out.println("**hbase Vsp merged**");
//		System.out.println("VspExtended size = " + Vspext.size());
		/**** Timer Stop****/timer.stop(); timeStruct.timeMergeVsps = timer.elapsed(TimeUnit.MILLISECONDS);
//		sortTs(Vspext);
//		print(Vspext);
		/**** Timer Start****/timer.reset();timer.start();
		List<Convoy> Vlo = K2HBaseUtil.extendRightIndl(Vspext, m, k, clusteringUtils, dataManager);

//		System.out.println("***hbase right extended ***");
//		System.out.println("Right extended size = " + Vlo.size());
//		sortTe(Vlo);
//		print(Vlo);
		/**** Timer Stop****/timer.stop(); timeStruct.timeExtendRight = timer.elapsed(TimeUnit.MILLISECONDS);
		/**** Timer Start****/timer.reset();timer.start();
		List<Convoy> Vrelaxed = K2HBaseUtil.extendLefttIndl(Vlo, m, k, clusteringUtils, dataManager);
		/**** Timer Stop****/timer.stop(); timeStruct.timeExtendLeft = timer.elapsed(TimeUnit.MILLISECONDS);
		/**** Timer Start****/timer.reset();timer.start();

//		System.out.println("***hbase relaxed ***");
//		sortTe(Vrelaxed);
//		print(Vrelaxed);

//		System.out.println("Semi-Relaxed Convoys=" + Vrelaxed.size());
		timeStruct.numPreValConvoys = Integer.toUnsignedLong(Vrelaxed.size());
		List<Convoy> k2strictConvoys = StrictConvoysUtil.mineStrictModelConvoys(Vrelaxed, eps, m, k, dataManager, clusteringUtils);
		/**** Timer Stop****/timer.stop(); timeStruct.timeStrictValidation = timer.elapsed(TimeUnit.MILLISECONDS);
		timeStruct.numPostValConvoys = Integer.toUnsignedLong(k2strictConvoys.size());
//		System.out.println("Strict Convoys=" + k2strictConvoys.size());
		return Tuple2.of(k2strictConvoys,timeStruct);
	}


	public static Tuple2<List<Convoy>,k2ExecutionTimeStruct> k2SeqHBaseIndl(String tableName, double eps, int m, int k) throws IOException, InterruptedException {
		ReadHBase.init(tableName);
		dataManager = new K2SeqDataManager(k);
		k2ExecutionTimeStruct timeStruct = new k2ExecutionTimeStruct();
		/**** Timer Start****/Stopwatch timer = Stopwatch.createStarted();
		/**** Timer Stop****/timer.stop(); timeStruct.timeDataLoading = timer.elapsed(TimeUnit.MILLISECONDS);
		clusteringUtils = new ClusteringUtils(eps, m-1);
//		List<Convoy> Vlo = K2SeqUtils.findVsp(dataManager, clusteringUtils);
		/**** Timer Start****/timer.reset();timer.start();
		LinkedList<List<Convoy>> VspBuckets = K2HBaseUtil.findVspBucketed(dataManager, k, clusteringUtils);
//		System.out.println("VspBuckets size = " + VspBuckets.size());
		/**** Timer Stop****/timer.stop(); timeStruct.timeHWMT = timer.elapsed(TimeUnit.MILLISECONDS);
//		System.out.println("***hbase VspBuckets***");
//		System.out.println("VspBuckets size = " + VspBuckets.size());
//		for (List<Convoy> l :
//				VspBuckets) {
//			for (Convoy v :
//					l) {
//				System.out.println(v);
//			}
//		}
//		//Iterate over buckets and merge with the next bucket
		/**** Timer Start****/timer.reset();timer.start();
		List<Convoy> Vspext = K2SeqUtils.mergeVspBuckets(VspBuckets, m, k);
//		System.out.println("**hbase Vsp merged**");
//		System.out.println("VspExtended size = " + Vspext.size());
		/**** Timer Stop****/timer.stop(); timeStruct.timeMergeVsps = timer.elapsed(TimeUnit.MILLISECONDS);
//		sortTs(Vspext);
//		print(Vspext);
		/**** Timer Start****/timer.reset();timer.start();
		List<Convoy> Vlo = K2HBaseUtil.extendRightIndl(Vspext, m, k, clusteringUtils, dataManager);

//		System.out.println("***hbase right extended ***");
//		System.out.println("Right extended size = " + Vlo.size());
//		sortTe(Vlo);
//		print(Vlo);
		/**** Timer Stop****/timer.stop(); timeStruct.timeExtendRight = timer.elapsed(TimeUnit.MILLISECONDS);
		/**** Timer Start****/timer.reset();timer.start();
		List<Convoy> Vrelaxed = K2HBaseUtil.extendLefttIndl(Vlo, m, k, clusteringUtils, dataManager);
		/**** Timer Stop****/timer.stop(); timeStruct.timeExtendLeft = timer.elapsed(TimeUnit.MILLISECONDS);
		/**** Timer Start****/timer.reset();timer.start();

//		System.out.println("***hbase relaxed ***");
//		sortTe(Vrelaxed);
//		print(Vrelaxed);

//		System.out.println("Semi-Relaxed Convoys=" + Vrelaxed.size());
		timeStruct.numPreValConvoys = Integer.toUnsignedLong(Vrelaxed.size());
		List<Convoy> k2strictConvoys = StrictConvoysUtil.mineStrictModelConvoys(Vrelaxed, eps, m, k, dataManager, clusteringUtils);
		/**** Timer Stop****/timer.stop(); timeStruct.timeStrictValidation = timer.elapsed(TimeUnit.MILLISECONDS);
		timeStruct.numPostValConvoys = Integer.toUnsignedLong(k2strictConvoys.size());
//		System.out.println("Strict Convoys=" + k2strictConvoys.size());
		return Tuple2.of(k2strictConvoys,timeStruct);
	}


}
