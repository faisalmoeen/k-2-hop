package dk.aau.convoy.functions;

import dk.aau.convoy.model.K2VertexValue;
import dk.aau.convoy.model.ClusterablePoint;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.graph.Vertex;
import org.apache.flink.util.Collector;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by faisal on 8/20/16.
 */
public class GroupByPartition implements GroupReduceFunction<Tuple3<Long, Long, HashMap<Long, ClusterablePoint>>,
		Vertex<Long, K2VertexValue>> {

	@Override
	public void reduce(Iterable<Tuple3<Long, Long, HashMap<Long, ClusterablePoint>>> values,
					   Collector<Vertex<Long, K2VertexValue>> out) throws Exception {

		Iterator<Tuple3<Long, Long, HashMap<Long, ClusterablePoint>>> iter = values.iterator();
		// Hashmap<time, Hashmap<oid,point>>
		Map<Long,Map<Long, ClusterablePoint>> hashMapPartition = new HashMap();

		Tuple3<Long, Long, HashMap<Long, ClusterablePoint>> tuple3 = iter.next();
		Long p = tuple3.getField(0);//partition id
		hashMapPartition.put((Long) tuple3.getField(1), (HashMap<Long, ClusterablePoint>) tuple3.getField(2));
		long min = tuple3.getField(1);
		long max = tuple3.getField(1);
		while (iter.hasNext()){
			tuple3 = iter.next();
			hashMapPartition.put((Long)tuple3.getField(1), (HashMap<Long, ClusterablePoint>)tuple3.getField(2));
			if((long)tuple3.getField(1)<min){
				min = tuple3.getField(1);
			}
			if((long)tuple3.getField(1)>max){
				max = tuple3.getField(1);
			}
		}
		out.collect(new Vertex(p, new K2VertexValue(hashMapPartition, min, max)));
	}
}
