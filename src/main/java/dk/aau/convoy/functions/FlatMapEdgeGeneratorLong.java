package dk.aau.convoy.functions;

import dk.aau.convoy.model.K2VertexValue;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.graph.Edge;
import org.apache.flink.graph.Vertex;
import org.apache.flink.types.NullValue;
import org.apache.flink.util.Collector;

/**
 * Created by faisal on 8/20/16.
 */
public class FlatMapEdgeGeneratorLong implements FlatMapFunction<Long,
		Edge<Long,NullValue>> {

	@Override
	public void flatMap(Long value,
						Collector<Edge<Long, NullValue>> out) throws Exception {
		for(Long i = new Long(1); i<value; i++){
			out.collect(new Edge( i, new Long(i-1), NullValue.getInstance() ));
			out.collect(new Edge( new Long(i-1), i, NullValue.getInstance() ));
		}
	}
}
