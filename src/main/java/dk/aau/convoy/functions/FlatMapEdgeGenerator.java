package dk.aau.convoy.functions;

import dk.aau.convoy.model.K2VertexValue;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.graph.Edge;
import org.apache.flink.graph.Vertex;
import org.apache.flink.types.NullValue;
import org.apache.flink.util.Collector;

/**
 * Created by faisal on 8/20/16.
 */
public class FlatMapEdgeGenerator implements FlatMapFunction<Tuple2<Vertex<Long,K2VertexValue>, Vertex<Long,K2VertexValue>>,
		Edge<Long,NullValue>> {

	@Override
	public void flatMap(Tuple2<Vertex<Long, K2VertexValue>,Vertex<Long,K2VertexValue>> value,
						Collector<Edge<Long, NullValue>> out) throws Exception {
		Long minPartitionCount = (Long)((Vertex)value.getField(0)).getId();
		Long maxPartitionCount = (Long)((Vertex)value.getField(1)).getId();
		for(Long i = minPartitionCount+1; i<maxPartitionCount; i++){
			out.collect(new Edge( i, new Long(i-1), NullValue.getInstance() ));
			out.collect(new Edge( new Long(i-1), i, NullValue.getInstance() ));
		}
		//if there is only one partition, create a self edge
		if(minPartitionCount==maxPartitionCount){
			out.collect(new Edge( minPartitionCount, maxPartitionCount, NullValue.getInstance() ));
		}
	}
}
