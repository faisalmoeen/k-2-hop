package dk.aau.convoy.functions;

import dk.aau.convoy.model.ClusterablePoint;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.util.Collector;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by faisal on 8/20/16.
 */
public class GroupByTime implements GroupReduceFunction<Tuple4<Long, Long, Double, Double>,
		Tuple3<Long, Long, HashMap<Long, ClusterablePoint>>> {
	private Integer k;
	public GroupByTime() {
		super();
	}
	public GroupByTime(int k){
		super();
		this.k = k;
	}


	@Override
	public void reduce(Iterable<Tuple4<Long, Long, Double, Double>> values,
					   Collector<Tuple3<Long, Long, HashMap<Long, ClusterablePoint>>> out) throws Exception {
		Iterator<Tuple4<Long, Long, Double, Double>> iter = values.iterator();
		HashMap<Long, ClusterablePoint> oidToPointMap = new HashMap<Long, ClusterablePoint>();

		Tuple4 tuple4 = iter.next();
		Long t = (Long) tuple4.getField(1);
		oidToPointMap.put((Long)tuple4.getField(0), new ClusterablePoint(tuple4));

		while (iter.hasNext()){
			tuple4 = iter.next();
			oidToPointMap.put((Long)tuple4.getField(0), new ClusterablePoint(tuple4));
		}
//		outputs (partitionID, time, HashMap<oid,Point>)
		out.collect(Tuple3.of((long)t/(k/2), t, oidToPointMap));
	}
}
