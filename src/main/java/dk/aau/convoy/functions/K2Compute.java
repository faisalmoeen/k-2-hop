package dk.aau.convoy.functions;

import dk.aau.convoy.utils.ConvoyUtil;
import dk.aau.convoy.K2Utils;
import dk.aau.convoy.clustering.ClusteringUtils;
import dk.aau.convoy.model.Convoy;
import dk.aau.convoy.model.K2Message;
import dk.aau.convoy.model.K2VertexValue;
import dk.aau.convoy.model.ClusterablePoint;
import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.graph.Vertex;
import org.apache.flink.graph.pregel.ComputeFunction;
import org.apache.flink.graph.pregel.MessageIterator;
import org.apache.flink.types.NullValue;

import java.util.*;

/**
 * Created by faisal on 8/20/16.
 */
public class K2Compute extends ComputeFunction<Long, K2VertexValue, NullValue, K2Message> {

	private int k = 60;
	private int m = 3;
	private float eps = 0.00006f;
	private long startVertex;
	private long endVertex;
	private ClusteringUtils clusteringUtils;
	private ConvoyUtil convoyUtil;
	private Long hopWindowWidth;
	private int gap=0;

	@Override
	public void preSuperstep() throws Exception {
		super.preSuperstep();
		if (getSuperstepNumber()==1){//if first superstep
			List<Tuple2<Vertex<Long,K2VertexValue>, Vertex<Long,K2VertexValue>>> minMaxPartitionTuple = (List) getBroadcastSet("minMaxPartitionTuple");
			startVertex = ((Vertex<Long, K2VertexValue>) ((Tuple2<Vertex<Long, K2VertexValue>, Vertex<Long, K2VertexValue>>) minMaxPartitionTuple.get(0)).getField(0)).getId();
			endVertex = ((Vertex<Long, K2VertexValue>) ((Tuple2<Vertex<Long, K2VertexValue>, Vertex<Long, K2VertexValue>>) minMaxPartitionTuple.get(0)).getField(1)).getId();
			clusteringUtils = new ClusteringUtils(eps,m);
			convoyUtil = new ConvoyUtil(m, k, eps);
		}
		hopWindowWidth = k/ ((long) Math.pow(2, getSuperstepNumber()));

		startVertex+=gap;
		gap = K2Utils.gap(getSuperstepNumber());
	}

	@Override
	public void compute(Vertex<Long, K2VertexValue> vertex, MessageIterator<K2Message> messageIterator) throws Exception {
//		TODO: Get broadcasted value of m and k
		if(getSuperstepNumber()==1){
			Long benchmark=vertex.getValue().getTs();	//The starting timestamp of a partition is a benchmark point
			Long partition = vertex.getId();	//The vertex id is the partition id
			System.out.println(partition);
			//we cluster all the objects in the benchmark point (first timestamp in a partition/vertex)
			List<Convoy> clustersToTransmit = clusteringUtils.cluster(benchmark,vertex.getValue().getData());
			if(clustersToTransmit==null || clustersToTransmit.size()==0){	//if no clusters found
				vertex.getValue().setContainsVsp(false);	//there can't be spanning convoys in this hw
				vertex.getValue().setContainsVe(false);		//there can't be ending convoys in this hw
			}
			else {	//if clusters found, convert them to a set format and update vertex value
				vertex.getValue().setCCsp(clustersToTransmit);	//set CCsp in vertex value to the new clusters
			}
			if (vertex.getId()!= startVertex) {	// The first partition vertex should not send any message.om
				sendMessageTo(vertex.getId() - 1, new K2Message(clustersToTransmit, vertex.getId()));	// Each vertex sends its CCsp to the previous partition vertexes
			}
			// if there is only one vertex in the path graph, it should send the clusters of the last timestamp as
			// a message to itself to be able to run hop-window mining tree in the next superstep
			if (startVertex==endVertex){
				clustersToTransmit = clusteringUtils.cluster(vertex.getValue().getTe(),
						vertex.getValue().getData());
				sendMessageTo(vertex.getId(), new K2Message(clustersToTransmit, vertex.getId()));
			}
			setNewVertexValue(vertex.getValue()); //update the vertex value.

		}else if (getSuperstepNumber()==2){
			// Now each vertex has their benchmark clusters and also have received in this superstep, the benchmark
			// clusters of the next partition
			// Intersect the clusters from surrounding benchmark points to find Vsp
			System.out.println("Superstep:2, Vid:"+vertex.getId());
			long benchmark = vertex.getValue().getTs() + hopWindowWidth;
			List<Convoy> nextBenchMarkClusters = null;
			//In 2nd superstep, each vertex expects a single message containing 1st order benchmark clusters of the next partition
			// lets get those clusters
			if (messageIterator.hasNext()) {
				nextBenchMarkClusters = messageIterator.next().getMessage();
			}
			if(nextBenchMarkClusters==null){	//if no message containing clusters from the front vertex, there can be no spanning or starting convoys
				vertex.getValue().setContainsVsp(false);
				vertex.getValue().setContainsVs(false);
			}
			//Run full CCsp hop-window mining tree if
			if (vertex.getValue().getContainsVsp()){
				List<Convoy> CCsp = vertex.getValue().getCCsp();
				// match this vertex's benchmark clusters with next partitions's benchmark clusters to get VspCandidates
				List<Convoy> VspCandidates = ConvoyUtil.matchSnapshotConvoyLists(CCsp, nextBenchMarkClusters,m); //Data structure for holding the result of matching benchmark clusters
				// while running the hop-window mining tree, we need to keep track of the already processed timestamps
				Set<Long> processedTimeStamps = new HashSet<>();
				// The clusters from the benchmark point have been matched so mark the benchmark point as processed
				// Remember, the benchmark point of the current partition is the starting timestamp of the partition
				processedTimeStamps.add(vertex.getValue().getTs());
				List<Set<Long>> CCNewSpSet= clusteringUtils.runCCspHWMT(vertex.getValue().getTs(),vertex.getValue().getTe(),
						processedTimeStamps, 1L, ConvoyUtil.convoyToClusterList(VspCandidates), vertex.getValue().getData());
//				VspCandidates = clusteringUtils.reCluster(benchmark, VspCandidates, vertex.getValue().getData());
				List<Convoy> Vsp = ConvoyUtil.objsListToConvoyList(CCNewSpSet,vertex.getValue().getTs(), vertex.getValue().getTe());
				vertex.getValue().setCCsp(Vsp);
				if (Vsp==null || Vsp.size()==0){
					vertex.getValue().setContainsVsp(false);
				}
				if (vertex.getId()!= startVertex) {	// The first partition vertex should not send any message to the backward vertex/partition
					sendMessageTo(vertex.getId() - 1, new K2Message(Vsp, vertex.getId()));	// Each vertex sends its CCsp to the previous partition vertexes
				}
				if (vertex.getId()!= endVertex) {	// The last partition vertex should not send any message to the forward partition/vertex
					sendMessageTo(vertex.getId() + 1, new K2Message(Vsp, vertex.getId()));	// Each vertex sends its CCsp to the previous partition vertexes
				}
			}
			setNewVertexValue(vertex.getValue());
		}
		else if (getSuperstepNumber()==3){
//			All spanning convoys Vsp have been found. Now we need to use this info to find out starting and ending convoys.
//			An ending convoy is a subset of previous Vsp but not necessarily a subset of next Vsp
//			Similarly, a starting convoy is a subset of the next Vsp but not the previous Vsp
//			We can start a reduce tree
			System.out.println("Superstep:3, Vid:"+vertex.getId());
			List<Convoy> leftVsp = null; // to be used for mining ending convoys
			List<Convoy> rightVsp = null; // to be used for mining starting convoys
			K2Message message = null;
			while (messageIterator.hasNext()) {	//In 3rd superstep, each vertex expects 2 messages if its not a start or end vertex
				message = messageIterator.next();
				if (message.getSource()==vertex.getId()-1){
					leftVsp = message.getMessage();
				}else if(message.getSource() == vertex.getId()+1){
					rightVsp = message.getMessage();
				}
			}
			List<Convoy> Vsp = vertex.getValue().getCCsp();
			Tuple2<List<Convoy>, List<Convoy>> crossWindowConvoysR = null;
			Tuple2<List<Convoy>, List<Convoy>> crossWindowConvoysRL = null;
			if (vertex.getValue().getContainsVs()) {
				//Run an iteration for cross-window spanning convoys to the right
				//returns a tuple<open-convoys,closed-convoys>
				crossWindowConvoysR = convoyUtil.matchConvoys(Vsp, rightVsp);
				//Run an iteration for cross-window spanning convoys to the left
				if (crossWindowConvoysR!=null) {
					crossWindowConvoysRL = convoyUtil.matchConvoys(leftVsp, crossWindowConvoysR.f0);
				}
			}
			//Run an iteration of Ve ending convoys
			List<Convoy> Ve = null;
			if (leftVsp!=null && vertex.getValue().getContainsVe()){
				// we can't get any closed convoys from this method as leftVsp is of order 1
				Ve = convoyUtil.findEndingConvoys(Vsp, //the returned convoys won't be a subset of Vsp
						leftVsp, //the Vsp that came from the left
						vertex.getValue().getTs(), //the start time of the partition
						vertex.getValue().getTe(), //the end time of the partition
						vertex.getValue().getData()); //partition data
			}
			//Run an iteration of Vs starting convoys
			List<Convoy> Vs = null;
			if (rightVsp!=null && rightVsp.size()>0 && vertex.getValue().getContainsVs()){
				// we can't get any closed convoys from this method as rightVsp is of order 1
				Vs = convoyUtil.findStartingConvoys(Vsp,
						rightVsp,
						vertex.getValue().getTs(),
						vertex.getValue().getTe(),
						vertex.getValue().getData());
			}


			//we need to combine all closed convoys to be sent to the last vertex
			List<Convoy> closedConvoys = new ArrayList<>();

			if(crossWindowConvoysR!=null && crossWindowConvoysR.f1!=null && crossWindowConvoysR.f1.size()>0) {
				closedConvoys.addAll(crossWindowConvoysR.f1);
			}
			if(crossWindowConvoysRL!=null && crossWindowConvoysRL.f1!=null && crossWindowConvoysRL.f1.size()>0) {
				closedConvoys.addAll(crossWindowConvoysRL.f1);
			}
			if(closedConvoys.size()>0){
				sendMessageTo(endVertex,new K2Message(closedConvoys,vertex.getId()));
			}

			//we need to combine all open convoys either to be sent to the aggregating vertex if this vertex is a source
			//or save it in the vertex if it is an agregating vertex
			List<Convoy> openConvoys = new ArrayList<>();
			if (crossWindowConvoysRL!=null && crossWindowConvoysRL.f0!=null && crossWindowConvoysRL.f0.size()>0) {
				openConvoys.addAll(crossWindowConvoysRL.f0);
			}
			if (Vs!=null){
				openConvoys.addAll(Vs);
			}
			if (Ve!=null) {
				openConvoys.addAll(Ve);
			}
			//If this is a source vertex, it should send its open convoys to an aggregating vertex
			if(K2Utils.isSourceVertex(vertex.getId(),startVertex,gap)){
				long targetVertexId = vertex.getId()+gap;
				if (targetVertexId>endVertex){ //if the aggregating vertex id does not exist, send data to the endvertex
					targetVertexId = endVertex;
				}
				sendMessageTo(targetVertexId,new K2Message(null,vertex.getId()));
			} else {
				vertex.getValue().setCCsp(openConvoys);
				setNewVertexValue(vertex.getValue());
			}

		} else if (getSuperstepNumber()>3){
			//this vertex received a message because it is an aggregating vertex
			//it gets only one message from a source vertex on the left if it is not the last vertex
			List<Convoy> leftConvoys = messageIterator.next().getMessage();
			Tuple2<List<Convoy>, List<Convoy>> convoys = convoyUtil.matchConvoys(leftConvoys, vertex.getValue().getCCsp());

			if (convoys!=null && convoys.f1!=null && convoys.f1.size()>0){ //f1 contains closed convoys which should be sent to the end Vertex
				sendMessageTo(endVertex, new K2Message(convoys.f1, vertex.getId()));
			}


			if(K2Utils.isSourceVertex(vertex.getId(),startVertex,gap)){
				long targetVertexId = vertex.getId()+gap;
				if (targetVertexId>endVertex){ //if the aggregating vertex id does not exist, send data to the endvertex
					targetVertexId = endVertex;
				}
				sendMessageTo(targetVertexId,new K2Message(null,vertex.getId()));
			} else {
				if (convoys==null){
					vertex.getValue().setCCsp(null);
				}else {
					vertex.getValue().setCCsp(convoys.f0);
				}
				setNewVertexValue(vertex.getValue());
			}

			if (vertex.getId()==endVertex && vertex.getValue().getCCsp()!=null){
				for (Convoy v :
						vertex.getValue().getCCsp()) {
					System.out.println(v);
				}
			}
		}

//		run tree step for CCsp: send message to both neighbours to update their CCs and CCe
//		run tree step for CCs: include any feedback from the front neighbour
//		run tree step for CCe: include any feedback from the rear neighbour
	}


	public ArrayList<Set<Long>> convertToMessage(List<Cluster<ClusterablePoint>> clustersList){
		ArrayList<Set<Long>> message = new ArrayList<>();
		for (Cluster c: clustersList) {
			Set<Long> clusteredOids = new HashSet<>();
			for (ClusterablePoint tuple: (List<ClusterablePoint>)c.getPoints()) {
				clusteredOids.add(tuple.oid);
			}
			message.add(clusteredOids);
		}
		return message;
	}


}
