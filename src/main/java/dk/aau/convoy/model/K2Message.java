package dk.aau.convoy.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by faisal on 8/21/16.
 */
public class K2Message {
	private List<Convoy> message;
	private Long source;

	public K2Message() {
	}

	public K2Message(List<Convoy> message, Long source) {
		this.message = message;
		this.source = source;
	}

	public List<Convoy> getMessage() {
		return message;
	}

	public void setMessage(ArrayList<Convoy> message) {
		this.message = message;
	}

	public Long getSource() {
		return source;
	}

	public void setSource(Long source) {
		this.source = source;
	}
}
