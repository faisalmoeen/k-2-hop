package dk.aau.convoy.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by faisal on 8/21/16.
 */
public class K2VertexValue {
	private Long ts;
	private Long te;
	private Map<Long, Map<Long, ClusterablePoint>> data; // <time, HashMap<oid, point>>
	private Map<Long, K2Clusters> clustersHashMap;
	private List<Convoy> CCsp;
	private Boolean containsVsp = true;
	private Boolean containsVs = true;
	private Boolean containsVe = true;
	private List<Long> currentBenchmarks = new ArrayList<Long>();

	public K2VertexValue(){}

	public K2VertexValue(Map<Long, Map<Long, ClusterablePoint>> data, long minTime, long maxTime){
		this.data = data;
		this.ts = minTime;
		this.te = maxTime;
		currentBenchmarks.add(this.ts);
		currentBenchmarks.add(this.te+1);
	}

	public Long getTs() {
		return ts;
	}

	public void setTs(Long ts) {
		this.ts = ts;
	}

	public Long getTe() {
		return te;
	}

	public void setTe(Long te) {
		this.te = te;
	}

	public Map<Long, Map<Long, ClusterablePoint>> getData() {
		return data;
	}

	public void setData(Map<Long, Map<Long, ClusterablePoint>> data) {
		this.data = data;
	}

	public Boolean getContainsVsp() {
		return containsVsp;
	}

	public void setContainsVsp(Boolean containsVsp) {
		this.containsVsp = containsVsp;
	}

	public Boolean getContainsVs() {
		return containsVs;
	}

	public void setContainsVs(Boolean containsVs) {
		this.containsVs = containsVs;
	}

	public Boolean getContainsVe() {
		return containsVe;
	}

	public void setContainsVe(Boolean containsVe) {
		this.containsVe = containsVe;
	}

	public List<Convoy> getCCsp() {
		return CCsp;
	}

	public void setCCsp(List<Convoy> CCsp) {
		this.CCsp = CCsp;
	}

	public List<Long> getCurrentBenchmarks() {
		return currentBenchmarks;
	}

	public void setCurrentBenchmarks(List<Long> currentBenchmarks) {
		this.currentBenchmarks = currentBenchmarks;
	}

	public Map<Long, K2Clusters> getClustersHashMap() {
		return clustersHashMap;
	}

	public void setClustersHashMap(HashMap<Long, K2Clusters> clustersHashMap) {
		this.clustersHashMap = clustersHashMap;
	}
}
