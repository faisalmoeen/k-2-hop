package dk.aau.convoy.model;

import org.apache.commons.math3.ml.clustering.Clusterable;
import org.apache.flink.api.java.tuple.Tuple4;

/**
 * Created by faisal on 8/20/16.
 */
public class ClusterablePoint implements Clusterable {
	public Long oid;
	public double[] points = new double[2];
	public ClusterablePoint(Tuple4<Long, Long, Double, Double> tuple4) {
		oid = tuple4.getField(0);
		points[0] = tuple4.getField(2);
		points[1] = tuple4.getField(3);
	}
	public ClusterablePoint(Long oid, double x, double y) {
		this.oid = oid;
		points[0] = x;
		points[1] = y;
	}
	@Override
	public double[] getPoint() {
		return points;
	}

	public Long getOid() {
		return oid;
	}

	public void setOid(Long oid) {
		this.oid = oid;
	}

}
