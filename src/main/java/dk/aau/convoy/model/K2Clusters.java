package dk.aau.convoy.model;

import java.util.ArrayList;
import java.util.Set;

/**
 * Created by faisal on 8/29/16.
 */
public class K2Clusters {
//	private ArrayList<Set<Long>> CCsp;
	private ArrayList<Set<Long>> CCs;
	private ArrayList<Set<Long>> CCe;

//	public ArrayList<Set<Long>> getCCsp() {
//		return CCsp;
//	}
//
//	public K2Clusters setCCsp(ArrayList<Set<Long>> CCsp) {
//		this.CCsp = CCsp;
//		return this;
//	}

	public ArrayList<Set<Long>> getCCs() {
		return CCs;
	}

	public K2Clusters setCCs(ArrayList<Set<Long>> CCs) {
		this.CCs = CCs;
		return this;
	}

	public ArrayList<Set<Long>> getCCe() {
		return CCe;
	}

	public K2Clusters setCCe(ArrayList<Set<Long>> CCe) {
		this.CCe = CCe;
		return this;
	}
}
