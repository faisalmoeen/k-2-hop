package dk.aau.convoy.model;

/**
 * Created by faisal on 1/3/17.
 */
public class k2ExecutionTimeStruct {
	public Long numPreValConvoys, numPostValConvoys, timeDataLoading, timeHWMT, timeMergeVsps, timeExtendRight, timeExtendLeft, timeStrictValidation, timeTotal;

	public String getText(char sep){
		return ""+numPreValConvoys+sep+numPostValConvoys+sep+timeDataLoading+sep+ timeHWMT+sep+ timeMergeVsps+sep+ timeExtendRight+sep+ timeExtendLeft+sep+ timeStrictValidation+sep+ timeTotal;
	}
}
