package dk.aau.convoy.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class ConvoyBuilder {
	private long ts;
	private long te;
	private boolean leftOpen = true;
	private boolean rightOpen = true;
	private List<ClusterablePoint> cluster;
	private Set<Long> objs;

	public ConvoyBuilder setTs(long ts) {
		this.ts = ts;
		return this;
	}

	public ConvoyBuilder setTe(long te) {
		this.te = te;
		return this;
	}

	public ConvoyBuilder setObjs(Set<Long> objs) {
		this.objs = objs;
		return this;
	}

	public ConvoyBuilder setObjs(List<Long> objs) {
		this.objs = new HashSet<>(objs);
		return this;
	}

	public ConvoyBuilder setIntObjs(List<Integer> objs) {
		this.objs = new HashSet<>();
		for (Integer i :
				objs) {
			this.objs.add((long)i);
		}
		return this;
	}

	public ConvoyBuilder setLeftOpen(boolean leftOpen) {
		this.leftOpen = leftOpen;
		return this;
	}

	public ConvoyBuilder setRightOpen(boolean rightOpen) {
		this.rightOpen = rightOpen;
		return this;
	}

	public ConvoyBuilder setCluster(List<ClusterablePoint> cluster) {
		this.cluster = cluster;
		this.objs = new TreeSet<>();
		for (ClusterablePoint point :
				cluster) {
			objs.add(point.getOid());
		}
		return this;
	}

	public Convoy createConvoy() {
		return new Convoy(ts, te, objs, leftOpen, rightOpen);
	}
}