package dk.aau.convoy.model;

import com.google.common.collect.Sets;
import dk.aau.convoy.utils.ConvoyUtil;
import org.apache.flink.api.common.typeutils.base.LongComparator;

import java.util.*;

import static java.util.Collections.sort;

/**
 * Created by faisal on 8/30/16.
 */
public class Convoy implements Cloneable{
	private long ts;
	private long te;
	private Set<Long> objs;
	private boolean extended=false;
	private boolean absorbed=false;
	private boolean matched=false;
	private boolean rightOpen=false;
	private boolean leftOpen=false;
	private boolean validated=false;

	public Convoy() {
	}

	public Convoy(long ts, long te, Set<Long> objs, boolean leftOpen, boolean rightOpen) {
		this.ts = ts;
		this.te = te;
		this.objs = objs;
	}
	public Convoy(long ts, long te, List<ClusterablePoint> cluster) {
		this.ts = ts;
		this.te = te;
		this.objs = new TreeSet<>();
		for (ClusterablePoint obj:
			 cluster) {
			this.objs.add(obj.getOid());
		}
	}
	public Convoy(long ts, long te, List<ClusterablePoint> cluster, boolean leftOpen, boolean rightOpen) {
		this.ts = ts;
		this.te = te;
		this.leftOpen=leftOpen;
		this.rightOpen = rightOpen;
		this.objs = new HashSet<>();
		for (ClusterablePoint obj:
			 cluster) {
			this.objs.add(obj.getOid());
		}
	}

	public void setTime(long l){
		this.ts = l;
		this.te = l;
	}

	public int size(){
		return objs.size();
	}

	public Convoy simpleIntersection(Convoy v, int m){
		if(v!=null && v.size()>=m) {
			Set<Long> objs2 = v.getObjs();
			Set<Long> intersection = new HashSet<>();
			if (ts < v.getTs()) {
				return ConvoyUtil.matchClusters(objs, ts, v.getObjs(), v.getTe(), m);
			} else {
				return ConvoyUtil.matchClusters(v.getObjs(), v.getTs(), objs, te, m);
			}
		}
		return null;
	}


	public boolean isSubsetInObjs(Convoy v){
		if(v.getObjs().containsAll(objs))
			return true;
		else
			return false;
	}

	public boolean isSubsetInTime(Convoy v){
		if (this.getTs() >= v.getTs() && this.getTe() <= v.getTe()) {
			return true;
		} else {
		}
			return false;
	}

	public boolean isSubset(Convoy v){
		if(this.isSubsetInObjs(v) && this.isSubsetInTime(v))
			return true;
		else
			return false;
	}

	public long getTs() {
		return ts;
	}

	public void setTs(long ts) {
		this.ts = ts;
	}

	public long getTe() {
		return te;
	}

	public void setTe(long te) {
		this.te = te;
	}

	public long lifetime(){
		return te-ts+1;
	}

	public Set<Long> getObjs() {
		return objs;
	}

	public void setObjs(Set<Long> objs) {
		this.objs = objs;
	}

	public boolean isExtended() {
		return extended;
	}

	public void setExtended(boolean extended) {
		this.extended = extended;
	}

	public boolean isAbsorbed() {
		return absorbed;
	}

	public void setAbsorbed(boolean absorbed) {
		this.absorbed = absorbed;
	}

	public boolean isMatched() {
		return matched;
	}

	public void setMatched(boolean matched) {
		this.matched = matched;
	}

	public boolean isRightOpen() {
		return rightOpen;
	}

	public void setRightOpen(boolean rightOpen) {
		this.rightOpen = rightOpen;
	}

	public boolean isLeftOpen() {
		return leftOpen;
	}

	public void setLeftOpen(boolean leftOpen) {
		this.leftOpen = leftOpen;
	}

	public boolean isValidated() {
		return validated;
	}

	public void setValidated(boolean validated) {
		this.validated = validated;
	}

	public boolean isClosed(){
		if(!isLeftOpen() && !isRightOpen()){
			return true;
		}
		return false;
	}
	public boolean isOpen(){
		if(isLeftOpen() || isRightOpen()){
			return true;
		}
		return false;
	}

	//to match convoy that is non-overlapping in time
	public Convoy disjointMatchToTheRight(Convoy rightConvoy, int m){
		Set<Long> intersection = Sets.intersection(this.objs,rightConvoy.objs);
		if(intersection.size()>=m){
			return new ConvoyBuilder().setTs(this.ts).setTe(rightConvoy.getTe())
					.setObjs(intersection)
					.setLeftOpen(this.leftOpen)
					.setRightOpen(rightConvoy.isRightOpen())
					.createConvoy();
		}
		return null;
	}//to match convoy that is non-overlapping in time
	public Convoy disjointMatchToTheLeft(Convoy leftConvoy, int m){
		Set<Long> intersection = Sets.intersection(this.objs,leftConvoy.objs);
		if(intersection.size()>=m){
			return new ConvoyBuilder().setTs(leftConvoy.ts).setTe(this.te)
					.setObjs(intersection)
					.setLeftOpen(leftConvoy.leftOpen)
					.setRightOpen(this.isRightOpen())
					.createConvoy();
		}
		return null;
	}

	public String toString(){
		String s = "|objs|="+ objs.size() +", life = ["+ts+"  "+te+"], length="+(lifetime())+" , objs = [";
		int i=0;
		List<Long> objList = new ArrayList<>(objs);
		Collections.sort(objList, (o1, o2) -> ((Long) o1.longValue() > (Long) o2.longValue()) ? 1 : -1);
		for(long obj : objList){
			if(i==0){
				s = s + obj;
			}
			else{
				s = s+"  "+obj;
			}
			i++;
		}
		return s+"] :: leftOpen = "+leftOpen+" rightOpen = "+rightOpen+"\n";
	}

	public boolean hasSameObjs(Convoy v){
		Set<Long> objs2 = v.getObjs();
		if(objs.size()==objs2.size() && objs.containsAll(objs2) && objs2.containsAll(objs)){
			return true;
		}
		else
			return false;
	}
}
