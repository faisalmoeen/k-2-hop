/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.idx;

/**
 *
 * @author Analysis
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.common.geometry.MBR;
import org.common.geometry.Point;
import org.common.geometry.PolyLine;

import java.util.*;

public class IDistance
{
	public IDistance()
	{
		_tol = 0;
	}
	public void dispose()
	{
	}
	public final boolean isBuilt()
	{
		return (_dists_sorted.size() > 0);
	}
	public final void build(ArrayList<PolyLine> lns, boolean star, double e, double tol)
	{
		_tol = tol;
//C++ TO JAVA CONVERTER TODO TASK: Java does not have an equivalent to pointers to variables (in Java, the variable no longer points to the original when the original variable is re-assigned):
//ORIGINAL LINE: _lns = lns;
		_lns = lns;
		_star = star;

		int nline = _lns.size();
		MBR ext = new MBR();
		int mint = _lns.get(0).startTime();
		int maxt = _lns.get(0).endTime();
		for (int i = 0; i < nline; i++)
		{
			mint = Math.min(mint,_lns.get(i).startTime());
			maxt = Math.max(maxt,_lns.get(i).endTime());
			ext.unionWith(_lns.get(i).ext);
		}

		_ref = ext.center();

		_dists.ensureCapacity(nline);
		for (int lid = 0; lid < nline; lid++)
		{
			double d1;
			//if(star) 
			//	_ref.distLL_ST(&_lns->at(lid),&d1);
			//else
				//_ref.distLL(&_lns->at(lid),&d1);
			Point pp = _lns.get(lid).ext.center();
			d1 = PDIST(_ref,_lns.get(lid).ext.center());
			//_dists_sorted.insert(multimap<Double,Integer>.value_type(d1,lid));
                        _dists_sorted.put(d1,lid);
			_dists.add(d1);
			_diago.add(_lns.get(lid).ext.diagonal() / 2);
		}
	}
        
        public double PDIST(Point p1, Point p2){
            return Math.sqrt(PDIST2(p1,p2));
        }
        public double PDIST2(Point p1, Point p2){
                return Math.sqrt((((p2).x-(p1).x)*((p2).x-(p1).x) + ((p2).y-(p1).y)*((p2).y-(p1).y)));
        }
        
	public final void rangeQuery(int qry, double r, TreeSet<Integer> rst)
	{
		PolyLine q = (_lns.get(qry)); // query polyline
		Double R = r + q.tol + _tol;

		//int ncand=0;
		double ub = _dists.get(qry) + _diago.get(qry);
		double lb = _dists.get(qry) - _diago.get(qry);
                
//		for (multimap<Double,Integer>.iterator dist = _dists_sorted.begin(); dist != _dists_sorted.end(); dist++)
//		{
                for (Map.Entry<Double,  Collection<Integer>> entry : _dists_sorted.asMap().entrySet()) {
                        Double key = entry.getKey();
                        Collection<Integer> value =  _dists_sorted.get(key);
//                        System.out.println(key + ":" + value);
                        for (Iterator<Integer> iterator = value.iterator(); iterator.hasNext();) {
                        Integer lid = iterator.next();
                        
                    
//			int lid = dist.second;

			PolyLine l = (_lns.get(lid));

			R = r + q.tol + l.tol;
			double R2 = R * R;
			Double  d2 = new Double(0);
			q.ext.min_dist2(l.ext, d2);

			ub += R + _diago.get(lid);
			lb -= (R + _diago.get(lid));

			if ((lb <= _dists.get(lid)) && (_dists.get(lid) <= ub))
			{
				//ncand++;
				if (d2 <= R2)
				{
					if (_star)
					{
						d2 = q.distLL2_ST(l);
					}
					else
					{
						q.distLL2(l, d2);
					}

					if (d2 <= R2)
					{
						rst.add(lid);
					}
				}
			}
                    }
                }
		//double prune = (1 - ncand/(double)_dists_sorted.size())*100;
	}



	protected Point _ref = new Point(); // reference polyline
	protected double _tol;
	protected ArrayList<PolyLine> _lns;
	protected boolean _star;

	protected Multimap<Double, Integer> _dists_sorted =  ArrayListMultimap.create(); // index entries, each entry is <distance2.lid>
	protected ArrayList<Double> _diago = new ArrayList<Double>(); // index entries, each entry is <distance2.lid>
	protected ArrayList<Double> _dists = new ArrayList<Double>(); // index entries, each entry is <distance2.lid>
}

