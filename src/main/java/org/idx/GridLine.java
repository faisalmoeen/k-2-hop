/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.idx;

/**
 *
 * @author Analysis
 */

import org.common.geometry.MBR;
import org.common.geometry.PolyLine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.TreeSet;

public class GridLine extends Grid
{
	public GridLine()
	{
		_ncol = _nrow = -1;
	}
	

public final void getCells(PolyLine l, int bufsize, TreeSet<Integer> rst)
	{
		int c1 = getCell(l.ext.min());
		int c2 = getCell(l.ext.max());
		for (int i = 1; i < bufsize; i++) // get more(bufsize) cells around the cells of given polyline
		{
			c1 = leftBelowOf(c1);
			c2 = rightUpperOf(c2);
		}
		int c1col = (c1 < _ncol) ? c1 : c1 % _ncol;
		int c2col = (c2 < _ncol) ? c2 : c2 % _ncol;
		int c1row = (c1 - c1col) / _ncol;
		int c2row = (c2 - c2col) / _ncol;
		int ncol = c2col - c1col + 1;
		int nrow = c2row - c1row + 1;

		// overlapping between the bounding box of l and cells
		int lloop = l.size() - 1;
		for (int i = 0; i < nrow; i++)
		{
			for (int j = 0; j < ncol; j++)
			{
				// removing deadspace from the bounding box overlapping areas
				int cellid = c1col + j + (c1row + i) * _ncol;
				//for(int z=0; z<lloop;z++)
				//	if(_mbrs[cellid].intersect(*l[z],*l[z+1]))
						rst.add(cellid);
			}
		}
	}
	public final void getCells(MBR m, int bufsize, TreeSet<Integer> rst)
	{
		int c1 = getCell(m.min());
		int c2 = getCell(m.max());
		for (int i = 1; i < bufsize; i++) // get more(bufsize) cells around the cells of given polyline
		{
			c1 = leftBelowOf(c1);
			c2 = rightUpperOf(c2);
		}
		int c1col = (c1 < _ncol) ? c1 : c1 % _ncol;
		int c2col = (c2 < _ncol) ? c2 : c2 % _ncol;
		int c1row = (c1 - c1col) / _ncol;
		int c2row = (c2 - c2col) / _ncol;
		int ncol = c2col - c1col + 1;
		int nrow = c2row - c1row + 1;

		// overlapping between the bounding box of l and cells
		for (int i = 0; i < nrow; i++)
		{
			for (int j = 0; j < ncol; j++)
			{
				rst.add(c1col + j + (c1row + i) * _ncol);
			}
		}
	}


        public final void build(ArrayList<PolyLine> lns, boolean star, double e, double tol)
	{
//C++ TO JAVA CONVERTER TODO TASK: Java does not have an equivalent to pointers to variables (in Java, the variable no longer points to the original when the original variable is re-assigned):
//ORIGINAL LINE: _lns = lns;
		_lns = lns;
		_star = star;

		// compute the extent of the data space
		double maxarea = 0;
		for (int i = 0; i < _lns.size(); i++)
		{
			_ext.unionWith(_lns.get(i).ext);
			//maxarea = MAX(maxarea,_lns->at(i).ext.area());  // important for speeding up
		}

		//_csize = sqrt(maxarea);
		_csize = e + 2 * tol;
		//_csize = e;

		// setting the grid size
		_ncol = (int) Math.ceil(_ext.width() / _csize);
		_nrow = (int) Math.ceil(_ext.height() / _csize);

		if (_ncol * _nrow < 4)
		{
			return;
		}

		if (_nodes.size() > 0)
		{
			_nodes.clear();
		}

		for (int i = 0; i < _nrow; i++)
		{
			for (int j = 0; j < _ncol; j++)
			{
				int c = i * _ncol + j;
				MBR m = new MBR();
				cell2MBR(c,m);
				_mbrs.put(c, m);//[c] = m;
			}
		}

		for (int lid = 0; lid < _lns.size(); lid++)
		{
			TreeSet<Integer> cells = new TreeSet<Integer>();
			getCells(_lns.get(lid), 0, cells);
			for (Iterator<Integer> cell = cells.iterator(); cell.hasNext();)
			{
//				_nodes.insert(multimap<Integer,Integer>.value_type(cell.next(),lid));
                                _nodes.put(cell.next(),lid);
			}
		}
	}
	public final void rangeQuery(int qry, double r, TreeSet<Integer> rst)
	{
		PolyLine q = (_lns.get(qry)); // query polyline
		TreeSet<Integer> cands = new TreeSet<Integer>(); // candidate cells that overlap to the query range
		//int bufsize = 1 + int(r/_csize);
		getCells(q, 1, cands);

		//double prune = 1 - (double)cands.size()/_lns->size();

		for (Iterator<Integer> it = cands.iterator(); it.hasNext();) // for each candidate cell
		{
			// find all entries (points/pid) having the key (cell id)
			//tangible.Pair<GridEntry, GridEntry> entries = _nodes.equal_range(it.next());
                        Collection<Integer> entries = _nodes.get(it.next());

			//for (GridEntry entry = entries.first; entry != entries.second; entry++)
			//{
                         for (Iterator<Integer> iterator = entries.iterator(); iterator.hasNext();) {
                                Integer lid = iterator.next();
				//int lid = entry.second;
				PolyLine l = (_lns.get(lid));
				Double r2 = (r + q.tol + l.tol) * (r + q.tol + l.tol);
				Double d2 = new Double(0);
				q.ext.min_dist2(l.ext, d2);
				if (d2 <= r2)
				{
					if (_star) // CuTS*
					{
						d2 = q.distLL2_ST(l);
					}
					else // CuTS
					{
						q.distLL2(l,d2);
					}

					if (d2 <= r2)
					{
						rst.add(lid);
					}
				}
			}
		}
	}

	

	public final int rightUpperOf(int cid)
	{
		int r = rightOf(cid);
		if (r > -1)
		{
			int ru = upperOf(r);
			return (ru > -1) ? ru : r;
		}
		int u = upperOf(cid);
		return (u > -1) ? u : cid;
	}
	public final int leftBelowOf(int cid)
	{
		int l = leftOf(cid);
		if (l > -1)
		{
			int lb = belowOf(l);
			return (lb > -1) ? lb : l;
		}
		int b = belowOf(cid);
		return (b > -1) ? b : cid;
	}


	protected ArrayList<PolyLine> _lns;
	protected boolean _star;
}

