/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.idx;

/**
 *
 * @author Analysis
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.common.geometry.IPoint;
import org.common.geometry.MBR;
import org.common.geometry.Point;

import java.util.*;

public class Grid
{
	public Grid()
	{
		_ncol = _nrow = -1;
	}
//	public void dispose()
//	{
//	}


	 // compute the center coordinates of cid cell and put the result into the given point
	//void	getCenter(int cid, Point &rst); 
	public final int getCell(Point p)
	{
		int col = (int) ((p.x - _ext.minx) / _csize);
		int row = (int) ((p.y - _ext.miny) / _csize);

		return row * _ncol + col;
	}
	public final MBR getExtent()
	{
		return _ext;
	}
public final void getNeighbors(int cid, TreeSet<Integer> rst)
	{
		int r = rightOf(cid);
		if (r > -1)
		{
			rst.add(r);
		}
		int l = leftOf(cid);
		if (l > -1)
		{
			rst.add(l);
		}
		int u = upperOf(cid);
		if (u > -1)
		{
			rst.add(u);
		}
		int b = belowOf(cid);
		if (b > -1)
		{
			rst.add(b);
		}
		if (r > -1 && u > -1)
		{
			rst.add(upperOf(r)); // right-upper
		}
		if (r > -1 && b > -1)
		{
			rst.add(belowOf(r)); // right-below
		}
		if (l > -1 && u > -1)
		{
			rst.add(upperOf(l)); // left-upper
		}
		if (l > -1 && b > -1)
		{
			rst.add(belowOf(l)); // left-below
		}
	}

	public final void cell2MBR(int cell, MBR rst)
	{
		int col = (cell < _ncol) ? cell : cell % _ncol;
		int row = (cell - col) / _ncol;
		rst.minx = col * _csize + _ext.minx;
		rst.miny = row * _csize + _ext.miny;
		rst.maxx = rst.minx + _csize;
		rst.maxy = rst.miny + _csize;
	}

	public final void build(ArrayList<IPoint> pts, double cellsize)
	{
		if (pts.size() < 4)
		{
			return;
		}

		// compute the extent of the data space
		for (int i = 0; i < pts.size(); i++)
		{
			_ext.unionWith(pts.get(i).p);
		}

		_csize = cellsize;

		// setting the grid size
		_ncol = (int) Math.ceil(_ext.width() / _csize);
		_nrow = (int) Math.ceil(_ext.height() / _csize);

		if (_nodes.size() > 0)
		{
			_nodes.clear();
		}

		for (int pid = 0; pid < pts.size(); pid++)
		{
			int nodeid = getCell(pts.get(pid).p);
//			_nodes.insert(multimap<Integer,Integer>.value_type(nodeid,pid));
                        _nodes.put(nodeid,pid);
			if (!_mbrs.containsKey(nodeid))
			{
				MBR m = new MBR();
				cell2MBR(nodeid, m);
//				_mbrs.insert(TreeMap<Integer,MBR>.value_type(nodeid,m));
                                _mbrs.put(nodeid,m);
			}
		}
	}
        public final void rangeQuery(ArrayList<IPoint> pts, int qry, double radius, TreeSet<Integer> rst)
	{
		Point q = pts.get(qry).p; // query point
		int qryCell = getCell(q);
		TreeSet<Integer> cands = new TreeSet<Integer>(); // candidate cells that overlap to the query range
		cands.add(qryCell);
		getNeighbors(qryCell, cands);

		/* refinement */
		double radius2 = radius * radius;
		for (Iterator<Integer> i = cands.iterator(); i.hasNext();) // for each candidate cell
		{
			// find all entries (points/pid) having the key (nodeid/cell id)
//			tangible.Pair<GridEntry, GridEntry> entries = _nodes.equal_range(i.next());
                        Collection<Integer> entries = _nodes.get(i.next());

			//for (GridEntry entry=entries.first; entry != entries.second; entry++)
                        for (Iterator<Integer> iterator = entries.iterator(); iterator.hasNext();) {
                        Integer next = iterator.next();
                        if (PDIST2(q,pts.get(next).p) <= radius2)
				{
					rst.add(next);
				}
                    }
//			for (GridEntry entry = entries.first; entry != entries.second; ++entry)
//			{
//				int pid = entry.second;
//				if (PDIST2(q,*(pts.get(pid).p)) <= radius2)
//				{
//					rst.add(pid);
//				}
//			}
		}
	}
        public double PDIST2(Point p1, Point p2){
                return Math.sqrt((((p2).x-(p1).x)*((p2).x-(p1).x) + ((p2).y-(p1).y)*((p2).y-(p1).y)));
        }
	public final int numCells()
	{
		return _nrow * _ncol;
	}
	public final boolean isBuilt()
	{
		return (_nodes.size() > 0);
	}


	protected Multimap<Integer, Integer> _nodes = ArrayListMultimap.create();	 // index entries, each entry is <cell id, pid>
	protected TreeMap<Integer, MBR> _mbrs = new TreeMap<Integer, MBR>(); // <cell id, mbr>
	protected int _ncol; // # of columns
	protected int _nrow; // # of rows
	protected double _csize; // the width/height of each cell
	protected MBR _ext = new MBR(); // entire extent of the map

        protected final int rightOf(int cid)
	{
		int rst = cid + 1;
		return (rst % _ncol == 0) ? -1 : rst;
	}
	protected final int upperOf(int cid)
	{
		int rst = cid + _ncol;
		return (rst >= _nrow * _ncol) ? -1 : rst;
	}
	protected final int belowOf(int cid)
	{
		int rst = cid - _ncol;
		return (rst < 0) ? -1 : rst;
	}
	protected final int leftOf(int cid)
	{
		int rst = cid - 1;
		return (cid % _ncol == 0) ? -1 : rst;
	}
        
}

