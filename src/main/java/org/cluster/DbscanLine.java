/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cluster;

/**
 *
 * @author Analysis
 */

import org.common.geometry.PolyLine;
import org.cv.Convoy;
import org.idx.GridLine;
import org.idx.IDistance;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;

public class DbscanLine
{
	public DbscanLine(ArrayList<PolyLine> lines, int minlines, double eps, boolean st)
	{
//C++ TO JAVA CONVERTER TODO TASK: Java does not have an equivalent to pointers to variables (in Java, the variable no longer points to the original when the original variable is re-assigned):
//ORIGINAL LINE: _lns=lines;
		_lns = lines;
		_minlns = minlines;
		_eps = eps;
		_star = st;
		_gl = null;
		_idx = null;
	}
	/* perform dbscan algorithm */
	public final void perform()
	{
		/* initializing clusterID values */
//		_cids.resize(_lns.size(),DefineConstants.UNCLASSIFIED);
            for (PolyLine _ln : _lns) {
                _cids.add(DefineConstants.UNCLASSIFIED);
            }
		dbscan();
		buildClustSet();
	}

	public final void setIndex(GridLine gl)
	{
//C++ TO JAVA CONVERTER TODO TASK: Java does not have an equivalent to pointers to variables (in Java, the variable no longer points to the original when the original variable is re-assigned):
//ORIGINAL LINE: _gl = gl;
		_gl = gl;
	} // perform with the grid index
	public final void setIndex(IDistance id)
	{
//C++ TO JAVA CONVERTER TODO TASK: Java does not have an equivalent to pointers to variables (in Java, the variable no longer points to the original when the original variable is re-assigned):
//ORIGINAL LINE: _idx = id;
		_idx = id;
	} // perform with the grid index

	/* return the number of clustered discovered */
	public final int numClusters()
	{
		return _clsts.size();
	}

	/* return i-th cluster */
	public final Convoy getCluster(int i)
	{
		return _clsts.get(i);
	}

	/* return all the clusters discovered */
	public final ArrayList<Convoy> getClusterSet()
	{
		return _clsts;
	}


	// lid is a order of lines in _lns. i.e., _lns[lid] indicates a line segment
	// cid is a cluster id.
	protected ArrayList<PolyLine> _lns; // input point set
	protected int _minlns; // this is not MinLines, but min. # of objects
	protected double _eps; // dbscan parameter
	protected ArrayList<Integer> _cids = new ArrayList<Integer>(); // cluster id container
	protected ArrayList<Convoy> _clsts = new ArrayList<Convoy>(); // result clusters discovered
	protected boolean _star; // measuring distance with considering spatial only or
	protected GridLine _gl; // gridline index
	protected IDistance _idx; // iDistance index

	/* main dbscan function */
	protected final void dbscan()
	{
		int ClusterId = nextId(DefineConstants.NOISE); // the first cluster id begins from 0

		int n = _lns.size();
		for (int i = 0; i < n; i++)
		{
			if (_cids.get(i) == DefineConstants.UNCLASSIFIED)
			{
				if (expandCluster(i, ClusterId))
				{
					ClusterId = nextId(ClusterId);
				}
			}
		}
	}

	/* expanding a cluster */
        protected final boolean expandCluster(int Point, int ClId)
	{
		TreeSet<Integer> seeds = new TreeSet<Integer>();
		regionQuery(Point, _eps, seeds);

		/* no core point, but still can become a boundary point of another */
		if (seeds.size() < _minlns)
		{
			changeClId(Point, DefineConstants.NOISE);
			return false;
		}

		/* all points in seeds are density-reachable from Point */
		else
		{
			changeClIds(seeds, ClId); // put ClId value to all members in seeds
			seeds.remove(Point);

			while (!seeds.isEmpty())
			{
				int currentP = (seeds.iterator().next());
				TreeSet<Integer> result = new TreeSet<Integer>();
				regionQuery(currentP, _eps, result);

				if (result.size() >= _minlns)
				{
					for (Iterator<Integer> i = result.iterator(); i.hasNext();)
					{
						int resultP = i.next();

						if (clId(resultP) <= DefineConstants.NOISE)
						{
							if (clId(resultP) == DefineConstants.UNCLASSIFIED)
							{
								seeds.add(resultP);
							}
							changeClId(resultP, ClId);
						}
					}
				}
				seeds.remove(currentP);
			}
			return true;
		}
	}



	/* return a cid corresponding to oid */
	protected final int clId(int lid)
	{
		return _cids.get(lid);
	}

	/* assign cluster id value*/
	protected final int nextId(int cid)
	{
		return (cid == DefineConstants.NOISE)? DefineConstants.CLASSIFIED_START:++cid;
	}

	/* set seed point with the given id value */
	protected final void changeClId(int lid, int cid)
	{
		_cids.set(lid, cid);
	}

	/* set all seed points with the given id value */
        protected final void changeClIds(TreeSet<Integer> seeds, int cid)
	{
		for (Iterator<Integer> i = seeds.iterator(); i.hasNext();)
		{
			int current = i.next();
			if (_cids.get(current) < DefineConstants.CLASSIFIED_START) //=> result is different why?
			{
				_cids.set(current, cid);
			}
			//else
			//	cerr << "DbscanLine::changeClIds - overriding" << endl;
		}
	}


	/* perform a range query with a given center point and radius */
        protected final void regionQuery(int query, double r, TreeSet<Integer> rst)
	{
		if (_idx != null && _idx.isBuilt()) // with a grid index
		{
			_idx.rangeQuery(query,r,rst);
		}
		else if (_gl != null && _gl.isBuilt())
		{
			_gl.rangeQuery(query,r,rst);
		}
		else // without index
		{
			PolyLine q = _lns.get(query); // query
			int n = _lns.size();
			for (int i = 0; i < n; i++)
			{
				PolyLine l = _lns.get(i);
				//double r2 = (r + q->tol + l->tol) * (r + q->tol + l->tol);
				double r2 = r + q.tol + l.tol;
				r2 = r2 * r2;
				Double d2 = new Double(0);
				q.ext.min_dist2(l.ext, d2);
				if (d2 <= r2)
				{
					if (_star) // CuTS*
					{
						d2 = q.distLL2_ST(l);
					}
					else // CuTS
					{
						q.distLL2(l, d2);
					}

					if (d2 <= r2)
					{
						rst.add(i);
					}
				}
			}
		}
	}


	/* construct the result cluster set from cid array(cids) */
        protected final void buildClustSet()
	{
		int n = _lns.size();
		ArrayList<Convoy> tmp = new ArrayList<Convoy>();
		for (int i = 0; i < n; i++)
		{
			int cid = _cids.get(i);
			if (cid > DefineConstants.NOISE)
			{
				/* enlarging the storage to store new clusters */
				while (tmp.size() < cid + 1)
				{
					Convoy c = new Convoy();
					tmp.add(c);
				}
				tmp.get(cid).add(_lns.get(i).o);
			}
		}
		for (int j = 0; j < tmp.size(); j++)
		{
			if (tmp.get(j).size() >= _minlns)
			{
				_clsts.add(tmp.get(j));
			}
		}

		// computing each cluster's lifetime and start time
		/*			int lts = _lns->at(i).ext.mint;
		int lte = _lns->at(i).ext.maxt;
	
		if (_clsts[cid]._ts < 0)
		_clsts[cid]._ts = lts;
		else
		_clsts[cid]._ts = MIN(_clsts[cid]._ts,lts);
	
		_clsts[cid]._te = MAX(_clsts[cid]._te, lte);	*/
	}


}


