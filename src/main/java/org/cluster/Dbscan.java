/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cluster;

/**
 *
 * @author Analysis
 */
import org.common.geometry.IPoint;
import org.common.geometry.Point;
import org.cv.Convoy;
import org.idx.Grid;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;

public class Dbscan
{
	public Dbscan(ArrayList<IPoint> ptset, int minpts, double eps)
	{
//C++ TO JAVA CONVERTER TODO TASK: Java does not have an equivalent to pointers to variables (in Java, the variable no longer points to the original when the original variable is re-assigned):
//ORIGINAL LINE: _pts=ptset;
		_pts = ptset;
		_minpts = minpts;
		_eps = eps;
		_grid = null;
	}
	
	/* perform dbscan algorithm */
	public final void perform()
	{
            /* initializing clusterID values */
            //_cids.resize(_pts.size(),DefineConstants.UNCLASSIFIED);
            for (IPoint _pt : _pts) {
                _cids.add(DefineConstants.UNCLASSIFIED);
            }
		dbscan();
		buildClustSet();
	}

	public final void setIndex(Grid grid)
	{
//C++ TO JAVA CONVERTER TODO TASK: Java does not have an equivalent to pointers to variables (in Java, the variable no longer points to the original when the original variable is re-assigned):
//ORIGINAL LINE: _grid = grid;
		_grid = grid;
	} // perform with the grid index

	/* return the number of clustered discovered */
	public final int numClusters()
	{
		return _clsts.size();
	}

	/* return i-th cluster */
	public final Convoy getCluster(int i)
	{
		return _clsts.get(i);
	}

	/* return all the clusters discovered */
	public final ArrayList<Convoy> getClusterSet()
	{
		return _clsts;
	}


	protected ArrayList<IPoint> _pts; // input point set
	protected int _minpts; // dbscan parameter
	protected double _eps; // dbscan parameter
	protected ArrayList<Integer> _cids = new ArrayList<Integer>(); // cluster id container
	protected ArrayList<Convoy> _clsts = new ArrayList<Convoy>(); // result clusters discovered
	protected Grid _grid; // grid index

	/* main dbscan function */
	protected final void dbscan()
	{
		int ClusterId = nextId(DefineConstants.NOISE); // the first cluster id begins from 0

		for (int i = 0; i < _pts.size(); i++)
		{
			if (_cids.get(i) == DefineConstants.UNCLASSIFIED)
			{
				if (expandCluster(i, ClusterId))
				{
					ClusterId = nextId(ClusterId);
				}
			}
		}
	}

	/* expanding a cluster */
        protected final boolean expandCluster(int Point, int ClId)
	{
		TreeSet<Integer> seeds = new TreeSet<Integer>();
		regionQuery(Point, _eps, seeds);

		/* no core point */
		if (seeds.size() < _minpts)
		{
			changeClId(Point, DefineConstants.NOISE);
			return false;
		}

		/* for core points */
		else
		{
			changeClIds(seeds, ClId); // put ClId value to all members in seeds
			seeds.remove(Point);
                        //Iterator<Integer> iter = seeds.iterator();
			//while (iter.hasNext())
                        while (!seeds.isEmpty())
			{
//				Integer currentP = iter.next();
                                //pollFirst = first()+removeFirst()
                                Integer currentP = seeds.pollFirst();//iter.next();
				TreeSet<Integer> result = new TreeSet<Integer>();
				regionQuery(currentP, _eps, result);

				if (result.size() >= _minpts)
				{
					for (Iterator<Integer> i = result.iterator(); i.hasNext();)
					{
						int resultP = i.next();

						if ((clId(resultP) == DefineConstants.UNCLASSIFIED) || (clId(resultP) == DefineConstants.NOISE))
						{
							if (clId(resultP) == DefineConstants.UNCLASSIFIED)
							{
								seeds.add(resultP);
							}
							changeClId(resultP, ClId);
						}
					} // for
				} // if
//				seeds.remove(currentP);
			} // while

			return true;
		}
	}


	/* return a cid corresponding to oid */
	protected final int clId(int row)
	{
		return _cids.get(row);
	}

	/* assign cluster id value*/
	protected final int nextId(int cid)
	{
		return (cid == DefineConstants.NOISE)? DefineConstants.CLASSIFIED_START:++cid;
	}

	/* set seed point with the given id value */
	protected final void changeClId(int row, int cid)
	{
		_cids.set(row, cid);
	}

	/* set all seed points with the given id value */
        protected final void changeClIds(TreeSet<Integer> seeds, int cid)
	{
		for (Iterator<Integer> i = seeds.iterator(); i.hasNext();)
		{
			int current = i.next();
			if (_cids.get(current) < DefineConstants.CLASSIFIED_START)
			{
				_cids.set(current, cid);
			}
		}
	}


	/* perform a range query with a given center point and radius */
protected final void regionQuery(int query, double radius, TreeSet<Integer> rst)
	{
		if (_grid != null && _grid.isBuilt()) // with a grid index
		{
			_grid.rangeQuery(_pts,query,radius,rst);
		}
		else // without index
		{
			double radius2 = radius * radius;
			for (int i = 0; i < _pts.size(); i++)
			{
				if (PDIST2(_pts.get(query).p,_pts.get(i).p) <= radius2)
				{
					rst.add(i);
				}
			}
		}
	}
        public double PDIST2(Point p1, Point p2){
                return Math.sqrt((((p2).x-(p1).x)*((p2).x-(p1).x) + ((p2).y-(p1).y)*((p2).y-(p1).y)));
        }

	/* construct the result cluster set from cid array(cids) */
        protected final void buildClustSet()
	{
		//verify(); // debugging purpose
		ArrayList<Convoy> tmp = new ArrayList<Convoy>();
		for (int pid = 0; pid < _pts.size(); pid++)
		{
			int cid = _cids.get(pid);
			if (cid > DefineConstants.NOISE)
			{
				/* enlarging the space to store new clusters */
				while (tmp.size() < cid + 1)
				{
					Convoy c = new Convoy();
					tmp.add(c);
				}

				int oid = _pts.get(pid).o;
				tmp.get(cid).add(oid);
			}
		}

		for (int j = 0; j < tmp.size(); j++)
		{
			if (tmp.get(j).size() >= _minpts)
			{
				_clsts.add(tmp.get(j));
			}
		}
	}


	/* find the iterator (location) having given value */
//C++ TO JAVA CONVERTER TODO TASK: The implementation of the following method could not be found:
//	Iterator<int> find(ArrayList<int> v, int value);

}

final class DefineConstants
{
	public static final int UNCLASSIFIED = -2;
	public static final int NOISE = -1;
	public static final int CLASSIFIED_START = 0;
}

