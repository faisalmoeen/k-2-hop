package org;

import com.google.common.base.Stopwatch;
import org.common.buffer.Buffer;
import org.cv.CoherentMovingCluster;
import org.cv.CuTS;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by faisal on 1/7/17.
 */
public class ExperimentsIndividual {


    private static SortedMap<Integer,SortedMap<Integer, k2ExecutionTimeStruct>> vcodaExpResults= new TreeMap<>();

    private static char sep = ',';
    public static void main(String[] args){
        int m = Integer.parseInt(args[1]);
        int k = Integer.parseInt(args[2]);
        double e = Double.parseDouble(args[3]);
        String inputFilePath = args[4];
        String outputFilePath = args[5];
        PrintWriter pw = null;
        FileWriter fw = null;
        try {
            fw = new FileWriter(outputFilePath, true);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        BufferedWriter bw = new BufferedWriter(fw);
        pw = new PrintWriter(bw);
        Stopwatch timer = Stopwatch.createUnstarted();
        Buffer b = null;
        CoherentMovingCluster cmc = null;
        k2ExecutionTimeStruct cmcTimeStruct = null;
        CuTS c = null;

        switch (args[0]) {
            case "cmc":
                timer.reset();
                timer.start();

                b =  new Buffer(inputFilePath);
                cmc = new CoherentMovingCluster(b);
                cmc.useIndex(0);
                cmc.discover(m,k,e);

                timer.stop();
                cmcTimeStruct = new k2ExecutionTimeStruct();
                cmcTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
                pw.println(inputFilePath+sep+m+sep+k+sep+e+sep+"cmc"+sep+cmcTimeStruct.getText(sep));
                pw.flush();

                break;
            case "cmc-index":
                timer.reset();
                timer.start();

                b = new Buffer(inputFilePath);
                cmc = new CoherentMovingCluster(b);
                cmc.useIndex(1);
                cmc.discover(m,k,e);

                timer.stop();
                cmcTimeStruct = new k2ExecutionTimeStruct();
                cmcTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
                pw.println(inputFilePath+sep+m+sep+k+sep+e+sep+"cmc-index"+sep+cmcTimeStruct.getText(sep));
                pw.flush();

                break;
            case "CuTS":
                timer.reset();
                timer.start();

                b = new Buffer(inputFilePath);
                c = new CuTS(b);
                c.lambda(4);
                c.star(false);
                c.plus(false);
                c.simplify(8);
                c.discover(m,k,e);

                timer.stop();
                cmcTimeStruct = new k2ExecutionTimeStruct();
                cmcTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
                pw.println(inputFilePath+sep+m+sep+k+sep+e+sep+"CuTS"+sep+cmcTimeStruct.getText(sep));
                pw.flush();
                break;
            case "CuTS+":
                timer.reset();
                timer.start();

                b = new Buffer(inputFilePath);
                c = new CuTS(b);
                c.lambda(4);
                c.star(false);
                c.plus(true);
                c.simplify(8);
                c.discover(m,k,e);

                timer.stop();
                cmcTimeStruct = new k2ExecutionTimeStruct();
                cmcTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
                pw.println(inputFilePath+sep+m+sep+k+sep+e+sep+"CuTS+"+sep+cmcTimeStruct.getText(sep));
                pw.flush();
                break;
            case "CuTS*":
                timer.reset();
                timer.start();

                b = new Buffer(inputFilePath);
                c = new CuTS(b);
                c.lambda(4);
                c.star(true);
                c.plus(false);
                c.simplify(8);
                c.discover(m,k,e);

                timer.stop();
                cmcTimeStruct = new k2ExecutionTimeStruct();
                cmcTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
                pw.println(inputFilePath+sep+m+sep+k+sep+e+sep+"CuTS*"+sep+cmcTimeStruct.getText(sep));
                pw.flush();
                break;
            default:
        }
        pw.close();
    }
}
