/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.common.buffer;

/**
 *
 * @author Analysis
 */

import org.common.geometry.Point;

import java.util.ArrayList;

public class Trajectory extends ArrayList<Point>
{
//C++ TO JAVA CONVERTER TODO TASK: The implementation of the following method could not be found:
//	void print();
		public boolean notEqualsTo(Trajectory t)
		{
			return !((this).equalsTo(t));
		}
	public boolean equalsTo(Trajectory t)
	{
		int n = size();
		if (n != t.size())
		{
			return false;
		}
		for (int i = 0; i < n;i++)
		{
			if (this.get(i) != t.get(i))
			{
				return false;
			}
		}
		return true;
	}

}
