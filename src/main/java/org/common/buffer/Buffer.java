/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.common.buffer;

/**
 *
 * @author Analysis
 */

import org.common.geometry.IPoint;
import org.common.geometry.MBR;
import org.common.geometry.MBR3;
import org.common.geometry.Point;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Buffer
{
	public Buffer()
	{
	}
	public Buffer(String dbfile)
	{
		open(dbfile);
	}
	

	public final Database getDB()
	{
		return _db;
	}

	public final void open(String dbfile)
	{
		_avglen = 0;
		load(dbfile);
	}

	public final MBR extent()
	{
		return _ext;
	}

	public final long num_points()
	{
		return _numpts;
	}
	/* number of objects in DB */
	public final int num_obj()
	{
		return _db.size();
	}

	/* maximum number of timestamps in DB */
	public final int num_tstamp()
	{
		return _tlookup.size();
	}

	public final int num_time()
	{
		return _ext.timewidth();
	}

	public final int timeof(int timestamp)
	{
		return _ts2t.get(timestamp);
	}
	public final int tstampof(int time)
	{
		return _t2ts.get(time);
	}

	public final int nextof(int time)
	{
            //System.out.println("nextof " + time + " : " + lasttime());
//            System.out.println("nextof " + time + " : " + lasttime() + " : " + ct.getKey());
            Map.Entry<Integer,Integer> ct =  _t2ts.tailMap(time, false).firstEntry(); 
//            System.out.println("nextof " + time + " : " + lasttime() + " : " + ct.getKey());
//            Map.Entry<Integer,Integer> ct = _t2ts.floorEntry(time+1);
            //System.out.println("nextof " + time + " : " + lasttime() + " : " + ct.getKey());
//				Iterator<Integer,Integer> ct = _t2ts.upper_bound(time);
//C++ TO JAVA CONVERTER TODO TASK: Iterators are only converted within the context of 'while' and 'for' loops:
            return (time == lasttime()) ? lasttime() : ct.getKey();
	}
	public final int beforeof(int time)
	{
            //System.out.println("beforeof " + time + " : " + lasttime());
            //System.out.println("beforeof " + _t2ts);
            Map.Entry<Integer,Integer> ct =  _t2ts.headMap(time, false).lastEntry(); 
            //System.out.println("beforeof " + time + " : " + lasttime() + " : " + ct.getKey());
            //Map.Entry<Integer,Integer> ct = _t2ts.ceilingEntry(time);
//            Iterator<Integer,Integer> ct = _t2ts.lower_bound(time);
//C++ TO JAVA CONVERTER TODO TASK: Iterators are only converted within the context of 'while' and 'for' loops:
            return (time == begintime()) ? begintime() :ct.getKey();
	}

	public final int lasttime()
	{
		return _ext.maxt;
	}

	public final int begintime()
	{
		return _ext.mint;
	}


	public final TreeMap<Integer,ArrayList<IPoint>> tlookup()
	{
		return _tlookup;
	}
        public final void dumpTimeDB(String outf)
	{
		FileWriter fout = null;
            try {
                fout = new FileWriter(outf);
                //		fout.setf(ios_base.fixed, ios_base.floatfield);
//		fout.precision(1);
            fout.write( "time : # of objects having a point at the time" + "\n");
            Iterator tb = _tlookup.keySet().iterator();
            while (tb.hasNext())
            {
                Integer key = (Integer)tb.next();
                ArrayList<IPoint> current = _tlookup.get(key);
                fout.write( "t=" + key +" : " + current.size() + "\n");
            }
            fout.close();
            } catch (IOException ex) {
                Logger.getLogger(Buffer.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fout.close();
                } catch (IOException ex) {
                    Logger.getLogger(Buffer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
	}


	/* read object files as the RamBuffer size */
	public final ArrayList<IPoint> objAtTstamp(int tstamp)
	{
//            System.out.println("objAtTstamp : " + tstamp);
//            System.out.println("_ts2t.get(tstamp) : "+ _ts2t);
//            System.out.println("_ts2t.get(tstamp) : " + _ts2t.get(tstamp));
//            System.out.println("__tlookup.get(_ts2t.get(tstamp)) " + _tlookup.get(_ts2t.get(tstamp)));
		return _tlookup.get(_ts2t.get(tstamp));
	}
	public final ArrayList<IPoint> objAtTime(int time)
	{
		return _tlookup.get(time);
	}


	public final void save(int ts, int num, String rst, int npoint)
	{
		FileOutputStream fout = null;
            try {
                fout = new FileOutputStream(rst, true);
                //fout.setf(ios_base.fixed, ios_base.floatfield);
                //Database.iterator it = _db.begin();
                Set keyset = _db.keySet();
                Iterator iter = keyset.iterator();
                while (iter.hasNext())
                {
                    int oid = (Integer)iter.next();
                    
                    //Trajectory trj = (Trajectory)iter.next();
                    Trajectory trj = (Trajectory)_db.get(oid);
                    for (int i = ts; i < ts + num; i++)
                    {
                        if (i >= trj.size())
                        {
                            break;
                        }
                        String line = oid + " "  +trj.get(i).t + " " + trj.get(i).x + " " + trj.get(i).y;
                        fout.write( line.getBytes());// + "\n";
                    }
                    //it++;
                }
//                fout.close();
            } catch (Exception ex) {
                Logger.getLogger(Buffer.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fout.close();
                } catch (IOException ex) {
                    Logger.getLogger(Buffer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
	}

//C++ TO JAVA CONVERTER TODO TASK: The implementation of the following method could not be found:
//	static void extractSameTime(tangible.RefObject<String> dbfile, tangible.RefObject<String> outf);
//C++ TO JAVA CONVERTER TODO TASK: The implementation of the following method could not be found:
//	static void beijing(tangible.RefObject<String> dbfile, tangible.RefObject<String> outf);
//C++ TO JAVA CONVERTER TODO TASK: The implementation of the following method could not be found:
//	static void beijing(tangible.RefObject<String> dbfile, tangible.RefObject<String> newdbfile, int numobj);
//C++ TO JAVA CONVERTER TODO TASK: The implementation of the following method could not be found:
//	static void truck(tangible.RefObject<String> dbfile, tangible.RefObject<String> outf);
//C++ TO JAVA CONVERTER TODO TASK: The implementation of the following method could not be found:
//	static void akta(tangible.RefObject<String> dir, tangible.RefObject<String> outf, int year, int month, int day_s, int day_e);
//C++ TO JAVA CONVERTER TODO TASK: The implementation of the following method could not be found:
//	static void akta2(tangible.RefObject<String> dir, tangible.RefObject<String> outf, int year, int month, int day_s, int day_e);

//C++ TO JAVA CONVERTER TODO TASK: The implementation of the following method could not be found:
//	static void timestat(tangible.RefObject<String> inf, tangible.RefObject<String> outf);
	
    public static void matlab(ArrayList<Trajectory> trjs, String outf)
	{
		matlab(trjs, outf, 2, -1);
	}
	public static void matlab(ArrayList<Trajectory> trjs, String outf, int dim, int length)
	{
		FileWriter fout = null;
            try {
                fout = new FileWriter(outf, true);
//                fout.setf(ios_base.fixed, ios_base.floatfield);
//                fout.precision(0);
                String color = "krbgcm";
                for (int k = 0; k < trjs.size(); k++)
                {
                    Trajectory trj = trjs.get(k);
                    int len = (length < 0) ? trj.size() : Math.min(length,trj.size());
                    
                    fout.write( "x" + k + "=[");
                    for (int i = 0; i < len; i++)
                    {
                        fout.write( trj.get(i).x + " ");
                        //fout << trj[i].x << " ";
                    }
                    fout.write( "]" + "\n");
//                    fout << "]" << "\n";
                     fout.write( "y" + k + "=[");
//                    fout << "y" << k << "=[";
                    for (int i = 0; i < len; i++)
                    {
                        fout.write( trj.get(i).y + " ");
//                        fout << trj[i].y << " ";
                    }
                     fout.write( "]" + "\n");
//                    fout << "]" << "\n";
                    
                    if (dim == 3)
                    {
                        fout.write(  "t" + k + "=[");
                        //fout << "t" << k << "=[";
                        for (int i = 0; i < len; i++)
                        {
                              fout.write( trj.get(i).t + " ");
//                            fout << trj[i].t << " ";
                        }
                        fout.write(  "]" +"\n");
//                        fout << "]" << "\n";
                    }
                }
                // ex => plot(x0,y0,'k',x1,y1,'r',x2,y2,'b'
                fout.write( "\nplot(");
                for (int i = 0; i < trjs.size(); i++)
                {
                    int rand = i % 6; // random color
                    fout.write("x" + i + ",y" + i);
//                    fout << "x" << i << ",y" << i;
                    if (dim == 3)
                    {
                        fout.write(",t" + i);
                        //fout << ",t" << i;
                    }
                    
                    fout.write( ",'" + color.charAt(i % 6) + ".-'"); // shape of each point / line. see the option
                    if (i < trjs.size() - 1)
                    {
                        fout.write( ",");
                    }
                }
                fout.write( "),xlabel('X'),ylabel('Y'),");
                if (dim == 3)
                {
                    fout.write( "zlabel('T'),");
                }
                // ex, legend('Trj 1','Trj 2','Trj 3','Trj 4')
                
                fout.write( "legend(");
                for (int i = 0; i < trjs.size(); i++)
                {
                    fout.write( "'trajectory " + i + "'");
                    if (i < trjs.size() - 1)
                    {
                        fout.write( ",");
                    }
                }
                //fout << "),grid" << endl;
                fout.write( ")" + "\n");
                fout.close();
            } catch (IOException ex) {
                Logger.getLogger(Buffer.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fout.close();
                } catch (IOException ex) {
                    Logger.getLogger(Buffer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
	}


//	public static class IPoint2 extends Point
//	{
//		public int oid;
//                public Point p;
//                public IPoint2(int oid, Point p){
//                    this.oid = oid;
//                    this.p = p;
//                }
//	}

	public long _timedb_t = Calendar.getInstance().getTimeInMillis();


	protected Database _db = new Database();
	protected MBR3 _ext = new MBR3(); // extent of the map
	protected int _avglen;
	protected long _numpts;

	protected TreeMap<Integer,ArrayList<IPoint>> _tlookup = new TreeMap<Integer,ArrayList<IPoint>>(); //<time, (oid,point pointer)>
	protected ArrayList<Integer> _ts2t = new ArrayList<Integer>(); // timestamp to time
	protected TreeMap<Integer,Integer> _t2ts = new TreeMap<Integer,Integer>(); // time to timestamp
	/* load all data into the memory */
        public void load(String dbfile)
	{
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new FileReader(dbfile));
                
                //byte[] buf = new byte[1024];
                String buf = null;
                _numpts = 0;
                _avglen = 0;
//                of = new FileInputStream(dbfile);
                if (reader == null)
                {
                    System.out.println( "Buffer::load - invalid filename \"" +dbfile+ "\n");
                    return;
                }
                LineNumberReader lineReader = new LineNumberReader(reader);
                TreeSet<Integer> times = new TreeSet<Integer>();
                 Trajectory trj = null;
                while (true)
                {
                    buf = lineReader.readLine();
//                    System.out.println( "Buffer::read " + buf);
                    if(buf == null)
                        break;
                    Point p = new Point();
                    int oid = p.parse(buf);
                    //System.out.println( "Buffer::Point " + p.t+":" +p.x+":" +p.y);
                    if (oid < 0)
                    {
                        System.out.println( "Buffer::load - incorrect input data at line"+ _numpts + "\n");
                        continue;
                    }
                    if (_db.get(oid) ==null ) // no id exists
                    {
                        trj = new Trajectory();
                        trj.add(p);
                        _db.put(oid,trj);
                    }
                    else // already exists
                    {
//                        _db.put(oid,trj);
                        _db.get(oid).add(p);
                    }
                    
                    _ext.unionWith(p); // data extent
                    times.add(p.t);
                    
                    if (_numpts++%100000 == 0 && _numpts > 1)
                    {
                        System.out.print(_numpts - 1);
                        System.out.print(" points have been loaded");
                        System.out.print("\n");
                    }
                }
                //_ts2t.reserve(times.size());
                Iterator<Integer> time = times.iterator();
                int ts = 0;
                while (time.hasNext())
                {
                    int current = time.next();
                    _ts2t.add(current);
                    _t2ts.put(current,ts);
                    ts++;
                }
                 buildtimedb();
                lineReader.close();
                summary();
            } catch (Exception ex) {
                Logger.getLogger(Buffer.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
               
                 
                
            }
	}
        
        public final void summary()
	{
		System.out.print("N=");
		System.out.print(num_obj());
		System.out.print(", T=");
		System.out.print(num_tstamp());
		System.out.print(", avg trj=");
		System.out.print(avglen());
		System.out.print(", num_points=");
		System.out.print(_numpts);
		System.out.print("\n");
	}
        
        public final int avglen()
	{
		if (_avglen > 0)
		{
			return _avglen;
		}
		Iterator di = _db.keySet().iterator();
		int len = 0;
		while (di.hasNext())
		{
			len += _db.get(di.next()).size();
			
		}
		_avglen = (int) len / _db.size();
		return _avglen;
	}

//	public final void summary(ofstream fo)
//	{
//		fo << "N=" << num_obj() << ", T=" << num_tstamp() << ", avg trj=" << avglen() << ", num_points=" << _numpts << "\n";
//	}

        public long buildtimedb()
	{
		long s = Calendar.getInstance().getTimeInMillis();
		long lensum = 0;
		Iterator di = _db.keySet().iterator();
		int maxobj = 0;
		while (di.hasNext())
		{
//			Trajectory trj = (di.second);
                        int oid = (Integer) di.next();
                        Trajectory trj = _db.get(oid);
                        
			int n = trj.size();
			
			lensum += n;
			for (int i = 0; i < n; i++)
			{
				Point p1 = trj.get(i);
				Point p2 = (i == n - 1) ? p1 : trj.get(i + 1);
                                //System.out.println("Checking in _tlookup" + p1.t);
				if (_tlookup.get(p1.t) == null )
				{
					//_tlookup[p1.t].reserve(_db.size());
                                    //System.out.println("Adding in _tlookup" + p1.t);
                                    _tlookup.put(p1.t, new ArrayList<IPoint>());
				}
    
				_tlookup.get(p1.t).add(new IPoint(oid,p1));
    
				maxobj = Math.max(_tlookup.get(p1.t).size(),maxobj);
    
				// put intermediate points between p1 and p2 if any
                                //System.out.println("Before while " + p1.t + " : " +  p2.t);
				int t = nextof(p1.t);
                                //System.out.println("while " + p1.t + " : " + t + " : " + p2.t);
				while (t < p2.t) // split
				{
					double d = (double)(t - p1.t) / (p2.t - p1.t);
                                        Point tmp = new Point(t,p1.x + d * (p2.x - p1.x),p1.y + d * (p2.y - p1.y));
					_splits.add(new Point(t,p1.x + d * (p2.x - p1.x),p1.y + d * (p2.y - p1.y)));
					
					_tlookup.get(t).add(new IPoint(oid,tmp));
					t = nextof(t);
    
					maxobj = Math.max(_tlookup.get(t).size(),maxobj);
				}
			}
//			di++;
		}
		_avglen = (int) lensum / _db.size();
		return Calendar.getInstance().getTimeInMillis() - s;
	}


        

	protected LinkedList<Point> _splits = new LinkedList<Point>();


}



