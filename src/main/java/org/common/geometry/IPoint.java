/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.common.geometry;

/**
 *
 * @author Analysis
 */
public class IPoint
{
	public int o; // object id
	public Point p; // point pointer
	public IPoint(int oid, Point pt)
	{
		o = oid;
//C++ TO JAVA CONVERTER TODO TASK: Java does not have an equivalent to pointers to variables (in Java, the variable no longer points to the original when the original variable is re-assigned):
//ORIGINAL LINE: p=pt;
		p = pt;
	}
	public IPoint()
	{
	}
}
