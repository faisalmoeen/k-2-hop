/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.common.geometry;

import java.util.ArrayList;

/**
 *
 * @author Analysis
 */
public class MBR
{
	public MBR()
	{
		haspoint = false;
	}
	public MBR(double smallx, double smally, double bigx, double bigy)
	{
		minx = smallx;
		miny = smally;
		maxx = bigx;
		maxy = bigy;
		haspoint = true;
	}
	public MBR(Point2D min, Point2D max)
	{
		minx = min.x;
		miny = min.y;
		maxx = max.x;
		maxy = max.y;
		haspoint = true;
	}
	public MBR(Point min, Point max)
	{
		minx = min.x;
		miny = min.y;
		maxx = max.x;
		maxy = max.y;
		haspoint = true;
	}
	public void dispose()
	{
	}

	public double minx;
	public double miny;
	public double maxx;
	public double maxy;
	public final int size()
	{
		return 4;
	}
public void unionWith(Point p)
	{
		if (haspoint)
		{
			minx = Math. min(minx, p.x);
			maxx = Math. max(maxx, p.x);
			miny = Math. min(miny, p.y);
			maxy = Math. max(maxy, p.y);
		}
		else
		{
			minx = maxx = p.x;
			miny = maxy = p.y;
			haspoint = true;
		}
	}
	public final void unionWith(MBR mbr)
	{
		if (haspoint)
		{
			minx = Math. min(minx,mbr.minx);
			miny = Math. max(miny,mbr.miny);
			maxx = Math. min(maxx,mbr.maxx);
			maxy = Math. max(maxy,mbr.maxy);
		}
		else
		{
			minx = mbr.minx;
			miny = mbr.miny;
			maxx = mbr.maxx;
			maxy = mbr.maxy;
			haspoint = true;
		}
	}
//	public final void unionWith(Point pt)
//	{
//		unionWith(pt);
//	}
	public final void unionWith(ArrayList<Point> pts)
	{
		for (int i = 0; i < pts.size(); i++)
		{
			unionWith(pts.get(i));
		}
	}
	public final double area()
	{
		return (maxx - minx) * (maxy - miny);
	}
	public final double width()
	{
		return maxx - minx;
	}
	public final double height()
	{
		return maxy - miny;
	}
	public final double diagonal()
	{
		return Math.sqrt((maxy - miny) * (maxy - miny) + (maxx - minx) * (maxx - minx));
	}
	public final Point center()
	{
		return new Point(-1,(minx + maxx) / 2,(miny + maxy) / 2);
	}

	public Point min()
	{
		return new Point(minx,miny);
	}
	public Point max()
	{
		return new Point(maxx,maxy);
	}

	public final boolean intersect(Point p1, Point p2)
	{
		double st;
		double et;
		double fst = 0;
		double fet = 1;
//C++ TO JAVA CONVERTER TODO TASK: Pointer arithmetic is detected on this variable, so pointers on this variable are left unchanged:
		double bmin = minx;
//C++ TO JAVA CONVERTER TODO TASK: Pointer arithmetic is detected on this variable, so pointers on this variable are left unchanged:
		double bmax = maxx;
//C++ TO JAVA CONVERTER TODO TASK: Pointer arithmetic is detected on this variable, so pointers on this variable are left unchanged:
		double si = p1.x;
//C++ TO JAVA CONVERTER TODO TASK: Pointer arithmetic is detected on this variable, so pointers on this variable are left unchanged:
		double ei = p2.x;
		for (int i = 0; i < 2; i++)
		{
			if (si < ei)
			{
				if (si > bmax || ei < bmin)
				{
					return false;
				}
				double di = ei - si;
				st = (si < bmin)? (bmin - si) / di: 0;
				et = (ei > bmax)? (bmax - si) / di: 1;
			}
			else
			{
				if (ei > bmax || si < bmin)
				{
					return false;
				}
				double di = ei - si;
				st = (si > bmax)? (bmax - si) / di: 0;
				et = (ei < bmin)? (bmin - si) / di: 1;
			}
			if (st > fst)
			{
				fst = st;
			}
			if (et < fet)
			{
				fet = et;
			}
			if (fet < fst)
			{
				return false;
			}
			bmin++;
			bmax++;
			si++;
			ei++;
		}
		return true;
	}

	public final void min_dist(MBR m, RefObject<Double> rst)
	{
//		min_dist(m, rst);
//	}
//	public final void min_dist(MBR m, RefObject<Double> rst)
//	{
		double d=0;
		Double tempRef_d = new Double(d);
		min_dist2(m, tempRef_d);
		d = tempRef_d;
		rst.argValue = Math.sqrt(d);
	}
	public final void min_dist2(MBR m, Double rst)
	{
//		min_dist2(m, rst);
//	}
//        public final void MBR.min_dist2(MBR m, tangible.RefObject<Double> rst)
//{
		double xd;
		double yd;
		if (maxx < m.minx)
		{
			xd = m.minx - maxx;
		}
		else if (minx > m.maxx)
		{
			xd = minx - m.maxx;
		}
		else // overlapping
		{
			xd = 0;
		}
		if (maxy < m.miny)
		{
			yd = m.miny - maxy;
		}
		else if (miny > m.maxy)
		{
			yd = miny - m.maxy;
		}
		else // overlapping
		{
			yd = 0;
		}
		rst = xd * xd + yd * yd;
}

	public final void max_dist2(MBR m, Double rst)
	{
		double xd = ((maxx) > (m.maxx) ? (maxx) : (m.maxx))- ((minx) < (m.minx) ? (minx) : (m.minx));
		double yd = ((maxy) > (m.maxy) ? (maxy) : (m.maxy)) - ((miny) < (m.miny) ? (miny) : (m.miny));
		rst = xd * xd + yd * yd;

		rst = xd * xd + yd * yd;
	}
         
public final boolean contain(Point pt)
{
	return ((pt.x >= minx) && (pt.x <= maxx) && (pt.y >= miny) && (pt.y <= maxy));
}

	protected boolean haspoint; // if this MBR does not contain any point, it is false
}
