/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.common.geometry;

/**
 *
 * @author Analysis
 */
public class Point2D
{
	public Point2D()
	{
	}
	public Point2D(double xx, double yy)
	{
		x = xx;
		y = yy;
	}
	
        public double x;
	public double y;

//C++ TO JAVA CONVERTER NOTE: This 'copyFrom' method was converted from the original copy assignment operator:
//ORIGINAL LINE: Point2D Point2D::operator =(const Point& p)
	public final Point2D copyFrom(Point p)
	{
		return new Point2D(x = p.x, y = p.y);
	}
	public Point2D add(Point2D p)
	{
		return new Point2D(x + p.x, y + p.y);
	}
	public Point2D subtract(Point2D p)
	{
		return new Point2D(x - p.x, y - p.y);
	}
	public Point2D add(Point p)
	{
		return new Point2D(x + p.x, y + p.y);
	}
	public Point2D subtract(Point p)
	{
		return new Point2D(x - p.x, y - p.y);
	}
	public Point2D multiply(double c)
	{
		return new Point2D(x * c, y * c);
	}
	public Point2D divide(double c)
	{
		return new Point2D(x / c, y / c);
	}
}