/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.common.geometry;

/**
 *
 * @author Analysis
 */

import java.util.ArrayList;

public class PolyLine extends ArrayList<Point>
{
	public int o;
	public double tol;
	public MBR ext = new MBR();
        public double LARGE_NUM = 1.7976931348623158e+308;
        public double SMALL_NUM = 0.0000001;
	public PolyLine()
	{
	}
	public PolyLine(int oid)
	{
		o = oid;
	}
        @Override
	public final boolean add(Point p)
	{
		ext.unionWith(p);
		return super.add(p);
	}
	public final void push_back(int oid, Point p1, Point p2)
	{
		o = oid;
		add(p1);
		add(p2);
	}
	public final MBR mbr()
	{
		return ext;
	}
	public final void computeExtent()
	{
		for (int i = 0; i < size();i++)
		{
			ext.unionWith(this.get(i));
		}
	}
	public final double totalLength()
	{
		double len = 0;
		if (size() > 1)
		{
                    for (int i = 0; i < size() - 1;i++)
                    {
                        len += Math.sqrt((this.get(i + 1).x - this.get(i).x) * (this.get(i + 1).x - this.get(i).x) 
                                + (this.get(i + 1).y - this.get(i).y) * (this.get(i + 1).y - this.get(i).y));
                    }
                }
                return len;
	}
        
        public double PDIST(Point p1, Point p2)
        {
            return (((p2).x-(p1).x)*((p2).x-(p1).x) + ((p2).y-(p1).y)*((p2).y-(p1).y));
        }
//        public double max(double x, double y){
//            return ( (x) > (y) ? (x) : (y) );
//        }
//        public double min(double x, double y){
//            return ( (x) > (y) ? (x) : (y) );
//        }
        
	public final double avgLength()
	{
		return totalLength() / size();
	}
	public final double maxSegLength()
	{
		double maxlen = 0;
		if (size() > 1)
		{
                    for (int i = 0; i < size() - 1;i++)
                    {
                        maxlen = Math.max(maxlen,PDIST((this.get(i)),((this.get(i + 1)))));
                    }
		}
                return maxlen;
	}
	public final int startTime()
	{
		return this.get(0).t;
	}
	public final int endTime()
	{
		return this.get(size() - 1).t;
	}

	public static Point split(int t, Point p1, Point p2)
	{
		double d = (p2.t == p1.t)? 0 : (double)(t - p1.t) / (p2.t - p1.t);
		return new Point(t,p1.x + d * (p2.x - p1.x),p1.y + d * (p2.y - p1.y));
	}
	// distance between polylines
//	public final void distLL(PolyLine l, RefObject<Double> rst)
//	{
//		distLL(l, rst);
//	}
	public final void distLL(PolyLine l, RefObject<Double> rst)
	{
		double d=0;
		Double tempRef_d = new Double(d);
		distLL2(l, tempRef_d);
		d = tempRef_d;
		rst.argValue = Math.sqrt(d);
	}
//	public final void distLL2(PolyLine l, RefObject<Double> rst)
//	{
//		distLL2(l, rst);
//	}
	public final void distLL2(PolyLine l, Double rst)
	{
		rst = LARGE_NUM;
		double d=0;
		int n1 = size() - 1;
		int n2 = l.size() - 1;
		int lastj = 0;
		for (int i = 0; i < n1; i++)
		{
			for (int j = lastj; j < n2; j++)
			{
				if (l.get(j + 1).t < this.get(i).t)
				{
					break;
				}

				if (this.get(i + 1).t < l.get(j).t)
				{
					continue;
				}
				else
				{
					lastj = j;
				}
				d =dist_ll2(this.get(i), this.get(i + 1), l.get(j), l.get(j + 1));
				rst = Math.min(rst,d);
			}
		}
	}
        
//        public final void dist_ll2(Point p1, Point p2, Point q1, Point q2, RefObject<Double> rst)
//	{
//		dist_ll2(p1, p2, q1, q2, rst);
//	}
        public double dot(Point2D u, Point2D v){
            return  ((u).x * (v).x + (u).y * (v).y);
        }
        
	public double dist_ll2(Point p1, Point p2, Point q1, Point q2)
	{
            double rst = 0;
		Point2D u = new Point2D();
		Point2D v = new Point2D();
		Point2D w = new Point2D();
		Point2D dP = new Point2D();
		u = new Point2D().copyFrom(p2.subtract(p1));
                
		v = new Point2D().copyFrom(q2.subtract(q1));
		w = new Point2D().copyFrom(p1.subtract(q1));
		double a = dot(u,u); // always >= 0
		double b = dot(u,v);
		double c = dot(v,v); // always >= 0
		double d = dot(u,w);
		double e = dot(v,w);
		double D = a * c - b * b; // always >= 0
		double sc; // sc = sN / sD, default sD = D >= 0
		double sN;
		double sD = D;
		double tc; // tc = tN / tD, default tD = D >= 0
		double tN;
		double tD = D;

		// compute the line parameters of the two closest points
		if (D < SMALL_NUM)
		{ // the lines are almost parallel
			sN = 0.0; // force using point P0 on segment S1
			sD = 1.0; // to prevent possible division by 0.0 later
			tN = e;
			tD = c;
		}
		else
		{ // get the closest points on the infinite lines
			sN = (b * e - c * d);
			tN = (a * e - b * d);
			if (sN < 0.0)
			{ // sc < 0 => the s=0 edge is visible
				sN = 0.0;
				tN = e;
				tD = c;
			}
			else if (sN > sD)
			{ // sc > 1 => the s=1 edge is visible
				sN = sD;
				tN = e + b;
				tD = c;
			}
		}

		if (tN < 0.0)
		{ // tc < 0 => the t=0 edge is visible
			tN = 0.0;
			// recompute sc for this edge
			if (-d < 0.0)
			{
				sN = 0.0;
			}
			else if (-d > a)
			{
				sN = sD;
			}
			else
			{
				sN = -d;
				sD = a;
			}
		}
		else if (tN > tD)
		{ // tc > 1 => the t=1 edge is visible
			tN = tD;
			// recompute sc for this edge
			if ((-d + b) < 0.0)
			{
				sN = 0;
			}
			else if ((-d + b) > a)
			{
				sN = sD;
			}
			else
			{
				sN = (-d + b);
				sD = a;
			}
		}
		// finally do the division to get sc and tc
		sc = (Math.abs(sN) < SMALL_NUM ? 0.0 : sN / sD);
		tc = (Math.abs(tN) < SMALL_NUM ? 0.0 : tN / tD);

		// get the difference of the two closest points
		dP = w.add((u.multiply(sc)).subtract(v.multiply( tc))); // = S1(sc) - S2(tc)
		rst = dot(dP, dP);
		if (rst > 0.0 && rst < SMALL_NUM)
		{
			rst = 0.0;
		}
                return rst;
	}


//        public final double distLL_ST(PolyLine l)
//        {
//               return  distLL_ST(l);
//        }
	public final double distLL_ST(PolyLine l)
	{
		double d = 0;
		Double tempRef_d = new Double(d);
		distLL2_ST(l);
		d = tempRef_d;
		return Math.sqrt(d);
	}
//	public final double distLL2_ST(PolyLine l, Double rst)
//	{
//		distLL2_ST(l, rst);
//	}
	public final double distLL2_ST(PolyLine l)
	{
		double d=0;
		Double rst = LARGE_NUM;
		int n1 = size() - 1;
		int n2 = l.size() - 1;
		int lastj = 0;
		for (int i = 0; i < n1; i++)
		{
			for (int j = lastj; j < n2; j++)
			{
				if (l.get(j + 1).t < this.get(i).t)
				{
					break;
				}

				if (this.get(i + 1).t < l.get(j).t)
				{
					continue;
				}
				else
				{
					lastj = j;
				}

				Double tempRef_d = new Double(d);
				tempRef_d = cpa_distance2(this.get(i), this.get(i + 1), l.get(j), l.get(j + 1));
				d = tempRef_d;
				rst = Math.min(rst,d);
			}
		}
                return rst;
	}

	// distance of closest point of approach (CPA)
	public final double cpa_distance2(Point p1, Point p2, Point q1, Point q2)
	{
		double ct,rst; // cpa time
		int dt1 = (p2.t - p1.t);
		int dt2 = (q2.t - q1.t);
		Point2D v1 = new Point2D(.0,.0);
		Point2D v2 = new Point2D(.0,.0);
		Point2D dv = new Point2D();
		if (dt1 > 0)
		{
			v1 = new Point2D().copyFrom((p2.subtract(p1)).divide((double)dt1)); // velocity of p1-p2
		}
		if (dt2 > 0)
		{
                    v2 = new Point2D().copyFrom((q2.subtract(q1)).divide((double)dt2));
			//v2 = (q2 - q1) / (double)dt2; // velocity of q1-q2
		}
		dv = v1.subtract( v2);
		double dv2 = dot(dv,dv);

		int mint = Math.max(p1.t,q1.t); // minimum overlapping time
		int maxt = Math.min(p2.t,q2.t); // maximum overlapping time

		if (dv2 < SMALL_NUM) // the tracks are almost parallel, not to have an overflow
		{
			ct = mint; // any time is ok.  Use time 0.
		}
		else
		{
			Point2D w0 = new Point2D();
			w0 = new Point2D().copyFrom(p1.subtract(q1));
			ct = (-dot(w0,dv)) / (double)dv2;
		}

		if (ct < mint)
		{
			ct = mint;
		}
		else if (ct > maxt)
		{
			ct = maxt;
		}

		rst = PDIST2(((v1.multiply(ct - p1.t)).add(p1)),((v2.multiply(ct - q1.t)).add(q1))); // distance at CPA
		if (rst > 0 && rst < SMALL_NUM)
		{
			rst = 0.0;
		}
                return rst;
	}

public double PDIST2(Point2D p1, Point2D p2){
        return Math.sqrt((((p2).x-(p1).x)*((p2).x-(p1).x) + ((p2).y-(p1).y)*((p2).y-(p1).y)));
}


}
