/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.common.geometry;

/**
 *
 * @author Analysis
 */

public class Point
{
	public Point()
	{
		t = -1;
	}
	public Point(String buf)
	{
		parse(buf);
	}
	public Point(double xx, double yy)
	{
		x = xx;
		y = yy;
	}
	public Point(int tt, double xx, double yy)
	{
		t = tt;
		x = xx;
		y = yy;
	}
	public void dispose()
	{
	}

	public int t;
	public double x;
	public double y;

	public boolean equalsTo(Point p)
	{
		return (t == p.t && x == p.x && y == p.y);
	}
	public boolean notEqualsTo(Point p)
	{
		return !(this.equalsTo(p));
	}
	public Point add(Point p)
	{
		return new Point(x + p.x, y + p.y);
	}
	public Point add(double c)
	{
		return new Point(x + c, y + c);
	}
	public Point subtract(Point p)
	{
		return new Point(x - p.x, y - p.y);
	}
	public Point multiply(double c)
	{
		return new Point(x * c, y * c);
	}
	public Point divide(double c)
	{
		return new Point(x / c, y / c);
	}
	public final int parse(String buf)
	{
//		ArrayList<String> tok = Function.tokenize(buf.argValue," \t");
//                String[] tok = buf.split(" \t");
                String[] tok = buf.split(",");
		if (tok.length!= 4)
		{
			return -1;
		}

		this.t = Integer.parseInt(tok[1]);
		this.x = Double.parseDouble(tok[2]);
		this.y = Double.parseDouble(tok[3]);
		return Integer.parseInt(tok[0]);
	}
	public final boolean parse(String buf, String oid)
	{
//		ArrayList<String> tok = Function.tokenize(buf.argValue," \t");
                String[] tok  = buf.split(" \t");
//		if (tok.size() != 4)
                if (tok.length!= 4)
		{
			return false;
		}

		t = Integer.parseInt(tok[1]);
		x = Double.parseDouble(tok[2]);
		y = Double.parseDouble(tok[3]);
		oid = tok[0];
		return true;
	}
}

