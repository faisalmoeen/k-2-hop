/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.common.geometry;

/**
 *
 * @author Analysis
 */
public class MBR3 extends MBR
{
	public MBR3()
	{
		haspoint = false;
	}
	public void dispose()
	{
	}

	public int mint;
	public int maxt;
        @Override
	public final void unionWith(Point p)
	{
		if (haspoint)
		{
			mint = ((mint) < (p.t) ? (mint) : (p.t));
			maxt = ((maxt) > (p.t) ? (maxt) : (p.t));
			minx = ((minx) < (p.x) ? (minx) : (p.x));
			maxx = ((maxx) > (p.x) ? (maxx) : (p.x));
			miny = ((miny) < (p.y) ? (miny) : (p.y));
			maxy = ((maxy) > (p.y) ? (maxy) : (p.y));

		}
		else
		{
			mint = maxt = p.t;
			minx = maxx = p.x;
			miny = maxy = p.y;
			haspoint = true;
		}
	}
	public final void unionWith(MBR3 mbr)
	{
		if (haspoint)
		{
			minx = ((minx) < (mbr.minx) ? (minx) : (mbr.minx));
			miny = ((miny) < (mbr.miny) ? (miny) : (mbr.miny));
			mint = ((mint) < (mbr.mint) ? (mint) : (mbr.mint));
			maxx = ((maxx) > (mbr.maxx) ? (maxx) : (mbr.maxx));
			maxy = ((maxy) > (mbr.maxy) ? (maxy) : (mbr.maxy));
			maxt = ((maxt) > (mbr.maxt) ? (maxt) : (mbr.maxt));

		}
		else
		{
			mint = mbr.mint;
			minx = mbr.minx;
			miny = mbr.miny;
			maxt = mbr.maxt;
			maxx = mbr.maxx;
			maxy = mbr.maxy;
			haspoint = true;
		}
	}
	public final int timewidth()
	{
		return maxt - mint;
	}
	public final Point min()
	{
		return new Point(mint,minx,miny);
	}
	public final Point max()
	{
		return new Point(maxt,maxx,maxy);
	}

	private boolean haspoint; // if this MBR does not contain any point, it is false
}

