/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.common;

/**
 *
 * @author Analysis
 */

import org.common.geometry.Point;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Function
{
	public Function()
	{
	}
	public void dispose()
	{
	}

	/*
	 *	random functions
	 */
	public static double drand48()
	{
		return uniform(0.0, 1.0);
	}
	public static double uniform(double _min, double _max)
	{
		int int_r = RandomNumbers.nextNumber();
		int base = 0x7fff - 1;
		double f_r = ((double) int_r) / base;
		return (_max - _min) * f_r + _min;
	}
public static int uniform(int _min, int _max)
	{
		return (int)uniform((double) _min, (double) _max);
	}
	public static int uniform(int _min, int _max, int avoid)
	{
		double rst = 0;
		while (rst == avoid || rst < _min || rst> _max)
		{
			rst = uniform((double) _min, (double) _max);
		}

		return (int)rst;
	}

	public static double new_uniform(int _d_num)
	{
		double base = 1;
		double sum = 0;
		for (int i = 0; i < _d_num; i++)
		{
			int digit = (int)uniform(0, 10);
			if (digit == 10)
			{
				digit = 9;
			}
			sum += base * digit;
			base *= 10;
		}
		return sum;
	}
public static double gaussian(double mean, double sigma)
	{
	   double v1;
	   double v2;
	   double s;
	   double x;

	   do
	   {
		  v1 = 2 * uniform(0, 1) - 1;
		  v2 = 2 * uniform(0, 1) - 1;
		  s = v1 * v1 + v2 * v2;
	   } while (s >= 1.0);

	   x = v1 * Math.sqrt(-2.0 * Math.log(s) / s);

	   /*  x is normally distributed with mean 0 and sigma 1.  */
	   x = x * sigma + mean;

	   return (x);
	}

public static double zipf(double x1, double x2, double p)
	{
	   double x;
	   double i;
	   double r;
	   double HsubV;
	   double sum;
	   int V = 100;

	   //double uniform();

	   /* calculate the V-th harmonic number HsubV. WARNING: V>1 */
	   HsubV = 0.0;
	   for (i = 1; i <= V; i++)
	   {
		  HsubV += 1.0 / Math.pow((double)i, p);
	   }

	   r = uniform(0.0, 1.0) * HsubV;
	   sum = 1.0;
	   i = uniform(0, 1);
	   while (sum < r)
	   {
		  //i++;  //commented by Yufei Tao
		  i += uniform(1, 2);
		  sum += 1.0 / Math.pow((double)i, p);
	   }

	   /* i follows Zipf distribution and lies between 1 and V */

	   /* x lies between 0. and 1. and then between x1 and x2 */
	   x = ((double) i - 1.0) / ((double) V - 1.0);
	   x = (x2 - x1) * x + x1;

	   return (x);
	}

	public static double new_uniform(double _min, double _max)
	{
		double ran_base = 9999999;
		double ran = new_uniform(7);
		return ran / ran_base * (_max - _min) + _min;
	}

	/*	Moving Point functions */
	public static double velocity(Point[] pts, int reftm, int retro)
	{
		double dist = 0.0;
		for (int i = reftm; i > reftm - retro; i--)
		{
			dist += PDIST2(pts[i], pts[i - 1]);
		}
		return dist / retro;
	}
        public static double PDIST2(Point p1, Point p2){
        return Math.sqrt((((p2).x-(p1).x)*((p2).x-(p1).x) + ((p2).y-(p1).y)*((p2).y-(p1).y)));
}

	/* Misc */
public static void progress(int now, int total)
	{
		int ten = total / 10;
		if (now < ten - 1)
		{
			return;
		}
		else if (now == ten)
		{
			System.out.print("10% - done");
			System.out.print("\n");
		}
		else if (now == ten * 2)
		{
			System.out.print("20% - done");
			System.out.print("\n");
		}
		else if (now == ten * 3)
		{
			System.out.print("30% - done");
			System.out.print("\n");
		}
		else if (now == ten * 4)
		{
			System.out.print("40% - done");
			System.out.print("\n");
		}
		else if (now == ten * 5)
		{
			System.out.print("50% - done");
			System.out.print("\n");
		}
		else if (now == ten * 6)
		{
			System.out.print("60% - done");
			System.out.print("\n");
		}
		else if (now == ten * 7)
		{
			System.out.print("70% - done");
			System.out.print("\n");
		}
		else if (now == ten * 8)
		{
			System.out.print("80% - done");
			System.out.print("\n");
		}
		else if (now == ten * 9)
		{
			System.out.print("90% - done");
			System.out.print("\n");
		}
		else if (now == total)
		{
			System.out.print("100% - done");
			System.out.print("\n");
		}

	}

        public static int numLine(String fname)
	{
            FileReader fr = null;
            int cnt = 0;
            try {
                
                byte c;
                File fp = new File(fname, "r");
                if (fp==null || !fp.exists() )
                {
                    System.out.print("Function::numLine: cannot open the given file\n");
                    System.exit(0);
                }
                fr = new FileReader(fp);
                do
                {
                    c = (byte)fr.read();
                    if (c == '\n')
                    {
                        cnt++;
                    }
                } while (c != -1);
                fr.close();
                return cnt;
            } catch (Exception ex) {
                ex.printStackTrace();
                Logger.getLogger(Function.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fr.close();
                } catch (IOException ex) {
                    Logger.getLogger(Function.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return cnt;
	}
        
        //Function does not apply to current scenario and has been commneted

//        public final void extent(String fn, int nline, Double maxx, Double maxy, Double minx, Double miny)
//	{
//		File fip = new File(fn, "r");
//		if (fip == null || !fip.exists())
//		{
//			System.out.print("Database.extent: could not read the file\n");
//			return;
//		}
//		double x=0;
//		double y=0;
//		int t;
//                Scanner sc = new Scanner(fip);
//		for (int i = 0; i < nline; i++)
//		{
//                    
//                        sc.n
//			fscanf(fip, "%d %lf %lf\n", t, x, y);
//			if (i == 0)
//			{
//				maxx = minx = x;
//				maxy = miny = y;
//			}
//			maxx = Math.max(maxx, x);
//			minx = Math.min(minx, x);
//			maxy = Math.max(maxy, y);
//			miny = Math.min(miny, y);
//		}
//	}
//        public static void sort(ArrayList<Point> trj)
//	{
//		sort(trj);
//	}
public static void sort(ArrayList<Point> trj)
	{
		Point temp = new Point();
		for (int i = 0; i < trj.size() - 1; i++)
		{
			for (int j = i + 1; j < trj.size(); j++)
			{
				if (trj.get(i).t > trj.get(j).t)
				{
					temp = trj.get(i); //swapping entire struct
					trj.set(i, trj.get(j));
					trj.set(j,  temp);
				}
			}
		}
	}

        //this function is built in java
	/* string control */
//	public static void trim(String str)
//	{
//		int pos = str.find_last_not_of(' ');
//		if (pos != -1)
//		{
//			str = str.substring(0, pos + 1);
//			pos = str.find_first_not_of(' ');
//			if (pos != -1)
//			{
//				str = str.substring(0, 0) + str.substring(0 + pos);
//			}
//		}
//		else
//		{
//			str = str.substring(0, str.iterator()) + str.substring(str.iterator() + str.end());
//		}
//	}
    // this function is built in java equalsTo
//	public static boolean isSame(String s1, String s2)
//	{
//			trim(s1);
//			trim(s2);
//			return (s1.compareTo(s2) == 0);
//	}

//	public static boolean isSame(tangible.RefObject<String> s1, tangible.RefObject<String> s2)
//	{
//			return isSame((String)s1.argValue, (String)s2.argValue);
//	}
        //StringTokenizer cn be used
//	public static ArrayList<String> tokenize(String str, tangible.RefObject<String> delim)
//	{
//		ArrayList<String> tokens = new ArrayList<String>();
//		String deli = (new Character((char)delim.argValue)).toString();
//		// Skip delimiters at beginning.
//		int lastPos = str.find_first_not_of(deli, 0);
//		// Find first "non-delimiter".
//		int pos = str.find_first_of(deli, lastPos);
//		while (-1 != pos || -1 != lastPos)
//		{
//			// Found a token, add it to the vector.
//			tokens.add(str.substring(lastPos, pos));
//			// Skip delimiters.  Note the "not_of"
//			lastPos = str.find_first_not_of(deli, pos);
//			// Find next "non-delimiter"
//			pos = str.find_first_of(deli, lastPos);
//		}
//		return tokens;
//	}

//	public static ArrayList<String> tokenize(tangible.RefObject<String> s, tangible.RefObject<String> delim)
//	{
//			return tokenize((String)s.argValue, delim);
//	}
//	public static String combine(tangible.RefObject<String> s1, tangible.RefObject<String> s2)
//	{
//		return ((String)s1.argValue + (String)s2.argValue).c_str();
//	}

}





