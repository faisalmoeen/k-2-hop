/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cv;

/**
 *
 * @author Analysis
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.common.buffer.Buffer;
import org.common.geometry.IPoint;
import org.common.geometry.Point;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class ConvoyQuery
{
	public ConvoyQuery()
	{
		_buf = null;
		_showprogress = true;
		_e = -1.0;
//		_index = false;
                _index = 0;
		_index_t = 0;
		initialize();
	}
	
	public int _fneg; // # of false negatives
	public int _fpos;
	public int _fneg2; // # of false negatives
	public int _fpos2;
	public int _exac; // # of exact answers
	public int _corr; // # of correct answers

	public abstract long discover(int m, int k);
	public abstract long discover(int m, int k, double e);
	public abstract void summary(FileWriter fout);

	/*---------------------------------------------------------------------
	   DESCRIPTION: 
	   PARAMETERS
		  - ofstream& fo :
	   AUTHOR: Hoyoung Jeung, 14/12/2007
	*---------------------------------------------------------------------*/
	public final void write(FileWriter fo)
	{
		summary(fo);
		//for(int i=0; i<_rst.size(); i++)
		//	_rst[i].write(fo);
            try {
		for (int i = 0; i < _rst.size(); i++)
		{
			fo.write( "k=" + (_rst.get(i)._te - _rst.get(i)._ts + 1) + "[" + _rst.get(i)._ts + "," +
                                _rst.get(i)._te + "]([" + _buf.tstampof(_rst.get(i)._ts) + "," + _buf.tstampof(_rst.get(i)._te) 
                                + "]), m=" + _rst.get(i).size() + "[");
			for (Iterator<Integer> it = _rst.get(i).iterator(); it.hasNext();)
			{
                           
                                Integer next = it.next();
                            //				if (it != _rst.get(i).begin())
                            //				{
                            fo.write( ",");
                            //				}
                            fo.write( it.next());
                            
			}
			fo.write( "]" + "\n");
		}
                } catch (IOException ex) {
                                Logger.getLogger(ConvoyQuery.class.getName()).log(Level.SEVERE, null, ex);
                            }
	}

	/*---------------------------------------------------------------------
		DESCRIPTION: report params/result cv information to the given file
	    PARAMETERS  
			1. fname: report file name to write    
	    AUTHOR: Hoyoung Jeung, 6 June 2006
	    NOTES: debugging purpose
	*---------------------------------------------------------------------*/
	public final void write(String fname)
	{
		FileWriter fout = null;
            try {
                fout = new FileWriter(fname);
                
//                fout.setf(ios_base.boolalpha);
//                fout.precision(1);
//                fout.setf(ios_base.fixed,ios_base.floatfield);
                write(fout);
                fout.close();
            } catch (IOException ex) {
                Logger.getLogger(ConvoyQuery.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fout.close();
                } catch (IOException ex) {
                    Logger.getLogger(ConvoyQuery.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
	}
	public final void showProgress(boolean mode)
	{
		_showprogress = mode;
	}
	public final void setRange(double e)
	{
		_e = e;
	}
	public final void useIndex(int mode)
	{
		_index = mode;
	}
        public final void validate(ArrayList<Convoy> ans)
	{
		_fpos = _fneg = _exac = _corr = 0;
		if (_rst.size() == 0)
		{
			_fneg = ans.size();
			return;
		}
		if (ans.size() == 0)
		{
			_fpos = _rst.size();
			return;
		}
		for (Iterator<Convoy> a = ans.iterator(); a.hasNext();)
		{
			Convoy current = a.next();
			for (Iterator<Convoy> c = _rst.iterator(); c.hasNext();)
			{
				current = c.next();
				//cout << "\nanswer "; a->print(); cout << "candidate "; c->print();
				if (current == current)
				{
					_exac++;
				}
				else if (current.lessThanOrEqualTo(current))
				{
					_corr++;
				}
			}
		}
		_fneg2 = (int) ((1.0 - (_exac + _corr) / (double)ans.size()) * 100);
		_fpos2 = (int) ((1.0 - (_exac + _corr) / (double)_rst.size()) * 100);
		_fneg = (int) ((1.0 - (_exac) / (double)ans.size()) * 100);
		_fpos = (int) ((1.0 - (_exac) / (double)_rst.size()) * 100);
	}

        public final void output(Convoy c)
	{
		if (_rst.isEmpty())
		{
			_rst.add(c);
			return;
		}
		else
		{
			for (int j = 0; j < _rst.size(); j++)
			{
				if ((c._te < _rst.get(j)._ts) || (_rst.get(j)._te < c._ts))
				{
					continue;
				}

//				TreeSet<Integer> is = new TreeSet<Integer>();
//                                is.addAll(_rst);
//                                
//				set_intersection(_rst.get(j).begin(),_rst.get(j).end(), c.begin(),c.end(), insert_iterator<TreeSet<Integer>>(is,is.iterator()));
                                TreeSet<Integer> temp = new TreeSet<Integer>();
                                temp.addAll(_rst.get(j));
                                temp.retainAll(c);
                                
                                
				if (temp.size() == c.size())
				{
					_rst.get(j)._ts = Math.min(_rst.get(j)._ts,c._ts);
					_rst.get(j)._te = Math.max(_rst.get(j)._te,c._te);
					return;
				}
			}
			_rst.add(c);
		}
	}

	public final ArrayList<Convoy> getResults()
	{
		return _rst;
	}



	protected int _m;
	protected int _k;
	protected double _e;
	protected Buffer _buf; // inputs
	protected long _elapsed_t = new Long(0); // elapsed time
	protected int _index;
	protected long _index_t = new Long(0);
	protected boolean _showprogress;
	protected ArrayList<Convoy> _rst = new ArrayList<Convoy>(); // output

    protected final double findEps(int m, int k)
	{
		int ntrial = 300;
		ntrial = (ntrial > _buf.num_tstamp()) ? _buf.num_tstamp() : ntrial;
		int stop = 1000;
		int nsample = 500;

		TreeSet<Double> es = new TreeSet<Double>();

		TreeMap<Integer,ArrayList<IPoint>> tg = _buf.tlookup();
		//Map<Integer,Integer> tt = new LinkedHashMap<Integer,Integer>(); // (# of points, time) -> this is sorted by the number
                Multimap<Integer, Integer> tt = ArrayListMultimap.create();
//C++ TO JAVA CONVERTER TODO TASK: Iterators are only converted within the context of 'while' and 'for' loops:
		for (Iterator it = tg.keySet().iterator();it.hasNext();)
		{
                     Integer next = (Integer) it.next();
//C++ TO JAVA CONVERTER TODO TASK: Iterators are only converted within the context of 'while' and 'for' loops:
//			tt.insert(multimap<Integer,Integer>.value_type(it.second.size(),it.first));
                        tt.put(next, tg.get(next).size());
		}

//		multimap<Integer,Integer>.reverse_iterator tti = tt.rbegin();
                Integer[] tti = (Integer[])tt.keySet().toArray();
                int tti_index = tti.length;
		for (int j = 0; j < ntrial; j++)
		{
			ArrayList<IPoint> pts = _buf.objAtTime(tt.get(tti[tti_index]).iterator().next());
//			tti++;
                        tti_index--;
			int n = pts.size();
			if (n < m)
			{
				continue;
			}

			double e2 = 0.0;
			for (int z = 0;z < n - 1;z++)
			{
				e2 += PDIST2(pts.get(z).p,pts.get(z + 1).p);
			}
			e2 /= n;
			//e2 = PDIST2(*(pts->at(0).p),*(pts->at(1).p));

			int cnt = 0;
			while (true)
			{
				cnt++;
				int found = 0; // number of objects found within the current eps

				// range query	(random point is the first one)
				int nloop = (n > nsample) ? nsample : n;
				for (int i = 0; i < nloop; i++)
				{
					if (PDIST2(pts.get(0).p,pts.get(i).p) <= e2)
					{
						found++;
					}
				}

				if (found == 0)
				{
					e2 *= 1.9;
				}
				else if (found > (int)m)
				{
					e2 /= 2;
				}
				else
				{
					double r = (double) n / _buf.num_obj();
					e2 *= r * r;
					break;
				}
				if (cnt > stop)
				{
					break;
				}
				//assert(cnt<stop);
			}
			es.add(e2);
		}

		Iterator<Double> it = es.iterator();
		//int nloop = (int)es.size()/2; // median
		int nloop = (int)(es.size() * k / _buf.num_tstamp()); // T : T' = k : k'
		if (nloop > es.size())
		{
			nloop = es.size();
		}
		for (int z = 0; z < nloop; z++)
		{
//C++ TO JAVA CONVERTER TODO TASK: Iterators are only converted within the context of 'while' and 'for' loops:
			it.next();
		}
//C++ TO JAVA CONVERTER TODO TASK: Iterators are only converted within the context of 'while' and 'for' loops:
		return Math.sqrt(it.next());
	}
        public double PDIST2(Point p1, Point p2){
                return Math.sqrt((((p2).x-(p1).x)*((p2).x-(p1).x) + ((p2).y-(p1).y)*((p2).y-(p1).y)));
        }

	/*---------------------------------------------------------------------
	   DESCRIPTION: initialize all the variable to perform new query processing
	   AUTHOR: Hoyoung Jeung, 25/10/2007
	*---------------------------------------------------------------------*/
	protected void initialize()
	{
		_rst.clear();
		_elapsed_t = 0;
		_fneg = _fpos = _exac = _corr = 0;
	}

}

