/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cv;

/**
 *
 * @author Analysis
 */

import org.cluster.Dbscan;
import org.common.Function;
import org.common.buffer.Buffer;
import org.common.geometry.IPoint;
import org.idx.Grid;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MovingCluster extends ConvoyQuery
{
	public MovingCluster()
	{
	}
	public MovingCluster(Buffer b)
	{
		_buf = b;
		initialize();
	}
	public void dispose()
	{
	}

	public long discover(int m, int k)
	{
		return mc2(0.8, m, k);
	}
	public long discover(int m, int k, double e)
	{
		setRange(e);
		return mc2(0.8, m, k);
	}
        public long mc2(double theta,int m, int k)
	{
		initialize();
		_timedb_t = _buf._timedb_t;
    
		_elapsed_t = Calendar.getInstance().getTimeInMillis();
    
		if (_e < 0)
		{
			_e = findEps(m,k);
		}
		if (_index == 1 && _index_t == 0)
		{
			_index_t = buildIndex(_e);
		}
		_m = m;
		_k = k;
    
		ArrayList<Convoy> G = new ArrayList<Convoy>(); // set of current clusters
    
		int n = _buf.num_tstamp();
		for (int s = 0; s < n; s++)
		{
			ArrayList<Convoy> Gnext = new ArrayList<Convoy>();
			Iterator<Convoy> c;
			Iterator<Convoy> g;
    
			// less number of points in this timestamp
			ArrayList<IPoint> pts = _buf.objAtTstamp(s);
			if (pts.size() < m)
			{
				for (g = G.iterator(); g.hasNext();)
				{       
                                        Convoy gNext = g.next();
					//if(g->lifetime()>=k)			
					if (gNext.lifetime() >= 2)
					{
						output(gNext);
					}
				}
				G = new ArrayList<Convoy>(Gnext);
				continue;
			}
    
			/*	DBSCAN */
			Dbscan ds = new Dbscan(pts,m,_e);
			if (_index == 1)
			{
//				ds.setIndex(_grids[s]);
                                ds.setIndex(_grids.get(s));
			}
			ds.perform();
    
			/* adding a candidate set to result */
			ArrayList<Convoy> L = ds.getClusterSet();
    
			for (c = L.iterator(); c.hasNext();)
			{
                            Convoy convy = c.next();
				convy._assigned = false;
			}
    
			for (g = G.iterator(); g.hasNext();)
			{
				Convoy current = g.next();
				current._assigned = false;
                                Convoy cv = null;
    
				int trial = 0;
				int cid = -1;
    
				/* pick a random object and check if it belongs to c */
				while (current.size() - trial >= m && cid == -1)
				{
					/* pick a random object (first point) from a cluster */	
                                        
					Integer tmp = current.iterator().next();
					for (int z = 0;z < trial;z++)
					{
						tmp++;
					}
					Integer oid = tmp;
    
					/* find a cluster in L contains the object */							
					int counter = 0;
					for (c = L.iterator(); c.hasNext();)
					{
                                             cv = c.next();
//						if (!(cv.contains(oid) == c.end()))
                                                if (cv.contains(oid))
						{
							cid = counter;
							break;
						}
						counter++;
					}
					trial++;
				}
    
				if (cid > -1) // when a cluster found
				{
					ArrayList<Integer> intsec = new ArrayList<Integer>();
					ArrayList<Integer> unio = new ArrayList<Integer>();
					ArrayList<Integer> diff = new ArrayList<Integer>();
					Iterator<Integer> intsecit;
					Iterator<Integer> unioit;
					Iterator<Integer> diffit;
                                        intsec.addAll(current);
                                        intsec.retainAll(cv);
                                        //intsecit = set_intersection(g.begin(),g.end(),c.begin(), c.end(), intsec.iterator());
                                        unio.addAll(current);
                                        unio.addAll(cv);
					//unioit = set_union(g.begin(),g.end(),c.begin(), c.end(), unio.iterator());
                                        diff.addAll(current);
                                        diff.removeAll(cv);
//					diffit = set_difference(g.begin(),g.end(),c.begin(), c.end(), diff.iterator());
    
					/* trim rst to contain only results */
	//C++ TO JAVA CONVERTER TODO TASK: There is no direct equivalent to the STL vector 'erase' method in Java:
//					intsec.erase(intsecit, intsec.end());
//	//C++ TO JAVA CONVERTER TODO TASK: There is no direct equivalent to the STL vector 'erase' method in Java:
//					unio.erase(unioit, unio.end());
//	//C++ TO JAVA CONVERTER TODO TASK: There is no direct equivalent to the STL vector 'erase' method in Java:
//					diff.erase(diffit, diff.end());
    
					/* verification of moving cluster */
					double ratio = (double)intsec.size() / (double)unio.size();
					if (ratio >= theta)
					{
						current._assigned = true;
						current._te = _buf.timeof(s);
    
						/* Gnext = Gnext U g o c, replace g to c  */
						current.clear();
						for (Iterator<Integer> cc = cv.iterator();cc.hasNext();)
						{
							current.add(cc.next());
						}
    
						Gnext.add(current);
						cv._assigned = true;
					}
    
				} // if
    
				if (!current._assigned)
				{
					//if(g->lifetime()>=k)
					if (current.lifetime() >= 2)
					{
						output(current);
					}
				}
    
			} // for (g)
    
			for (c = L.iterator(); c.hasNext();)
			{
                            Convoy cvn = c.next();
				if (!cvn._assigned)
				{
					cvn._ts = cvn._te = _buf.timeof(s);
					Gnext.add(c.next());
				}
			}
    
    
			if (s == n - 1)
			{
				for (g = Gnext.iterator(); g.hasNext();)
				{
                                    Convoy gn = g.next();
					if (gn._assigned && gn.lifetime() >= 2)
					{
					//if(g->_assigned && g->lifetime()>=k)
						  output(gn);
					}
				}
				break;
			}
    
			G = new ArrayList<Convoy>(Gnext);
			if (_showprogress)
			{
                            Function.progress(s,n);
			}
		}
    
		_elapsed_t = Calendar.getInstance().getTimeInMillis() - _elapsed_t + _timedb_t;
		System.out.print("MC2 (theta=");
		System.out.print(theta);
		System.out.print(") - ");
		print();
		return _elapsed_t;
	}

	public final void print()
	{
		System.out.print("(m=");
		System.out.print(_m);
		System.out.print(" k=");
		System.out.print(_k);
		System.out.print(" e=");
		System.out.print(_e);
		System.out.print(") : ");
		System.out.print(_rst.size());
		System.out.print(" convoys, ");
		System.out.print(_elapsed_t / 1000);
		System.out.print(" sec (timedb ");
		System.out.print(_timedb_t / 1000);
		System.out.print(", idx (");
		System.out.print(_index);
		System.out.print(") ");
		System.out.print(_index_t / 1000);
		System.out.print(")\n");

	}
	public final void summary(FileWriter fout)
	{
            try {
                fout.write( " (m=" + _m + " k=" + _k + " e=" + _e + ") : ");
                fout.write( _rst.size() + " convoys, " + _elapsed_t / 1000 + " sec (timedb " + _timedb_t / 1000);
                if (_index == 1)
                {
                    fout.write( ", index (" + _index + ") " + _index_t / 1000 + " sec");
                }
                if (_fneg > 0 || _fpos > 0 || _exac > 0 || _corr > 0)
                {
                    fout.write( ", exact " + _exac + ", correct " + _corr + ", f.pos " + _fpos + "%," + " f.neg " + _fneg + "% ");
                }
                fout.write( ")" + "\n");
            } catch (IOException ex) {
                ex.printStackTrace();
                Logger.getLogger(MovingCluster.class.getName()).log(Level.SEVERE, null, ex);
            }
	}



	protected TreeMap<Integer,Grid> _grids = new TreeMap<Integer,Grid>();
	protected long _timedb_t = Calendar.getInstance().getTimeInMillis();

	protected final long buildIndex(double e)
	{
		if (e == 0)
		{
			return 0;
		}

		int n = _buf.num_tstamp();
		if (!_grids.isEmpty())
		{
			_grids.clear();
		}

		long start = Calendar.getInstance().getTimeInMillis();

		for (int i = 0; i < n; i++)
		{
			_grids.put(i,new Grid());
			_grids.get(i).build(_buf.objAtTstamp(i),e);
		}

		return Calendar.getInstance().getTimeInMillis() - start;
	}



}
