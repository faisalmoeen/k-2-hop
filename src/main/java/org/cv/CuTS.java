/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cv;

/**
 *
 * @author Analysis
 */

import org.cluster.Dbscan;
import org.cluster.DbscanLine;
import org.common.Function;
import org.common.buffer.Buffer;
import org.common.buffer.Trajectory;
import org.common.geometry.IPoint;
import org.common.geometry.MBR;
import org.common.geometry.Point;
import org.common.geometry.PolyLine;
import org.idx.GridLine;
import org.idx.IDistance;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CuTS extends ConvoyQuery // Convoy using Trajectory Simplification
{
	public CuTS()
	{
		defaultSetting();
	}
	public CuTS(Buffer b)
	{
		_buf = b;
		defaultSetting();
	}
	
        @Override
        public final long discover(int m, int k)
	{
		initialize();
		_cands.clear();
		_m = m;
		_k = k;
		if (_e < 0)
		{
			_e = findEps(m,k);
		}

		if (_index!=0)
		{
			_index_t = buildIndex(); // ----- important
		}

		_filter_t = Calendar.getInstance().getTimeInMillis();
		ArrayList<Convoy> V = new ArrayList<Convoy>(); // set of current clusters
		int n = _tpart.size();
		int s = 0;
		Iterator<Map.Entry<Integer,PolyLines>> mi = _tpart.entrySet().iterator();
//C++ TO JAVA CONVERTER TODO TASK: Iterators are only converted within the context of 'while' and 'for' loops:
                //System.out.println("_tpart :: " + _tpart.keySet().size() + " : " + n);
		
                Map.Entry<Integer,PolyLines> mic = mi.next();
                s++;
                int ltp = mic.getKey()- 1; // last time partition id
		while (s < n) // for each polyline groups, it is sorted by time
		{
                        //System.out.println("while :: " +s + " : "+n + " : " +mic.getKey() );
			ArrayList<Convoy> Vnext = new ArrayList<Convoy>();
			Iterator<Convoy> c;
			Iterator<Convoy> v;
                        
//C++ TO JAVA CONVERTER TODO TASK: Iterators are only converted within the context of 'while' and 'for' loops:
			if (mic.getValue().size() < m || (ltp + 1) != mic.getKey())
			{
				for (v = V.iterator(); v.hasNext();)
				{
                                    Convoy vn = v.next();
					if (vn.lifetime() >= k)
					{
						candidate(vn);
					}
				}
				V = new ArrayList<Convoy>(Vnext);
//C++ TO JAVA CONVERTER TODO TASK: Iterators are only converted within the context of 'while' and 'for' loops:
				ltp = mic.getKey();
//C++ TO JAVA CONVERTER TODO TASK: Iterators are only converted within the context of 'while' and 'for' loops:
//				mi++;
                                mic = mi.next();
				s++;
				continue;
			}

//C++ TO JAVA CONVERTER TODO TASK: Iterators are only converted within the context of 'while' and 'for' loops:
			DbscanLine ds = new DbscanLine((mic.getValue()), m, _e, _star); //    DBSCAN for polylines
			if (_index == IDISTANCE)
			{
				ds.setIndex(_idist.get(s));
			}
			else if (_index == GRIDLINE)
			{
				ds.setIndex(_gline.get(s));
			}
			ds.perform();

			ArrayList<Convoy> L = ds.getClusterSet();
			for (v = V.iterator(); v.hasNext();)
			{
				Convoy current = v.next();
				current._assigned = false;
				for (c = L.iterator(); c.hasNext();)
				{
                                    Convoy cn = c.next();
					/* we may need consecutive time check here */
					ArrayList<Integer> is = new ArrayList<Integer>(current.size());
                                        is.addAll(current);
                                        is.retainAll(cn);
//					Iterator<Integer> ii = set_intersection(v.begin(),v.end(), c.begin(),c.end(), is.iterator());
//C++ TO JAVA CONVERTER TODO TASK: There is no direct equivalent to the STL vector 'erase' method in Java:
//					is.erase(ii, is.end()); // trim rst to contain only results
					int is_size = is.size();
					if (is_size >= m) // verification of convoy
					{
						current._assigned = true;
						current._te = (s == n - 1) ? _buf.lasttime() : _buf.timeof((s + 1) * (_lamda - 1));
						//v->_te = mi->second._ext.maxt;
						current.clear(); // Vnext = Vnext U (v intersect c)
						//v.insert(is.iterator(),is.end());
                                                current.addAll(is);
						Vnext.add(current);
						cn._assigned = true;
					}
				}
				if (!current._assigned)
				{
					if (current.lifetime() >= k)
					{
						 candidate(current);
					}
				}
			}
			if (s == n - 1)
			{
				for (v = V.iterator(); v.hasNext();)
				{
                                    Convoy vn = v.next();
					if (vn.lifetime() >= k)
					{
						candidate(vn);
					}
				}
				break;
			}

			/* the entry of the active list */
			for (c = L.iterator(); c.hasNext();)
			{
                            Convoy cn = c.next();
				if (!cn._assigned)
				{
					cn._ts = _buf.timeof(s * (_lamda - 1));
					cn._te = (s == n - 1) ? _buf.lasttime() : _buf.timeof((s + 1) * (_lamda - 1));
					Vnext.add(cn);
				}
			}

			V = new ArrayList<Convoy>(Vnext);
//C++ TO JAVA CONVERTER TODO TASK: Iterators are only converted within the context of 'while' and 'for' loops:
			ltp = mic.getKey();
//C++ TO JAVA CONVERTER TODO TASK: Iterators are only converted within the context of 'while' and 'for' loops:
			mic = mi.next();
			s++;
			if (_showprogress)
			{
                            Function.progress(s,n);
			}
		}

		/* 3. Refinement Phase  */
		refine(m,k,_e);

		_filter_t = Calendar.getInstance().getTimeInMillis() - _filter_t;
		_elapsed_t = _filter_t + _simple_t + _refine_t;
		analyze(_cands); // for debugging
		analyze2(_cands); // for debugging
		print();
		return _elapsed_t;
	}
        @Override
	public final long discover(int m, int k, double e)
	{
		setRange(e);
		return discover(m, k);
	}
        public long simplify(double tol)
	{
		if (_lamda < 0)
		{
			_lamda = 50;
		}
		if (!_tpart.isEmpty())
		{
			_tpart.clear();
		}
		_simple_t = Calendar.getInstance().getTimeInMillis();
		_tol = tol;
                
//		Database.iterator it = _buf.getDB().begin();
                Iterator<Map.Entry<Integer,Trajectory>> it = _buf.getDB().entrySet().iterator();
                Map.Entry<Integer,Trajectory> itn;// = null;
//		Database.iterator itend = _buf.getDB().end();
                Iterator<Map.Entry<Integer,Trajectory>> itend = _buf.getDB().descendingMap().entrySet().iterator();
                Map.Entry<Integer,Trajectory> itendn;// = null;
		double ratiosum = 0.0;
		int ec = (int) _buf.num_obj(); // expected capacity (polylines) for each time partition.
                itn = it.next();itendn = itend.next();
		while (itn.getKey().intValue() != itendn.getKey().intValue())
		{
                    //System.out.println("Inside while :: itn key " + itn.getKey() + " itendn key " +  itendn.getKey());
			Trajectory trj = (itn.getValue()); // original trajectory
			int n = trj.size();
			int oid = itn.getKey();
    
			ArrayList<Double> tols = new ArrayList<Double>(); // tolerance recorder
                        for(int trjj = 0; trjj<n;trjj++){ tols.add(new Double(0));}
			dp(itn.getValue(), 0, n - 1, tols);
			int npt = 1; // # of simplified points
			Point lp = (trj.get(0)); // last point
			double ltol = 0.0; // maximum tolerance of each polyline
			int tp1;
			int tp2;
			PolyLine l = new PolyLine(oid);
			l.add(lp);
			tp1 = _buf.tstampof(lp.t) / (_lamda - 1); // last time partition
			boolean virgin = true;
			for (int j = 1; j < n; j++) // for each point on an original trajectory
			{
                            //System.out.println("Going Fine for");
				if ((tols.get(j) <= _tol) && (j != n - 1))
				{
					ltol = Math.max(ltol,tols.get(j));
				}
				else
				{
                                        //System.out.println("Going Fine");
					Point cp = (trj.get(j)); // current position
					tp2 = _buf.tstampof(cp.t) / (_lamda - 1); // current time partition
					l.add(cp);
    
					if (tp1 < tp2 || j == n - 1)
					{
						int tend = (j == n - 1 && _buf.tstampof(cp.t) != _buf.num_tstamp() - 1) ? tp2 + 1:tp2;
                                                //System.out.println("tend " + tend + " tp1 " + tp1);
						for (; tp1 < tend; tp1++)
						{
							if (_tpart.get(tp1) == null)
							{
                                                            //System.out.println("_tpart.get(tp1)");
//								_tpart[tp1].reserve(ec);
                                                                _tpart.put(tp1, new PolyLines());//[tp1].reserve(ec);
							}
    
							int ps = tp1 * (_lamda - 1); // partition start timestamp
							int pe = (tp1 + 1) * (_lamda - 1); // partition end timestamp
    
							PolyLine l_ = new PolyLine(oid);
							int lb = 0; // lower bound
							int ub = l.size() - 1; // upper bound
							for (int i = 0; i < l.size();i++)
							{
								int t_ = _buf.tstampof(l.get(i).t);
								if (virgin)
								{
									continue;
								}
								else if (t_ < ps)
								{
									lb = (i == 0) ? 0 : Math.max(lb,i);
								}
								else if (ps == t_)
								{
									lb = i;
								}
    
								if (j == n - 1)
								{
									ub = l.size() - 1;
								}
								else if (pe < t_)
								{
									ub = (i == l.size() - 1) ? l.size() - 1 : Math.min(ub,i);
								}
								else if (pe == t_)
								{
									ub = i;
								}
							}
    
							for (int k = lb; k <= ub;k++)
							{
								l_.add(l.get(k));
							}
    
							if (_acttol)
							{
								l_.tol = ltol;
								ltol = .0; // individual tolerance
							}
							else
							{
								l_.tol = _tol; // global tolerance
							}
                                                         //System.out.println("_tpart.get(tp1).add(l_)");
							_tpart.get(tp1).add(l_);
							virgin = false;
						}
    
						PolyLine ll = new PolyLine(oid);
						//ll.reserve(_plen); // mem alloc for speeding up
						l = ll; // maybe faster than clear() ;
						if (_buf.tstampof(lp.t) < (tp1 * (_lamda - 1)))
						{
							l.add(lp);
						}
						l.add(cp);
					}
	//C++ TO JAVA CONVERTER TODO TASK: Java does not have an equivalent to pointers to variables (in Java, the variable no longer points to the original when the original variable is re-assigned):
	//ORIGINAL LINE: lp = cp;
					lp = cp;
					npt++;
				}
			}
			ratiosum += (double) npt / (double) n;
                        itn = it.next();itendn = itend.next();
			//it++;
		}
		_reduc = 100 - (double)(ratiosum / (double) _buf.num_obj()) * 100; // %
		_simple_t = Calendar.getInstance().getTimeInMillis() - _simple_t;
		return _simple_t;
	}

	//clock_t simplify(){_tol = compute_tol();return simplify(_tol);}

	// setting	
	public final void lambda(int glen)
	{
		if (glen > 0)
		{
			_lamda = glen;
		}
	}
	public final void star(boolean mode)
	{
		_star = mode;
	}
	public final void plus(boolean mode)
	{
		_plus = mode;
	}
	public final void batch(int mode)
	{
		_batch = mode;
	} // 0: no batch, 1-semi batch, 2-batch
	public final void actualTol(boolean mode)
	{
		_acttol = mode;
	}
	public final int reduction()
	{
		return (int)_reduc;
	}
	public final double getTolerance()
	{
		return _tol;
	}
        @Override
        public void summary(FileWriter fout)
	{
            try {
                //		fout.setf(ios_base.fixed,ios_base.floatfield);
                //		fout.precision(1);
                //double cand = (_ncand==0) ? 0 : (double)_rst.size()/_ncand*100;
                double elapsed = (_elapsed_t == 0) ? 0 : _elapsed_t / 1000;
                double filter = (_filter_t == 0) ? 0 : _filter_t / 1000;
                double simple = (_simple_t == 0) ? 0 : _simple_t / 1000;
                double refine = (_refine_t == 0) ? 0 : _refine_t / 1000;
                fout.write( "CuTS");
                if (_star)
                {
                    fout.write( "*");
                }
                if (_plus)
                {
                    fout.write( "+");
                }
                fout.write( " (m=" + _m + " k=" + _k + " e=" + _e + " tol " + _tol + ") : ");
                fout.write( "\t");
                fout.write( _rst.size() + "/" + _cands.size() + " convoys, " + elapsed + " sec (filter " + filter + ", refine " + refine + ", simpl " + simple + ", " + _reduc + "% reduc, " + "index " + _index_t + " sec, " + _tpart.size() + " parts (lambda=" + _lamda + ")");
                if (_fneg > 0 || _fpos > 0 || _exac > 0 || _corr > 0)
                {
                    fout.write( ", exact " + _exac + ", correct " + _corr + ", f.pos " + _fpos + "%," + " f.neg " + _fneg + "% ");
                }
                if (_effec!=0)
                {
                    fout.write( ", refinement cost " + _effec);
                }

                /* showing cadidate details */
                fout.write( "candidates " + "\n");
                for (int i = 0; i < _cands.size(); i++)
                {
                    //_cands[i].write(fout);
                    fout.write( "k=" + (_cands.get(i)._te - _cands.get(i)._ts + 1) + "[" + _cands.get(i)._ts + "," + _cands.get(i)._te + "]([" + _buf.tstampof(_cands.get(i)._ts) + "," + _buf.tstampof(_cands.get(i)._te) + "]), m=" + _cands.get(i).size() + "[");
                    for (Integer itn : _cands.get(i)) {
                        if (itn.intValue() != _cands.get(i).iterator().next().intValue())
                        {
                            fout.write( ",");
                        }
                        fout.write( itn);
                    }
                    fout.write( "]" + "\n");
                }
                fout.write( "\n");
                fout.write( "results " + "\n");
            } catch (IOException ex) {
                Logger.getLogger(CuTS.class.getName()).log(Level.SEVERE, null, ex);
            }
	}

        public void summary2(FileWriter fout)
	{
            try {
                //		fout.setf(ios_base.fixed,ios_base.floatfield);
                //		fout.precision(1);
                //double cand = (_ncand==0) ? 0 : (double)_rst.size()/_ncand*100;
                double elapsed = (_elapsed_t == 0) ? 0 : _elapsed_t / 1000;
                double filter = (_filter_t == 0) ? 0 : _filter_t / 1000;
                double simple = (_simple_t == 0) ? 0 : _simple_t / 1000;
                double refine = (_refine_t == 0) ? 0 : _refine_t / 1000;
                fout.write(  "CuTS");
                if (_star)
                {
                    fout.write(  "*");
                }
                if (_plus)
                {
                    fout.write(  "+");
                }
                fout.write( " (m=" + _m + " k=" + _k + " e=" + _e + " tol " + _tol + ") : ");
                fout.write(  "\t");
                fout.write(  _rst.size() + "/" + _cands.size() + " convoys, " + elapsed + " sec (filter " + filter + ", refine " + refine + ", simpl " + simple + ", " + _reduc + "% reduc, " + "index " + _index_t + " sec, " + _tpart.size() + " parts (lambda=" + _lamda + ")");
                if (_fneg > 0 || _fpos > 0 || _exac > 0 || _corr > 0)
                {
                    fout.write(  ", exact " + _exac + ", correct " + _corr + ", f.pos " + _fpos + "%," + " f.neg " + _fneg + "% ");
                }
                if (_effec!=0)
                {
                    fout.write(  ", refinement cost " + _effec);
                }
                fout.write(  "\n");
            } catch (IOException ex) {
                Logger.getLogger(CuTS.class.getName()).log(Level.SEVERE, null, ex);
            }
	}

         public void print()
	{
	//C++ TO JAVA CONVERTER TODO TASK: Calls to 'setf' using two arguments are not converted by C++ to Java Converter:
//		cout.setf(ios_base.fixed,ios_base.floatfield);
		double filter = (_elapsed_t == 0) ? 0 : (double)_filter_t / _elapsed_t * 100;
		double simple = (_elapsed_t == 0) ? 0 : (double)_simple_t / _elapsed_t * 100;
		double refine = (_elapsed_t == 0) ? 0 : (double)_refine_t / _elapsed_t * 100;
		System.out.printf("CuTS");
		if (_star)
		{
			System.out.printf( "*");
		}
		if (_plus)
		{
			System.out.printf( "+");
		}
		System.out.printf( " (m=");
		System.out.printf("%d", _m);
		System.out.printf( " k=");
		System.out.printf("%d", _k);
		System.out.printf( " e=");
		System.out.printf("%.1f", _e);
		System.out.printf( " tol ");
		System.out.printf("%.1f", _tol);
		System.out.printf( ") : ");
		System.out.printf("%d", _rst.size());
		System.out.printf( "/");
		System.out.printf("%d", _cands.size());
		System.out.printf( " convoys (==");
		System.out.printf("%d", _corr);
		System.out.printf( "  -");
		System.out.printf("%d", _fneg);
		System.out.printf( "),  ");
		System.out.printf("%.1f",(double) _elapsed_t / 1000);
		System.out.printf( " sec (simple ");
		System.out.printf("%.1f", (double)_simple_t / 1000);
		System.out.printf( ", idx (");
		System.out.printf("%d", _index);
		System.out.printf( ") ");
		System.out.printf("%.1f", (double)_index_t / 1000);
		System.out.printf( "), reduc ");
		System.out.printf("%.1f", _reduc);
		System.out.print( "%, part_len ");
		System.out.printf("%d", _lamda);
		System.out.printf( "\n");
			//<< "  (filter " << filter <<"%, refine " << refine << "%, simpl " << simple 
			//<< _tpart.size()  << " group (len=" << _glen << ")" 
	}

        public void matlab(int nth, String outf)
	{
		ArrayList<Trajectory> trjs = new ArrayList<Trajectory>();
    
		int cnt = 0;
		int oid = -1;
                Iterator<Map.Entry<Integer,Trajectory>> di = _buf.getDB().entrySet().iterator();
                Map.Entry<Integer,Trajectory> din;// = null;
//		Database.iterator di = _buf.getDB().begin();

		while (di.hasNext())
		{
                    din = di.next();
			if (cnt++==nth)
			{
				oid = din.getKey();
				trjs.add(din.getValue());
				break;
			}
//			di++;
		}
    
		Iterator<Map.Entry<Integer,PolyLines>> pi = _tpart.entrySet().iterator();
		Trajectory tr = new Trajectory();
    
		while (pi.hasNext())
		{
			Map.Entry<Integer,PolyLines> current = pi.next();
			for (int i = 0; i < current.getValue().size();i++)
			{
				PolyLine l = (current.getValue().get(i));
				if (l.o == oid)
				{
					for (int j = 0; j < l.size(); j++)
					{
						Point p = (l.get(j));
						if (tr.isEmpty())
						{
							tr.add(p);
						}
						else if (p == tr.get(tr.size() - 1))
						{
							continue;
						}
    
						tr.add(p);
    
					}
					break;
				}
			}
		}
		trjs.add(tr);
		Buffer.matlab(trjs,outf);
	}

        public void dumpPartition(String fn)
	{
		FileWriter fout = null;
            try {
                fout = new FileWriter(fn);
                //		fout.setf(ios_base.fixed, ios_base.floatfield);
                //		fout.precision(0);
//                Database.iterator it = _buf.getDB().begin();
                Iterator<Map.Entry<Integer,Trajectory>> it = _buf.getDB().entrySet().iterator();
                Map.Entry<Integer,Trajectory> itn;// = null;
//		Database.iterator itend = _buf.getDB().end();
                Iterator<Map.Entry<Integer,Trajectory>> itend = _buf.getDB().entrySet().iterator();
                Map.Entry<Integer,Trajectory> itendn;// = null;
                
                double ratiosum = 0.0;
                // writing simplified trajectory
                int cnt = 0;
                TreeSet<Integer> oids = new TreeSet<Integer>();
                fout.write(  "simplified trajectory" + "\n");
                itn = it.next();itendn = itend.next();
		while (itn.getKey().intValue() != itendn.getKey().intValue())
//                while (it != itend) // for each object
                {
                    Trajectory trj = (itn.getValue()); // original trajectory
                    int n = trj.size();
                    oids.add(itn.getKey());

                    ArrayList<Double> tols = new ArrayList<Double>(n); // tolerance recorder for bottomup dp
                    dp(itn.getValue(), 0, n - 1, tols);

                    //fout.write( +"oid " + it->first + " : " + trj->at(0).t ;
                    fout.write(  "oid " + itn.getKey() + " : " + _buf.tstampof(trj.get(0).t));
                    for (int j = 1; j < n; j++) // for each point on an original trajectory
                    {
                        if (tols.get(j) > _tol)
                        {
                            //fout.write( + " " + trj->at(j).t;
                            fout.write(  " " + _buf.tstampof(trj.get(j).t));
                        }
                    }
                    //fout.write( +" " + trj->at(n-1).t + endl;
                    fout.write(  " " + _buf.tstampof(trj.get(n - 1).t) + "\n");
                    cnt++;
                     itn = it.next();itendn = itend.next();
                }
                // writing partitions of the simplified trajectory
                fout.write( "\n" + "\n");
                Iterator<Map.Entry<Integer,PolyLines>> pi = _tpart.entrySet().iterator();
                Trajectory tr = new Trajectory();
                fout.write(  "partition length (# timestamps) " + _lamda + "\n");
                while (pi.hasNext())
                {
                    Map.Entry<Integer,PolyLines> current = pi.next();
                    int et = (current.getKey() + 1) * (_lamda - 1);
                    fout.write( "part " + current.getKey() + " : " + current.getValue().size() + " lines, " + " timestamp[" + current.getKey() * (_lamda - 1) + "," + (current.getKey() + 1) * (_lamda - 1) + "]" + "\n");
                    for (int i = 0; i < current.getValue().size();i++) // # of polylines
                    {
                        PolyLine l = (current.getValue().get(i));
                        if (oids.contains(l.o))
                        {
                            continue;
                        }

                        fout.write(  "\t<oid " + l.o + ">");
                        for (int j = 0; j < l.size(); j++) // # of points in each polyline
                        {
                            //fout.write( +" " + l->at(j)->t ;
                            fout.write(  " " + _buf.tstampof(l.get(j).t));
                        }
                        fout.write(  "\n");
                    }
                    fout.write(  "\n");
                }
                fout.close();
            } catch (IOException ex) {
                Logger.getLogger(CuTS.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
//                try {
//                    //fout.close();
//                } catch (IOException ex) {
//                    Logger.getLogger(CuTS.class.getName()).log(Level.SEVERE, null, ex);
//                }
            }
	}

        public final double compute_tol(double e)
	{
		int ntrial = (int) _buf.num_obj() / 10; // 10 %

		Iterator<Map.Entry<Integer,Trajectory>> it = _buf.getDB().entrySet().iterator();
                Map.Entry<Integer,Trajectory> itn;// = null;
		double tolsum = .0;
		int cnt = 0;
		for (int i = 0;i < ntrial; i++) // check the first ntrial numbers of trajectories
		{
                        itn = it.next();
			TreeSet<Double> sorted_tols = new TreeSet<Double>(); // storage for tolerances sorted
			compute_tols(itn.getValue(), 0, itn.getValue().size() - 1, sorted_tols);

			int n = sorted_tols.size();
			if (n < 1)
			{
				itn = it.next();
				continue;
			}
			Iterator<Double> tol = sorted_tols.iterator();
			double maxdv = -1;
			double rst = 0;
//C++ TO JAVA CONVERTER TODO TASK: Iterators are only converted within the context of 'while' and 'for' loops:
			double toln= tol.next();
                        double lasttol = toln;
                        
			for (int z = 0; z < n; z++)
			{
//C++ TO JAVA CONVERTER TODO TASK: Iterators are only converted within the context of 'while' and 'for' loops:
				if (toln > e)
				{
					break;
				}

//C++ TO JAVA CONVERTER TODO TASK: Iterators are only converted within the context of 'while' and 'for' loops:
				double dv = toln - lasttol;
				//double dv = *tol/lasttol;
				if (maxdv < dv)
				{
					maxdv = dv;
//C++ TO JAVA CONVERTER TODO TASK: Iterators are only converted within the context of 'while' and 'for' loops:
					rst = toln;
				}

//C++ TO JAVA CONVERTER TODO TASK: Iterators are only converted within the context of 'while' and 'for' loops:
				lasttol = toln;
//C++ TO JAVA CONVERTER TODO TASK: Iterators are only converted within the context of 'while' and 'for' loops:
				toln= tol.next();
			}

			tolsum += rst;
			cnt++;
		}
		return tolsum / cnt;
	}

        public final int computeLambda(int m, int k, double reduc)
	{
		//double nm = _buf->num_tstamp() * _buf->num_obj() - _buf->num_points();
		//if(nm==0)
		//	return 2;
		//return 2 * _buf->num_tstamp() * _buf->num_obj() / nm * 100/reduc;

		//double nm = _buf->num_tstamp() *  _buf->num_obj() * _buf->num_obj()/1620;
		//if(nm==0)
		//	return 2;

		int c = (int)((1 - k / _buf.avglen()) * 10);
		if (c <= 0)
		{
			c = 1;
		}
		return (int) (2 * c * 100 / reduc);
	}


	public static class PolyLines extends ArrayList<PolyLine>
	{
		public MBR _ext = new MBR();
                @Override
		public final boolean add(PolyLine l)
		{
			_ext.unionWith(l.ext);
			return super.add(l);
		}
	}



	protected TreeMap<Integer,PolyLines> _tpart = new TreeMap<Integer,PolyLines>(); // <id,polylines> time partition
	protected TreeMap<Integer,GridLine> _gline = new TreeMap<Integer,GridLine>();
	protected TreeMap<Integer,IDistance> _idist = new TreeMap<Integer,IDistance>();
	protected int _lamda; // the number of time of each time partition
	protected double _tol; // tolerance for simplification
	protected boolean _star; // spatio-temporal mode    cuts-star
	protected boolean _plus; // optimization for DP performance, cuts-plus
	protected boolean _acttol;
	protected double _reduc; // reduction ratio.
	protected double _effec;
	protected int _batch;
	protected long _simple_t = Calendar.getInstance().getTimeInMillis();
	protected long _refine_t = Calendar.getInstance().getTimeInMillis(); // elapsed time of refinement step
	protected long _filter_t = Calendar.getInstance().getTimeInMillis(); // elapsed time of  step
	protected ArrayList<Convoy> _cands = new ArrayList<Convoy>(); // candidates
	protected LinkedList<Point> _splits = new LinkedList<Point>();

        public static final int NOINDEX = 0;
	public static final int IDISTANCE = 1;
	public static final int GRIDLINE = 2;
        
        public void dp(ArrayList<Point> v, int j, int k, ArrayList<Double> tols)
	{
		if (k <= j + 1)
		{
			return; // there is nothing to simplify
		}
		int maxi = j; // index of vertex farthest from S
		double maxd2 = 0; // distance squared of farthest vertex
		int ceni = j + (int)(k - j) / 2; // index of the center of S. it's for dp-plus
		int mincend = (int)(k - j) / 2; // index of the minimum distance to ceni
		int minceni = -1;
		Point u = new Point();
		Point w = new Point();
		Point Pb = new Point();
		u = v.get(k).subtract( v.get(j));
		double cu = DOT(u,u); // segment length squared
		double b; // dv2 = distance v->at(i) to S squared
		double cw;
		double dv2;
		double tol2 = _tol * _tol;
    
		for (int i = j + 1; i < k; i++)
		{
			if (_star) // spatio-temporal => time ratio distance from the beginning point
			{
				b = (double)(v.get(i).t - v.get(j).t) / (double)(v.get(k).t - v.get(j).t);
				Pb = v.get(j).add( u.multiply( b));
				dv2 = PDIST2(v.get(i), Pb);
			}
			else // spatial => original DP
			{
				w = v.get(i).subtract( v.get(j));
				cw = DOT(w,u);
    
				if (cw <= 0) // current point is left of the segment
				{
					dv2 = PDIST2(v.get(i), v.get(j));
				}
				else if (cu <= cw) // current point is right of the segment
				{
					dv2 = PDIST2(v.get(i), v.get(k));
				}
				else
				{ // current point is over the segment
					b = cw / cu;
					Pb = v.get(j).add( u.multiply( b));
					dv2 = PDIST2(v.get(i), Pb);
				}
			}
    
			if (_plus)
			{
				int idist = Math.abs(ceni - i);
				if (idist < mincend)
				{
					mincend = idist;
					minceni = i;
				}
			}
    
			// test with current max distance squared
			if (dv2 <= maxd2)
			{
				continue;
			}
    
			maxi = i;
			maxd2 = dv2;
		}
    
		if (maxd2 > tol2)
		{
			if (_plus && minceni > -1)
			{
				maxi = minceni;
			}
    
			tols.set(maxi, Math.sqrt(maxd2));
			dp(v, j, maxi, tols); // polyline v->at(j) to v->at(maxi)
			dp(v, maxi, k, tols); // polyline v->at(maxi) to v->at(k)
		}
		else
		{
			for (int i = j + 1; i < k; i++) // putting individual tolerances
			{
				tols.set(i, Math.sqrt(maxd2));
			}
		}
	}

        public double DOT(Point u, Point v) {
            return ((u).x * (v).x + (u).y * (v).y);
        }
        public void compute_tols(ArrayList<Point> v, int j, int k, TreeSet<Double> order)
	{
		if (k <= j + 1)
		{
			return; // there is nothing to simplify
		}
		int maxi = j; // index of vertex farthest from S
		double maxd2 = 0; // distance squared of farthest vertex
		int ceni = j + (int)(k - j) / 2; // index of the center of S. it's for dp-plus
		int mincend = (int)(k - j) / 2; // index of the minimum distance to ceni
		int minceni = -1;
		Point u = new Point();
		Point w = new Point();
		Point Pb = new Point();
		u = v.get(k).subtract( v.get(j));
		double cu = DOT(u,u); // segment length squared
		double b; // dv2 = distance v->at(i) to S squared
		double cw;
		double dv2;
    
		for (int i = j + 1; i < k; i++)
		{
			if (_star) // spatio-temporal => time ratio distance from the beginning point
			{
				b = (double)(v.get(i).t - v.get(j).t) / (double)(v.get(k).t - v.get(j).t);
				Pb = v.get(j).add( u.multiply(b));
				dv2 = PDIST2(v.get(i), Pb);
			}
			else // spatial => original DP
			{
				w = v.get(i).subtract(v.get(j));
				cw = DOT(w,u);
    
				if (cw <= 0) // current point is left of the segment
				{
					dv2 = PDIST2(v.get(i), v.get(j));
				}
				else if (cu <= cw) // current point is right of the segment
				{
					dv2 = PDIST2(v.get(i), v.get(k));
				}
				else
				{ // current point is over the segment
					b = cw / cu;
					Pb = v.get(j).add(u.multiply( b));
					dv2 = PDIST2(v.get(i), Pb);
				}
			}
    
			if (_plus)
			{
				int idist = Math.abs(ceni - i);
				if (idist < mincend)
				{
					mincend = idist;
					minceni = i;
				}
			}
    
			// test with current max distance squared
			if (dv2 <= maxd2)
			{
				continue;
			}
    
			maxi = i;
			maxd2 = dv2;
		}
    
		if (maxd2 > 0)
		{
			if (_plus && minceni > -1)
			{
				maxi = minceni;
			}
			order.add(Math.sqrt(maxd2));
			compute_tols(v, j, maxi, order); // polyline v->at(j) to v->at(maxi)
			compute_tols(v, maxi, k, order); // polyline v->at(maxi) to v->at(k)
		}
	}


        protected final void refine(int m, int k, double e)
	{
		if (_cands.size() < 1)
		{
			return;
		}

		long start = Calendar.getInstance().getTimeInMillis();
		ArrayList<Convoy> newrst = new ArrayList<Convoy>();
		_fpos = _fneg = _exac = _corr = 0;

		for (int i = 0; i < _cands.size(); i++) // for old results
		{
			ArrayList<Convoy> rtn = new ArrayList<Convoy>();
			verify(_cands.get(i), rtn, m, k, e);
			for (int z = 0;z < rtn.size(); z++)
			{
//				_rst.push_back(rtn.get(z));
                                _rst.add(rtn.get(z));
			}
		}
		_refine_t = Calendar.getInstance().getTimeInMillis()- start;
	}

        public void verify(Convoy cand, ArrayList<Convoy> rst, int m, int k, double e)
	{
		int csize = cand.size();
		ArrayList<Convoy> V = new ArrayList<Convoy>(); // set of current clusters
		int s = cand._ts;
                //System.out.println("call beforeof : " + s + " : " +  cand._te );
		int be = _buf.beforeof(cand._te); // before ending
		while (s <= cand._te)
		{
                    //System.out.println("while start: " + s + " : " +  cand._te );
			ArrayList<Convoy> Vnext = new ArrayList<Convoy>();
			Iterator<Convoy> c;
			Iterator<Convoy> v;
    
			/*	DBSCAN for only objects that the candidate contains */	
			ArrayList<IPoint> tmp = _buf.objAtTime(s);
			int n = tmp.size();
			if (n < m || s == be)
			{   
                                //System.out.println("Inside if ");
				for (v = V.iterator(); v.hasNext();)
				{   
                                    Convoy vn = v.next();
					if (vn.lifetime() >= k)
					{
						output(vn);
					}
				}
				if (s == be)
				{
					return;
				}
				V = new ArrayList<Convoy>(Vnext);
				s = _buf.nextof(s);
				continue;
			}
                        //System.out.println("After if ");
			ArrayList<IPoint> pts = new ArrayList<IPoint>();
			pts.ensureCapacity(csize);
			for (int i = 0; i < n; i++)
			{   
                                //System.out.println("inside for ");
				if (cand.contains(tmp.get(i).o) )
				{
					pts.add(tmp.get(i));
				}
			}
                        //System.out.println("Dbscan  ");    
			Dbscan ds = new Dbscan(pts, m, e);
			ds.perform();
    
			ArrayList<Convoy> L = ds.getClusterSet();
                        //System.out.println("getClusterSet  ");  
			for (v = V.iterator(); v.hasNext();)
			{
				Convoy current = v.next();
                                //System.out.println("getClusterSet  " + current); 
				current._assigned = false;
				for (c = L.iterator(); c.hasNext();)
				{
                                        Convoy cn = c.next();
					ArrayList<Integer> is = new ArrayList<Integer>();
                                        is.addAll(current);
                                        is.retainAll(cn);
//					Iterator<Integer> intsecit = set_intersection(v.begin(),v.end(),c.begin(), c.end(),is.iterator());
	//C++ TO JAVA CONVERTER TODO TASK: There is no direct equivalent to the STL vector 'erase' method in Java:
//					is.erase(intsecit, is.end()); // trim rst to contain only results
					int is_size = is.size();
					if (is_size >= m) // verification of convoy
					{
						current._assigned = true;
						current._te = s;
						current.clear(); // Vnext = Vnext U (v intersect c)
//						v.insert(is.iterator(), is.end());
                                                current.addAll(is);
						Vnext.add(current);
						cn._assigned = true;
					}
				}
				if (!current._assigned && current.lifetime() >= k)
				{
					output(current);
				}
			}
                        //System.out.println("c = L.iterator(); c.hasNext();  ");  
			for (c = L.iterator(); c.hasNext();)
			{
                            Convoy cn = c.next();
				if (!cn._assigned)
				{
					cn._ts = cn._te = s;
					Vnext.add(cn);
				}
			}
    
			V = new ArrayList<Convoy>(Vnext);
                        //System.out.println("call nextof : " + s + " : " +  cand._te );
			s = _buf.nextof(s);
                        //System.out.println("after call nextof : " + s + " : " +  cand._te );
		}
	}

        public void candidate(Convoy c)
	{
		if (_cands.isEmpty())
		{
			_cands.add(c);
			return;
		}
    
		if (_batch == 0) // no batch
		{
			_cands.add(c);
		}
		else if (_batch == 1) // semi-batch processing
		{
			for (int j = 0; j < _cands.size(); j++)
			{
				if ((c._te < _cands.get(j)._ts) || (_cands.get(j)._te < c._ts))
				{
					continue;
				}
    
				TreeSet<Integer> is = new TreeSet<Integer>();
                                is.addAll(_cands.get(j));
                                is.retainAll(c);
//				set_intersection(_cands[j].begin(),_cands[j].end(), c.begin(),c.end(), insert_iterator<TreeSet<Integer>>(is,is.iterator()));
				//if (is.size()==c.size())
				int threshold = (int)(c.size() * 0.8);
				if (is.size() >= threshold) // 90% overlapping
				{
					_cands.get(j)._ts = Math.min(_cands.get(j)._ts,c._ts);
					_cands.get(j)._te = Math.max(_cands.get(j)._te,c._te);
					return;
				}
			}
			_cands.add(c);
		}
		else if (_batch == 2) // batch
		{
			for (int j = 0; j < _cands.size(); j++)
			{
				if ((c._te < _cands.get(j)._ts) || (_cands.get(j)._te < c._ts))
				{
					continue;
				}
    
				_cands.get(j).addAssignment( c);
				return;
			}
			_cands.add(c);
		}
	}

        protected final void analyze(ArrayList<Convoy> cands)
	{
		if (cands.size() < 1)
		{
			return;
		}
		_fpos = _fneg = _exac = _corr = 0;

		for (int i = 0; i < _rst.size(); i++) // for old results
		{
			for (int j = 0; j < cands.size(); j++)
			{
                            //System.out.println("Check " + i + " : " + j + " : " + _rst.get(i) + " : " + cands.get(j));
				if (_rst.get(i).equalsTo(cands.get(j)))
				{
					_exac++;
				}
                                
				else if (_rst.get(i).lessThanOrEqualTo( cands.get(j)))
				{
					_corr++;
				}
			}
		}
		_fpos2 = (int) ((1.0 - (_exac + _corr) / (double)cands.size()) * 100);
		_fneg2 = (int) ((1.0 - (_exac + _corr) / (double)_rst.size()) * 100);
		_fpos = (int) ((1.0 - (_exac) / (double)cands.size()) * 100);
		_fneg = (int) ((1.0 - (_exac) / (double)_rst.size()) * 100);
	}
	protected final void analyze2(ArrayList<Convoy> cands)
	{
		_effec = 0;
		for (int j = 0; j < cands.size(); j++)
		{
			_effec += cands.get(j).size() * cands.get(j).size() * cands.get(j).lifetime();
		}
	}

        protected final long buildIndex()
	{
		if (!_gline.isEmpty())
		{
			_gline.clear();
		}
		if (!_idist.isEmpty())
		{
			_idist.clear();
		}

		long start = Calendar.getInstance().getTimeInMillis();

		Iterator<Map.Entry<Integer,PolyLines>> mi = _tpart.entrySet().iterator();
		int i = 0;
		while (mi.hasNext()) // for each polyline groups, it is sorted by time
		{
			Map.Entry<Integer,PolyLines> current = mi.next();
			if (current.getValue().size() >= 4)
			{
				if (_index == IDISTANCE)
				{
					_idist.get(i).build(current.getValue(), _star, _e, _tol);
				}
				else if (_index == GRIDLINE)
				{
					_gline.get(i).build(current.getValue(), _star, _e, _tol);
				}
			}
			i++;
		}
		return Calendar.getInstance().getTimeInMillis() - start;
	}

	protected final void defaultSetting()
	{
		super.initialize();
		_star = _plus = false;
		_simple_t = _refine_t = _index_t = 0;
		_batch = 1;
		_reduc = 90.0;
		_lamda = -1;
		_tol = -1.0;
		_index = NOINDEX;
		_acttol = true;
	}
}

