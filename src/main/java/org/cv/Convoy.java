/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cv;

/**
 *
 * @author Analysis
 */

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Convoy extends TreeSet<Integer> implements Comparable<Convoy>
{
	public int compareTo(Convoy otherInstance)
	{
		if (lessThan(otherInstance))
		{
			return -1;
		}
		else if (otherInstance.lessThan(this))
		{
			return 1;
		}

		return 0;
	}

	public Convoy()
	{
		_ts = _te = -1;
		_assigned = false;
	}
	public Convoy(int start, int end)
	{
		_ts = start;
		_te = end;
	}
//	public void dispose()
//	{
//	}
        
	public int _ts; // start of convoy duration (time)
	public int _te; // end of convoy duration (time)
	public boolean _assigned; // for moving cluster

	public final int lifetime()
	{
		return _te - _ts + 1;
	} // the time length of this convoy
	public final boolean has(int oid)
	{
		return (this.contains(oid));
	}
	public final boolean has(int oid, int time)
	{
		// when no given data, objs.find() will return objs.end()
		boolean c1 = this.contains(oid);
		boolean c2 = _ts <= time;
		boolean c3 = _te >= time;
		return (c1 && c2 && c3);
	}
//	public final void add(int oid)
//	{
//		if (!this.a(oid).second) // if oid already exists
//		{
//			System.out.print("Convoy::addMember - added member already exists");
//		}
//	}
//	public final void add(TreeSet<Integer> oids)
//	{
//		for (Iterator<Integer> oid = oids.iterator(); oid.hasNext();)
//		{
//			add(oid.next());
//		}
//	}
	public final void setLifetime(int start, int end)
	{
		_ts = start;
		_te = end;
	}
	public final void print()
	{
		System.out.print("k=");
		System.out.print(_te - _ts + 1);
		System.out.print("[");
		System.out.print(_ts);
		System.out.print(",");
		System.out.print(_te);
		System.out.print("], m=");
		System.out.print(size());
		System.out.print("[");
                for (Iterator<Integer> iterator = this.iterator(); iterator.hasNext();) {
                    Integer next = iterator.next();
                	System.out.print(",");
			
			System.out.print(next);
		}
		System.out.print("]");
		System.out.print("\n");
	}
	public final void write(FileWriter fout)
	{
            try {
                fout.write( "k=" + (_te - _ts + 1) + "[" + _ts + "," + _te + "], m=" + size() + "[");
                for (Iterator<Integer> it = this.iterator(); it.hasNext();)
                {
                     Integer next = it.next();
//                    if (it != begin())
//                    {
                        fout.write( ",");
//                    }

                     fout.write( next);
                }
                 fout.write( "]" + "\n");
            } catch (IOException ex) {
                Logger.getLogger(Convoy.class.getName()).log(Level.SEVERE, null, ex);
            }
	}
            public static void print(ArrayList<Convoy> fs)
	{
		//printf("number of cv: %d\n",fs->size());
		for (int i = 0; i < fs.size(); i++)
		{
			System.out.print("--------\n");
			fs.get(i).print();
		}
	}


	public boolean equalsTo(Convoy c)
	{
		if (c._ts != _ts || c._te != _te)
		{
			return false;
		}

		TreeSet<Integer> is = new TreeSet<Integer>();
                is.addAll(this);
                is.retainAll(c);
//		set_intersection(begin(),end(),c.iterator(),c.end(),insert_iterator<TreeSet<Integer>>(is,is.iterator()));
		return (is.size() == size());
	}
//	public boolean notEqualsTo(Convoy c)
//	{
//		return !(this.equalsTo(c));
//		//bool check1 = (c._ts==_ts && c._te==_te);
//		//set<int> is;
//		//set_intersection(begin(),end(),c.begin(),c.end(),insert_iterator<set<int>>(is,is.begin()));
//		//bool check2 = (is.size()==size());
//		//return (!check1 && !check2);
//	}

	// check if a given convoy is a superset of this
	public boolean lessThanOrEqualTo(Convoy c)
	{
             TreeSet<Integer>  temp = new TreeSet<Integer>();
            temp.addAll(this);
            temp.removeAll(c);
            return (temp.isEmpty()&& this.size()<=c.size());
//		if (c._ts > _ts || c._te < _te)
//		{
//			return false;
//		}
//
//		TreeSet<Integer> dif = new TreeSet<Integer>();
//		set_difference(begin(),end(),c.iterator(),c.end(),insert_iterator<TreeSet<Integer>>(dif,dif.iterator()));
//		return (dif.size() == 0 && size() <= c.size());
	}

	// check if a given convoy is a subset of this
	public boolean greaterThanOrEqualTo(Convoy c)
	{
                TreeSet<Integer>  temp = new TreeSet<Integer> ();
            temp.addAll(this);
            c.removeAll(temp);
            return (c.isEmpty()&& this.size()>=c.size());
//		if (c._ts < _ts || c._te> _te)
//		{
//			return false;
//		}
//
//		TreeSet<Integer> dif = new TreeSet<Integer>();
//		set_difference(c.iterator(),c.end(),begin(),end(),insert_iterator<TreeSet<Integer>>(dif,dif.iterator()));
//		return (dif.size() == 0 && size() >= c.size());
	}

	// check if a given convoy is a superset of this
	public boolean lessThan(Convoy c)
	{
            TreeSet<Integer>  temp = new TreeSet<Integer> ();
            temp.addAll(this);
            temp.removeAll(c);
            return (temp.isEmpty()&& this.size()<c.size());
//		if (c._ts >= _ts != 0 || c._te <= _te)
//		{
//			return false;
//		}
//
//		TreeSet<Integer> dif = new TreeSet<Integer>();
//		set_difference(begin(),end(),c.iterator(),c.end(),insert_iterator<TreeSet<Integer>>(dif,dif.iterator()));
//		return (dif.size() == 0 && size() < c.size());
	}

	// check if a given convoy is a subset of this
	public boolean greaterThan(Convoy c)
	{
            TreeSet<Integer>  temp = new TreeSet<Integer> ();
            temp.addAll(this);
            c.removeAll(temp);
            return (c.isEmpty()&& this.size()>c.size());
//		if (c._ts <= _ts != 0 || c._te >= _te)
//		{
//			return false;
//		}
//
//		TreeSet<Integer> dif = new TreeSet<Integer>();
//		set_difference(c.iterator(),c.end(),begin(),end(),insert_iterator<TreeSet<Integer>>(dif,dif.iterator()));
//		return (dif.size() == 0 && size() > c.size());
	}
            
	public void addAssignment(Convoy c)
	{
                this.addAll(c);
//		TreeSet<Integer> uni = new TreeSet<Integer>();
//		set_union(begin(),end(),c.iterator(),c.end(),insert_iterator<TreeSet<Integer>>(uni,uni.iterator()));
//		clear();
//		insert(uni.iterator(),uni.end());
		_ts = Math.min(_ts,c._ts);
		_te = Math.max(_te,c._te);
	}


}
