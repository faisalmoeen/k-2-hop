/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cv;

/**
 *
 * @author Analysis
 */

import org.cluster.Dbscan;
import org.common.Function;
import org.common.buffer.Buffer;
import org.common.geometry.IPoint;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

public class CoherentMovingCluster extends MovingCluster
{
	public CoherentMovingCluster(Buffer b)
	{
		_buf = b;
		initialize();
	}
	public void dispose()
	{
	}

	public final long discover(int m, int k)
	{
		initialize();
		_timedb_t = _buf._timedb_t;

		if (_e < 0)
		{
			_e = findEps(m,k);
		}
		if (_index!=0)
		{
			_index_t = buildIndex(_e);
		}
		_m = m;
		_k = k;

		_elapsed_t = Calendar.getInstance().getTimeInMillis();
		int n = _buf.num_tstamp();

		ArrayList<Convoy> V = new ArrayList<Convoy>(); // set of current clusters
		int s = 0; // sweep line of timestamps
		while (s < n)
		{
			ArrayList<Convoy> Vnext = new ArrayList<Convoy>();
			Iterator<Convoy> c;
			Iterator<Convoy> v;

			// less number of points in this timestamp
			ArrayList<IPoint> pts = _buf.objAtTstamp(s);
			if (pts.size() < m)
			{
				for (v = V.iterator(); v.hasNext();)
				{
                                    Convoy vn = v.next();
					if (vn.lifetime() >= k)
					{
						output(vn);
					}
				}
				s++;
				V = new ArrayList<Convoy>(Vnext);
				continue;
			}

			/*	DBSCAN */
			Dbscan ds = new Dbscan(pts,m,_e);
			if (_index!=0)
			{
				ds.setIndex(_grids.get(s));
			}
			ds.perform();

			/* adding a candidate set to result */
			ArrayList<Convoy> L = ds.getClusterSet();

			for (v = V.iterator(); v.hasNext();)
			{
				Convoy current = v.next();
				current._assigned = false;
				for (c = L.iterator(); c.hasNext();)
				{
                                    Convoy cn = c.next();
					ArrayList<Integer> is = new ArrayList<Integer>();
                                        is.addAll(current);
                                        is.retainAll(cn);
//					Iterator<Integer> intsecit = set_intersection(v.begin(),v.end(),c.begin(), c.end(),is.iterator());
//C++ TO JAVA CONVERTER TODO TASK: There is no direct equivalent to the STL vector 'erase' method in Java:
//					is.erase(intsecit, is.end()); // trim rst to contain only results
					int is_size = is.size();
					if (is_size >= m) // verification of convoy
					{
						current._assigned = true;
						current._te = _buf.timeof(s);
						current.clear(); // Vnext = Vnext U (v intersect c)
//						current.insert(is.iterator(), is.end());
                                                current.addAll(is);
						Vnext.add(current);
						cn._assigned = true;
					}
				}
				if (!current._assigned)
				{
					if (current.lifetime() >= k)
					{
						output(current);
					}
				}
			} // for (v)

			if (s == n - 1)
			{
				for (v = V.iterator(); v.hasNext();)
				{
                                    Convoy vn = v.next();
					if (vn.lifetime() >= k)
					{
						output(vn);
					}
				}
				break;
			}

			for (c = L.iterator(); c.hasNext();)
			{
                            Convoy cn = c.next();
				if (!cn._assigned)
				{
					cn._ts = cn._te = _buf.timeof(s);
					Vnext.add(cn);
				}
			}

			V = new ArrayList<Convoy>(Vnext);
			s++;
			if (_showprogress)
			{
				Function.progress(s,n);
			}
		}

		_elapsed_t = Calendar.getInstance().getTimeInMillis() - _elapsed_t + _index_t + _timedb_t;

		System.out.print("CMC ");
		print();
		return _elapsed_t;
	}
	public final long discover(int m, int k, double e)
	{
		setRange(e);
		return discover(m, k);
	}

}

