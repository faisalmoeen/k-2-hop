package org;

import com.google.common.base.Stopwatch;
import org.common.buffer.Buffer;
import org.cv.CoherentMovingCluster;
import org.cv.CuTS;

import java.io.*;
import java.util.Arrays;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by faisal on 1/7/17.
 */
public class Experiments {

    private static int[] kArray = {200,40,60,80,100,120,140,160,180};
    private static int[] mArray = {20,3,4,5,6,7,8,9,10};
    private static double[] eArray = {0.000006f, 0.00006f, 0.0006f, 0.006f, 0.06f, 0.6f};

    private static SortedMap<Integer,SortedMap<Integer, k2ExecutionTimeStruct>> k2ExpResults= new TreeMap<>();
    private static SortedMap<Integer,SortedMap<Integer, k2ExecutionTimeStruct>> vcodaExpResults= new TreeMap<>();

    private static char sep = ',';
    public static void main(String[] args){
        File inputDir = new File(args[0]);
        File outputFile = new File(args[1]+"/output.txt");
        FileWriter fo = null;
        try (BufferedReader br = new BufferedReader(new FileReader(args[2]))) {
            String mline = br.readLine();
            String kline = br.readLine();
            String eline = br.readLine();
            mArray = Arrays.stream(mline.split(","))
                    .map(String::trim).mapToInt(Integer::parseInt).toArray();
            kArray = Arrays.stream(kline.split(","))
                    .map(String::trim).mapToInt(Integer::parseInt).toArray();
            eArray = Arrays.stream(eline.split(","))
                    .map(String::trim).mapToDouble(Double::parseDouble).toArray();
        }catch (IOException e){
            e.printStackTrace();
        }
        PrintWriter pw=null;
        try {
            outputFile.createNewFile();
            pw = new PrintWriter(outputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] inputFiles = inputDir.list();
        Stopwatch timer = Stopwatch.createUnstarted();
        for (String inputFile :
                inputFiles) {
            String inputFilePath = inputDir+"/"+inputFile;
            for (int m :
                    mArray) {
                k2ExpResults.put(m, new TreeMap<Integer, k2ExecutionTimeStruct>());
                vcodaExpResults.put(m, new TreeMap<Integer, k2ExecutionTimeStruct>());
                for (int k:
                        kArray) {
                    for (double e :
                            eArray) {

                        timer.reset();
                        timer.start();

                        Buffer b = new Buffer(inputFilePath);
                        CoherentMovingCluster cmc = new CoherentMovingCluster(b);
                        cmc.useIndex(0);
                        cmc.discover(m,k,e);

                        timer.stop();
                        k2ExecutionTimeStruct cmcTimeStruct = new k2ExecutionTimeStruct();
                        cmcTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
                        pw.println(inputFilePath+sep+m+sep+k+sep+e+sep+"cmc"+sep+cmcTimeStruct.getText(sep));
                        pw.flush();

//                      ***********************************************************
//                      ***********************************************************
                        timer.reset();
                        timer.start();

                        b = new Buffer(inputFilePath);
                        cmc = new CoherentMovingCluster(b);
                        cmc.useIndex(1);
                        cmc.discover(m,k,e);

                        timer.stop();
                        cmcTimeStruct = new k2ExecutionTimeStruct();
                        cmcTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
                        pw.println(inputFilePath+sep+m+sep+k+sep+e+sep+"cmc-index"+sep+cmcTimeStruct.getText(sep));
                        pw.flush();
//                      ***********************************************************
//                      ***********************************************************

                        timer.reset();
                        timer.start();

                        b = new Buffer(inputFilePath);
                        CuTS c = new CuTS(b);
                        c.lambda(4);
                        c.star(false);
                        c.plus(false);
                        c.simplify(8);
                        c.discover(m,k,e);

                        timer.stop();
                        cmcTimeStruct = new k2ExecutionTimeStruct();
                        cmcTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
                        pw.println(inputFilePath+sep+m+sep+k+sep+e+sep+"CuTS"+sep+cmcTimeStruct.getText(sep));
                        pw.flush();
//                      *************************************************************
//                      ***********************************************************

                        timer.reset();
                        timer.start();

                        b = new Buffer(inputFilePath);
                        c = new CuTS(b);
                        c.lambda(4);
                        c.star(false);
                        c.plus(true);
                        c.simplify(8);
                        c.discover(m,k,e);

                        timer.stop();
                        cmcTimeStruct = new k2ExecutionTimeStruct();
                        cmcTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
                        pw.println(inputFilePath+sep+m+sep+k+sep+e+sep+"CuTS+"+sep+cmcTimeStruct.getText(sep));
                        pw.flush();
//                      *************************************************************
//                      ***********************************************************


                        timer.reset();
                        timer.start();

                        b = new Buffer(inputFilePath);
                        c = new CuTS(b);
                        c.lambda(4);
                        c.star(true);
                        c.plus(false);
                        c.simplify(8);
                        c.discover(m,k,e);

                        timer.stop();
                        cmcTimeStruct = new k2ExecutionTimeStruct();
                        cmcTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
                        pw.println(inputFilePath+sep+m+sep+k+sep+e+sep+"CuTS*"+sep+cmcTimeStruct.getText(sep));
                        pw.flush();
//                      *************************************************************
//                      ***********************************************************



                    }

                }
            }
        }
//        pw.flush();
//        for (String inputFile :
//                inputFiles) {
//            String inputFilePath = inputDir+"/"+inputFile;
//            for (int k:
//                    kArray) {
//                K2SeqDataManager dataManager =  new K2SeqDataManager(inputFilePath, k);
//                k2ExecutionTimeStruct timeStruct = new k2ExecutionTimeStruct();
//                timer.reset();
//                timer.start();
//                while(dataManager.getNextK2Batch()!=null){
//
//                }
//                timer.stop();
//                timeStruct.timeDataLoading = timer.elapsed(TimeUnit.MILLISECONDS);
//                pw.println(inputFilePath+sep+0+sep+k+sep+timeStruct.getText(sep));
//            }
//        }
//        pw.flush();
        pw.close();
    }
}
