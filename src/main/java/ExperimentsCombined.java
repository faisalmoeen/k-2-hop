import com.google.common.base.Stopwatch;
import dk.aau.convoy.K2Seq;
import dk.aau.convoy.model.Convoy;
import dk.aau.convoy.model.k2ExecutionTimeStruct;
import dk.aau.convoy.utils.K2SeqDataManager;
import org.apache.flink.api.java.tuple.Tuple2;
import org.common.buffer.Buffer;
import org.cv.CoherentMovingCluster;
import org.cv.CuTS;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by faisal on 8/29/16.
 */
public class ExperimentsCombined {

//	private static String inputFilePath = "/media/faisal/Cold Storage/Cloud Storage/owncloud/PhD Work/working-folder/experiments/trucks_dataset/trucks273s.txt";
//	private static String inputFilePath = "/media/faisal/Cold Storage/data/Birkinhoff/output/1.txt";
	private static int[] kArray = {200,40,60,80,100,120,140,160,180};
	private static int[] mArray = {20,3,4,5,6,7,8,9,10};
	private static double[] eArray = {0.000006f, 0.00006f, 0.0006f, 0.006f, 0.06f, 0.6f};
	private static double eps = 0.00006;
	private static int batch;
	private static SortedMap<Integer,SortedMap<Integer,k2ExecutionTimeStruct>> k2ExpResults= new TreeMap<>();
	private static SortedMap<Integer,SortedMap<Integer,k2ExecutionTimeStruct>> vcodaExpResults= new TreeMap<>();
	private static char sep = ',';

	public static void main(String[] args) throws IOException, InterruptedException {
		int m = Integer.parseInt(args[1]);
		int k = Integer.parseInt(args[2]);
		double e = Double.parseDouble(args[3]);
		String inputFilePath = args[4];
		String outputFilePath = args[5];
		PrintWriter pw = null;
		FileWriter fw = null;
		try {
			fw = new FileWriter(outputFilePath, true);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		BufferedWriter bw = new BufferedWriter(fw);
		pw = new PrintWriter(bw);
		Stopwatch timer = Stopwatch.createUnstarted();
		Buffer b = null;
		CoherentMovingCluster cmc = null;
		org.k2ExecutionTimeStruct cmcTimeStruct = null;
		CuTS c = null;

		switch (args[0]) {
			case "k2-streaming":
				System.out.println("Running k2-Streaming m=" + m + ",k=" + k + ",e=" + e);
				timer.reset();
				timer.start();

				Tuple2<List<Convoy>,k2ExecutionTimeStruct> k2StrStrictConvoys = K2Seq.k2SeqStreaming(inputFilePath, e, m, k);

				timer.stop();
				k2ExecutionTimeStruct k2StrTimeStruct = k2StrStrictConvoys.f1;
				k2StrTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
				pw.println(inputFilePath + sep + m + sep + k + sep + e + sep + "k2-streaming" + sep + k2StrTimeStruct.getText(sep));
				System.out.println(inputFilePath + sep + m + sep + k + sep + e + sep + "k2-streaming" + sep + k2StrTimeStruct.getText(sep));
				pw.flush();

				break;

			case "k2indl":
				System.out.println("Running k2 Indl m=" + m + ",k=" + k + ",e=" + e);
				timer.reset();
				timer.start();

				Tuple2<List<Convoy>,k2ExecutionTimeStruct> k2strictConvoys = K2Seq.k2Seq(inputFilePath, e, m, k);

				timer.stop();
				k2ExecutionTimeStruct k2TimeStruct = k2strictConvoys.f1;
				k2TimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
				pw.println(inputFilePath + sep + m + sep + k + sep + e + sep + "k2indl" + sep + k2TimeStruct.getText(sep));
				System.out.println(inputFilePath + sep + m + sep + k + sep + e + sep + "k2indl" + sep + k2TimeStruct.getText(sep));
				pw.flush();

				break;

			case "k2batch":
				System.out.println("Running k2 Batch m=" + m + ",k=" + k + ",e=" + e);
				timer.reset();
				timer.start();

				Tuple2<List<Convoy>,k2ExecutionTimeStruct> k2BatchStrictConvoys = K2Seq.k2SeqBatch(inputFilePath, e, m, k);

				timer.stop();
				k2ExecutionTimeStruct k2BatchTimeStruct = k2BatchStrictConvoys.f1;
				k2BatchTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
				pw.println(inputFilePath + sep + m + sep + k + sep + e + sep + "k2batch" + sep + k2BatchTimeStruct.getText(sep));
				System.out.println(inputFilePath + sep + m + sep + k + sep + e + sep + "k2batch" + sep + k2BatchTimeStruct.getText(sep));
				pw.flush();

				break;
			case "k2hbaseindl":
				System.out.println("Running k2 hbase indl m=" + m + ",k=" + k + ",e=" + e);
				timer.reset();
				timer.start();

				Tuple2<List<Convoy>,k2ExecutionTimeStruct> k2HBaseStrictConvoys = K2Seq.k2SeqHBaseIndl(inputFilePath, e, m, k);

				timer.stop();
				k2ExecutionTimeStruct k2HBaseTimeStruct = k2HBaseStrictConvoys.f1;
				k2HBaseTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
				pw.println(inputFilePath + sep + m + sep + k + sep + e + sep + "k2hbaseindl" + sep + k2HBaseTimeStruct.getText(sep));
				System.out.println(inputFilePath + sep + m + sep + k + sep + e + sep + "k2hbaseindl" + sep + k2HBaseTimeStruct.getText(sep));
				pw.flush();

				break;

			case "k2hbasebatch":
				System.out.println("Running k2 hbase batch m=" + m + ",k=" + k + ",e=" + e);
				timer.reset();
				timer.start();

				Tuple2<List<Convoy>,k2ExecutionTimeStruct> k2HBaseBatchStrictConvoys = K2Seq.k2SeqHBaseBatch(inputFilePath, e, m, k);

				timer.stop();
				k2ExecutionTimeStruct k2HBaseBatchTimeStruct = k2HBaseBatchStrictConvoys.f1;
				k2HBaseBatchTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
				pw.println(inputFilePath + sep + m + sep + k + sep + e + sep + "k2hbasebatch" + sep + k2HBaseBatchTimeStruct.getText(sep));
				System.out.println(inputFilePath + sep + m + sep + k + sep + e + sep + "k2hbasebatch" + sep + k2HBaseBatchTimeStruct.getText(sep));
				pw.flush();

				break;

			case "k2rdbmsindl":
				System.out.println("Running k2 rdbms indl m=" + m + ",k=" + k + ",e=" + e);
				timer.reset();
				timer.start();

				Tuple2<List<Convoy>,k2ExecutionTimeStruct> k2HRDBMSIndlStrictConvoys = K2Seq.k2SeqRDBMSIndl(inputFilePath, e, m, k);

				timer.stop();
				k2ExecutionTimeStruct k2RDBMSIndlTimeStruct = k2HRDBMSIndlStrictConvoys.f1;
				k2RDBMSIndlTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
				pw.println(inputFilePath + sep + m + sep + k + sep + e + sep + "k2rdbmsIndl" + sep + k2RDBMSIndlTimeStruct.getText(sep));
				System.out.println(inputFilePath + sep + m + sep + k + sep + e + sep + "k2rdbmsIndl" + sep + k2RDBMSIndlTimeStruct.getText(sep));
				pw.flush();

				break;

			case "k2rdbmsbatch":
				System.out.println("Running k2 rdbms batch m=" + m + ",k=" + k + ",e=" + e);
				timer.reset();
				timer.start();

				Tuple2<List<Convoy>,k2ExecutionTimeStruct> k2HRDBMSBatchStrictConvoys = K2Seq.k2SeqRDBMSBatch(inputFilePath, e, m, k);

				timer.stop();
				k2ExecutionTimeStruct k2RDBMSBatchTimeStruct = k2HRDBMSBatchStrictConvoys.f1;
				k2RDBMSBatchTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
				pw.println(inputFilePath + sep + m + sep + k + sep + e + sep + "k2rdbmsbatch" + sep + k2RDBMSBatchTimeStruct.getText(sep));
				System.out.println(inputFilePath + sep + m + sep + k + sep + e + sep + "k2rdbmsbatch" + sep + k2RDBMSBatchTimeStruct.getText(sep));
				pw.flush();

				break;

			case "vcoda":
				System.out.println("Running Vcoda m=" + m + ",k=" + k + ",e=" + e);
				timer.reset();
				timer.start();

				K2Seq.k2Seq(inputFilePath, e, m, k);
				Tuple2<List<Convoy>,k2ExecutionTimeStruct> vCodaStrictConvoys = K2Seq.vcoda(inputFilePath, e, m, k);

				timer.stop();
				k2ExecutionTimeStruct vcodaTimeStruct = vCodaStrictConvoys.f1;
				vcodaTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
				pw.println(inputFilePath + sep + m + sep + k + sep + e + sep + "vcoda" + sep + vcodaTimeStruct.getText(sep));
				System.out.println(inputFilePath + sep + m + sep + k + sep + e + sep + "vcoda" + sep + vcodaTimeStruct.getText(sep));
				pw.flush();

				break;
			case "vcodanlogn":
				System.out.println("Running Vcoda Nlogn m=" + m + ",k=" + k + ",e=" + e);
				timer.reset();
				timer.start();

//				K2Seq.k2Seq(inputFilePath, e, m, k);
				Tuple2<List<Convoy>,k2ExecutionTimeStruct> vCodaNLogNStrictConvoys = K2Seq.vcodaNlogN(inputFilePath, e, m, k);

				timer.stop();
				k2ExecutionTimeStruct vcodaNLogNTimeStruct = vCodaNLogNStrictConvoys.f1;
				vcodaNLogNTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
				pw.println(inputFilePath + sep + m + sep + k + sep + e + sep + "vcodanlogn" + sep + vcodaNLogNTimeStruct.getText(sep));
				System.out.println(inputFilePath + sep + m + sep + k + sep + e + sep + "vcodanlogn" + sep + vcodaNLogNTimeStruct.getText(sep));
				pw.flush();

				break;
			case "cmc":
				timer.reset();
				timer.start();

				b =  new Buffer(inputFilePath);
				cmc = new CoherentMovingCluster(b);
				cmc.useIndex(0);
				cmc.discover(m,k,e);

				timer.stop();
				cmcTimeStruct = new org.k2ExecutionTimeStruct();
				cmcTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
				pw.println(inputFilePath+sep+m+sep+k+sep+e+sep+"cmc"+sep+cmcTimeStruct.getText(sep));
				System.out.println(inputFilePath+sep+m+sep+k+sep+e+sep+"cmc"+sep+cmcTimeStruct.getText(sep));
				pw.flush();

				break;
			case "cmc-index":
				timer.reset();
				timer.start();

				b = new Buffer(inputFilePath);
				cmc = new CoherentMovingCluster(b);
				cmc.useIndex(1);
				cmc.discover(m,k,e);

				timer.stop();
				cmcTimeStruct = new org.k2ExecutionTimeStruct();
				cmcTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
				pw.println(inputFilePath+sep+m+sep+k+sep+e+sep+"cmc-index"+sep+cmcTimeStruct.getText(sep));
				System.out.println(inputFilePath+sep+m+sep+k+sep+e+sep+"cmc-index"+sep+cmcTimeStruct.getText(sep));
				pw.flush();

				break;
			case "cuts":
				timer.reset();
				timer.start();

				b = new Buffer(inputFilePath);
				c = new CuTS(b);
				c.lambda(4);
				c.star(false);
				c.plus(false);
				c.simplify(8);
				c.discover(m,k,e);

				timer.stop();
				cmcTimeStruct = new org.k2ExecutionTimeStruct();
				cmcTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
				pw.println(inputFilePath+sep+m+sep+k+sep+e+sep+"CuTS"+sep+cmcTimeStruct.getText(sep));
				System.out.println(inputFilePath+sep+m+sep+k+sep+e+sep+"CuTS"+sep+cmcTimeStruct.getText(sep));
				pw.flush();
				break;
			case "cuts+":
				timer.reset();
				timer.start();

				b = new Buffer(inputFilePath);
				c = new CuTS(b);
				c.lambda(4);
				c.star(false);
				c.plus(true);
				c.simplify(8);
				c.discover(m,k,e);

				timer.stop();
				cmcTimeStruct = new org.k2ExecutionTimeStruct();
				cmcTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
				pw.println(inputFilePath+sep+m+sep+k+sep+e+sep+"CuTS+"+sep+cmcTimeStruct.getText(sep));
				System.out.println(inputFilePath+sep+m+sep+k+sep+e+sep+"CuTS+"+sep+cmcTimeStruct.getText(sep));
				pw.flush();
				break;
			case "cuts*":
				timer.reset();
				timer.start();

				b = new Buffer(inputFilePath);
				c = new CuTS(b);
				c.lambda(4);
				c.star(true);
				c.plus(false);
				c.simplify(8);
				c.discover(m,k,e);

				timer.stop();
				cmcTimeStruct = new org.k2ExecutionTimeStruct();
				cmcTimeStruct.timeTotal = timer.elapsed(TimeUnit.MILLISECONDS);
				pw.println(inputFilePath+sep+m+sep+k+sep+e+sep+"CuTS*"+sep+cmcTimeStruct.getText(sep));
				pw.flush();
				break;
			case "load-test":
				System.out.println("Running Load test");
				K2SeqDataManager dataManager =  new K2SeqDataManager(inputFilePath, k);
				k2ExecutionTimeStruct timeStruct = new k2ExecutionTimeStruct();
				timer.reset();
				timer.start();

				while(dataManager.getNextK2Batch()!=null){

				}

				timer.stop();
				timeStruct.timeDataLoading = timer.elapsed(TimeUnit.MILLISECONDS);
				pw.println(inputFilePath + sep + 0 + sep + k + sep + 0 + sep + "load-test" + sep + timeStruct.getText(sep));
				pw.flush();

			default:
		}


		pw.close();
	}


}
