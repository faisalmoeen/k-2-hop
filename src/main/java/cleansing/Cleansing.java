/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cleansing;

import java.io.Console;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Analysis
 */
public class Cleansing {


    
    public static void main(String[] args) {
        try {
//            System.out.print("Please enter path of dataset: ");
//            String path = System.console().readLine();
            String path = null;
            path = "/home/faisal/disk/k2/datasets/tdrive/06";
            // dataType = 1 for T-Drive, 2 for GeoLife
            Integer dataType = 1;
            Date startdate = new Date();
            System.out.println("Directory path : " + path);
            System.out.println("%%%%%%%%%%%% Job Started at " + startdate);
            if(dataType == 1){
                TDriveCleansing cleansing = new TDriveCleansing();
                cleansing.readData(path);
            }else if(dataType == 2){
                GeoLifeCleansing cleansing = new GeoLifeCleansing();
                cleansing.readData(path);
            }
            Date enddate = new Date();
            System.out.println("%%%%%%%%%%%% Job Finished at " + enddate);
            System.out.println("%%%%%%%%%%%% Elapsed Time(s) " + (enddate.getTime() - startdate.getTime())/1000 );
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
       
    }
    
}
