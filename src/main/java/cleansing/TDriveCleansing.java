/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cleansing;

import org.apache.flink.api.java.tuple.Tuple2;

import java.io.Console;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Analysis
 */
public class TDriveCleansing {

    public static Date minTime;
    public static Date maxTime;
    public static HashMap<String, Tuple2<File, String>> vehInputOutputMap = new HashMap<>();

//    private String separator = Environment.getProperty("separator");

    public void readData(String path) throws FileNotFoundException {


        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        for (File file : listOfFiles) {
            if (file.isFile()) {
                //System.out.println(file.getName());
                readFileAndSplit(file, path);
//                String fileId = file.getName().substring(0,file.getName().lastIndexOf("."));
//                File directory = new File(path);
//                readAndInterpolateData(directory, path, fileId);
//                break;
            }
        }
        for (String vehId : vehInputOutputMap.keySet()) {
            readAndInterpolateData(vehInputOutputMap.get(vehId).f0, vehInputOutputMap.get(vehId).f1, vehId);
        }


    }

    public void readFileAndSplit(File file, String path) throws FileNotFoundException {
        Scanner input = new Scanner(file);
        int tripIndex = 0;
        ArrayList<TDriveDataEntry> tripList = new ArrayList<TDriveDataEntry>();
        List<ArrayList<TDriveDataEntry>> trips = new ArrayList<>();
        TDriveDataEntry prevEntry = null;
        TDriveDataEntry entry = null;

        while (input.hasNext()) {
            String nextLine = input.nextLine();
            String[] tokens = nextLine.split(Constants.TDriveConstants.SEPARATOR);
            if (entry != null)
                prevEntry = entry;
            entry = new TDriveDataEntry(tokens);
            if (prevEntry != null) {
                Calendar test = Calendar.getInstance();
                test.setTime(prevEntry.getTime());
                test.add(Calendar.SECOND, Constants.TDriveConstants.TRIP_BREAK_INTERVAL);
                if (entry.getTime().getTime() > test.getTimeInMillis()) {
                    if (minTime == null) {
                        minTime = tripList.get(0).getTime();
                    } else {
                        if (tripList.get(0).getTime().before(minTime)) {
                            minTime = tripList.get(0).getTime();
                        }
                    }
                    if (maxTime == null) {
                        maxTime = tripList.get(tripList.size() - 1).getTime();
                    } else {
                        if (tripList.get(tripList.size() - 1).getTime().after(maxTime)) {
                            maxTime = tripList.get(tripList.size() - 1).getTime();
                        }
                    }
                    tripList.get(0).getTime().setSeconds(Constants.TDriveConstants.TRIP_START_SECONDS);
                    tripIndex++;
                    trips.add(tripList);
                    tripList = new ArrayList<>();
                }
            }
            tripList.add(entry);

        }
        //add last trip
        if (minTime == null) {
            minTime = tripList.get(0).getTime();
        } else {
            if (tripList.get(0).getTime().before(minTime)) {
                minTime = tripList.get(0).getTime();
            }
        }
        if (maxTime == null) {
            maxTime = tripList.get(tripList.size() - 1).getTime();
        } else {
            if (tripList.get(tripList.size() - 1).getTime().after(maxTime)) {
                maxTime = tripList.get(tripList.size() - 1).getTime();
            }
        }
        tripList.get(0).getTime().setSeconds(Constants.TDriveConstants.TRIP_START_SECONDS);
        tripIndex++;
        trips.add(tripList);

        System.out.println("%%%%%%%%%%%%%%%%% " + file.getName() + " : Summary %%%%%%%%%%%%%%%%%%%%%%");
        String fileId = file.getName().substring(0, file.getName().lastIndexOf("."));
        if (entry != null) {

            System.out.println("Vehicle Id :: " + entry.getVehicleId());
            System.out.println("No of Trips :: " + tripIndex);
            System.out.println("Sorting trips and Writing trips to files");
            File directory = new File(path + File.separator +//entry.getVehicleId());
                    fileId);
            directory.mkdirs();
            writeTripsToFiles(directory, trips);
            System.out.println("Done . ");
            vehInputOutputMap.put(fileId, Tuple2.of(directory, path));
//            readAndInterpolateData(directory, path, fileId);
        } else
            System.out.println("File is empty.");
    }


    public void readAndInterpolateData(File directory, String path, String vehicleId) throws FileNotFoundException {

        System.out.println("%%%%%%%%%%%%%%%%%% Interpolating trips %%%%%%%%%%%%");
        List<ArrayList<TDriveDataEntry>> trips = new ArrayList<>();
        File[] listOfFiles = directory.listFiles();
        ArrayList<TDriveDataEntry> interpolatedTrip = null;
//        minTime = null;

        for (File file : listOfFiles) {
            if (file.isFile()) {
                //System.out.println(file.getName());
                interpolatedTrip = readFileAndInterpolate(file);
                trips.add(interpolatedTrip);
//                break;
            }
        }
        System.out.println("Interpolating trips Done.");
        System.out.println("Sorting interpolated trips and Writing to output file");
        File outputdirectory = new File(path + File.separator + "output");
        outputdirectory.mkdirs();
        writeInterpolatedTripsToFile(outputdirectory, trips, vehicleId);
        //directory.delete();

    }


    public ArrayList<TDriveDataEntry> readFileAndInterpolate(File file) throws FileNotFoundException {

        Scanner input = new Scanner(file);

        ArrayList<TDriveDataEntry> tripList = new ArrayList<TDriveDataEntry>();

        TDriveDataEntry prevEntry = null;
        TDriveDataEntry entry = null;
        TDriveDataEntry interpolatedEntry = null;
        long tprev = 0;
        long t1 = 0, t2 = 0, t = 0;
        double deltaLat = 0, deltaLon = 0, latInter = 0, lonInter = 0;
        int step = Constants.TDriveConstants.OUTPUT_SAMPLING_INTERVAL * 1000;
        float t0_1 = 0;
        if (input.hasNext()){
            String nextLine = input.nextLine();

            String[] tokens = nextLine.split(Constants.TDriveConstants.SEPARATOR);
            prevEntry = new TDriveDataEntry(tokens);
            prevEntry.setTime(minTime);
        }
        boolean lastEntry=false;
        while (!lastEntry) {
            if (input.hasNext()) {
                String nextLine = input.nextLine();

                String[] tokens = nextLine.split(Constants.TDriveConstants.SEPARATOR);
                if (entry != null)
                    prevEntry = entry;
                entry = new TDriveDataEntry(tokens);
            } else {
                entry = new TDriveDataEntry(prevEntry);
                entry.setTime(maxTime);
                lastEntry=true;
            }
//            System.out.println("Retreived entry :: " + entry.toString());
//            tripList.add(entry);

            if (prevEntry != null) {
                t1 = prevEntry.getTime().getTime();
                t2 = entry.getTime().getTime();
                deltaLat = entry.getLatitude() - prevEntry.getLatitude();
                deltaLon = entry.getLongitude() - prevEntry.getLongitude();

//                System.out.println("Pre Interpol ::: " + deltaLat+" : " + deltaLon +" : "+ t2 + ":" + t1);
                if (tprev == 0) tprev = t1;
//                if (!(deltaLat == 0 && deltaLon == 0)) {
                    for (t = tprev; t < t2; t += step) {


                        //                    long diff = (t - t1);
                        //                    long delta  = (t2 - t1);
                        //                    System.out.println("Interpol ::: " +diff+" : " + delta + " : " + (float) diff/delta);
                        t0_1 = (float) (t - t1) / (t2 - t1);
                        latInter = prevEntry.getLatitude() + deltaLat * t0_1;
                        lonInter = prevEntry.getLongitude() + deltaLon * t0_1;
                        //                    System.out.println("Interpol ::: " + t+" : " + t1 +" : "+ t2 + ":" + t0_1 );
                        interpolatedEntry = new TDriveDataEntry(new Date(t), entry.getVehicleId(),
                                lonInter, latInter);
                        //                    System.out.println("Adding entry ::: " + interpolatedEntry.getTime()+" : "
                        //                            + interpolatedEntry.getLatitude()+ ","+ interpolatedEntry.getLongitude());
                        //                    System.out.println(//"Adding entry ::: " + interpolatedEntry.getTime()+" : "+
                        //                             interpolatedEntry.getLatitude()+ ","+ interpolatedEntry.getLongitude());
                        tripList.add(interpolatedEntry);
                    }
//                }
                tprev = t;

            }
//            else if(prevEntry == null && tprev ==0)
//                tripList.add(entry);

        }
        if (tripList.size() == 0 && entry != null)
            tripList.add(entry);
        return tripList;
    }

    public void writeTripsToFiles(File directory, List<ArrayList<TDriveDataEntry>> trips) {

        int tripIndex = 0;
        for (ArrayList<TDriveDataEntry> trip : trips) {
            PrintWriter writer = null;
            try {
                Collections.sort(trip);
                writer = new PrintWriter(directory.getPath() + File.separator + "trip" + tripIndex + ".txt", "UTF-8");
                for (TDriveDataEntry dataEntry : trip) {
                    //System.out.println(dataEntry.getLatitude()+","+dataEntry.getLongitude());
                    writer.println(dataEntry.toString());
                }


                tripIndex++;
                //break;
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                writer.close();
            }
        }
    }

    public void writeInterpolatedTripsToFile(File directory, List<ArrayList<TDriveDataEntry>> trips, String vehicleId) {

//        int tripIndex = 0;
        PrintWriter writer = null;
        List<TDriveDataEntry> outputList = new ArrayList<>();
        try {

            writer = new PrintWriter(directory.getPath() + File.separator + vehicleId + ".txt", "UTF-8");
            for (ArrayList<TDriveDataEntry> trip : trips) {
                outputList.addAll(trip);
            }
            Collections.sort(outputList);


            for (TDriveDataEntry dataEntry : outputList) {
                //System.out.println(dataEntry.getLatitude()+","+dataEntry.getLongitude());
                dataEntry.setLongTime((dataEntry.getTime().getTime() - minTime.getTime()) / (Constants.TDriveConstants.OUTPUT_SAMPLING_INTERVAL * 1000));
                writer.println(dataEntry.toLongString());
            }


//                break;


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            writer.close();
        }
        System.out.println("Output file :: " + directory.getPath() + File.separator + vehicleId + ".txt");
    }


}
