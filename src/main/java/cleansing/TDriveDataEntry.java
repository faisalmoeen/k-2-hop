/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cleansing;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Analysis
 */
public class TDriveDataEntry implements Comparable<TDriveDataEntry>{
    
    private Date time ;
    private Long vehicleId;
    private Double longitude, latitude;



    private Long longTime;
    DateFormat formatter = new SimpleDateFormat(Constants.TDriveConstants.TIME_FORMAT);
    public TDriveDataEntry(String[] tokens)
    {
        try {
            vehicleId = Long.valueOf(tokens[Constants.TDriveConstants.VEHICLE_ID_INDEX]);
            
            time = (Date)formatter.parse(tokens[Constants.TDriveConstants.TIME_INDEX]);
            longitude = Double.valueOf(tokens[Constants.TDriveConstants.LONGITUDE_INDEX]);
            latitude = Double.valueOf(tokens[Constants.TDriveConstants.LATITUDE_INDEX]);
        } catch (ParseException ex) {
            Logger.getLogger(TDriveDataEntry.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public TDriveDataEntry(Date time, Long vehicleId, Double longitude, Double latitude) {
        this.time = time;
        this.vehicleId = vehicleId;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public TDriveDataEntry(TDriveDataEntry prevEntry) {
        this.setTime(prevEntry.getTime());
        this.setVehicleId(prevEntry.getVehicleId());
        this.setLatitude(prevEntry.getLatitude());
        this.setLongitude(prevEntry.getLongitude());
        this.setLongTime(prevEntry.getLongTime());
    }


    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setLongTime(Long longTime) {
        this.longTime = longTime;
    }

    public Long getLongTime() {
        return longTime;
    }
    
    @Override
    public String toString(){
        return this.vehicleId + Constants.TDriveConstants.SEPARATOR
                + formatter.format(this.time) +  Constants.TDriveConstants.SEPARATOR
                + this.longitude +  Constants.TDriveConstants.SEPARATOR
                + this.latitude ;
    }

    public String toLongString(){
        return this.vehicleId + Constants.TDriveConstants.SEPARATOR
                + this.longTime +  Constants.TDriveConstants.SEPARATOR
                + this.longitude +  Constants.TDriveConstants.SEPARATOR
                + this.latitude ;
    }
    
    @Override
    public int compareTo(TDriveDataEntry o) {
    return getTime().compareTo(o.getTime());
  }
    
}
