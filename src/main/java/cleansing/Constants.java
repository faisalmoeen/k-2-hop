package cleansing;



public final class Constants{     
	
    public final static class TDriveConstants{
        public static final String SEPARATOR = Environment.getProperty("tdrive_separator");
        public static final Integer VEHICLE_ID_INDEX = Integer.valueOf(Environment.getProperty("tdrive_vehicle_id_index"));
        public static final Integer TIME_INDEX = Integer.valueOf( Environment.getProperty("tdrive_time_index"));
        public static final Integer LONGITUDE_INDEX = Integer.valueOf( Environment.getProperty("tdrive_longitutde_index"));
        public static final Integer LATITUDE_INDEX = Integer.valueOf( Environment.getProperty("tdrive_latitude_index"));
        public static final String TIME_FORMAT = Environment.getProperty("tdrive_time_format");
        public static final Integer OUTPUT_SAMPLING_INTERVAL = Integer.valueOf(Environment.getProperty("tdrive_output_sampling_interval"));
        public static final Integer TRIP_BREAK_INTERVAL = Integer.valueOf(Environment.getProperty("tdrive_trip_break_interval"));
        public static final Integer TRIP_START_SECONDS = Integer.valueOf(Environment.getProperty("tdrive_trip_start_seconds"));

    }
    
    public final static class GeoLifeConstants{
        public static final String SEPARATOR = Environment.getProperty("geolife_separator");
        //public static final Integer VEHICLE_ID_INDEX = Integer.valueOf(Environment.getProperty("geolife_vehicle_id_index"));
        public static final Integer TIME_INDEX = Integer.valueOf( Environment.getProperty("geolife_time_index"));
        public static final Integer DATE_INDEX = Integer.valueOf( Environment.getProperty("geolife_date_index"));
        public static final Integer LONGITUDE_INDEX = Integer.valueOf( Environment.getProperty("geolife_longitutde_index"));
        public static final Integer LATITUDE_INDEX = Integer.valueOf( Environment.getProperty("geolife_latitude_index"));
        public static final String TIME_FORMAT = Environment.getProperty("geolife_time_format");
        public static final Integer OUTPUT_SAMPLING_INTERVAL = Integer.valueOf(Environment.getProperty("geolife_output_sampling_interval"));
        public static final Integer TRIP_BREAK_INTERVAL = Integer.valueOf(Environment.getProperty("geolife_trip_break_interval"));
    
    }
    
}
