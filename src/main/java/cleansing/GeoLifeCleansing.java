/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cleansing;

import java.io.Console;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Analysis
 */
public class GeoLifeCleansing {

//    private String separator = Environment.getProperty("separator");
    
    public void readData(String path) throws FileNotFoundException{
        

        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        for (File file : listOfFiles) {
            if (file.isFile()) {
                //System.out.println(file.getName());
                readFileAndSplit(file, path);
//                break;
            }
        }


    }
    
    public void readFileAndSplit(File file,String path) throws FileNotFoundException{
        Scanner input = new Scanner(file);
        int tripIndex = 0; 
        ArrayList<GeoLifeDataEntry> tripList = new ArrayList<GeoLifeDataEntry>();
        List<ArrayList<GeoLifeDataEntry>> trips = new ArrayList<>();
        GeoLifeDataEntry prevEntry = null;
        GeoLifeDataEntry entry = null;
        //skip first six lines of file
        for (int i=0;i<6;i++){
            //System.out.println(
                    input.nextLine();
            //);
        }
        while(input.hasNext()) {
            String nextLine = input.nextLine();
            String[] tokens = nextLine.split(Constants.GeoLifeConstants.SEPARATOR);
            if(entry !=null)
                prevEntry = entry;
            entry = new GeoLifeDataEntry(tokens);
            if(prevEntry!=null){
                Calendar test = Calendar.getInstance();
                test.setTime(prevEntry.getTime());
                test.add(Calendar.SECOND, Constants.GeoLifeConstants.TRIP_BREAK_INTERVAL);
                if(entry.getTime().getTime() > test.getTimeInMillis())
                {
                    
                    tripIndex ++;
                    trips.add(tripList);
                    tripList = new ArrayList<>();
                }
            }
            tripList.add(entry);
            
        }
        System.out.println("%%%%%%%%%%%%%%%%% " + file.getName()+" : Summary %%%%%%%%%%%%%%%%%%%%%%");
        String fileId = file.getName().substring(0,file.getName().lastIndexOf("."));
        if(entry!=null){
            
            //System.out.println("Vehicle Id :: " + entry.getVehicleId());
            System.out.println("No of Trips :: " + tripIndex);
            System.out.println("Sorting trips and Writing trips to files");
            File directory = new File(path +File.separator +//entry.getVehicleId());
                    fileId);
            directory.mkdirs();
            writeTripsToFiles (directory, trips);
            System.out.println("Done . ");
            readAndInterpolateData(directory, path, fileId);
        }
        else
            System.out.println("File is empty.");
    }
    
    
    public void readAndInterpolateData(File directory, String path , String vehicleId) throws FileNotFoundException{
        
        System.out.println("%%%%%%%%%%%%%%%%%% Interpolating trips %%%%%%%%%%%%");
        List<ArrayList<GeoLifeDataEntry>> trips = new ArrayList<>();
        File[] listOfFiles = directory.listFiles();

        for (File file : listOfFiles) {
            if (file.isFile()) {
                //System.out.println(file.getName());
                trips.add(readFileAndInterpolate(file));
//                break;
            }
        }
        System.out.println("Interpolating trips Done.");
        System.out.println("Sorting interpolated trips and Writing to output file");
        File outputdirectory = new File(path +File.separator +"output");
        outputdirectory.mkdirs();
        writeInterpolatedTripsToFile(outputdirectory, trips, vehicleId);
        //directory.delete();

    }
    
    public ArrayList<GeoLifeDataEntry> readFileAndInterpolate(File file) throws FileNotFoundException{
        
        Scanner input = new Scanner(file);
        
        ArrayList<GeoLifeDataEntry> tripList = new ArrayList<GeoLifeDataEntry>();
        
        GeoLifeDataEntry prevEntry = null;
        GeoLifeDataEntry entry = null;
        GeoLifeDataEntry interpolatedEntry = null;
        long tprev = 0;
        long t1=0,t2=0, t=0;
        double deltaLat = 0, deltaLon = 0, latInter = 0, lonInter = 0;
        int step = Constants.GeoLifeConstants.OUTPUT_SAMPLING_INTERVAL * 1000;
        float t0_1 = 0;
        while(input.hasNext()) {
            String nextLine = input.nextLine();
            
            String[] tokens = nextLine.split(Constants.GeoLifeConstants.SEPARATOR);
            if(entry !=null)
                prevEntry = entry;
            entry = new GeoLifeDataEntry(tokens);
//            System.out.println("Retreived entry :: " + entry.toString());
//            tripList.add(entry);
            
            if(prevEntry!=null){
                t1 = prevEntry.getTime().getTime();
                t2 = entry.getTime().getTime() ;
                deltaLat = entry.getLatitude() - prevEntry.getLatitude();
                deltaLon =  entry.getLongitude()- prevEntry.getLongitude();
                
//                System.out.println("Pre Interpol ::: " + deltaLat+" : " + deltaLon +" : "+ t2 + ":" + t1);
                if(tprev == 0) tprev = t1;
                    if(!(deltaLat==0 && deltaLon ==0)){
                    for ( t = tprev; t < t2; t+= step) {


    //                    long diff = (t - t1);
    //                    long delta  = (t2 - t1);
    //                    System.out.println("Interpol ::: " +diff+" : " + delta + " : " + (float) diff/delta); 
                        t0_1 = (float) (t - t1) / (t2 - t1);
                        latInter = prevEntry.getLatitude() + deltaLat  * t0_1;
                        lonInter = prevEntry.getLongitude()+ deltaLon  * t0_1;
    //                    System.out.println("Interpol ::: " + t+" : " + t1 +" : "+ t2 + ":" + t0_1 );
                        interpolatedEntry = new GeoLifeDataEntry(new Date(t), //entry.getVehicleId(), 
                                lonInter, latInter);
    //                    System.out.println("Adding entry ::: " + interpolatedEntry.getTime()+" : "
    //                            + interpolatedEntry.getLatitude()+ ","+ interpolatedEntry.getLongitude());
    //                    System.out.println(//"Adding entry ::: " + interpolatedEntry.getTime()+" : "+
    //                             interpolatedEntry.getLatitude()+ ","+ interpolatedEntry.getLongitude());
                        tripList.add(interpolatedEntry);
                    }
                }
                tprev = t;
               
            }
//            else if(prevEntry == null && tprev ==0)
//                tripList.add(entry);
            
        }
        if(tripList.size() == 0 && entry !=null)
            tripList.add(entry);
        return tripList;
    }
    
    public void writeTripsToFiles(File directory, List<ArrayList<GeoLifeDataEntry>> trips){
        
        int tripIndex = 0;
        for (ArrayList<GeoLifeDataEntry> trip : trips) {
            PrintWriter writer = null;
            try {
                Collections.sort(trip);
                writer = new PrintWriter(directory.getPath()+File.separator+"trip" + tripIndex + ".txt", "UTF-8");
                for (GeoLifeDataEntry dataEntry : trip) {
                    //System.out.println(dataEntry.getLatitude()+","+dataEntry.getLongitude());
                    writer.println(dataEntry.toString());
                }
               
                
                tripIndex++;
                //break;
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                writer.close();
            }
        }
    }
    
    public void writeInterpolatedTripsToFile(File directory, List<ArrayList<GeoLifeDataEntry>> trips, String vehicleId){
        
//        int tripIndex = 0;
        PrintWriter writer = null;
        List<GeoLifeDataEntry> outputList = new ArrayList<>();
        try {
           
            writer = new PrintWriter(directory.getPath()+File.separator+ vehicleId + ".txt", "UTF-8");
            for (ArrayList<GeoLifeDataEntry> trip : trips) {
                outputList.addAll(trip);
            }
                Collections.sort(outputList);
                
                
                for (GeoLifeDataEntry dataEntry : outputList) {
                    //System.out.println(dataEntry.getLatitude()+","+dataEntry.getLongitude());
                    writer.println(dataEntry.toString());
                }
               
                
                
//                break;
            
        
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                writer.close();
            }
        System.out.println("Output file :: " + directory.getPath()+File.separator+ vehicleId + ".txt");
    }
    
    
   
    
}
