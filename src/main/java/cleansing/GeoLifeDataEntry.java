/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cleansing;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Analysis
 */
public class GeoLifeDataEntry implements Comparable<GeoLifeDataEntry>{
    
    private Date time ;
    //private Long vehicleId;
    private Double longitude, latitude;
    DateFormat formatter = new SimpleDateFormat(Constants.GeoLifeConstants.TIME_FORMAT);
    public GeoLifeDataEntry(String[] tokens)
    {
        try {
//            vehicleId = Long.valueOf(tokens[Constants.VEHICLE_ID_INDEX]);
            if(tokens.length > 3){
                time = (Date)formatter.parse(tokens[Constants.GeoLifeConstants.DATE_INDEX] 
                        + " " + tokens[Constants.GeoLifeConstants.TIME_INDEX]);
                longitude = Double.valueOf(tokens[Constants.GeoLifeConstants.LONGITUDE_INDEX]);
                latitude = Double.valueOf(tokens[Constants.GeoLifeConstants.LATITUDE_INDEX]);
            }else{
                time = (Date)formatter.parse(tokens[0]);
                longitude = Double.valueOf(tokens[1]);
                latitude = Double.valueOf(tokens[2]);
            }
        } catch (ParseException ex) {
            Logger.getLogger(GeoLifeDataEntry.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public GeoLifeDataEntry(Date time, Double longitude, Double latitude) {
        this.time = time;
//        this.vehicleId = vehicleId;
        this.longitude = longitude;
        this.latitude = latitude;
    }
    
    
    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

//    public Long getVehicleId() {
//        return vehicleId;
//    }
//
//    public void setVehicleId(Long vehicleId) {
//        this.vehicleId = vehicleId;
//    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }
    
    @Override
    public String toString(){
        return //this.vehicleId + Constants.SEPARATOR+
                 formatter.format(this.time) +  Constants.GeoLifeConstants.SEPARATOR
                + this.longitude +  Constants.GeoLifeConstants.SEPARATOR
                + this.latitude ;
    }
    
    
    @Override
    public int compareTo(GeoLifeDataEntry o) {
    return getTime().compareTo(o.getTime());
  }
    
}
