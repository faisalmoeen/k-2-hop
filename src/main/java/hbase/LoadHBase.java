package hbase;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;
import org.joda.time.DateTime;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by faisal on 2/21/17.
 */
public class LoadHBase {

	static String filePath1 = "/home/faisal/disk/k2/datasets/tdrive-d.txt";
	static String filePath2 = "/home/faisal/disk/k2/datasets/brink-d.txt";
	static String filePath3 = "/home/faisal/disk/k2/datasets/trucks-d.txt";
	static int batchSize=1000;

	public static void main(String[] args) throws IOException {

		loadCSV("tdrive-load",filePath1);
		loadCSV("tdrive-load",filePath1);
		loadCSV("tdrive-load",filePath1);
	}

	public static void loadCSV(String tableName, String inputPath) throws IOException {
		Configuration config = HBaseConfiguration.create();

		// Instantiating HTable class
		HTable hTable = new HTable(config, "tdrive");
		byte[] colFamily = Bytes.toBytes("a");
		byte[] xQualifier = Bytes.toBytes("x");
		byte[] yQualifier = Bytes.toBytes("y");

		Put p = null;
		long count=0;
		long tStart=0;
		long tEnd=0;
		long recordCount=0;
		List<Put> batch = new ArrayList<>();
		try {
			try (BufferedReader br = new BufferedReader(new FileReader(inputPath))) {
				String line;
				try {
					line = br.readLine();
					tStart=System.currentTimeMillis();
					while ((line = br.readLine()) != null) {
						String[] values = line.split(",");
						long time = Long.valueOf(values[1]);
						char sep = ':';
						long oid = Long.valueOf(values[0]);
						byte[] rowKey = Bytes.add(Bytes.toBytes(time),Bytes.toBytes(":"),Bytes.toBytes(oid));
						byte[] x = Bytes.toBytes(Float.valueOf(values[2]));
						byte[] y = Bytes.toBytes(Float.valueOf(values[3]));

						// Instantiating Put class
						// accepts a row name.
						p = new Put(rowKey);
						p.add(colFamily, xQualifier, x);
						p.add(colFamily, yQualifier, y);
						batch.add(p);
						recordCount++;
						if (recordCount == batchSize) {
							hTable.put(batch);
							batch = new ArrayList<>();
							recordCount=0;
						}
						count++;
						if (count%1000000==0){
							tEnd = System.currentTimeMillis();
							System.out.println("records in million: " + count / 1000000 + " Time taken (ms): " + (tEnd - tStart));
							tStart = System.currentTimeMillis();
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		hTable.flushCommits();
		System.out.println("data inserted");

		// closing HTable
		hTable.close();

	}
}
