package hbase;

import com.google.common.base.Stopwatch;
import dk.aau.convoy.model.ClusterablePoint;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HConstants;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by faisal on 2/21/17.
 */
public class ReadHBase {

	private static Configuration config;
	private static HTable table;
	private static Scan scan;
	private static byte[] colFamily = Bytes.toBytes("a");
	private static byte[] xQualifier = Bytes.toBytes("x");
	private static byte[] yQualifier = Bytes.toBytes("y");

	public static void init(String tableName) throws IOException {
		config = HBaseConfiguration.create();
		config.setInt(HConstants.ZK_SESSION_TIMEOUT,500000);
		config.setInt("hbase.client.scanner.timeout.period",500000);
		// Instantiating HTable class
		table = new HTable(config, tableName);
		scan = new Scan();
		scan.addColumn(colFamily, xQualifier);
		scan.addColumn(colFamily, yQualifier);
	}

	public static void close() throws IOException {
		try {
			table.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws IOException, Exception{
		init("trucks-d");
		Stopwatch timer = Stopwatch.createStarted();
		for(int i=1;i<=340;i++) {
			Map<Long, ClusterablePoint> map = getTuplesForTime(i);
			for (Long l:
			map.keySet()){
				System.out.println("key:" + l + "   value:" + map.get(l).getPoint());
			}
		}
		timer.stop();
		System.out.println(timer.elapsed(TimeUnit.MILLISECONDS));
	}

//	public static List<ClusterablePoint> getTuplesForTime(long time) throws IOException {
//		scan.setStartRow(Bytes.add(Bytes.toBytes(time),Bytes.toBytes(":"),Bytes.toBytes(0L)));
//		scan.setStopRow(Bytes.add(Bytes.toBytes(time),Bytes.toBytes(":"),Bytes.toBytes(999999L)));
//		ResultScanner scanner = table.getScanner(scan);
//		int i=0;
//		List<ClusterablePoint> listClusterablePoint = new ArrayList<>();
//		for (Result result = scanner.next(); result != null; result = scanner.next()){
//			// Reading values from Result class object
//			long oid = Bytes.toLong(result.getRow(),9,8);
//			long t = Bytes.toLong(result.getRow(),0,8);
//			float x = Bytes.toDouble(result.getValue(colFamily,xQualifier));
//			float y = Bytes.toDouble(result.getValue(colFamily,yQualifier));
//			listClusterablePoint.add(new ClusterablePoint(oid,x,y));
////			System.out.println(t+":"+oid+":"+x+":"+y);
//		}
//		scanner.close();
//		if (listClusterablePoint.size() == 0) {
//			return null;
//		} else {
//			return listClusterablePoint;
//		}
//	}

	public static Map<Long, ClusterablePoint> getTuplesForTime(long time) throws IOException {
		scan.setStartRow(Bytes.add(Bytes.toBytes(time),Bytes.toBytes(":"),Bytes.toBytes(0L)));
		scan.setStopRow(Bytes.add(Bytes.toBytes(time),Bytes.toBytes(":"),Bytes.toBytes(999999L)));
		ResultScanner scanner = table.getScanner(scan);
		int i=0;
		Map<Long, ClusterablePoint> mapClusterablePoint = new HashMap<>();
		for (Result result = scanner.next(); result != null; result = scanner.next()){
			// Reading values from Result class object
			long oid = Bytes.toLong(result.getRow(),9,8);
			long t = Bytes.toLong(result.getRow(),0,8);
			double x = Bytes.toDouble(result.getValue(colFamily,xQualifier));
			double y = Bytes.toDouble(result.getValue(colFamily,yQualifier));
			mapClusterablePoint.put(oid,new ClusterablePoint(oid,x,y));
//			System.out.println(t+":"+oid+":"+x+":"+y);
		}
		scanner.close();
		if (mapClusterablePoint.size() == 0) {
			return null;
		} else {
			return mapClusterablePoint;
		}
	}

	public static void writeTuplesForTimeRange(long t1, long t2, String outputFile) throws IOException {
		scan.setStartRow(Bytes.add(Bytes.toBytes(t1),Bytes.toBytes(":"),Bytes.toBytes(0L)));
		scan.setStopRow(Bytes.add(Bytes.toBytes(t2),Bytes.toBytes(":"),Bytes.toBytes(999999L)));
		PrintWriter printWriter = new PrintWriter(outputFile);
		ResultScanner scanner = table.getScanner(scan);
		int i=0;
		Map<Long, ClusterablePoint> mapClusterablePoint = new HashMap<>();
		int count=0;
		for (Result result = scanner.next(); result != null; result = scanner.next()){
			// Reading values from Result class object
			long oid = Bytes.toLong(result.getRow(),9,8);
			long t = Bytes.toLong(result.getRow(),0,8);
			double x = Bytes.toDouble(result.getValue(colFamily,xQualifier));
			double y = Bytes.toDouble(result.getValue(colFamily,yQualifier));
//			System.out.println(count++ + "," + t + "," + "," + oid + "," + x + "," + y);
			printWriter.println(t + "," + oid + "," + x + "," + y);
//			mapClusterablePoint.put(oid,new ClusterablePoint(oid,x,y));
//			System.out.println(t+":"+oid+":"+x+":"+y);
		}
		scanner.close();
		printWriter.flush();
		printWriter.close();

//		if (mapClusterablePoint.size() == 0) {
//			return null;
//		} else {
//			return mapClusterablePoint;
//		}
	}

	public static ClusterablePoint get(long time, long oid) throws IOException {
		// Instantiating Get class
		byte[] rowKey = Bytes.add(Bytes.toBytes(time),Bytes.toBytes(":"),Bytes.toBytes(oid));
		Get g = new Get(rowKey);

		// Reading the data
		Result result = table.get(g);
		double x = Bytes.toDouble(result.getValue(colFamily,xQualifier));
		double y = Bytes.toDouble(result.getValue(colFamily,yQualifier));
//		System.out.println("get request for "+time+":"+oid);
		return new ClusterablePoint(oid,x,y);
	}

	public static Map<Long, ClusterablePoint> batchGet(long time, List<Set<Long>> clusterList) throws IOException, InterruptedException {
		Map<Long, ClusterablePoint> map = new HashMap<>();
		Set<Long> objs = new HashSet<>();
		for (Set<Long> cluster : clusterList) {
			for (Long obj :
					cluster) {
				objs.add(obj);
			}
		}
		//batching the get requests
		List<Row> batch = new ArrayList<Row>();
		for (Long oid :
				objs) {
			byte[] rowKey = Bytes.add(Bytes.toBytes(time),Bytes.toBytes(":"),Bytes.toBytes(oid));
			Get g = new Get(rowKey);
			batch.add(g);
		}
		Result[] results = new Result[batch.size()];
		table.batch(batch,results);
		for (Result result :
				results) {
			double x = Bytes.toDouble(result.getValue(colFamily,xQualifier));
			double y = Bytes.toDouble(result.getValue(colFamily,yQualifier));
			long oid = Bytes.toLong(result.getRow(),9,8);
//		System.out.println("get request for "+time+":"+oid);
			map.put(oid, new ClusterablePoint(oid,x,y));
		}
		return map;
	}

	public static Map<Long, ClusterablePoint> batchGet(long time, Set<Long> desiredObjs) throws IOException, InterruptedException {
		Map<Long, ClusterablePoint> map = new HashMap<>();
		//batching the get requests
		List<Row> batch = new ArrayList<Row>();
		for (Long oid :
				desiredObjs) {
			byte[] rowKey = Bytes.add(Bytes.toBytes(time),Bytes.toBytes(":"),Bytes.toBytes(oid));
			Get g = new Get(rowKey);
			batch.add(g);
		}
		Result[] results = new Result[batch.size()];
		table.batch(batch,results);
		for (Result result :
				results) {
			if (!result.isEmpty()) {
				double x = Bytes.toDouble(result.getValue(colFamily, xQualifier));
				double y = Bytes.toDouble(result.getValue(colFamily, yQualifier));
				long oid = Bytes.toLong(result.getRow(), 9, 8);
//		System.out.println("get request for "+time+":"+oid);
				map.put(oid, new ClusterablePoint(oid, x, y));
			}
			else{
				System.out.println("not found");
			}
		}
		return map;
	}

	public static boolean hasTimeStamp(long time) throws IOException {
		scan.setStartRow(Bytes.add(Bytes.toBytes(time),Bytes.toBytes(":"),Bytes.toBytes(0L)));
		scan.setStopRow(Bytes.add(Bytes.toBytes(time),Bytes.toBytes(":"),Bytes.toBytes(999999L)));
		scan.setMaxResultSize(1L);
		ResultScanner scanner = table.getScanner(scan);
		if (scanner.next() != null) {
			scanner.close();
			return true;
		} else {
			scanner.close();
			return false;
		}

	}
}
